<?php
include 'libreria/principal.php';
include 'libreria/evaluaciones.php';

esUsuario(); // será usuario registrado?
$IDusuario = $_SESSION['userID'];
$capitulo = 5;
$registro = verificarCuestionario($capitulo, $IDusuario,  utf8_decode($Fecha));


?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> Principios de Contabilidad. Cuestionario de evaluación. Capítulo 5</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="libreria/js_principal.js"></script>
<script src="libreria/vendor/jquery/jquery-1.11.0.min.js"></script>
<script src="libreria/calCap.js"></script>
<script src="libreria/rc5.js"></script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Cuestionario de autoevaluación</h1>
    </div>
    <div id="divTitularMenu">
        <a href="portada.php">Inicio</a> | 
        <a href="libreria/calculadora/calculadora.html" 
           target="_blank" 
           onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | 
        <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido" style="width: 900px">
  <div class="divContCuerpo">
      
      <h3 class="titulo-principal">Capítulo 5</h3>
      <div class="migas">
          <ul>
              <li><a href="portada.php">Portada</a></li>
              <li><a href="javascript:history.back();">Página anterior</a></li>
              <li><img src="imagenes/print.png" 
                       alt="" 
                       height="25" 
                       style="float: left; margin-right: 5px;" />
                  <a href="javascript:print();">Imprimir página</a></li>
          </ul>
      </div>
      
      <table class="mis-datos">
          <tr>
              <td>Universidad o Institución:</td>
              <td>
                  <input oninput="cCamCues('unidad',this.value,<?php echo $registro ?>);" 
                         name="unidad" 
                         id='unidad' 
                         required 
                         value="<?php echo xCamCues('unidad', $registro) ?>" />
              </td>
              <td>Escuela o facultad:</td>
              <td>
                  <input oninput="cCamCues('facultad',this.value,<?php echo $registro ?>);" 
                         name="facultad" 
                         id='facultad' 
                         required 
                         value="<?php echo xCamCues('facultad', $registro) ?>" />
              </td>
          </tr>
          <tr>
              <td>Asignatura:</td>
              <td>
                  <input oninput="cCamCues('asignatura',this.value,<?php echo $registro ?>);" 
                         name="asignatura" 
                         id='asignatura' 
                         required 
                         value="<?php echo xCamCues('asignatura', $registro) ?>" />
              </td>
              <td>Alumno:</td>
              <td>
                  <?php echo nombreUsuario($IDusuario) ?>
              </td>
          </tr>
          <tr>
              <td>Maestro:</td>
              <td>
                  <input oninput="cCamCues('maestro',this.value,<?php echo $registro ?>);" 
                         name="maestro" 
                         id='maestro' 
                         required 
                         value="<?php echo xCamCues('maestro', $registro) ?>" />
              </td>
              <td>Fecha:</td>
              <td>
                  <input oninput="cCamCues('fecha',this.value,<?php echo $registro ?>);" 
                         name="fecha" 
                         id='fecha' 
                         required 
                         value="<?php echo xCamCues('fecha', $registro) ?>" />
              </td>
          </tr>
      </table>
      
      <h4 class="subtit-4">OBJETIVOS DE APRENDIZAJE</h4>
      
      <div class="objetivos">
          <p>Al finalizar este capítulo, el alumno será capaz de:</p>
          <ol>
              <li>Comprender que la estructura financiera de una entidad se 
                  integra con recursos empleados para la realización de sus fines.</li>
              <li>Examinar y comprender el concepto de activo.</li>
              <li>Distinguir  las  características y clasificación del activo.</li>
          </ol>
      </div>
      
      <h4 class="subtit-4">EJERCICIOS 1a PARTE</h4>
      
      <h5>I. RESUELVA LO SIGUIENTE</h5>
      <div class="pregunta-libre">
          <p><span>1</span> ¿Cuál es el concepto de activo según la dualidad económica?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib01" 
                    id="prelib01"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib01'); ?></textarea>
      </div>
      <div class="pregunta-libre">
          <p><span>2</span> Para que un activo sea reconocido y presentado en la información financiera, debe reunir ciertas características, menciónelas.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib02" 
                    id="prelib02"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib02'); ?></textarea>
      </div>
      <div class="pregunta-libre">
          <p><span>3</span> ¿Cuál es la característica o cualidad fundamental de un activo?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib03" 
                    id="prelib03"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib03'); ?></textarea>
      </div>
      <div class="pregunta-libre">
          <p><span>4</span> Según la NIF A-5, atendiendo a su naturaleza, los activos de una entidad pueden ser de diferentes tipos, diga cuáles son.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib04" 
                    id="prelib04"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib04'); ?></textarea>
      </div>
      <div class="pregunta-libre">
          <p><span>5</span> ¿A qué se debe atender para reconocer un activo?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib05" 
                    id="prelib05"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib05'); ?></textarea>
      </div>
      <div class="pregunta-libre">
          <p><span>6</span> ¿Qué limita la vida de un activo?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib06" 
                    id="prelib06"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib06'); ?></textarea>
      </div>
      <div class="pregunta-libre">
          <p><span>7</span> Investigue otras definiciones de activo, cuando menos 3.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib07" 
                    id="prelib07"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib07'); ?></textarea>
      </div>
      <div class="pregunta-libre">
          <p><span>8</span> Proponga su definición personal de activo.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib08" 
                    id="prelib08"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib08'); ?></textarea>
      </div>
      <div class="pregunta-libre">
          <p><span>9</span> Según la NIF B-6, ¿cómo puede presentarse clasificado el activo?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib09" 
                    id="prelib09"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib09'); ?></textarea>
      </div>
      <div class="pregunta-libre">
          <p><span>10</span> La NIF B-6 dice que una entidad debe clasificar un 
              activo a corto plazo cuando se cumpla con ciertas consideraciones, 
              indique cuáles son.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib010" 
                    id="prelib010"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib010'); ?></textarea>
      </div>
      <div class="pregunta-libre">
          <p><span>11</span> ¿Qué se entiende por disponibilidad?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib011" 
                    id="prelib011"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib011'); ?></textarea>
      </div>
      <div class="pregunta-libre">
          <p><span>12</span> Señale que son las propiedades, planta y equipo.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib012" 
                    id="prelib012"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib012'); ?></textarea>
      </div>
      
      <h5>II. COMPLETE LAS LÍNEAS EN BLANCO</h5>
      <div class="completar-lineas">
          <span class="num">1</span>
          Según la NIF A–5, un activo es un <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_1" 
                    name="comLin_1" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_1'); ?>" /> controlado por una 
          entidad, <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_2" 
                    name="comLin_2" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_2'); ?>" />, cuantificado en 
          términos monetarios y del que se esperan fundadamente 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_3" 
                    name="comLin_3" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_3'); ?>" /> económicos futuros, 
          derivados de operaciones ocurridas en el pasado, que han afectado 
          económicamente a dicha entidad.
      </div>
      <div class="completar-lineas">
          <span class="num">2</span>
          Según el autor, el activo son todos los <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_4" 
                    name="comLin_4" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_4'); ?>" /> 
          de que dispone la <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_5" 
                    name="comLin_5" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_5'); ?>" />  para la 
          realización de sus fines, los cuales representan 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_6" 
                    name="comLin_6" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_6'); ?>" />  económicos 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_7" 
                    name="comLin_7" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_7'); ?>" />  fundadamente 
          esperados, <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_8" 
                    name="comLin_8" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_8'); ?>" />  por una entidad 
          económica, provenientes de transacciones, transformaciones internas y 
          eventos de todo tipo, devengados, <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_9" 
                    name="comLin_9" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_9'); ?>" />  
          y cuantificables en <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_10" 
                    name="comLin_10" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_10'); ?>" />  monetarias.
      </div>
      <div class="completar-lineas">
          <span class="num">3</span>
          Los activos a <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_11" 
                    name="comLin_11" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_11'); ?>" />  plazo (circulantes) 
          pueden presentarse como categorías <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_12" 
                    name="comLin_12" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_12'); ?>" />  
          en el estado de situación financiera, <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_13" 
                    name="comLin_13" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_13'); ?>" />  cuando una 
          presentación basada en el grado de <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_14" 
                    name="comLin_14" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_14'); ?>" />  proporcione una 
          información confiable que sea más relevante. Cuando se aplique esa 
          excepción, <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_15" 
                    name="comLin_15" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_15'); ?>" />  los activos deben 
          presentarse ordenados atendiendo a su liquidez y <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_16" 
                    name="comLin_16" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_16'); ?>" /> . 
      </div>
      <div class="completar-lineas">
          <span class="num">4</span>
          El ciclo <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_17" 
                    name="comLin_17" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_17'); ?>" />  de la operación 
          de una entidad es el <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_18" 
                    name="comLin_18" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_18'); ?>" />  comprendido entre 
          la <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_19" 
                    name="comLin_19" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_19'); ?>" />  de los activos que 
          entran en el proceso productivo, y su <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_20" 
                    name="comLin_20" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_20'); ?>" />  en efectivo o 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_21" 
                    name="comLin_21" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_21'); ?>" />  de efectivo.
      </div>
      <div class="completar-lineas">
          <span class="num">5</span>
          El activo <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_22" 
                    name="comLin_22" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_22'); ?>" /> es el efectivo y 
          sus <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_23" 
                    name="comLin_23" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_23'); ?>" />;  y los recursos 
          que brindarán un <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_24" 
                    name="comLin_24" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_24'); ?>" /> económico futuro 
          fundadamente esperado, ya sean por su <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_25" 
                    name="comLin_25" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_25'); ?>" />, uso, 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_26" 
                    name="comLin_26" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_26'); ?>" /> o servicios, 
          normalmente en el plazo de <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_27" 
                    name="comLin_27" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_27'); ?>" /> año o en el ciclo 
          financiero a <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_28" 
                    name="comLin_28" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_28'); ?>" /> plazo, el que sea 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_29" 
                    name="comLin_29" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_29'); ?>" />.
      </div>
      <div class="completar-lineas">
          <span class="num">6</span>
          Según la NIF B-6, el término a <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_30" 
                    name="comLin_30" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_30'); ?>" /> plazo (no circulante) 
          incluye activos <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_31" 
                    name="comLin_31" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_31'); ?>" />, intangibles y 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_32" 
                    name="comLin_32" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_32'); ?>" /> que por su 
          naturaleza <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_33" 
                    name="comLin_33" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_33'); ?>" /> son 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_34" 
                    name="comLin_34" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_34'); ?>" /> durante el ciclo 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_35" 
                    name="comLin_35" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_35'); ?>" /> de operaciones. 
      </div>
      <div class="completar-lineas">
          <span class="num">7</span>
          Según la NIF C-8, los activos <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_36" 
                    name="comLin_36" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_36'); ?>" />, son aquellos 
          identificables, sin <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_37" 
                    name="comLin_37" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_37'); ?>" /> física, utilizados 
          para la <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_38" 
                    name="comLin_38" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_38'); ?>" /> o abastecimiento 
          de bienes, <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_39" 
                    name="comLin_39" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_39'); ?>" /> de servicios o 
          para propósitos administrativos, que generarán <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_40" 
                    name="comLin_40" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_40'); ?>" /> 
          económicos futuros controlados por la entidad. 
      </div>
      <div class="completar-lineas">
          <span class="num">8</span>
          Los otros activos se integran por aquellos <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_41" 
                    name="comLin_41" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_41'); ?>" /> 
          que no cumplen con los requisitos y <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_42" 
                    name="comLin_42" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_42'); ?>" /> 
          del activo no circulante, pero que por su naturaleza proporcionarán a 
          la entidad <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_43" 
                    name="comLin_43" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_43'); ?>" /> económicos 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_44" 
                    name="comLin_44" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_44'); ?>" /> fundadamente 
          esperados a un plazo <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_45" 
                    name="comLin_45" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_45'); ?>" /> de un año, lo cual 
          los convierte en activos para la empresa. 
      </div>
      <div class="completar-lineas">
          <button onclick="calificarCompletar('comLin',45,rc5_cl);">CALIFICAR</button>
          <span id="comLin_cal"></span>
      </div>
      
      <h5>III. CONTESTA SI LO QUE SE DICE ES VERDADERO (V) O FALSO (F), EXPLICANDO EL POR QUÉ.</h5>
      <table class="verdadero-falso" id="vf_simple01">
          <tr>
              <th>Caso</th>
              <th>V</th>
              <th>F</th>
              <th>Explicación</th>
          </tr>
          <tr>
              <td>
                  <span class="num">1</span>
                  Activo es una obligación controlada por una entidad, identificada, cuantificada en términos monetarios y del que se esperan sacrificios económicos futuros, derivados de operaciones y otros evento ocurridos en el pasado que han afectado económicamente dicha entidad.
              </td>
              <td id="vf_simple01_verfal_1v_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_1" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_1')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple01_verfal_1f_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_1" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_1')==='falso')?' checked':'' ?> /></td>
              <td><textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalex_1" id="verfalex_1"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalex_1'); ?></textarea></td>
          </tr>
          <tr>
              <td>
                  <span class="num">2</span>
                  La cualidad fundamental del activo es su capacidad de generar beneficios económicos futuros. 
              </td>
              <td id="vf_simple01_verfal_2v_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_2" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_2')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple01_verfal_2f_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_2" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_2')==='falso')?' checked':'' ?> /></td>
              <td><textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalex_2" id="verfalex_2"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalex_2'); ?></textarea></td>
          </tr>
          <tr>
              <td>
                  <span class="num">3</span>
                  El activo puede presentarse clasificado a corto y largo plazos o presentando en orden ascendente o decreciente de disponibilidad (grado de liquidez), sin clasificarlos a corto y largo plazos.
              </td>
              <td id="vf_simple01_verfal_3v_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_3" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_3')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple01_verfal_3f_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_3" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_3')==='falso')?' checked':'' ?> /></td>
              <td><textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalex_3" id="verfalex_3"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalex_3'); ?></textarea></td>
          </tr>
          <tr>
              <td>
                  <span class="num">4</span>
                  El ciclo normal de la operación de una entidad es el periodo comprendido entre la adquisición de los activos que entran en el proceso productivo y su realización en efectivo o equivalentes de efectivo.
              </td>
              <td id="vf_simple01_verfal_4v_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_4" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_4')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple01_verfal_4f_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_4" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_4')==='falso')?' checked':'' ?> /></td>
              <td><textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalex_4" id="verfalex_4"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalex_4'); ?></textarea></td>
          </tr>
          <tr>
              <td>
                  <span class="num">5</span>
                  Se dice que un bien, derecho, propiedad, etc., tiene menor grado de disponibilidad cuanto más fácil sea convertirlo en efectivo.
              </td>
              <td id="vf_simple01_verfal_5v_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_5" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_5')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple01_verfal_5f_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_5" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_5')==='falso')?' checked':'' ?> /></td>
              <td><textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalex_5" id="verfalex_5"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalex_5'); ?></textarea></td>
          </tr>
          <tr>
              <td>
                  <span class="num">6</span>
                  Cuando el ciclo normal de la operación no sea claramente identificable o sea menor de doce meses, se debe considerar que el corto plazo es de doce meses.
              </td>
              <td id="vf_simple01_verfal_6v_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_6" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_6')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple01_verfal_6f_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_6" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_6')==='falso')?' checked':'' ?> /></td>
              <td><textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalex_6" id="verfalex_6"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalex_6'); ?></textarea></td>
          </tr>
          <tr>
              <td>
                  <span class="num">7</span>
                  Activo no circulante son los recursos tangibles, intangibles y financieros que por su naturaleza no son recuperables durante el ciclo normal de operaciones. 
              </td>
              <td id="vf_simple01_verfal_7v_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_7" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_7')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple01_verfal_7f_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_7" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_7')==='falso')?' checked':'' ?> /></td>
              <td><textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalex_7" id="verfalex_7"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalex_7'); ?></textarea></td>
          </tr>
          <tr>
              <td>
                  <span class="num">8</span>
                  Según el boletín C-3, las cuentas caja, fondo fijo de caja 
                  chica y bancos, son parte de las cuentas por cobrar.
              </td>
              <td id="vf_simple01_verfal_8v_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_8" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_8')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple01_verfal_8f_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_8" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_8')==='falso')?' checked':'' ?> /></td>
              <td><textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalex_8" id="verfalex_8"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalex_8'); ?></textarea></td>
          </tr>
          <tr>
              <td>
                  <span class="num">9</span>
                  Según la NIF C-5, cuentas como papelería y útiles, propaganda y publicidas, primas de seguros, rentas cobradas por anticipado, son parte de los activos intangibles.
              </td>
              <td id="vf_simple01_verfal_9v_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_9" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_9')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple01_verfal_9f_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_9" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_9')==='falso')?' checked':'' ?> /></td>
              <td><textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalex_9" id="verfalex_9"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalex_9'); ?></textarea></td>
          </tr>
          <tr>
              <td>
                  <span class="num">10</span>
                  Según la NIF C-6, cuentas como terrenos, edificios, equipo de transporte, equipo de entrega y reparto, mobiliario y equipo, son parte del grupo propiedades, planta y equipo.
              </td>
              <td id="vf_simple01_verfal_10v_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_10" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_10')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple01_verfal_10f_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_10" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_10')==='falso')?' checked':'' ?> /></td>
              <td><textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalex_10" id="verfalex_10"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalex_10'); ?></textarea></td>
          </tr>
          <tr>
              <td colspan="3" id="vf_simple01_verfal_cal"></td>
              <td>
                  <button onclick="calificarVerFalSimple('verfal',10,rc5_verfalR,'vf_simple01');">CALIFICAR</button>
              </td>
          </tr>
      </table>
      
      <h5>IV. PREGUNTAS DE OPCIÓN MÚLTIPLE</h5>
      
      <table class="opcion-multiple">
          <tr>
              <th colspan="2">En los siguientes casos, señale cuál(es) 
                  cuenta(s) no pertenece(n) al grupo del activo circulante.</th>
          </tr>
          <tr>
              <td>Caja</td>
              <td id="opmul_1_Caja_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                         id="a_opmul_1_1" 
                         type="checkbox" 
                         name="opmul_1" 
                         value="Caja" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_1')==='Caja')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Instrumentos financieros</td>
              <td id="opmul_1_Instrumentos financieros_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                    id="a_opmul_1_2" type="checkbox" name="opmul_1" 
                    value="Instrumentos financieros" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_2')==='Instrumentos financieros')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Deudores</td>
              <td id="opmul_1_Deudores_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_3" type="checkbox" name="opmul_1" 
                      value="Deudores" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_3')==='Deudores')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Rentas pagadas por anticipado</td>
              <td id="opmul_1_Rentas pagadas por anticipado_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_4" type="checkbox" name="opmul_1" 
                      value="Rentas pagadas por anticipado" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_4')==='Rentas pagadas por anticipado')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Bancos</td>
              <td id="opmul_1_Bancos_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_5" type="checkbox" name="opmul_1" 
                      value="Bancos" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_5')==='Bancos')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Fondos a largo plazo</td>
              <td id="opmul_1_Fondos a largo plazo_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_6" type="checkbox" name="opmul_1" 
                      value="Fondos a largo plazo" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_6')==='Fondos a largo plazo')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Documentos por cobrar</td>
              <td id="opmul_1_Documentos por cobrar_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_7" type="checkbox" name="opmul_1" 
                      value="Documentos por cobrar" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_7')==='Documentos por cobrar')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Gastos de constitución</td>
              <td id="opmul_1_Gastos de constitución_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_8" type="checkbox" name="opmul_1" 
                      value="Gastos de constitución" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_8')==='Gastos de constitución')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>IVA acreditable</td>
              <td id="opmul_1_IVA acreditable_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_9" type="checkbox" name="opmul_1" 
                      value="IVA acreditable" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_9')==='IVA acreditable')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Propaganda y publicidad</td>
              <td id="opmul_1_Propaganda y publicidad_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_10" type="checkbox" name="opmul_1" 
                      value="Propaganda y publicidad" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_10')==='Propaganda y publicidad')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Papelería y útiles</td>
              <td id="opmul_1_Papelería y útiles_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_11" type="checkbox" name="opmul_1" 
                      value="Papelería y útiles" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_11')==='Papelería y útiles')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Equipo de entrega y reparto</td>
              <td id="opmul_1_Equipo de entrega y reparto_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_12" type="checkbox" name="opmul_1" 
                      value="Equipo de entrega y reparto" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_12')==='Equipo de entrega y reparto')?' checked':'' ?>  />
              </td>
          </tr>
          <tr>
              <td>Fondo fijo de caja chica</td>
              <td id="opmul_1_Fondo fijo de caja chica_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_13" type="checkbox" name="opmul_1" 
                      value="Fondo fijo de caja chica" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_13')==='Fondo fijo de caja chica')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>IVA pendiente de acreditar</td>
              <td id="opmul_1_IVA pendiente de acreditar_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_14" type="checkbox" name="opmul_1" 
                      value="IVA pendiente de acreditar" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_14')==='IVA pendiente de acreditar')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Primas de seguros y fianzas</td>
              <td id="opmul_1_Primas de seguros y fianzas_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_15" type="checkbox" name="opmul_1" 
                      value="Primas de seguros y fianzas" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_15')==='Primas de seguros y fianzas')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Cuentas por cobrar a largo plazo</td>
              <td id="opmul_1_Cuentas por cobrar a largo plazo_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_16" type="checkbox" name="opmul_1" 
                      value="Cuentas por cobrar a largo plazo" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_16')==='Cuentas por cobrar a largo plazo')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Clientes</td>
              <td id="opmul_1_Clientes_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_17" type="checkbox" name="opmul_1" 
                      value="Clientes" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_17')==='Clientes')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Equipo de transporte</td>
              <td id="opmul_1_Equipo de transporte_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_18" type="checkbox" name="opmul_1" 
                      value="Equipo de transporte" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_18')==='Equipo de transporte')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Inventarios o almacén</td>
              <td id="opmul_1_Inventarios o almacén_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_19" type="checkbox" name="opmul_1" 
                      value="Inventarios o almacén" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_19')==='Inventarios o almacén')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Mobiliario y equipo de oficina</td>
              <td id="opmul_1_Mobiliario y equipo de oficina_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_20" type="checkbox" name="opmul_1" 
                      value="Mobiliario y equipo de oficina" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_20')==='Mobiliario y equipo de oficina')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td id="opmul_1_cal"></td>
              <td>
                  <button onclick="calificarOpMul('opmul_1',rc5_opcmulA);">CALIFICAR</button>
              </td>
          </tr>
      </table>
      
      <table class="opcion-multiple" style="margin-top: 25px;">
          <tr>
              <th colspan="2">En el siguiente listado, indique cuál(es) cuenta(s) 
                  no pertenece(n) al grupo del activo no circulante propiedades, 
                  planta y equipo.</th>
          </tr>
          <tr>
              <td>Terrenos</td>
              <td id="opmul_2_Terrenos_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                         id="b_opmul_2_1" 
                         type="checkbox" 
                         name="opmul_2" 
                         value="Terrenos" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'b_opmul_2_1')==='Terrenos')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Patentes</td>
              <td id="opmul_2_Patentes_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                    id="b_opmul_2_2" type="checkbox" name="opmul_2" 
                    value="Patentes" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'b_opmul_2_2')==='Patentes')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Equipo de transporte</td>
              <td id="opmul_2_Equipo de transporte_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="b_opmul_2_3" type="checkbox" name="opmul_2" 
                      value="Equipo de transporte" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'b_opmul_2_3')==='Equipo de transporte')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Fondo fijo de caja chica</td>
              <td id="opmul_2_Fondo fijo de caja chica_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="b_opmul_2_4" type="checkbox" name="opmul_2" 
                      value="Fondo fijo de caja chica" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'b_opmul_2_4')==='Fondo fijo de caja chica')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Mobiliario y equipo de oficina</td>
              <td id="opmul_2_Mobiliario y equipo de oficina_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="b_opmul_2_5" type="checkbox" name="opmul_2" 
                      value="Mobiliario y equipo de oficina" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'b_opmul_2_5')==='Mobiliario y equipo de oficina')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Derechos de autor</td>
              <td id="opmul_2_Derechos de autor_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="b_opmul_2_6" type="checkbox" name="opmul_2" 
                      value="Derechos de autor" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'b_opmul_2_6')==='Derechos de autor')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Documentos por cobrar</td>
              <td id="opmul_2_Documentos por cobrar_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="b_opmul_2_7" type="checkbox" name="opmul_2" 
                      value="Documentos por cobrar" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'b_opmul_2_7')==='Documentos por cobrar')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Equipo de entrega y reparto</td>
              <td id="opmul_2_Equipo de entrega y reparto_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="b_opmul_2_8" type="checkbox" name="opmul_2" 
                      value="Equipo de entrega y reparto" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'b_opmul_2_8')==='Equipo de entrega y reparto')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Inventarios o almacén</td>
              <td id="opmul_2_Inventarios o almacén_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="b_opmul_2_9" type="checkbox" name="opmul_2" 
                      value="Inventarios o almacén" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'b_opmul_2_9')==='Inventarios o almacén')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Edificios</td>
              <td id="opmul_2_Edificios_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="b_opmul_2_10" type="checkbox" name="opmul_2" 
                      value="Edificios" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'b_opmul_2_10')==='Edificios')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td id="opmul_2_cal"></td>
              <td>
                  <button onclick="calificarOpMul('opmul_2',rc5_opcmulB);">CALIFICAR</button>
              </td>
          </tr>
      </table>
      
      <table class="opcion-multiple" style="margin-top: 25px;">
          <tr>
              <th colspan="2">Del siguiente grupo, indique cuál(es) cuenta(s) 
                  no pertenece(n) al grupo del activo no circulante intangibles.</th>
          </tr>
          <tr>
              <td>Fondos a largo plazo</td>
              <td id="opmul_3_Fondos a largo plazo_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                         id="c_opmul_3_1" 
                         type="checkbox" 
                         name="opmul_3" 
                         value="Fondos a largo plazo" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'c_opmul_3_1')==='Fondos a largo plazo')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Cuentas por cobrar a largo plazo</td>
              <td id="opmul_3_Cuentas por cobrar a largo plazo_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                    id="c_opmul_3_2" type="checkbox" name="opmul_3" 
                    value="Cuentas por cobrar a largo plazo" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'c_opmul_3_2')==='Cuentas por cobrar a largo plazo')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Gastos de constitución</td>
              <td id="opmul_3_Gastos de constitución_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="c_opmul_3_3" type="checkbox" name="opmul_3" 
                      value="Gastos de constitución" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'c_opmul_3_3')==='Gastos de constitución')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Franquicias</td>
              <td id="opmul_3_Franquicias_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="c_opmul_3_4" type="checkbox" name="opmul_3" 
                      value="Franquicias" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'c_opmul_3_4')==='Franquicias')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Pagos anticipados a largo plazo</td>
              <td id="opmul_3_Pagos anticipados a largo plazo_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="c_opmul_3_5" type="checkbox" name="opmul_3" 
                      value="Pagos anticipados a largo plazo" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'c_opmul_3_5')==='Pagos anticipados a largo plazo')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Marcas registradas</td>
              <td id="opmul_3_Marcas registradas_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="c_opmul_3_6" type="checkbox" name="opmul_3" 
                      value="Marcas registradas" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'c_opmul_3_6')==='Marcas registradas')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Inmuebles no utilizados</td>
              <td id="opmul_3_Inmuebles no utilizados_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="c_opmul_3_7" type="checkbox" name="opmul_3" 
                      value="Inmuebles no utilizados" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'c_opmul_3_7')==='Inmuebles no utilizados')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Patentes</td>
              <td id="opmul_3_Patentes_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="c_opmul_3_8" type="checkbox" name="opmul_3" 
                      value="Patentes" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'c_opmul_3_8')==='Patentes')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>IVA acreditable</td>
              <td id="opmul_3_IVA acreditable_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="c_opmul_3_9" type="checkbox" name="opmul_3" 
                      value="IVA acreditable" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'c_opmul_3_9')==='IVA acreditable')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Derechos de autor</td>
              <td id="opmul_3_Derechos de autor_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="c_opmul_3_10" type="checkbox" name="opmul_3" 
                      value="Derechos de autor" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'c_opmul_3_10')==='Derechos de autor')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td id="opmul_3_cal"></td>
              <td>
                  <button onclick="calificarOpMul('opmul_3',rc5_opcmulC);">CALIFICAR</button>
              </td>
          </tr>
      </table>
      
      <h5>V. RELACIONA LAS COLUMNAS 1 Y 2, ANOTANDO EN LA COLUMNA 1, EL NOMBRE DEL BOLETÍN O NIF QUE NORMA EL CONCEPTO SEÑALADO.</h5>
      <table class='correlaciones'>
          <tr>
              <th>Columna 1</th>
              <th>Columna 2</th>
          </tr>
          <tr>
              <td>
                  <span class="num">1</span>
                  <p>Inventarios</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_1' id='corr1_1'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C-7</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C-8</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–6</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>Boletín C–2</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–4</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>Boletín C-3</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–1</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–5</option>
                  </select>
                  <span id='corr1_1-parcial'></span>
              </td>
              <td rowspan="8">
                  <ol>
                      <li>NIF C-7</li>
                      <li>NIF C-8</li>
                      <li>NIF C–6</li>
                      <li>Boletín C–2</li>
                      <li>NIF C–4</li>
                      <li>Boletín C-3</li>
                      <li>NIF C–1</li>
                      <li>NIF C–5</li>
                  </ol>
              </td>
          </tr>
          <tr>
              <td>
                  <span class="num">2</span>
                  <p>Instrumentos financieros</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_2' id='corr1_2'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C-7</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C-8</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–6</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>Boletín C–2</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–4</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>Boletín C-3</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–1</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–5</option>
                  </select>
                  <span id='corr1_2-parcial'></span>
              </td>
          </tr>
          <tr>
              <td>
                  <span class="num">3</span>
                  <p>Activos intangibles</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_3' id='corr1_3'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C-7</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C-8</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–6</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>Boletín C–2</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–4</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>Boletín C-3</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–1</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–5</option>
                  </select>
                  <span id='corr1_3-parcial'></span>
              </td>
          </tr>
          <tr>
              <td>
                  <span class="num">4</span>
                  <p>Efectivo y equivalentes de efectivo</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_4' id='corr1_4'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C-7</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C-8</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–6</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>Boletín C–2</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–4</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>Boletín C-3</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–1</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–5</option>
                  </select>
                  <span id='corr1_4-parcial'></span>
              </td>
          </tr>
          <tr>
              <td>
                  <span class="num">5</span>
                  <p>Pagos anticipados</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_5' id='corr1_5'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C-7</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C-8</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–6</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>Boletín C–2</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–4</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>Boletín C-3</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–1</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–5</option>
                  </select>
                  <span id='corr1_5-parcial'></span>
              </td>
          </tr>
          <tr>
              <td>
                  <span class="num">6</span>
                  <p>Propiedades, planta y equipo</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_6' id='corr1_6'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C-7</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C-8</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–6</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>Boletín C–2</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–4</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>Boletín C-3</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–1</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–5</option>
                  </select>
                  <span id='corr1_6-parcial'></span>
              </td>
          </tr>
          <tr>
              <td>
                  <span class="num">7</span>
                  <p>Inversiones en asociadas, negocios conjuntos y otras inversiones permanente</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_7' id='corr1_7'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C-7</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C-8</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–6</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>Boletín C–2</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–4</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>Boletín C-3</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–1</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–5</option>
                  </select>
                  <span id='corr1_7-parcial'></span>
              </td>
          </tr>
          <tr>
              <td>
                  <span class="num">8</span>
                  <p>Cuentas por cobrar</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_8' id='corr1_8'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C-7</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C-8</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–6</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>Boletín C–2</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–4</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>Boletín C-3</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–1</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>NIF C–5</option>
                  </select>
                  <span id='corr1_8-parcial'></span>
              </td>
          </tr>
          
          <tr>
              <td id='corr1_cal'></td>
              <td><button onclick="calificarCorrelacion('corr1',8,rc5_corr1);" 
                          name='cal_corr1'>CALIFICAR</button></td>
          </tr>
      </table>
      
      <h5>VI. A CONTINUACIÓN SE PRESENTAN VARIAS CUENTAS, SEÑALA CON UNA X SI PERTENECEN AL ACTIVO CIRCULANTE O NO CIRCULANTE.</h5>
      
      <table class="verdadero-falso" id="vf_simple02">
          <tr>
              <th>Cuentas</th>
              <th>Circulante</th>
              <th>No circulante</th>
          </tr>
          <tr>
              <td>Franquicias</td>
              <td id="vf_simple02_verfal_1v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_1" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_1')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple02_verfal_1f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_1" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_1')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Terrenos</td>
              <td id="vf_simple02_verfal_2v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_2" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_2')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple02_verfal_2f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_2" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_2')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Clientes</td>
              <td id="vf_simple02_verfal_3v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_3" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_3')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple02_verfal_3f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_3" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_3')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Almacén o inventarios</td>
              <td id="vf_simple02_verfal_4v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_4" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_4')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple02_verfal_4f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_4" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_4')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Derechos de autor</td>
              <td id="vf_simple02_verfal_5v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_5" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_5')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple02_verfal_5f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_5" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_5')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Papelería y útiles</td>
              <td id="vf_simple02_verfal_6v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_6" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_6')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple02_verfal_6f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_6" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_6')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Edificios</td>
              <td id="vf_simple02_verfal_7v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_7" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_7')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple02_verfal_7f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_7" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_7')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Bancos</td>
              <td id="vf_simple02_verfal_8v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_8" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_8')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple02_verfal_8f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_8" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_8')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Caja</td>
              <td id="vf_simple02_verfal_9v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_9" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_9')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple02_verfal_9f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_9" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_9')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Pagos anticipados a largo plazo</td>
              <td id="vf_simple02_verfal_10v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_10" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_10')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple02_verfal_10f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_10" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_10')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Fondo fijo de caja chica</td>
              <td id="vf_simple02_verfal_11v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_11" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_11')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple02_verfal_11f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_11" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_11')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Mobiliario y equipo</td>
              <td id="vf_simple02_verfal_12v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_12" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_12')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple02_verfal_12f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_12" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_12')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Patentes</td>
              <td id="vf_simple02_verfal_13v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_13" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_13')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple02_verfal_13f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_13" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_13')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Marcas registradas</td>
              <td id="vf_simple02_verfal_14v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_14" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_14')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple02_verfal_14f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_14" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_14')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Instrumentos financieros</td>
              <td id="vf_simple02_verfal_15v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_15" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_15')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple02_verfal_15f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple02_verfal_15" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple02_verfal_15')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td colspan="2" id="vf_simple02_verfal_cal"></td>
              <td>
                  <button onclick="calificarVerFalSimple('verfal',15,rc5_simpleB,'vf_simple02');">CALIFICAR</button>
              </td>
          </tr>
      </table>
      
      <table class="verdadero-falso" id="vf_simple03" style="margin: 25px 0 25px 0">
          <tr>
              <th>Cuentas</th>
              <th>Circulante</th>
              <th>No circulante</th>
          </tr>
          <tr>
              <td>Deudores</td>
              <td id="vf_simple03_verfal_1v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_1" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_1')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_1f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_1" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_1')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Equipo de entrega y reparto</td>
              <td id="vf_simple03_verfal_2v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_2" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_2')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_2f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_2" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_2')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Documentos por cobrar</td>
              <td id="vf_simple03_verfal_3v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_3" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_3')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_3f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_3" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_3')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Almacén o inventarios</td>
              <td id="vf_simple03_verfal_4v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_4" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_4')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_4f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_4" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_4')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Franquicias</td>
              <td id="vf_simple03_verfal_5v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_5" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_5')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_5f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_5" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_5')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Propaganda y publicidad</td>
              <td id="vf_simple03_verfal_6v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_6" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_6')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_6f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_6" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_6')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Edificios</td>
              <td id="vf_simple03_verfal_7v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_7" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_7')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_7f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_7" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_7')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Bancos</td>
              <td id="vf_simple03_verfal_8v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_8" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_8')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_8f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_8" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_8')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Caja</td>
              <td id="vf_simple03_verfal_9v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_9" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_9')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_9f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_9" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_9')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Cuentas por cobrar a largo plazo</td>
              <td id="vf_simple03_verfal_10v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_10" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_10')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_10f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_10" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_10')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>IVA acreditable</td>
              <td id="vf_simple03_verfal_11v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_11" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_11')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_11f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_11" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_11')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Equipo de transporte</td>
              <td id="vf_simple03_verfal_12v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_12" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_12')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_12f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_12" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_12')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Gastos de constitución</td>
              <td id="vf_simple03_verfal_13v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_13" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_13')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_13f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_13" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_13')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Marcas registradas</td>
              <td id="vf_simple03_verfal_14v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_14" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_14')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_14f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_14" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_14')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>IVA pendiente de acreditar</td>
              <td id="vf_simple03_verfal_15v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_15" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_15')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_15f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_15" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_15')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td colspan="2" id="vf_simple03_verfal_cal"></td>
              <td>
                  <button onclick="calificarVerFalSimple('verfal',15,rc5_simpleC,'vf_simple03');">CALIFICAR</button>
              </td>
          </tr>
      </table>
      
      
      <h4 class="subtit-4">EJERCICIOS 2a PARTE</h4>
      
      <p>
          <a href="capitulo5a.php">Segunda parte</a>
      </p>
      
      
  </div>
  <!-- -->
  <div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
<script>
    var tc;
    var cCamCues = function(campo,valor,registro)
    {
      clearTimeout(tc);
      tc = window.setTimeout(function(){
          $.post('libreria/cCamCues.php',{
              modDatos: true,
              elcampo: campo,
              elvalor: valor,
              elregistro: registro
          },function(respuesta){
              if(respuesta === 'modificado')
              {
                  // Modificado
              } else {
                  // Error
                  console.log('-> '+respuesta);
              }
          });
      },1000);
    };
    var cResCues = function(cues,usu,cap,preg,resp)
    {
      clearTimeout(tc);
      tc = window.setTimeout(function(){
          $.post('libreria/cCamCues.php',{
              modRespuestas: true,
              cuestionario: cues,
              usuario: usu,
              capitulo: cap,
              pregunta: preg,
              respuesta:resp
          },function(respuesta){
              if(respuesta === 'modificado')
              {
                  // Modificado
              } else {
                  // Error
                  console.log('-> '+respuesta);
              }
          });
      },500);
    };
    
    var delResCues = function(cues,usu,cap,preg,resp)
    {
      clearTimeout(tc);
      tc = window.setTimeout(function(){
          $.post('libreria/cCamCues.php',{
              delRespuesta: true,
              cuestionario: cues,
              usuario: usu,
              capitulo: cap,
              pregunta: preg,
              respuesta:resp
          },function(respuesta){
              if(respuesta === 'eliminado')
              {
                  // Eliminado
              } else {
                  // Error
                  console.log('-> '+respuesta);
              }
          });
      },500);
    };
    
    var mostrarEstado = function(cues,usu,cap,id,valor)
    {
      var elemento = document.getElementById(id);
      var estado = elemento.checked;
      if(estado)
      {
          cResCues(cues,usu,cap,id,valor);
      } else {
          delResCues(cues,usu,cap,id,valor);
      }
    };
    
    window.onload = function(){
        protegerPLibres();
        protegerCompletar();
        protegerVerFalSimple('vf_simple01');
        protegerVerFalSimple('vf_simple02');
        protegerVerFalSimple('vf_simple03');
        protegerOpMul();
        protegerCorrelaciones();
    };
</script>
</body>
</html>