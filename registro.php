<?php
include("libreria/principal.php");

if(isset($_SESSION['registrado']))
{
    header("Location: portada.php");
    exit;
}

$submit = filter_input(INPUT_POST, 'submit');
if(isset($submit)) {
	
	$nombre = utf8_decode(filter_input(INPUT_POST, 'nombre',
                FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW));
	$apellidos =	utf8_decode(filter_input(INPUT_POST, 'apellidos',
                FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW));
	$correo =	utf8_decode(filter_input(INPUT_POST, 'correo',
                FILTER_SANITIZE_EMAIL));
	$institucion=	utf8_decode(filter_input(INPUT_POST, 'institucion',
                FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW));
	$tipo =		utf8_decode(filter_input(INPUT_POST, 'tipo',
                FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW));
	$usuario =	utf8_decode(filter_input(INPUT_POST, 'usuario',
                FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW));
	$pwd =		utf8_decode(filter_input(INPUT_POST, 'pwd',
                FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW));
        
        // Primero, comprobar que no haya duplicados
        $queryD = "SELECT COUNT(IDusuario) "
                . "FROM rom_usuarios "
                . "WHERE usuario = '$usuario'";
        $resultD = mysql_query($queryD) or die(mysql_error());
        $dataD = mysql_fetch_row($resultD);
        $numD = $dataD[0];
        if($numD != 0)
        {
            exit('Usuario duplicado, volver a intentar!');
        }
	
	// insertar usuario
	$query = "INSERT INTO rom_usuarios 
                    (nombre, apellidos, correo, usuario, pwd, institucion, tipo)
                    VALUES ('$nombre', '$apellidos', '$correo', '$usuario', '$pwd', '$institucion', '$tipo')";	
	mysql_query($query) or die(mysql_error());
	$userID = mysql_insert_id();
	
	$_SESSION['registrado'] = true;
	$_SESSION['userID'] = true;
	$_SESSION['userID'] = $userID;
	$_SESSION['nombre'] = true;
	$_SESSION['nombre'] = $nombre;
	
	header("Location: portada.php");
	exit;
}

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sistema Contable Romero</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="libreria/js_principal.js"></script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Registro de usuario</h1>
    </div>
    <div id="divTitularMenu">
        <a href="portada.php">Inicio</a> | 
        <a href="libreria/calculadora/calculadora.html" 
           target="_blank" 
           onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | 
        <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido">
  <div class="divContCuerpo">
      <div style="float: left; margin: 25px 0;">
          <img class="img-portada" src="imagenes/porPrincipiosConta.jpg" 
               alt="" height="375" />
      </div>
      <div style="float: left; margin-left: 100px; margin-top: 50px">
          <form class="form-2014" 
                action="registro.php" 
                method="POST" 
                autocomplete="off" 
                onsubmit="return Validador(this)">
              <fieldset>
                  <legend>Regístrate en el sistema</legend>
                  <div>
                      <input type="text" 
                             name="nombre" 
                             required 
                             placeholder="Nombre real" 
                             style="width: 20em"/>
                  </div>
                  <div>
                      <input type="text" 
                             name="apellidos" 
                             required 
                             placeholder="Apellidos" 
                             style="width: 20em" />
                  </div>
                  <div>
                      <input type="email" 
                             name="correo" 
                             required 
                             placeholder="Correo electrónico" 
                             style="width: 20em" />
                  </div>
                  <div>
                      <input type="text" 
                             name="institucion" 
                             required 
                             placeholder="Institución" 
                             style="width: 20em" />
                  </div>
                  <div>
                      <label style="width: 5em;" for="tP">Profesor</label>
                      <input type="radio" 
                             checked 
                             name="tipo" 
                             value="Maestro"
                             id="tP" /><br />
                      <label style="width: 5em;" for="tA">Alumno</label>
                      <input type="radio" 
                             name="tipo" 
                             value="Alumno"
                             id="tA" /> 
                  </div>
                  <div>
                      <input type="text" 
                             name="usuario" 
                             required 
                             placeholder="Nombre de usuario" 
                             oninput="validarUsuario(this.value);" />
                  </div>
                  <div id="msgUser"></div>
                  <div>
                      <input type="password" 
                             name="pwd" 
                             required 
                             placeholder="Contraseña" />
                  </div>
                  <div>
                      <button type="submit" id="submit" 
                              name="submit" disabled>Registrarse</button>
                      <a style="margin-left: 10px" 
                         href="sesion_registro.php">Página anterior</a>
                  </div>
              </fieldset>
          </form>
          
          <div style="background: rgba(200,200,255,.35); 
               padding: 10px; 
               margin-top: 50px;">
              <p>Si ya cuentas con un nombre <br /> de usuario, 
                  <a href="sesion_registro.php">haz clic aquí</a>.</p>
          </div>
      </div>
  </div>
  <!-- -->
  <div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
<script src="libreria/vendor/jquery/jquery-1.11.0.min.js"></script>
<script src="libreria/valUsu.js"></script>
</body>
</html>