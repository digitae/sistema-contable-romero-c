<?php
include 'libreria/principal.php';
include 'libreria/evaluaciones.php';

esUsuario(); // será usuario registrado?
$IDusuario = $_SESSION['userID'];
$capitulo = 51;
$registro = verificarCuestionario($capitulo, $IDusuario,  utf8_decode($Fecha));


?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> Principios de Contabilidad. Cuestionario de evaluación. Capítulo 5</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="libreria/js_principal.js"></script>
<script src="libreria/vendor/jquery/jquery-1.11.0.min.js"></script>
<script src="libreria/calCap.js"></script>
<script src="libreria/rc5a.js"></script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Cuestionario de autoevaluación</h1>
    </div>
    <div id="divTitularMenu">
        <a href="portada.php">Inicio</a> | 
        <a href="libreria/calculadora/calculadora.html" 
           target="_blank" 
           onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | 
        <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido" style="width: 900px">
  <div class="divContCuerpo">
      
      <h3 class="titulo-principal">Capítulo 5</h3>
      <div class="migas">
          <ul>
              <li><a href="portada.php">Portada</a></li>
              <li><a href="javascript:history.back();">Página anterior</a></li>
              <li><img src="imagenes/print.png" 
                       alt="" 
                       height="25" 
                       style="float: left; margin-right: 5px;" />
                  <a href="javascript:print();">Imprimir página</a></li>
          </ul>
      </div>
      
      <table class="mis-datos">
          <tr>
              <td>Universidad o Institución:</td>
              <td>
                  <input oninput="cCamCues('unidad',this.value,<?php echo $registro ?>);" 
                         name="unidad" 
                         id='unidad' 
                         required 
                         value="<?php echo xCamCues('unidad', $registro) ?>" />
              </td>
              <td>Escuela o facultad:</td>
              <td>
                  <input oninput="cCamCues('facultad',this.value,<?php echo $registro ?>);" 
                         name="facultad" 
                         id='facultad' 
                         required 
                         value="<?php echo xCamCues('facultad', $registro) ?>" />
              </td>
          </tr>
          <tr>
              <td>Asignatura:</td>
              <td>
                  <input oninput="cCamCues('asignatura',this.value,<?php echo $registro ?>);" 
                         name="asignatura" 
                         id='asignatura' 
                         required 
                         value="<?php echo xCamCues('asignatura', $registro) ?>" />
              </td>
              <td>Alumno:</td>
              <td>
                  <?php echo nombreUsuario($IDusuario) ?>
              </td>
          </tr>
          <tr>
              <td>Maestro:</td>
              <td>
                  <input oninput="cCamCues('maestro',this.value,<?php echo $registro ?>);" 
                         name="maestro" 
                         id='maestro' 
                         required 
                         value="<?php echo xCamCues('maestro', $registro) ?>" />
              </td>
              <td>Fecha:</td>
              <td>
                  <input oninput="cCamCues('fecha',this.value,<?php echo $registro ?>);" 
                         name="fecha" 
                         id='fecha' 
                         required 
                         value="<?php echo xCamCues('fecha', $registro) ?>" />
              </td>
          </tr>
      </table>
      
      <h4 class="subtit-4">OBJETIVOS DE APRENDIZAJE</h4>
      
      <div class="objetivos">
          <p>Al finalizar este capítulo, el alumno será capaz de:</p>
          <ol>
              <li>Comprender que la estructura financiera de una entidad se 
                  integra por recursos empleados para la realización de sus 
                  fines, los cuales provienen de fuentes externas o internas.</li>
              <li>Examinar y comprender el concepto de pasivo y capital contable.</li>
              <li>Distinguir  las  características y clasificación del pasivo y 
                  capital contable.</li>
              <li>Demostrar que la contabilidad, con base en su estructura 
                  financiera,  mantiene siempre una igualdad entre el activo y 
                  la suma del pasivo más el capital contable.</li>
          </ol>
      </div>
      
      <h4 class="subtit-4">EJERCICIOS 2a PARTE</h4>
      
      <h5>I. RESUELVA LO SIGUIENTE</h5>
      <div class="pregunta-libre">
          <p><span>1</span> 1. ¿Cuál es el concepto de pasivo según la dualidad 
              económica?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib01" 
                    id="prelib01"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib01'); ?></textarea>
      </div>
      <div class="pregunta-libre">
          <p><span>2</span> ¿Qué dice Finney-Miller, respecto del pasivo?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib02" 
                    id="prelib02"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib02'); ?></textarea>
      </div>
      <div class="pregunta-libre">
          <p><span>3</span> Mencione las características del pasivo.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib03" 
                    id="prelib03"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib03'); ?></textarea>
      </div>
      <div class="pregunta-libre">
          <p><span>4</span> Según la NIF A-5, los pasivos de una entidad pueden 
              ser de diferentes tipos atendiendo a su naturaleza, diga cuáles son.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib04" 
                    id="prelib04"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib04'); ?></textarea>
      </div>
      <div class="pregunta-libre">
          <p><span>5</span> Investigue otras definiciones de pasivo, cuando menos 3.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib05" 
                    id="prelib05"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib05'); ?></textarea>
      </div>
      <div class="pregunta-libre">
          <p><span>6</span> Proponga su definición personal de pasivo.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib06" 
                    id="prelib06"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib06'); ?></textarea>
      </div>
      <div class="pregunta-libre">
          <p><span>7</span> Según la NIF B-6, ¿cómo puede presentarse clasificado el pasivo?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib07" 
                    id="prelib07"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib07'); ?></textarea>
      </div>
      <div class="pregunta-libre">
          <p><span>8</span> La NIF B-6 dice que una entidad debe clasificar un 
              pasivo como circulante a corto plazo cuando cumpla con ciertas 
              consideraciones, indique cuáles son.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib08" 
                    id="prelib08"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib08'); ?></textarea>
      </div>
      <div class="pregunta-libre">
          <p><span>9</span> ¿Qué se entiende por exigibilidad?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib09" 
                    id="prelib09"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib09'); ?></textarea>
      </div>
      <div class="pregunta-libre">
          <p><span>10</span> En las reglas de presentación, el Boletín C-9 
              establece qué pasivos deben presentarse a corto plazo, menciónelos.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib010" 
                    id="prelib010"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib010'); ?></textarea>
      </div>
      <div class="pregunta-libre">
          <p><span>11</span> ¿Cómo se conceptúa el capital contable según la 
              dualidad económica?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib011" 
                    id="prelib011"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib011'); ?></textarea>
      </div>
      <div class="pregunta-libre">
          <p><span>12</span> ¿Cómo conceptúan el capital Finney-Miller?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib012" 
                    id="prelib012"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib012'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>13</span> Investigue otras definiciones de capital contable.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib13" 
                    id="prelib13"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib13'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>14</span> Proponga su propia definición de capital contable.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib14" 
                    id="prelib14"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib14'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>15</span> ¿Cómo se clasifica el capital contable en las 
              entidades lucrativas?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib15" 
                    id="prelib15"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib15'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>16</span> ¿Cómo se clasifica el patrimonio contable para 
              las entidades no lucrativas?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib16" 
                    id="prelib16"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib16'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>17</span> ¿Cuáles son las igualdades (ecuaciones) 
              fundamentales de la contabilidad?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib17" 
                    id="prelib17"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib17'); ?></textarea>
      </div>
      
      <h5>II. COMPLETE LAS LÍNEAS EN BLANCO</h5>
      
      <div class="completar-lineas">
          <span class="num">1</span>
          El pasivo es una <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_1" 
                    name="comLin_1" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_1'); ?>" /> presente de la entidad, 
          virtualmente <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_2" 
                    name="comLin_2" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_2'); ?>" />, identificada, 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_3" 
                    name="comLin_3" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_3'); ?>" /> en términos 
          monetarios y que representa una <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_4" 
                    name="comLin_4" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_4'); ?>" /> futura de 
          beneficios económicos, derivada de operaciones y otros eventos 
          ocurridos en el <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_5" 
                    name="comLin_5" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_5'); ?>" /> que han afectado 
          económicamente a dicha entidad.
      </div>
      
      <div class="completar-lineas">
          <span class="num">2</span>
          Según el autor, el pasivo representa los <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_6" 
                    name="comLin_6" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_6'); ?>" /> de que dispone una 
          entidad para la realización de sus fines, que han sido 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_7" 
                    name="comLin_7" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_7'); ?>" /> por fuentes 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_8" 
                    name="comLin_8" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_8'); ?>" /> de la entidad, 
          derivado de transacciones o eventos económicos ocurridos, que hacen 
          nacer una obligación <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_9" 
                    name="comLin_9" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_9'); ?>" />, virtualmente 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_10" 
                    name="comLin_10" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_10'); ?>" /> de transferir 
          efectivo, <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_11" 
                    name="comLin_11" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_11'); ?>" /> o servicios en el 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_12" 
                    name="comLin_12" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_12'); ?>" />, que reúnan los 
          requisitos de ser <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_13" 
                    name="comLin_13" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_13'); ?>" /> y cuantificables 
          en unidades monetarias.
      </div>
      
      <div class="completar-lineas">
          <span class="num">3</span>
          Según el Boletín C-9, el pasivo a <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_14" 
                    name="comLin_14" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_14'); ?>" /> plazo es aquel 
          cuyo <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_15" 
                    name="comLin_15" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_15'); ?>" /> se producirá dentro 
          de <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_16" 
                    name="comLin_16" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_16'); ?>" /> año o en el 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_17" 
                    name="comLin_17" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_17'); ?>" /> normal de las 
          operaciones, cuando éste sea <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_18" 
                    name="comLin_18" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_18'); ?>" /> a un año.
      </div>
      
      <div class="completar-lineas">
          <span class="num">4</span>
          Según el Boletín C-9, el pasivo no circulante (a largo plazo) son los 
          pasivos <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_19" 
                    name="comLin_19" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_19'); ?>" /> que proporcionan 
          financiamiento a <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_20" 
                    name="comLin_20" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_20'); ?>" /> plazo, es decir, 
          no forman parte del capital de <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_21" 
                    name="comLin_21" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_21'); ?>" /> utilizado en el 
          ciclo <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_22" 
                    name="comLin_22" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_22'); ?>" /> de operación de 
          la entidad y que <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_23" 
                    name="comLin_23" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_23'); ?>" /> deben liquidarse 
          dentro de los <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_24" 
                    name="comLin_24" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_24'); ?>" /> meses posteriores 
          a la fecha de cierre del periodo sobre el que se informa.
      </div>
      
      <div class="completar-lineas">
          <span class="num">5</span>
          Según la NIF A-5, el concepto de <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_25" 
                    name="comLin_25" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_25'); ?>" /> contable se utiliza 
          para las entidades <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_26" 
                    name="comLin_26" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_26'); ?>" /> y el de patrimonio 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_27" 
                    name="comLin_27" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_27'); ?>" />, para las entidades 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_28" 
                    name="comLin_28" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_28'); ?>" /> lucrativas;  
          sin embargo, en ambas entidades, estos conceptos son similares.
      </div>
      
      <div class="completar-lineas">
          <span class="num">6</span>
          Según la NIF A–5, el capital contable es el valor <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_29" 
                    name="comLin_29" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_29'); ?>" /> de los activos de 
          la entidad, una vez deducidos todos sus pasivos.
      </div>
      
      <div class="completar-lineas">
          <span class="num">7</span>
          De acuerdo con un enfoque <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_30" 
                    name="comLin_30" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_30'); ?>" />, el <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_31" 
                    name="comLin_31" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_31'); ?>" /> contable o 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_32" 
                    name="comLin_32" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_32'); ?>" /> contable representa 
          la porción del <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_33" 
                    name="comLin_33" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_33'); ?>" /> total financiada 
          por los <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_34" 
                    name="comLin_34" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_34'); ?>" /> o, en su caso, 
          por los patrocinadores de la entidad. Por lo tanto, mientras los 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_35" 
                    name="comLin_35" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_35'); ?>" /> se consideran 
          fuentes <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_36" 
                    name="comLin_36" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_36'); ?>" /> de recursos, el 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_37" 
                    name="comLin_37" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_37'); ?>" /> o patrimonio 
          contable es una fuente <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_38" 
                    name="comLin_38" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_38'); ?>" />.
      </div>
      
      <div class="completar-lineas">
          <span class="num">8</span>
          De acuerdo con el autor, para entidades con propósitos lucrativos el 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_39" 
                    name="comLin_39" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_39'); ?>" /> contable representa 
          los <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_40" 
                    name="comLin_40" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_40'); ?>" /> de que dispone la 
          entidad para la realización de sus fines, los cuales han sido 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_41" 
                    name="comLin_41" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_41'); ?>" /> por fuentes 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_42" 
                    name="comLin_42" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_42'); ?>" /> representadas por 
          los propietarios o dueños, y los provenientes de las operaciones, 
          transformaciones internas y otros eventos que la afecten. Los 
          propietarios adquieren un derecho <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_43" 
                    name="comLin_43" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_43'); ?>" /> sobre los activos 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_44" 
                    name="comLin_44" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_44'); ?>" />, el cual se ejerce 
          mediante <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_45" 
                    name="comLin_45" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_45'); ?>" /> o distribución.
      </div>
      
      <div class="completar-lineas">
          <span class="num">9</span>
          El capital <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_46" 
                    name="comLin_46" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_46'); ?>" /> está conformado 
          por las aportaciones de los propietarios de la entidad.
      </div>
      
      <div class="completar-lineas">
          <span class="num">10</span>
          La NIF A-5 establece que para que una entidad haya <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_47" 
                    name="comLin_47" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_47'); ?>" /> su capital contable 
          o patrimonio contable, éste debe tener un importe <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_48" 
                    name="comLin_48" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_48'); ?>" /> al principio y al 
          final del periodo.
      </div>
      
      <div class="completar-lineas">
          <span class="num">11</span>
          Patrimonio restringido <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_49" 
                    name="comLin_49" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_49'); ?>" /> es aquel cuyo uso 
          por parte de la entidad está limitado por disposiciones de los 
          patrocinadores, que no expiran con el paso del tiempo, y no 
          pueden ser <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_50" 
                    name="comLin_50" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_50'); ?>" /> por acciones de 
          la administración.
      </div>
      
      <div class="completar-lineas">
          <span class="num">12</span>
          Patrimonio restringido <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_51" 
                    name="comLin_51" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_51'); ?>" /> es aquel cuyo uso 
          por parte de la entidad está <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_52" 
                    name="comLin_52" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_52'); ?>" /> por disposiciones 
          de los patrocinadores, que <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_53" 
                    name="comLin_53" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_53'); ?>" /> con el paso del 
          tiempo o porque se ha cumplido con los propósitos establecidos por 
          dichos patrocinadores.
      </div>
      
      <div class="completar-lineas">
          <span class="num">13</span>
          Patrimonio <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_54" 
                    name="comLin_54" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_54'); ?>" /> restringido es 
          aquel que <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_55" 
                    name="comLin_55" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_55'); ?>" /> de todo 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_56" 
                    name="comLin_56" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_56'); ?>" /> de restricciones 
          por parte de los <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_57" 
                    name="comLin_57" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_57'); ?>" /> para que lo 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_58" 
                    name="comLin_58" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_58'); ?>" /> la entidad.
      </div>
      
      <div class="completar-lineas">
          <button onclick="calificarCompletar('comLin',58,rc5a_cl);">CALIFICAR</button>
          <span id="comLin_cal"></span>
      </div>
      
      
      <!-- ================= -->
      <h5>III. CONTESTA SI LO QUE SE DICE ES VERDADERO (V) O FALSO (F), EXPLICANDO EL POR QUÉ.</h5>
      <table class="verdadero-falso" id="vf_simple01">
          <tr>
              <th>Caso</th>
              <th>V</th>
              <th>F</th>
              <th>Explicación</th>
          </tr>
          <tr>
              <td>
                  <span class="num">1</span>
                  El pasivo es un derecho presente de la entidad, virtualmente 
                  ineludible, identificado, cuantificado en términos monetarios 
                  y que representa un incremento futuro de beneficios económicos, 
                  derivado de operaciones y otros eventos ocurridos en el pasado 
                  que han afectado económicamente a dicha entidad.
              </td>
              <td id="vf_simple01_verfal_1v_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_1" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_1')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple01_verfal_1f_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_1" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_1')==='falso')?' checked':'' ?> /></td>
              <td><textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalex_1" id="verfalex_1"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalex_1'); ?></textarea></td>
          </tr>
          <tr>
              <td>
                  <span class="num">2</span>
                  El pasivo a corto plazo es aquel cuyo vencimiento se producirá 
                  dentro de un año o en el ciclo normal de las operaciones, 
                  cuando éste sea mayor a un año. 
              </td>
              <td id="vf_simple01_verfal_2v_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_2" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_2')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple01_verfal_2f_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_2" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_2')==='falso')?' checked':'' ?> /></td>
              <td><textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalex_2" id="verfalex_2"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalex_2'); ?></textarea></td>
          </tr>
          <tr>
              <td>
                  <span class="num">3</span>
                  Los pasivos financieros que proporcionan financiamiento a 
                  largo plazo, es decir, no forman parte del capital de trabajo 
                  utilizado en el ciclo normal de operación de la entidad y no 
                  deben liquidarse dentro de los doce meses posteriores a la 
                  fecha de cierre del periodo sobre el que se informa, son 
                  pasivos a largo plazo. 
              </td>
              <td id="vf_simple01_verfal_3v_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_3" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_3')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple01_verfal_3f_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_3" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_3')==='falso')?' checked':'' ?> /></td>
              <td><textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalex_3" id="verfalex_3"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalex_3'); ?></textarea></td>
          </tr>
          <tr>
              <td>
                  <span class="num">4</span>
                  El capital contable es el valor de los activos de la entidad, 
                  sumados a todos sus pasivos. 
              </td>
              <td id="vf_simple01_verfal_4v_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_4" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_4')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple01_verfal_4f_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_4" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_4')==='falso')?' checked':'' ?> /></td>
              <td><textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalex_4" id="verfalex_4"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalex_4'); ?></textarea></td>
          </tr>
          <tr>
              <td>
                  <span class="num">5</span>
                  El concepto de capital contable se utiliza para las entidades 
                  lucrativas y capital ganado para las entidades no lucrativas. 
              </td>
              <td id="vf_simple01_verfal_5v_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_5" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_5')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple01_verfal_5f_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_5" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_5')==='falso')?' checked':'' ?> /></td>
              <td><textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalex_5" id="verfalex_5"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalex_5'); ?></textarea></td>
          </tr>
          <tr>
              <td>
                  <span class="num">6</span>
                  Las cuentas capital social, aportaciones para futuros aumentos 
                  de capital y prima enventa de acciones, son parte del capital 
                  contribuido. 
              </td>
              <td id="vf_simple01_verfal_6v_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_6" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_6')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple01_verfal_6f_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_6" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_6')==='falso')?' checked':'' ?> /></td>
              <td><textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalex_6" id="verfalex_6"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalex_6'); ?></textarea></td>
          </tr>
          <tr>
              <td>
                  <span class="num">7</span>
                  Los conceptos patrimonio restringido permanentemente, 
                  patrimonio restingido temporalmente y patrimonio no restringido, 
                  forman parte del capital ganado. 
              </td>
              <td id="vf_simple01_verfal_7v_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_7" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_7')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple01_verfal_7f_parcial"><input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_7" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_7')==='falso')?' checked':'' ?> /></td>
              <td><textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalex_7" id="verfalex_7"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalex_7'); ?></textarea></td>
          </tr>
          <tr>
              <td colspan="3" id="vf_simple01_verfal_cal"></td>
              <td>
                  <button onclick="calificarVerFalSimple('verfal',7,rc5a_verfalR,'vf_simple01');">CALIFICAR</button>
              </td>
          </tr>
      </table>
      
      <h5>IV. PREGUNTAS DE OPCIÓN MÚLTIPLE</h5>
      
      <table class="opcion-multiple">
          <tr>
              <th colspan="2">En los siguientes casos, señale cuál(es) 
                  cuenta(s) no pertenece(n) al grupo del pasivo.</th>
          </tr>
          <tr>
              <td>Proveedores</td>
              <td id="opmul_1_Proveedores_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                         id="a_opmul_1_1" 
                         type="checkbox" 
                         name="opmul_1" 
                         value="Proveedores" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_1')==='Proveedores')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Acreedores</td>
              <td id="opmul_1_Acreedores_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                    id="a_opmul_1_2" type="checkbox" name="opmul_1" 
                    value="Acreedores" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_2')==='Acreedores')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>IVA pendiente de acreditar</td>
              <td id="opmul_1_IVA pendiente de acreditar_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_3" type="checkbox" name="opmul_1" 
                      value="IVA pendiente de acreditar" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_3')==='IVA pendiente de acreditar')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Rentas pagadas por anticipado</td>
              <td id="opmul_1_Rentas pagadas por anticipado_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_4" type="checkbox" name="opmul_1" 
                      value="Rentas pagadas por anticipado" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_4')==='Rentas pagadas por anticipado')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Documentos por pagar</td>
              <td id="opmul_1_Documentos por pagar_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_5" type="checkbox" name="opmul_1" 
                      value="Documentos por pagar" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_5')==='Documentos por pagar')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Acreedores bancarios</td>
              <td id="opmul_1_Acreedores bancarios_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_6" type="checkbox" name="opmul_1" 
                      value="Acreedores bancarios" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_6')==='Acreedores bancarios')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>IVA causado</td>
              <td id="opmul_1_IVA causado_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_7" type="checkbox" name="opmul_1" 
                      value="IVA causado" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_7')==='IVA causado')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>ISR por pagar</td>
              <td id="opmul_1_ISR por pagar_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_8" type="checkbox" name="opmul_1" 
                      value="ISR por pagar" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_8')==='ISR por pagar')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Utilidades acumuladas</td>
              <td id="opmul_1_Utilidades acumuladas_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_9" type="checkbox" name="opmul_1" 
                      value="Utilidades acumuladas" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_9')==='Utilidades acumuladas')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>PTU por pagar</td>
              <td id="opmul_1_PTU por pagar_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_10" type="checkbox" name="opmul_1" 
                      value="PTU por pagar" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_10')==='PTU por pagar')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>IVA pendiente de causar</td>
              <td id="opmul_1_IVA pendiente de causar_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_11" type="checkbox" name="opmul_1" 
                      value="IVA pendiente de causar" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_11')==='IVA pendiente de causar')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Proveedores</td>
              <td id="opmul_1_Proveedores_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_12" type="checkbox" name="opmul_1" 
                      value="Proveedores" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_12')==='Proveedores')?' checked':'' ?>  />
              </td>
          </tr>
          <tr>
              <td>Pérdida neta del ejercicio</td>
              <td id="opmul_1_Pérdida neta del ejercicio_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_13" type="checkbox" name="opmul_1" 
                      value="Pérdida neta del ejercicio" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_13')==='Pérdida neta del ejercicio')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Acreedores bancarios a largo plazo</td>
              <td id="opmul_1_Acreedores bancarios a largo plazo_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_14" type="checkbox" name="opmul_1" 
                      value="Acreedores bancarios a largo plazo" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_14')==='Acreedores bancarios a largo plazo')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Primas de seguros y fianzas</td>
              <td id="opmul_1_Primas de seguros y fianzas_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_15" type="checkbox" name="opmul_1" 
                      value="Primas de seguros y fianzas" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_15')==='Primas de seguros y fianzas')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Cuentas por cobrar a largo plazo</td>
              <td id="opmul_1_Cuentas por cobrar a largo plazo_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_16" type="checkbox" name="opmul_1" 
                      value="Cuentas por cobrar a largo plazo" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_16')==='Cuentas por cobrar a largo plazo')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Rentas cobradas por anticipado</td>
              <td id="opmul_1_Rentas cobradas por anticipado_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_17" type="checkbox" name="opmul_1" 
                      value="Rentas cobradas por anticipado" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_17')==='Rentas cobradas por anticipado')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Documentos por pagar</td>
              <td id="opmul_1_Documentos por pagar_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_18" type="checkbox" name="opmul_1" 
                      value="Documentos por pagar" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_18')==='Documentos por pagar')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Documentos por pagar a largo plazo</td>
              <td id="opmul_1_Documentos por pagar a largo plazo_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_19" type="checkbox" name="opmul_1" 
                      value="Documentos por pagar a largo plazo" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_19')==='Documentos por pagar a largo plazo')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Impuestos y derechos por pagar</td>
              <td id="opmul_1_Impuestos y derechos por pagar_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="a_opmul_1_20" type="checkbox" name="opmul_1" 
                      value="Impuestos y derechos por pagar" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'a_opmul_1_20')==='Impuestos y derechos por pagar')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td id="opmul_1_cal"></td>
              <td>
                  <button onclick="calificarOpMul('opmul_1',rc5a_opcmulA);">CALIFICAR</button>
              </td>
          </tr>
      </table>
      
      <table class="opcion-multiple" style="margin-top: 25px;">
          <tr>
              <th colspan="2">En el siguiente listado, indique cuál(es) cuenta(s) 
                  no pertenece(n) al grupo del capital contable contribuido.</th>
          </tr>
          <tr>
              <td>Capital social</td>
              <td id="opmul_2_Capital social_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                         id="b_opmul_2_1" 
                         type="checkbox" 
                         name="opmul_2" 
                         value="Capital social" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'b_opmul_2_1')==='Capital social')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Patentes</td>
              <td id="opmul_2_Patentes_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                    id="b_opmul_2_2" type="checkbox" name="opmul_2" 
                    value="Patentes" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'b_opmul_2_2')==='Patentes')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Utilidad neta del ejercicio</td>
              <td id="opmul_2_Utilidad neta del ejercicio_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="b_opmul_2_3" type="checkbox" name="opmul_2" 
                      value="Utilidad neta del ejercicio" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'b_opmul_2_3')==='Utilidad neta del ejercicio')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Aportaciones para futuros aumentos de capital</td>
              <td id="opmul_2_Aportaciones para futuros aumentos de capital_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="b_opmul_2_4" type="checkbox" name="opmul_2" 
                      value="Aportaciones para futuros aumentos de capital" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'b_opmul_2_4')==='Aportaciones para futuros aumentos de capital')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Mobiliario y equipo de oficina</td>
              <td id="opmul_2_Mobiliario y equipo de oficina_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="b_opmul_2_5" type="checkbox" name="opmul_2" 
                      value="Mobiliario y equipo de oficina" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'b_opmul_2_5')==='Mobiliario y equipo de oficina')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Proveedores</td>
              <td id="opmul_2_Proveedores_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="b_opmul_2_6" type="checkbox" name="opmul_2" 
                      value="Proveedores" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'b_opmul_2_6')==='Proveedores')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Prima en venta de acciones</td>
              <td id="opmul_2_Prima en venta de acciones_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="b_opmul_2_7" type="checkbox" name="opmul_2" 
                      value="Prima en venta de acciones" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'b_opmul_2_7')==='Prima en venta de acciones')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Reserva legal</td>
              <td id="opmul_2_Reserva legal_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="b_opmul_2_8" type="checkbox" name="opmul_2" 
                      value="Reserva legal" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'b_opmul_2_8')==='Reserva legal')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Pérdidas acumuladas</td>
              <td id="opmul_2_Pérdidas acumuladas_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="b_opmul_2_9" type="checkbox" name="opmul_2" 
                      value="Pérdidas acumuladas" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'b_opmul_2_9')==='Pérdidas acumuladas')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Patrimonio restringido temporalmente</td>
              <td id="opmul_2_Patrimonio restringido temporalmente_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="b_opmul_2_10" type="checkbox" name="opmul_2" 
                      value="Patrimonio restringido temporalmente" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'b_opmul_2_10')==='Patrimonio restringido temporalmente')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td id="opmul_2_cal"></td>
              <td>
                  <button onclick="calificarOpMul('opmul_2',rc5a_opcmulB);">CALIFICAR</button>
              </td>
          </tr>
      </table>
      
      <table class="opcion-multiple" style="margin-top: 25px;">
          <tr>
              <th colspan="2">En el siguiente listado, indique cuál(es) cuenta(s) 
                  no pertenece(n) al grupo del capital contable ganado.</th>
          </tr>
          <tr>
              <td>Capital social</td>
              <td id="opmul_3_Capital social_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                         id="c_opmul_3_1" 
                         type="checkbox" 
                         name="opmul_3" 
                         value="Capital social" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'c_opmul_3_1')==='Capital social')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Utilidades acumuladas</td>
              <td id="opmul_3_Utilidades acumuladas_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                    id="c_opmul_3_2" type="checkbox" name="opmul_3" 
                    value="Utilidades acumuladas" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'c_opmul_3_2')==='Utilidades acumuladas')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Utilidad neta del ejercicio</td>
              <td id="opmul_3_Utilidad neta del ejercicio_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="c_opmul_3_3" type="checkbox" name="opmul_3" 
                      value="Utilidad neta del ejercicio" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'c_opmul_3_3')==='Utilidad neta del ejercicio')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Aportaciones para futuros aumentos de capital</td>
              <td id="opmul_3_Aportaciones para futuros aumentos de capital_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="c_opmul_3_4" type="checkbox" name="opmul_3" 
                      value="Aportaciones para futuros aumentos de capital" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'c_opmul_3_4')==='Aportaciones para futuros aumentos de capital')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Pérdida neta del ejercicio</td>
              <td id="opmul_3_Pérdida neta del ejercicio_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="c_opmul_3_5" type="checkbox" name="opmul_3" 
                      value="Pérdida neta del ejercicio" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'c_opmul_3_5')==='Pérdida neta del ejercicio')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>ISR por pagar</td>
              <td id="opmul_3_ISR por pagar_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="c_opmul_3_6" type="checkbox" name="opmul_3" 
                      value="ISR por pagar" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'c_opmul_3_6')==='ISR por pagar')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Prima en venta de acciones</td>
              <td id="opmul_3_Prima en venta de acciones_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="c_opmul_3_7" type="checkbox" name="opmul_3" 
                      value="Prima en venta de acciones" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'c_opmul_3_7')==='Prima en venta de acciones')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Reserva legal</td>
              <td id="opmul_3_Reserva legal_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="c_opmul_3_8" type="checkbox" name="opmul_3" 
                      value="Reserva legal" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'c_opmul_3_8')==='Reserva legal')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Pérdidas acumuladas</td>
              <td id="opmul_3_Pérdidas acumuladas_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="c_opmul_3_9" type="checkbox" name="opmul_3" 
                      value="Pérdidas acumuladas" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'c_opmul_3_9')==='Pérdidas acumuladas')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td>Patrimonio no restringido</td>
              <td id="opmul_3_Patrimonio no restringido_parcial">
                  <input onchange="mostrarEstado(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,this.id,this.value)" 
                      id="c_opmul_3_10" type="checkbox" name="opmul_3" 
                      value="Patrimonio no restringido" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'c_opmul_3_10')==='Patrimonio no restringido')?' checked':'' ?> />
              </td>
          </tr>
          <tr>
              <td id="opmul_3_cal"></td>
              <td>
                  <button onclick="calificarOpMul('opmul_3',rc5a_opcmulC);">CALIFICAR</button>
              </td>
          </tr>
      </table>
      
      
      
      <h5>V. A CONTINUACIÓN SE PRESENTAN VARIAS CUENTAS, SEÑALA SI 
          PERTENECEN AL PASIVO O AL CAPITAL.</h5>
      
      <table class="verdadero-falso" id="vf_simple03" style="margin: 25px 0 25px 0">
          <tr>
              <th>Cuentas</th>
              <th>Pasivo</th>
              <th>Capital</th>
          </tr>
          <tr>
              <td>Proveedores</td>
              <td id="vf_simple03_verfal_1v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_1" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_1')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_1f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_1" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_1')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Reserva legal</td>
              <td id="vf_simple03_verfal_2v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_2" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_2')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_2f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_2" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_2')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Impuestos y derechos retenidos por enterar</td>
              <td id="vf_simple03_verfal_3v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_3" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_3')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_3f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_3" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_3')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Documentos por pagar a largo plazo</td>
              <td id="vf_simple03_verfal_4v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_4" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_4')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_4f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_4" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_4')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Capital social</td>
              <td id="vf_simple03_verfal_5v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_5" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_5')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_5f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_5" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_5')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Acreedores hipotecarios</td>
              <td id="vf_simple03_verfal_6v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_6" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_6')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_6f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_6" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_6')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Aportaciones para futuros aumentos de capital</td>
              <td id="vf_simple03_verfal_7v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_7" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_7')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_7f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_7" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_7')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Utilidad neta del ejercicio</td>
              <td id="vf_simple03_verfal_8v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_8" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_8')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_8f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_8" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_8')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Prima en venta de acciones</td>
              <td id="vf_simple03_verfal_9v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_9" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_9')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_9f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_9" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_9')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Acreedores</td>
              <td id="vf_simple03_verfal_10v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_10" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_10')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_10f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_10" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_10')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>PTU por pagar</td>
              <td id="vf_simple03_verfal_11v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_11" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_11')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_11f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_11" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_11')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Acreedores bancarios</td>
              <td id="vf_simple03_verfal_12v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_12" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_12')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_12f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_12" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_12')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Utilidades acumuladas</td>
              <td id="vf_simple03_verfal_13v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_13" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_13')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_13f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_13" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_13')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Pérdida neta del ejercicio</td>
              <td id="vf_simple03_verfal_14v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_14" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_14')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_14f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_14" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_14')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td>Documentos por pagar</td>
              <td id="vf_simple03_verfal_15v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_15" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_15')==='verdadero')?' checked':'' ?> /></td>
              <td id="vf_simple03_verfal_15f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple03_verfal_15" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple03_verfal_15')==='falso')?' checked':'' ?> /></td>
          </tr>
          <tr>
              <td colspan="2" id="vf_simple03_verfal_cal"></td>
              <td>
                  <button onclick="calificarVerFalSimple('verfal',15,rc5a_simpleA,'vf_simple03');">CALIFICAR</button>
              </td>
          </tr>
      </table>
      
      <h5>VI. RESUELVA LO SIGUIENTE</h5>
      <p style="font-size: 12px">En los siguientes casos, clasifique y ordene las cuentas de los listados, 
          en los diferentes conceptos del activo, pasivo y capital contable, 
          de acuerdo a su disponibilidad, exigibilidad o el criterio aplicable, 
          según corresponda; asimismo, agrúpelas dentro del activo circulante, 
          o no circulante, en este caso, en propiedades, planta y equipo, 
          intangibles y otros activos; en el caso del pasivo, clasifíquelos en 
          a corto y a largo plazo, y en el del capital, dentro del contribuido 
          y del ganado.</p>
      <p style="font-size: 12px"><strong>Notas:</strong> Utilice únicamente letras minúsculas, incluso en los casos como 
          el iva, isr, ptu. </p>
      <p style="font-size: 12px">Anote completo el nombre de las cuentas, tal y como aparecen en los 
          listados, incluyendo expresiones como las siguientes: rentas cobradas 
          por anticipado a devengarse en el ejercicio; primas de seguros a 
          devengarse en 24 meses.</p>
      <p style="font-size: 12px">Empiece utilizando el primer renglón de cada grupo.</p>
      
      <p class="eje-vi-p"><span class="num">1</span> Acreedores a 18 meses, clientes, 
          utilidad neta del ejercicio, proveedores, caja, documentos por cobrar 
          a 24 meses, isr por pagar, ptu por pagar, iva acreditable, bancos, 
          papelería a consumirse en el presente ejercicio, terrenos, utilidades 
          acumuladas, reserva legal, mercancías, rentas cobradas por anticipado 
          a devengarse en el ejercicio, equipo de reparto, instrumentos 
          financieros, primas de seguros a devengarse en 24 meses, primas de 
          seguro a devengarse en el ejercicio.</p>
      
      <table class="eje-vi completar-lineas">
          <tr>
              <td>ACTIVO</td>
              <td>PASIVO</td>
              <td>CAPITAL CONTABLE</td>
          </tr>
          <tr>
              <th>Circulante</th>
              <th>A corto plazo</th>
              <th>Capital contribuido</th>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi1_1" 
                    name="ejevi1_1" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi1_1'); ?>" /></td>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi1_13" 
                    name="ejevi1_13" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi1_13'); ?>" /></td>
              <td></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi1_2" 
                    name="ejevi1_2" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi1_2'); ?>" /></td>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi1_14" 
                    name="ejevi1_14" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi1_14'); ?>" /></td>
              <td></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi1_3" 
                    name="ejevi1_3" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi1_3'); ?>" /></td>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi1_15" 
                    name="ejevi1_15" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi1_15'); ?>" /></td>
              <td></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi1_4" 
                    name="ejevi1_4" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi1_4'); ?>" /></td>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi1_16" 
                    name="ejevi1_16" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi1_16'); ?>" /></td>
              <td></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi1_5" 
                    name="ejevi1_5" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi1_5'); ?>" /></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi1_6" 
                    name="ejevi1_6" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi1_6'); ?>" /></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi1_7" 
                    name="ejevi1_7" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi1_7'); ?>" /></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi1_8" 
                    name="ejevi1_8" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi1_8'); ?>" /></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <th>No circulante</th>
              <th>A largo plazo</th>
              <th>Capital ganado</th>
          </tr>
          <tr>
              <th>Propiedades, planta y equipo</th>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi1_17" 
                    name="ejevi1_17" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi1_17'); ?>" /></td>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi1_18" 
                    name="ejevi1_18" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi1_18'); ?>" /></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi1_9" 
                    name="ejevi1_9" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi1_9'); ?>" /></td>
              <td></td>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi1_19" 
                    name="ejevi1_19" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi1_19'); ?>" /></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi1_10" 
                    name="ejevi1_10" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi1_10'); ?>" /></td>
              <td></td>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi1_20" 
                    name="ejevi1_20" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi1_20'); ?>" /></td>
          </tr>
          <tr>
              <th>Intangibles</th>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <th>Otros</th>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi1_11" 
                    name="ejevi1_11" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi1_11'); ?>" /></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi1_12" 
                    name="ejevi1_12" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi1_12'); ?>" /></td>
              <td></td>
              <td></td>
          </tr>
      </table>
      <!-- 
      <div class="completar-lineas">
          <button onclick="calificarCompletar('ejevi1',20,rc5a_vi1);">CALIFICAR</button>
          <span id="ejevi1_cal"></span>
      </div>
      -->
      
      <p class="eje-vi-p"><span class="num">2</span>Derechos de autor, 
          impuestos y derechos por pagar, impuestos y derechos retenidos por 
          enterar, iva causado, patentes y marcas, acreedores bancarios con 
          vencimiento en el ejercicio, acreedores hipotecarios con vencimiento 
          a cinco años, capital social, deudores, mobiliario y equipo, 
          acreedores bancarios con vencimiento a tres años, fondo fijo de caja 
          chica, aportaciones para futuros aumentos de capital, pérdida neta 
          del ejercicio, proveedores, inventarios.</p>
      
      <table class="eje-vi completar-lineas">
          <tr>
              <td>Activo</td>
              <td>Pasivo</td>
              <td>Capital contable</td>
          </tr>
          <tr>
              <th>Circulante</th>
              <th>A corto plazo</th>
              <th>Capital contribuido</th>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi2_1" 
                    name="ejevi2_1" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi2_1'); ?>" /></td>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi2_7" 
                    name="ejevi2_7" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi2_7'); ?>" /></td>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi2_14" 
                    name="ejevi2_14" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi2_14'); ?>" /></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi2_2" 
                    name="ejevi2_2" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi2_2'); ?>" /></td>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi2_8" 
                    name="ejevi2_8" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi2_8'); ?>" /></td>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi2_15" 
                    name="ejevi2_15" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi2_15'); ?>" /></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi2_3" 
                    name="ejevi2_3" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi2_3'); ?>" /></td>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi2_9" 
                    name="ejevi2_9" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi2_9'); ?>" /></td>
              <td></td>
          </tr>
          <tr>
              <td></td>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi2_10" 
                    name="ejevi2_10" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi2_10'); ?>" /></td>
              <td></td>
          </tr>
          <tr>
              <td></td>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi2_11" 
                    name="ejevi2_11" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi2_11'); ?>" /></td>
              <td></td>
          </tr>
          <tr>
              <th>No circulante</th>
              <th>A largo plazo</th>
              <th>Capital ganado</th>
          </tr>
          <tr>
              <th>Propiedades planta y equipo</th>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi2_12" 
                    name="ejevi2_12" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi2_12'); ?>" /></td>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi2_16" 
                    name="ejevi2_16" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi2_16'); ?>" /></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi2_4" 
                    name="ejevi2_4" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi2_4'); ?>" /></td>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi2_13" 
                    name="ejevi2_13" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi2_13'); ?>" /></td>
              <td></td>
          </tr>
          <tr>
              <th>Intangibles</th>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi2_5" 
                    name="ejevi2_5" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi2_5'); ?>" /></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi2_6" 
                    name="ejevi2_6" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi2_6'); ?>" /></td>
              <td></td>
              <td></td>
          </tr>
      </table>
      
      <!--
      <div class="completar-lineas">
          <button onclick="calificarCompletar('ejevi2',16,rc5a_vi2);">CALIFICAR</button>
          <span id="ejevi2_cal"></span>
      </div>
      -->
      
      <p class="eje-vi-p"><span class="num">3</span>Documentos por pagar, 
          documentos por cobrar a 20 meses, clientes, proveedores, acreedores, 
          deudores, edificios, acreedores hipotecarios a cinco años, rentas 
          cobradas por anticipado a devengarse en 16 meses, terrenos, crédito 
          mercantil, bancos, acreedores bancarios a tres años, acreedores 
          bancarios a un año, caja, fondo fijo de caja chica, inversiones 
          temporales a nueve meses, documentos por cobrar a seis meses, iva 
          acreditable, iva causado, papelería a devengarse en once meses, 
          propaganda a devengarse en el ejercicio, capital social, reserva 
          legal, utilidad neta del ejercicio, derechos de autor, rentas 
          pagadas por anticipado a devengarse en el ejercicio, gastos de 
          instalación, gastos de constitución, mercancías, mobiliario y 
          equipo de oficina, equipo de transporte, isr por pagar, ptu por 
          pagar.</p>
      
      <table class="eje-vi completar-lineas">
          <tr>
              <td>Activo</td>
              <td>Pasivo</td>
              <td>Capital contable</td>
          </tr>
          <tr>
              <th>Circulante</th>
              <th>A corto plazo</th>
              <th>Capital contribuido</th>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_1" 
                    name="ejevi3_1" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_1'); ?>" /></td>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_21" 
                    name="ejevi3_21" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_21'); ?>" /></td>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_31" 
                    name="ejevi3_31" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_31'); ?>" /></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_2" 
                    name="ejevi3_2" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_2'); ?>" /></td>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_22" 
                    name="ejevi3_22" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_22'); ?>" /></td>
              <td></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_3" 
                    name="ejevi3_3" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_3'); ?>" /></td>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_23" 
                    name="ejevi3_23" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_23'); ?>" /></td>
              <td></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_4" 
                    name="ejevi3_4" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_4'); ?>" /></td>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_24" 
                    name="ejevi3_24" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_24'); ?>" /></td>
              <td></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_5" 
                    name="ejevi3_5" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_5'); ?>" /></td>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_25" 
                    name="ejevi3_25" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_25'); ?>" /></td>
              <td></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_6" 
                    name="ejevi3_6" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_6'); ?>" /></td>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_26" 
                    name="ejevi3_26" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_26'); ?>" /></td>
              <td></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_7" 
                    name="ejevi3_7" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_7'); ?>" /></td>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_27" 
                    name="ejevi3_27" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_27'); ?>" /></td>
              <td></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_8" 
                    name="ejevi3_8" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_8'); ?>" /></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_9" 
                    name="ejevi3_9" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_9'); ?>" /></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_10" 
                    name="ejevi3_10" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_10'); ?>" /></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_11" 
                    name="ejevi3_11" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_11'); ?>" /></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <th>No circulante</th>
              <th>A largo plazo</th>
              <th>Capital ganado</th>
          </tr>
          <tr>
              <th>Propiedades, planta y equipo</th>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_28" 
                    name="ejevi3_28" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_28'); ?>" /></td>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_32" 
                    name="ejevi3_32" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_32'); ?>" /></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_12" 
                    name="ejevi3_12" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_12'); ?>" /></td>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_29" 
                    name="ejevi3_29" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_29'); ?>" /></td>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_33" 
                    name="ejevi3_33" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_33'); ?>" /></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_13" 
                    name="ejevi3_13" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_13'); ?>" /></td>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_30" 
                    name="ejevi3_30" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_30'); ?>" /></td>
              <td></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_14" 
                    name="ejevi3_14" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_14'); ?>" /></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_15" 
                    name="ejevi3_15" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_15'); ?>" /></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <th>Intangibles</th>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_16" 
                    name="ejevi3_16" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_16'); ?>" /></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_17" 
                    name="ejevi3_17" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_17'); ?>" /></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_18" 
                    name="ejevi3_18" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_18'); ?>" /></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_19" 
                    name="ejevi3_19" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_19'); ?>" /></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <th>Otros</th>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <td><input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="ejevi3_20" 
                    name="ejevi3_20" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ejevi3_20'); ?>" /></td>
              <td></td>
              <td></td>
          </tr>
      </table>
      
      <!--
      <div class="completar-lineas">
          <button onclick="calificarCompletar('ejevi3',33,rc5a_vi3);">CALIFICAR</button>
          <span id="ejevi3_cal"></span>
      </div>
      -->
      
      
      <h5>VII. RESUELVA LO SIGUIENTE</h5>
      
      <p style="font-size: 12px;">En los siguientes ejercicios, en primer lugar agrupe los conceptos que 
          se indican dentro del activo, pasivo o capital contable, según 
          corresponda, y después determine el importe correcto del activo, 
          pasivo y capital contable, y muéstrelos en la ecuación de la dualidad 
          económica, A = P + C, según el ejemplo siguiente:</p>
      
      <p class="eje-vii-p">
          Activo circulante $1 233 000; pasivo a corto plazo $100 000; pasivo a 
          largo plazo $20 000; capital contribuido $570 000; capital 
          ganado $543 000.
      </p>
      
      <table class="eje-vii">
          <tr>
              <th colspan="2">Activo</th>
              <th>=</th>
              <th colspan="2">Pasivo</th>
              <th>+</th>
              <th colspan="2">Capital contable</th>
          </tr>
          <tr>
              <td>Activo circulante</td>
              <td>$<input type="text" name="acM" value="1233000" disabled /></td>
              <td>=</td>
              <td>Pasivo a corto plazo</td>
              <td>$<input type="text" name="pcpM" value="100000" disabled /></td>
              <td>+</td>
              <td>Capital contribuido</td>
              <td>$<input type="text" name="cpM" value="570000" disabled /></td>
          </tr>
          <tr>
              <td></td>
              <td></td>
              <td></td>
              <td>Pasivo a largo plazo</td>
              <td>$<input type="text" name="plpM" value="20000" disabled /></td>
              <td>+</td>
              <td>Capital ganado</td>
              <td>$<input type="text" name="cpM" value="543000" disabled /></td>
          </tr>
          <tr>
              <td></td>
              <td>$<input type="text" name="taM" value="1233000" disabled /></td>
              <td>=</td>
              <td></td>
              <td>$<input type="text" name="tpM" value="120000" disabled /></td>
              <td>+</td>
              <td></td>
              <td>$<input type="text" name="tccM" value="1113000" disabled /></td>
          </tr>
      </table>
      
      <p class="eje-vii-p"><span class="num">1</span>Capital contribuido, 
          $450 000; activo circulante, $600 000; pasivo a corto plazo, $150 000.</p>
      <table class="eje-vii">
          <tr>
              <th colspan="2">Activo</th>
              <th>=</th>
              <th colspan="2">Pasivo</th>
              <th>+</th>
              <th colspan="2">Capital contable</th>
          </tr>
          <tr>
              <td>Activo circulante</td>
              <td>$ <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    id="ac_1" 
                    name="ac_1" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ac_1'); ?>" /></td>
              <td>=</td>
              <td>Pasivo a corto plazo</td>
              <td>$ <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    id="pcp_1" 
                    name="pcp_1" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'pcp_1'); ?>" /></td>
              <td>+</td>
              <td>Capital contribuido</td>
              <td>$ <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    id="cc_1" 
                    name="cc_1" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'cc_1'); ?>" /></td>
          </tr>
          <tr>
              <th>Activo</th>
              <td>$ <input type="text" name="ta1" id="ta1" value="" disabled /></td>
              <td></td>
              <th>Pasivo</th>
              <td>$ <input type="text" name="tp1" id="tp1" value="" disabled /></td>
              <td>+</td>
              <th>Capital contable</th>
              <td>$ <input type="text" name="tcc1" id="tcc1" value="" disabled /></td>
          </tr>
          <tr>
              <td></td>
              <td id="parcial_tta1">$ <input type="text" name="tta1" id="tta1" value="" disabled /></td>
              <td>=</td>
              <td id="parcial_ttpcc1">$ <input type="text" name="ttpcc1" id="ttpcc1" value="" disabled /></td>
              <td colspan="4"></td>
          </tr>
          <tr>
              <td colspan="6" id="parcial_suma1"></td>
              <td colspan="2"><button type="button" onclick="sumaA(1)">CALIFICAR</button></td>
          </tr>
      </table>
      
      <p class="eje-vii-p"><span class="num">2</span>Capital contribuido, 
          $250 000; activo circulante, $150 000; pasivo a corto plazo, $100 000; 
          pasivo a largo plazo, $90 000; capital ganado, $100 000; activo no 
          circulante, $390 000.</p>
      <table class="eje-vii">
          <tr>
              <th colspan="2">Activo</th>
              <th>=</th>
              <th colspan="2">Pasivo</th>
              <th>+</th>
              <th colspan="2">Capital contable</th>
          </tr>
          <tr>
              <td>Activo circulante</td>
              <td>$ <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    id="ac_2" 
                    name="ac_2" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ac_2'); ?>" /></td>
              <td>=</td>
              <td>Pasivo a corto plazo</td>
              <td>$ <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    id="pcp_2" 
                    name="pcp_2" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'pcp_2'); ?>" /></td>
              <td>+</td>
              <td>Capital contribuido</td>
              <td>$ <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    id="cc_2" 
                    name="cc_2" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'cc_2'); ?>" /></td>
          </tr>
          <tr>
              <td>Activo no circulante</td>
              <td>$ <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    id="anc_2" 
                    name="anc_2" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'anc_2'); ?>" /></td>
              <td>=</td>
              <td>Pasivo a largo plazo</td>
              <td>$ <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    id="plp_2" 
                    name="plp_2" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'plp_2'); ?>" /></td>
              <td>+</td>
              <td>Capital ganado</td>
              <td>$ <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    id="cg_2" 
                    name="cg_2" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'cg_2'); ?>" /></td>
          </tr>
          <tr>
              <th>Activo</th>
              <td>$ <input type="text" name="ta2" id="ta2" value="" disabled /></td>
              <td></td>
              <th>Pasivo</th>
              <td>$ <input type="text" name="tp2" id="tp2" value="" disabled /></td>
              <td>+</td>
              <th>Capital contable</th>
              <td>$ <input type="text" name="tcc2" id="tcc2" value="" disabled /></td>
          </tr>
          <tr>
              <td></td>
              <td id="parcial_tta2">$ <input type="text" name="tta2" id="tta2" value="" disabled /></td>
              <td>=</td>
              <td id="parcial_ttpcc2">$ <input type="text" name="ttpcc2" id="ttpcc2" value="" disabled /></td>
              <td colspan="4"></td>
          </tr>
          <tr>
              <td colspan="6" id="parcial_suma2"></td>
              <td colspan="2"><button type="button" onclick="sumaB(2)">CALIFICAR</button></td>
          </tr>
      </table>
      
      <p class="eje-vii-p"><span class="num">3</span>Activo circulante, 
          $110 000; pasivo a corto plazo, $50 000; activo no circulante, 
          $90 000; capital contribuido, $170 000; déficit, $20 000.</p>
      <table class="eje-vii">
          <tr>
              <th colspan="2">Activo</th>
              <th>=</th>
              <th colspan="2">Pasivo</th>
              <th>+</th>
              <th colspan="2">Capital contable</th>
          </tr>
          <tr>
              <td>Activo circulante</td>
              <td>$ <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    id="ac_3" 
                    name="ac_3" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ac_3'); ?>" /></td>
              <td>=</td>
              <td>Pasivo a corto plazo</td>
              <td>$ <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    id="pcp_3" 
                    name="pcp_3" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'pcp_3'); ?>" /></td>
              <td>+</td>
              <td>Capital contribuido</td>
              <td>$ <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    id="cc_3" 
                    name="cc_3" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'cc_3'); ?>" /></td>
          </tr>
          <tr>
              <td>Activo no circulante</td>
              <td>$ <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    id="anc_3" 
                    name="anc_3" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'anc_3'); ?>" /></td>
              <td>=</td>
              <td>Pasivo a largo plazo</td>
              <td></td>
              <td>+</td>
              <td>Capital ganado</td>
              <td>$ <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    id="cg_3" 
                    name="cg_3" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'cg_3'); ?>" /></td>
          </tr>
          <tr>
              <th>Activo</th>
              <td>$ <input type="text" name="ta3" id="ta3" value="" disabled /></td>
              <td></td>
              <th>Pasivo</th>
              <td>$ <input type="text" name="tp3" id="tp3" value="" disabled /></td>
              <td>+</td>
              <th>Capital contable</th>
              <td>$ <input type="text" name="tcc3" id="tcc3" value="" disabled /></td>
          </tr>
          <tr>
              <td></td>
              <td id="parcial_tta3">$ <input type="text" name="tta3" id="tta3" value="" disabled /></td>
              <td>=</td>
              <td id="parcial_ttpcc3">$ <input type="text" name="ttpcc3" id="ttpcc3" value="" disabled /></td>
              <td colspan="4"></td>
          </tr>
          <tr>
              <td colspan="6" id="parcial_suma3"></td>
              <td colspan="2"><button type="button" onclick="sumaC(3)">CALIFICAR</button></td>
          </tr>
      </table>
      
      <p class="eje-vii-p"><span class="num">4</span>Activo no circulante 
          $123 000; pasivo a corto plazo, $100 000; pasivo a largo plazo, 
          $20 000; capital contribuido, $70 000; capital ganado, $43 000; 
          activo circulante, $110 000.</p>
      <table class="eje-vii">
          <tr>
              <th colspan="2">Activo</th>
              <th>=</th>
              <th colspan="2">Pasivo</th>
              <th>+</th>
              <th colspan="2">Capital contable</th>
          </tr>
          <tr>
              <td>Activo circulante</td>
              <td>$ <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    id="ac_4" 
                    name="ac_4" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ac_4'); ?>" /></td>
              <td>=</td>
              <td>Pasivo a corto plazo</td>
              <td>$ <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    id="pcp_4" 
                    name="pcp_4" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'pcp_4'); ?>" /></td>
              <td>+</td>
              <td>Capital contribuido</td>
              <td>$ <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    id="cc_4" 
                    name="cc_4" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'cc_4'); ?>" /></td>
          </tr>
          <tr>
              <td>Activo no circulante</td>
              <td>$ <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    id="anc_4" 
                    name="anc_4" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'anc_4'); ?>" /></td>
              <td>=</td>
              <td>Pasivo a largo plazo</td>
              <td>$ <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    id="plp_4" 
                    name="plp_4" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'plp_4'); ?>" /></td>
              <td>+</td>
              <td>Capital ganado</td>
              <td>$ <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    id="cg_4" 
                    name="cg_4" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'cg_4'); ?>" /></td>
          </tr>
          <tr>
              <th>Activo</th>
              <td>$ <input type="text" name="ta4" id="ta4" value="" disabled /></td>
              <td></td>
              <th>Pasivo</th>
              <td>$ <input type="text" name="tp4" id="tp4" value="" disabled /></td>
              <td>+</td>
              <th>Capital contable</th>
              <td>$ <input type="text" name="tcc4" id="tcc4" value="" disabled /></td>
          </tr>
          <tr>
              <td></td>
              <td id="parcial_tta4">$ <input type="text" name="tta4" id="tta4" value="" disabled /></td>
              <td>=</td>
              <td id="parcial_ttpcc4">$ <input type="text" name="ttpcc4" id="ttpcc4" value="" disabled /></td>
              <td colspan="4"></td>
          </tr>
          <tr>
              <td colspan="6" id="parcial_suma4"></td>
              <td colspan="2"><button type="button" onclick="sumaB(4)">CALIFICAR</button></td>
          </tr>
      </table>
      
      <p class="eje-vii-p"><span class="num">5</span>Capital contribuido, 
          $500 000; activo circulante, $600 000; pasivo a corto plazo, $150 000; 
          pasivo a largo plazo, $75 000; capital ganado, $200 000; activo no 
          circulante, $325 000. </p>
      <table class="eje-vii">
          <tr>
              <th colspan="2">Activo</th>
              <th>=</th>
              <th colspan="2">Pasivo</th>
              <th>+</th>
              <th colspan="2">Capital contable</th>
          </tr>
          <tr>
              <td>Activo circulante</td>
              <td>$ <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    id="ac_5" 
                    name="ac_5" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'ac_5'); ?>" /></td>
              <td>=</td>
              <td>Pasivo a corto plazo</td>
              <td>$ <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    id="pcp_5" 
                    name="pcp_5" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'pcp_5'); ?>" /></td>
              <td>+</td>
              <td>Capital contribuido</td>
              <td>$ <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    id="cc_5" 
                    name="cc_5" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'cc_5'); ?>" /></td>
          </tr>
          <tr>
              <td>Activo no circulante</td>
              <td>$ <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    id="anc_5" 
                    name="anc_5" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'anc_5'); ?>" /></td>
              <td>=</td>
              <td>Pasivo a largo plazo</td>
              <td>$ <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    id="plp_5" 
                    name="plp_5" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'plp_5'); ?>" /></td>
              <td>+</td>
              <td>Capital ganado</td>
              <td>$ <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    id="cg_5" 
                    name="cg_5" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'cg_5'); ?>" /></td>
          </tr>
          <tr>
              <th>Activo</th>
              <td>$ <input type="text" name="ta5" id="ta5" value="" disabled /></td>
              <td></td>
              <th>Pasivo</th>
              <td>$ <input type="text" name="tp5" id="tp5" value="" disabled /></td>
              <td>+</td>
              <th>Capital contable</th>
              <td>$ <input type="text" name="tcc5" id="tcc5" value="" disabled /></td>
          </tr>
          <tr>
              <td></td>
              <td id="parcial_tta5">$ <input type="text" name="tta5" id="tta5" value="" disabled /></td>
              <td>=</td>
              <td id="parcial_ttpcc5">$ <input type="text" name="ttpcc5" id="ttpcc5" value="" disabled /></td>
              <td colspan="4"></td>
          </tr>
          <tr>
              <td colspan="6" id="parcial_suma5"></td>
              <td colspan="2"><button type="button" onclick="sumaB(5)">CALIFICAR</button></td>
          </tr>
      </table>
      
      
      <h5>VIII. PREGUNTAS DE OPCIÓN MÚLTIPLE</h5>
      
      <table class='correlaciones' style="width: 600px;">
        <caption>En los siguientes ejercicios, determine el importe correcto 
            del grupo que se indica para obtener la igualdad de la ecuación, 
            señalando en el espacio correspondiente la opción a), b), c), 
            según sea el caso, así como su importe.</caption>
          <tr>
              <td colspan="2">
                  <span class="num">1</span>
                  <p>Inmuebles, maquinaria y equipo:</p> 
                  
                  <p>Activo circulante, $240 000; pasivo a corto plazo, $250 000; 
                  capital contribuido, $600 000; pasivo a largo plazo, $150 000; 
                  intangibles, $250 000; déficit, $100 000.</p>
                  
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_1' id='corr1_1'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>$265 000</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>$280 000</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>$410 000</option>
                  </select> 
                  
                  <span id='corr1_1-parcial'></span>
              </td>
          </tr>
          <tr>
              <td colspan="2">
                  <span class="num">2</span>
                  <p>Pasivo a corto plazo:</p>
                  
                  <p>Capital contribuido, $150 000; capital ganado, $50 000; 
                      intangibles, $50 000; activo circulante, $160 000; 
                      inmuebles, maquinaria y equipo, $140 000.</p>
                  
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_2' id='corr1_2'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>$250 000</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>$150 000</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>$160 000</option>
                  </select>
                  
                  <span id='corr1_2-parcial'></span>
              </td>
          </tr>
          <tr>
              <td colspan="2">
                  <span class="num">3</span>
                  <p>Capital ganado:</p>
                  
                  <p>Inmuebles, maquinaria y equipo, $500 000; pasivo a corto 
                      plazo, $200 000; capital contribuido, $80 000.</p>
                  
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_3' id='corr1_3'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>$200 000</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>$210 000</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>$220 000</option>
                  </select>
                  
                  <span id='corr1_3-parcial'></span>
              </td>
          </tr>
          <tr>
              <td colspan="2">
              <span class="num">4</span>
                  <p>Déficit:</p>
                  
                  <p>Inmuebles, maquinaria y equipo, $500 000; pasivo a corto 
                      plazo, $200 000; capital contribuido, $340 000.</p>
                  
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_4' id='corr1_4'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>$40 000</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>$50 000</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>$140 000</option>
                  </select>
                  
                  <span id='corr1_4-parcial'></span>
              </td>
          </tr>
          <tr>
              <td colspan="2">
                  <span class="num">5</span>
                  <p>Activo circulante:</p>
                  
                  <p>Inmuebles, maquinaria y equipo, $420 000; intangibles, 
                      $300 000; otros activos, $66 000; pasivo circulante, 
                      $200 000; pasivo no circulante, $300 000; capital 
                      contribuido, $250 000; capital ganado, $126 000.</p>
                  
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_5' id='corr1_5'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>$90 000</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>$96 000</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>$86 000</option>
                  </select>
                  
                  <span id='corr1_5-parcial'></span>
              </td>
          </tr>
          
          <tr>
              <td id='corr1_cal'></td>
              <td><button onclick="calificarCorrelacion('corr1',5,rc5a_corr1);" 
                          name='cal_corr1'>CALIFICAR</button></td>
          </tr>
      </table>
      
  </div>
  <!-- -->
  <div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
<script>
    var tc;
    var cCamCues = function(campo,valor,registro)
    {
      clearTimeout(tc);
      tc = window.setTimeout(function(){
          $.post('libreria/cCamCues.php',{
              modDatos: true,
              elcampo: campo,
              elvalor: valor,
              elregistro: registro
          },function(respuesta){
              if(respuesta === 'modificado')
              {
                  // Modificado
              } else {
                  // Error
                  console.log('-> '+respuesta);
              }
          });
      },1000);
    };
    var cResCues = function(cues,usu,cap,preg,resp)
    {
      clearTimeout(tc);
      tc = window.setTimeout(function(){
          $.post('libreria/cCamCues.php',{
              modRespuestas: true,
              cuestionario: cues,
              usuario: usu,
              capitulo: cap,
              pregunta: preg,
              respuesta:resp
          },function(respuesta){
              if(respuesta === 'modificado')
              {
                  // Modificado
              } else {
                  // Error
                  console.log('-> '+respuesta);
              }
          });
      },500);
    };
    
    var delResCues = function(cues,usu,cap,preg,resp)
    {
      clearTimeout(tc);
      tc = window.setTimeout(function(){
          $.post('libreria/cCamCues.php',{
              delRespuesta: true,
              cuestionario: cues,
              usuario: usu,
              capitulo: cap,
              pregunta: preg,
              respuesta:resp
          },function(respuesta){
              if(respuesta === 'eliminado')
              {
                  // Eliminado
              } else {
                  // Error
                  console.log('-> '+respuesta);
              }
          });
      },500);
    };
    
    var mostrarEstado = function(cues,usu,cap,id,valor)
    {
      var elemento = document.getElementById(id);
      var estado = elemento.checked;
      if(estado)
      {
          cResCues(cues,usu,cap,id,valor);
      } else {
          delResCues(cues,usu,cap,id,valor);
      }
    };
    
    var protegerSumas = function()
    {
      var campos = document.querySelectorAll('.eje-vii input');
      var numcampos = campos.length;
      for(var i = 0; i<numcampos; i++)
      {
          if(campos[i].value !== '')
          {
              campos[i].disabled = true;
          }
      }
    };
    
    var sumaA = function(indice)
    {
        if(confirm('¿Está seguro de contestar? Una vez contestado el ejercicio no podrá modificar las respuestas'))
        {
        var ac = parseFloat(document.getElementById('ac_'+indice).value);
        var pcp = parseFloat(document.getElementById('pcp_'+indice).value);
        var cc = parseFloat(document.getElementById('cc_'+indice).value);
        
        var ta = ac;
        var tp = pcp;
        var tcc = cc;
        
        var tta = ta;
        var ttpcc = tp + tcc;
        
        document.getElementById('ta'+indice).value = ta;
        document.getElementById('tp'+indice).value = tp;
        document.getElementById('tcc'+indice).value = tcc;
        document.getElementById('tta'+indice).value = tta;
        document.getElementById('ttpcc'+indice).value = ttpcc;
        
        if(tta === ttpcc)
        {
            document.getElementById('parcial_tta'+indice).className = 'correcto';
            document.getElementById('parcial_ttpcc'+indice).className = 'correcto';
            document.getElementById('parcial_suma'+indice).className = 'calificacion';
            document.getElementById('parcial_suma'+indice).innerHTML = "¡Bien hecho!";
        } else {
            document.getElementById('parcial_tta'+indice).className = 'incorrecto';
            document.getElementById('parcial_ttpcc'+indice).className = 'incorrecto';
            document.getElementById('parcial_suma'+indice).className = 'calificacion';
            document.getElementById('parcial_suma'+indice).innerHTML = "¡Tienes un error!";
        }
        
        protegerSumas();
    }
        
    };
    
    var sumaB = function(indice)
    {
        if(confirm('¿Está seguro de contestar? Una vez contestado el ejercicio no podrá modificar las respuestas'))
        {
        var ac = parseFloat(document.getElementById('ac_'+indice).value);
        var pcp = parseFloat(document.getElementById('pcp_'+indice).value);
        var cc = parseFloat(document.getElementById('cc_'+indice).value);
        var anc = parseFloat(document.getElementById('anc_'+indice).value);
        var plp = parseFloat(document.getElementById('plp_'+indice).value);
        var cg = parseFloat(document.getElementById('cg_'+indice).value);
        
        var ta = ac + anc;
        var tp = pcp + plp;
        var tcc = cc + cg;
        
        var tta = ta;
        var ttpcc = tp + tcc;
        
        document.getElementById('ta'+indice).value = ta;
        document.getElementById('tp'+indice).value = tp;
        document.getElementById('tcc'+indice).value = tcc;
        document.getElementById('tta'+indice).value = tta;
        document.getElementById('ttpcc'+indice).value = ttpcc;
        
        if(tta === ttpcc)
        {
            document.getElementById('parcial_tta'+indice).className = 'correcto';
            document.getElementById('parcial_ttpcc'+indice).className = 'correcto';
            document.getElementById('parcial_suma'+indice).className = 'calificacion';
            document.getElementById('parcial_suma'+indice).innerHTML = "¡Bien hecho!";
        } else {
            document.getElementById('parcial_tta'+indice).className = 'incorrecto';
            document.getElementById('parcial_ttpcc'+indice).className = 'incorrecto';
            document.getElementById('parcial_suma'+indice).className = 'calificacion';
            document.getElementById('parcial_suma'+indice).innerHTML = "¡Tienes un error!";
        }
        
        protegerSumas();
    }
        
    };
    
    var sumaC = function(indice)
    {
        if(confirm('¿Está seguro de contestar? Una vez contestado el ejercicio no podrá modificar las respuestas'))
        {
            var ac = parseFloat(document.getElementById('ac_'+indice).value);
        var pcp = parseFloat(document.getElementById('pcp_'+indice).value);
        var cc = parseFloat(document.getElementById('cc_'+indice).value);
        var anc = parseFloat(document.getElementById('anc_'+indice).value);
        var cg = parseFloat(document.getElementById('cg_'+indice).value);
        
        var ta = ac + anc;
        var tp = pcp;
        var tcc = cc + cg;
        
        var tta = ta;
        var ttpcc = tp + tcc;
        
        document.getElementById('ta'+indice).value = ta;
        document.getElementById('tp'+indice).value = tp;
        document.getElementById('tcc'+indice).value = tcc;
        document.getElementById('tta'+indice).value = tta;
        document.getElementById('ttpcc'+indice).value = ttpcc;
        
        if(tta === ttpcc)
        {
            document.getElementById('parcial_tta'+indice).className = 'correcto';
            document.getElementById('parcial_ttpcc'+indice).className = 'correcto';
            document.getElementById('parcial_suma'+indice).className = 'calificacion';
            document.getElementById('parcial_suma'+indice).innerHTML = "¡Bien hecho!";
        } else {
            document.getElementById('parcial_tta'+indice).className = 'incorrecto';
            document.getElementById('parcial_ttpcc'+indice).className = 'incorrecto';
            document.getElementById('parcial_suma'+indice).className = 'calificacion';
            document.getElementById('parcial_suma'+indice).innerHTML = "¡Tienes un error!";
        }
        
        protegerSumas();
        }
    };
    
    window.onload = function(){
        protegerPLibres();
        protegerCompletar();
        protegerVerFalSimple('vf_simple01');
        protegerVerFalSimple('vf_simple02');
        protegerVerFalSimple('vf_simple03');
        protegerOpMul();
        protegerCorrelaciones();
        protegerSumas();
    };
</script>
</body>
</html>