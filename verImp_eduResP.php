<?php
include("libreria/principal.php");

// comprobar que han iniciado un ejercicio
if(isset($_GET['IDejercicio']) && isset($_GET['metodo']) && isset($_GET['asiento'])){
	$IDejercicio = $_GET['IDejercicio'];
	$metodo = $_GET['metodo'];
	$asiento = $_GET['asiento'];
} else {
	header("Location: portada.php?ejercicio=false");
	exit;	
}
//fin

// variables
$C8 = (pintarCantidadDa("Mdebe", "4107", $IDejercicio))*-1;
$C9 = (pintarCantidadDa("Mdebe", "4108", $IDejercicio))*-1;
$C12 = pintarCantidad("Shaber", "4206", $IDejercicio);
$C13 = (pintarCantidadDa("Mdebe", "4109", $IDejercicio))*-1;
$C16 = pintarCantidadDa("Mhaber", "4207", $IDejercicio);
$C17 = (pintarCantidadDa("Mdebe", "4110", $IDejercicio))*-1;

$D4 = pintarCantidad("Shaber", "P4202", $IDejercicio);
$D5 = pintarCantidad("Sdebe", "P4101", $IDejercicio);
$D6 = $D4-$D5;
$D7 = $C8+$C9;
$D10 = $D6+$D7;
$D11 = $C12+$C13;
$D14 = $D10+$D11;
$D15 = $C16+$C17;
$D18 = $D14+$D15;
$D19 = ($D18>0)?$D18*0.3:0;
$D20 = $D18-$D19;
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Balanza antes de ajustes</title>
<link href="css/imprimible.css" rel="stylesheet" type="text/css" />

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<div id="header">
  <h1>Sistema Contable Romero</h1>
</div>
<h2>Estado de resultados</h2>
<div id="info"><strong>Ejercicio:</strong> <span class="divContCuerpo">
<?php pintarNejercicio($IDejercicio); ?>
</span><br />
<strong>ESTADO DE RESULTADOS</strong> del <?php arregloFecha(fechasPU("ASC", $IDejercicio)); ?>
 al 
 <?php arregloFechaAs(fechasAs($IDejercicio)); ?>.</div>
<div>
  <table align="center" cellpadding="0" cellspacing="1">
    <tr height="20">
      <td height="20" class="celdaEduRes">Ventas    netas</td>
      <td rowspan="5" valign="bottom" class="celdaEduRes">$ <?php echo number_format($C8,2); ?></td>
      <td class="celdaEduRes">$ <?php echo number_format($D4,2); ?></td>
    </tr>
    <tr height="21">
      <td height="21" class="celdaEduRes">Costo de ventas</td>
      <td class="celdaEduRes">$ <?php echo number_format($D5,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Utilidad bruta</td>
      <td class="celdaEduRes">$ <?php echo number_format($D6,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Gastos de operación</td>
      <td class="celdaEduRes">$ <?php echo number_format($D7,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Gastos de venta</td>
      <td rowspan="3" valign="bottom" class="celdaEduRes">$ <?php echo number_format($D10,2); ?></td>
    </tr>
    <tr height="21">
      <td height="21" class="celdaEduRes">Gastos de administración</td>
      <td class="celdaEduRes">$ <?php echo number_format($C9,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Utilidad (pérdida) de    operación</td>
      <td rowspan="3" valign="bottom" class="celdaEduRes">$ <?php echo number_format($C12,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Otros ingresos y gastos</td>
      <td class="celdaEduRes">$ <?php echo number_format($D11,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Ingresos</td>
      <td rowspan="3" valign="bottom" class="celdaEduRes">$ <?php echo number_format($D14,2); ?></td>
    </tr>
    <tr height="21">
      <td height="21" class="celdaEduRes">Gastos</td>
      <td class="celdaEduRes">$ <?php echo number_format($C13,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">&nbsp;</td>
      <td rowspan="3" valign="bottom" class="celdaEduRes">$ <?php echo number_format($C16,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Resultado integral de    financiamiento</td>
      <td class="celdaEduRes">$ <?php echo number_format($D15,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">A favor</td>
      <td rowspan="3" valign="bottom" class="celdaEduRes">$ <?php echo number_format($D18,2); ?></td>
    </tr>
    <tr height="21">
      <td height="21" class="celdaEduRes">A cargo</td>
      <td class="celdaEduRes">$ <?php echo number_format($C17,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Utilidad antes de impuestos</td>
      <td rowspan="3" class="celdaEduRes">&nbsp;</td>
    </tr>
    <tr height="21">
      <td height="21" class="celdaEduRes">ISR</td>
      <td class="celdaEduRes">$ <?php echo number_format($D19,2); ?></td>
    </tr>
    <tr height="21">
      <td height="21" class="celdaEduRes">Utilidad neta del ejercicio</td>
      <td class="celdaEduRes">$ <?php echo number_format($D20,2); ?></td>
    </tr>
  </table>
</div>
<div id="footer">&copy;2011 - Sistema Contable Romero | Todos los Derechos Reservados.</div>
</body>
</html>
