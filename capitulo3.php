<?php
include 'libreria/principal.php';
include 'libreria/evaluaciones.php';

esUsuario(); // será usuario registrado?
$IDusuario = $_SESSION['userID'];
$capitulo = 3;
$registro = verificarCuestionario($capitulo, $IDusuario,$Fecha);


?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> Principios de Contabilidad. Cuestionario de evaluación. Capítulo 3</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="libreria/js_principal.js"></script>
<script src="libreria/vendor/jquery/jquery-1.11.0.min.js"></script>
<script src="libreria/calCap.js"></script>
<script src="libreria/rc3.js"></script>
<script>
    var corr1 = [10,8,7,12,3,2,9,4,11,5,1,6];
</script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Cuestionario de autoevaluación</h1>
    </div>
    <div id="divTitularMenu">
        <a href="portada.php">Inicio</a> | 
        <a href="libreria/calculadora/calculadora.html" 
           target="_blank" 
           onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | 
        <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido" style="width: 900px">
  <div class="divContCuerpo">
      
      <h3 class="titulo-principal">Capítulo 3</h3>
      <div class="migas">
          <ul>
              <li><a href="portada.php">Portada</a></li>
              <li><a href="javascript:history.back();">Página anterior</a></li>
              <li><img src="imagenes/print.png" 
                       alt="" 
                       height="25" 
                       style="float: left; margin-right: 5px;" />
                  <a href="javascript:print();">Imprimir página</a></li>
          </ul>
      </div>
      
      <table class="mis-datos">
          <tr>
              <td>Universidad o Institución:</td>
              <td>
                  <input oninput="cCamCues('unidad',this.value,<?php echo $registro ?>);" 
                         name="unidad" 
                         id='unidad' 
                         required 
                         value="<?php echo xCamCues('unidad', $registro) ?>" />
              </td>
              <td>Escuela o facultad:</td>
              <td>
                  <input oninput="cCamCues('facultad',this.value,<?php echo $registro ?>);" 
                         name="facultad" 
                         id='facultad' 
                         required 
                         value="<?php echo xCamCues('facultad', $registro) ?>" />
              </td>
          </tr>
          <tr>
              <td>Asignatura:</td>
              <td>
                  <input oninput="cCamCues('asignatura',this.value,<?php echo $registro ?>);" 
                         name="asignatura" 
                         id='asignatura' 
                         required 
                         value="<?php echo xCamCues('asignatura', $registro) ?>" />
              </td>
              <td>Alumno:</td>
              <td>
                  <?php echo nombreUsuario($IDusuario) ?>
              </td>
          </tr>
          <tr>
              <td>Maestro:</td>
              <td>
                  <input oninput="cCamCues('maestro',this.value,<?php echo $registro ?>);" 
                         name="maestro" 
                         id='maestro' 
                         required 
                         value="<?php echo xCamCues('maestro', $registro) ?>" />
              </td>
              <td>Fecha:</td>
              <td>
                  <input oninput="cCamCues('fecha',this.value,<?php echo $registro ?>);" 
                         name="fecha" 
                         id='fecha' 
                         required 
                         value="<?php echo xCamCues('fecha', $registro) ?>" />
              </td>
          </tr>
      </table>
      
      <h4 class="subtit-4">OBJETIVOS DE APRENDIZAJE</h4>
      
      <div class="objetivos">
          <p>Al finalizar este capítulo, el alumno será capaz de:</p>
          <ol>
              <li>Entender que el objetivo de la contabilidad financiera es 
                  proporcionar mediante estados financieros, información útil 
                  en la toma de decisiones.</li>
              <li>Comprender y analizar la definición.</li>
              <li>Conocer los distintos tipos de contabilidad.</li>
          </ol>
      </div>
      
      <h4 class="subtit-4">EJERCICIOS</h4>
      
      <h5>I. RESUELVA LO SIGUIENTE</h5>
      
      <div class="pregunta-libre">
          <p><span>1</span> Mencione cuál es el objetivo de la información 
              financiera.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib01" 
                    id="prelib01"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib01'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>2</span> ¿Cuál es la manifestación fundamental de la 
              información financiera?.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib02" id="prelib02"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib02'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>3</span> El suministro de información que contenga elementos 
              de juicio confiables ¿qué evaluaciones le permiten hacer al 
              usuario?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib03" id="prelib03"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib03'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>4</span> De conformidad con la NIF A-3, ¿qué información de 
              las entidades comunican los estados financieros?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib04" id="prelib04"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib04'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>5</span> Mencione a qué se refiere la situación financiera.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib05" id="prelib05"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib05'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>6</span> Señale que es la actividad operativa.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib06" id="prelib06"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib06'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>7</span> El conocimiento de la liquidez, ¿qué le 
              permite evaluar al usuario?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib07" id="prelib07"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib07'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>8</span> Diga en qué estados financieros se comunica la 
              información relativa a la situación financiera, la actividad 
              operativa, los flujos de efectivo, o en su caso, los cambios en 
              su situación financiera, en su capital contable y las 
              revelaciones sobre políticas contables, entorno y viabilidad como 
              negocio en marcha.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib08" id="prelib08"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib08'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>9</span> Investigue otras definiciones de contabilidad, 
              transcríbalas y coméntelas (cuando menos debe ser una de cada 
              forma de conceptualización: arte, ciencia, técnica).</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib09" id="prelib09"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib09'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>10</span> La contabilidad financiera ha sido descrita y 
              conceptuada como ciencia, arte y técnica. En su opinión, 
              ¿cuál la conceptúa o define más acertadamente? Fundamente su 
              respuesta.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib10" id="prelib10"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib10'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>11</span> Proponga su propia definición de contabilidad 
              financiera. Coméntela y discútala con sus compañeros.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib11" id="prelib11"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib11'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>12</span> Explique si, en su opinión, la empresa debe llevar 
              tres tipos de contabilidad (financiera, administrativa y fiscal) 
              o qué se debería hacer.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib12" id="prelib12"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib12'); ?></textarea>
      </div>
      
      <h5>II. COMPLETE LAS LÍNEAS EN BLANCO</h5>
      
      <div class="completar-lineas">
          <span class="num">1</span>
          Según la NIF A-1, la información financiera que emana de la 
          contabilidad es información <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_1" 
                    name="comLin_1" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_1'); ?>" /> expresada en unidades 
          monetarias y <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_2" 
                    name="comLin_2" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_2'); ?>" />, que muestra la posición y desempeño 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_3" 
                    name="comLin_3" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_3'); ?>" /> de una entidad, y 
          cuyo objetivo es el de ser <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_4" 
                    name="comLin_4" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_4'); ?>" /> al usuario general 
          en la toma de sus <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_5" 
                    name="comLin_5" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_5'); ?>" /> económicas.
      </div>
      
      <div class="completar-lineas">
          <span class="num">2</span>
          Los <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_6" 
                    name="comLin_6" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_6'); ?>" /> financieros son el 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_7" 
                    name="comLin_7" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_7'); ?>" /> mediante el cual se 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_8" 
                    name="comLin_8" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_8'); ?>" /> la información 
          financiera que precisa el <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_9" 
                    name="comLin_9" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_9'); ?>" /> general para fundamentar y 
          tomar decisiones.
      </div>
      
      <div class="completar-lineas">
          <span class="num">3</span>
          Esta doble evaluación se apoya especialmente en la posibilidad de 
          obtener <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_10" 
                    name="comLin_10" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_10'); ?>" /> y de generar 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_11" 
                    name="comLin_11" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_11'); ?>" />, y requiere el conocimiento 
          de la situación <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_12" 
                    name="comLin_12" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_12'); ?>" /> de la entidad, de 
          su actividad <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_13" 
                    name="comLin_13" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_13'); ?>" /> y de sus 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_14" 
                    name="comLin_14" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_14'); ?>" /> en el capital contable o 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_15" 
                    name="comLin_15" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_15'); ?>" /> contable y en los 
          flujos de <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_16" 
                    name="comLin_16" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_16'); ?>" /> o, en su caso, en 
          los cambios de la <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_17" 
                    name="comLin_17" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_17'); ?>" /> financiera.
      </div>
      
      <div class="completar-lineas">
          <span class="num">4</span>
          Según el CINIF, la contabilidad es una 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_18" 
                    name="comLin_18" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_18'); ?>" /> que se utiliza para el registro de las 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_19" 
                    name="comLin_19" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_19'); ?>" /> que afectan 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_20" 
                    name="comLin_20" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_20'); ?>" /> a una entidad y 
          que produce sistemática y <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_21" 
                    name="comLin_21" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_21'); ?>" /> información 
          financiera; las operaciones que afectan económicamente a una entidad 
          incluyen <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_22" 
                    name="comLin_22" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_22'); ?>" />, transformaciones y 
          otros <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_23" 
                    name="comLin_23" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_23'); ?>" />.
      </div>
      
      <div class="completar-lineas">
          <span class="num">5</span>
          Según el autor, la contabilidad financiera es una técnica mediante 
          la cual se registran, <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_24" 
                    name="comLin_24" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_24'); ?>" /> y 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_25" 
                    name="comLin_25" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_25'); ?>" /> las operaciones, 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_26" 
                    name="comLin_26" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_26'); ?>" /> 
          <input type="hidden" 
                    id="comLin_27" 
                    name="comLin_27" 
                    value="internas" />  y eventos económicos, 
          naturales y de cualquier tipo, <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_28" 
                    name="comLin_28" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_28'); ?>" /> y cuantificables 
          que afectan a la entidad, estableciendo los medios de 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_29" 
                    name="comLin_29" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_29'); ?>" /> que permitan 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_30" 
                    name="comLin_30" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_30'); ?>" /> información 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_31" 
                    name="comLin_31" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_31'); ?>" /> expresada en 
          unidades monetarias, e información cualitativa, 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_32" 
                    name="comLin_32" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_32'); ?>" /> e interpretada, 
     para que los diversos interesados tomen 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_33" 
                    name="comLin_33" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_33'); ?>" /> en relación con dicha entidad económica.
      </div>
      
      <div class="completar-lineas">
          <span class="num">6</span>
          La contabilidad <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_34" 
                    name="comLin_34" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_34'); ?>" /> está orientada a 
          los aspectos <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_35" 
                    name="comLin_35" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_35'); ?>" /> de la empresa y 
          sus informes no trascenderán la compañía, o sea, su uso es estrictamente 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_36" 
                    name="comLin_36" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_36'); ?>" /> y serán utilizados 
          por los administradores o propietarios para 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_37" 
                    name="comLin_37" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_37'); ?>" /> y evaluar el 
          desarrollo de la entidad a la luz de las políticas, metas u 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_38" 
                    name="comLin_38" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_38'); ?>" /> preestablecidos 
          por la dirección de la empresa.
      </div>
            
      <div class="completar-lineas">
          <button onclick="calificarCompletar('comLin',38,rc3_cl);">CALIFICAR</button>
          <span id="comLin_cal"></span>
      </div>
      
      <h5>III. CORRELACIONE LAS COLUMNAS 1 Y 2</h5>
      
      <table class='correlaciones'>
          <tr>
              <th>Columna 1</th>
              <th>Columna 2</th>
          </tr>
          <tr>
              <td>
                  <span class="num">1</span>
                  <p>Clientes y beneficiarios</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_1' id='corr1_1'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>10</option>
                      <option value="11" <?php selOpcion(11, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>11</option>
                      <option value="12" <?php selOpcion(12, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>12</option>
                  </select>
                  <span id='corr1_1-parcial'></span>
              </td>
              <td rowspan="12">
                  <ol>
                      <li>Son los responsables de supervisar y evaluar la 
                          administración de las entidades</li>
                      <li>Son los que proporcionan bienes y servicios para 
                          la operación de la entidad</li>
                      <li>Incluye a otros interesados no comprendidos en los 
                          apartados anteriores, tales como público inversionista, 
                          analistas financieros y consultores</li>
                      <li>Son los responsables de cumplir el mandato de los 
                          cuerpos de gobierno (incluyendo los patrocinadores o 
                          accionistas) y de dirigir las actividades operativas</li>
                      <li>Incluye a instituciones financieras y otro tipo de 
                          acreedores</li>
                      <li>Son los encargados de regular, promover y vigilar los 
                          mercados financieros</li>
                      <li>Son los responsables de establecer políticas económicas, 
                          monetarias y fiscales, así como participar en la 
                          actividad económica al conseguir financiamiento y asignar 
                          presupuesto gubernamental</li>
                      <li>Son aquellos que fundamentalmente aportan al fisco y 
                          están interesados en la actuación y rendición de cuentas 
                          de las unidades gubernamentales</li>
                      <li>Incluyen a socios, asociados y miembros que proporcionan 
                          recursos a la entidad, directamente compensados de 
                          acuerdo con sus aportaciones (entidades lucrativas)</li>
                      <li>Son los que reciben servicios o productos de las entidades</li>
                      <li>Son los que laboran para la entidad</li>
                      <li>Incluye a patronos, donantes, asociados y miembros, 
                          que proporcionan recursos que no son directamente 
                          compensados (entidades con propósitos no lucrativos)</li>
                  </ol>
              </td>
          </tr>
          <tr>
              <td>
                  <span class="num">2</span>
                  <p>Contribuyentes de impuestos</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_2' id='corr1_2'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>10</option>
                      <option value="11" <?php selOpcion(11, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>11</option>
                      <option value="12" <?php selOpcion(12, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>12</option>
                  </select>
                  <span id='corr1_2-parcial'></span>
              </td>
          </tr>
          <tr>
              <td>
                  <span class="num">3</span>
                  <p>Unidades gubernamentales</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_3' id='corr1_3'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>10</option>
                      <option value="11" <?php selOpcion(11, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>11</option>
                      <option value="12" <?php selOpcion(12, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>12</option>
                  </select>
                  <span id='corr1_3-parcial'></span>
              </td>
          </tr>
          <tr>
              <td>
                  <span class="num">4</span>
                  <p>Patrocinadores</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_4' id='corr1_4'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>10</option>
                      <option value="11" <?php selOpcion(11, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>11</option>
                      <option value="12" <?php selOpcion(12, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>12</option>
                  </select>
                  <span id='corr1_4-parcial'></span>
              </td>
          </tr>
          <tr>
              <td>
                  <span class="num">5</span>
                  <p>Otros usuarios</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_5' id='corr1_5'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>10</option>
                      <option value="11" <?php selOpcion(11, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>11</option>
                      <option value="12" <?php selOpcion(12, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>12</option>
                  </select>
                  <span id='corr1_5-parcial'></span>
              </td>
          </tr>
          <tr>
              <td>
                  <span class="num">6</span>
                  <p>Proveedores</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_6' id='corr1_6'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>10</option>
                      <option value="11" <?php selOpcion(11, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>11</option>
                      <option value="12" <?php selOpcion(12, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>12</option>
                  </select>
                  <span id='corr1_6-parcial'></span>
              </td>
          </tr>
          <tr>
              <td>
                  <span class="num">7</span>
                  <p>Accionistas o dueños</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_7' id='corr1_7'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>10</option>
                      <option value="11" <?php selOpcion(11, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>11</option>
                      <option value="12" <?php selOpcion(12, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>12</option>
                  </select>
                  <span id='corr1_7-parcial'></span>
              </td>
          </tr>
          <tr>
              <td>
                  <span class="num">8</span>
                  <p>Administradores</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_8' id='corr1_8'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>10</option>
                      <option value="11" <?php selOpcion(11, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>11</option>
                      <option value="12" <?php selOpcion(12, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>12</option>
                  </select>
                  <span id='corr1_8-parcial'></span>
              </td>
          </tr>
          <tr>
              <td>
                  <span class="num">9</span>
                  <p>Empleados</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_9' id='corr1_9'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>10</option>
                      <option value="11" <?php selOpcion(11, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>11</option>
                      <option value="12" <?php selOpcion(12, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>12</option>
                  </select>
                  <span id='corr1_9-parcial'></span>
              </td>
          </tr>
          <tr>
              <td>
                  <span class="num">10</span>
                  <p>Acreedores</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_10' id='corr1_10'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>10</option>
                      <option value="11" <?php selOpcion(11, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>11</option>
                      <option value="12" <?php selOpcion(12, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>12</option>
                  </select>
                  <span id='corr1_10-parcial'></span>
              </td>
          </tr>
          <tr>
              <td>
                  <span class="num">11</span>
                  <p>Órganos de supervisión y vigilancia corporativos, 
                      internos o externos.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_11' id='corr1_11'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_11')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_11')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_11')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_11')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_11')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_11')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_11')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_11')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_11')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_11')) ?>>10</option>
                      <option value="11" <?php selOpcion(11, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_11')) ?>>11</option>
                      <option value="12" <?php selOpcion(12, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_11')) ?>>12</option>
                  </select>
                  <span id='corr1_11-parcial'></span>
              </td>
          </tr>
          <tr>
              <td>
                  <span class="num">12</span>
                  <p>Organismos reguladores</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_12' id='corr1_12'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_12')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_12')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_12')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_12')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_12')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_12')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_12')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_12')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_12')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_12')) ?>>10</option>
                      <option value="11" <?php selOpcion(11, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_12')) ?>>11</option>
                      <option value="12" <?php selOpcion(12, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_12')) ?>>12</option>
                  </select>
                  <span id='corr1_12-parcial'></span>
              </td>
          </tr>
          
          <tr>
              <td id='corr1_cal'></td>
              <td><button onclick="calificarCorrelacion('corr1',12,corr1);" 
                          name='cal_corr1'>CALIFICAR</button></td>
          </tr>
      </table>

  </div>
  <!-- -->
  <div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
<script>
    var tc;
    var cCamCues = function(campo,valor,registro)
    {
      clearTimeout(tc);
      tc = window.setTimeout(function(){
          $.post('libreria/cCamCues.php',{
              modDatos: true,
              elcampo: campo,
              elvalor: valor,
              elregistro: registro
          },function(respuesta){
              if(respuesta === 'modificado')
              {
                  // Modificado
              } else {
                  // Error
                  console.log('-> '+respuesta);
              }
          });
      },1000);
    };
    var cResCues = function(cues,usu,cap,preg,resp)
    {
      clearTimeout(tc);
      tc = window.setTimeout(function(){
          $.post('libreria/cCamCues.php',{
              modRespuestas: true,
              cuestionario: cues,
              usuario: usu,
              capitulo: cap,
              pregunta: preg,
              respuesta:resp
          },function(respuesta){
              if(respuesta === 'modificado')
              {
                  // Modificado
              } else {
                  // Error
                  console.log('-> '+respuesta);
              }
          });
      },500);
    };
    
    window.onload = function(){
        protegerPLibres();
        protegerCompletar();
        protegerCorrelaciones();
    };
</script>
</body>
</html>