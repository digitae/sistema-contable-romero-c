<?php
include("libreria/principal.php");

esUsuario(); // será usuario registrado?
$msgError = FALSE;

###################
## SCRIPT GENERAL ##

// Navegación por planillas
if(isset($_POST['subNav'])){
	$planilla = $_POST['planilla'];
	$IDejercicio = $_POST['IDejercicio'];
	$metodo = $_POST['metodo'];
	$asiento = $_POST['asiento'];
	
	header("Location: $planilla?IDejercicio=$IDejercicio&metodo=$metodo&asiento=$asiento");
	exit;		
}

// comprobar que han iniciado un ejercicio
if(isset($_GET['IDejercicio']) && isset($_GET['metodo']) && isset($_GET['asiento'])){
	$IDejercicio = $_GET['IDejercicio'];
	$metodo = $_GET['metodo'];
	$asiento = $_GET['asiento'];
} else {
	header("Location: portada.php?ejercicio=false");
	exit;	
}
//fin
###################

// Extraer fecha del asiento
$queryFecha = "SELECT Fasiento, redaccion, status FROM rom_asiento WHERE asientoR = '$asiento' AND Easiento = '$IDejercicio'";
$resultFecha = mysql_query($queryFecha);
$rowFecha = mysql_fetch_assoc($resultFecha);
$fechaAsiento = $rowFecha['Fasiento'];
$statusAsiento = $rowFecha['status']; // status del asiento ** IMPORTANTE **

// pintar navegación de plantillas
$queryP		= "SELECT * FROM rom_planillas WHERE Mplanilla = 4 OR Mplanilla = '$metodo' ORDER BY IDplanilla ASC";
$resultP	= mysql_query($queryP) or die (mysql_error());
$rowP		= mysql_fetch_assoc($resultP);

// verificar si hay una balanza_da y balanza_aa
$queryBaa = "SELECT COUNT(IDbalanza_da) as bda FROM rom_balanza_da WHERE ejercicio = '$IDejercicio'";
$resulBaa = mysql_query($queryBaa);
$datoBaa = mysql_fetch_assoc($resulBaa);
$bda = $datoBaa['bda'];

if($bda == 0) { // no hay balanza_da
	$msgError = "<strong>Advertencia:</strong> No hay BALANZA DESPUES DE AJUSTES. Cree una y vuelva a intentar.";
}

$D_C12 = pintarCantReglaNeg("Sdebe", "1106", $IDejercicio);
$D_C13 = pintarCantidadDa("Shaber", "5101", $IDejercicio);
$D_C35 = pintarCantReglaNeg("Sdebe", "1202", $IDejercicio);
$D_C36 = pintarCantidadDa("Shaber", "5201", $IDejercicio);
$D_C37 = pintarCantReglaNeg("Sdebe", "1203", $IDejercicio);
$D_C38 = pintarCantidadDa("Shaber", "5202", $IDejercicio);
$D_C39 = pintarCantReglaNeg("Sdebe", "1204", $IDejercicio);
$D_C40 = pintarCantidadDa("Shaber", "5203", $IDejercicio);
$D_C41 = pintarCantReglaNeg("Sdebe", "1205", $IDejercicio);
$D_C42 = pintarCantidadDa("Shaber", "5204", $IDejercicio);
$D_C43 = pintarCantReglaNeg("Sdebe", "1206", $IDejercicio);
$D_C44 = pintarCantidadDa("Shaber", "5205", $IDejercicio);
$D_C45 = pintarCantReglaNeg("Sdebe", "1207", $IDejercicio);
$D_C46 = pintarCantidadDa("Shaber", "5206", $IDejercicio);
$D_C47 = pintarCantReglaNeg("Sdebe", "1208", $IDejercicio);
$D_C48 = pintarCantidadDa("Shaber", "5207", $IDejercicio);
$D_C50 = pintarCantReglaNeg("Sdebe", "1209", $IDejercicio);
$D_C51 = pintarCantidadDa("Shaber", "5208", $IDejercicio);
$D_C52 = pintarCantReglaNeg("Sdebe", "1210", $IDejercicio);
$D_C53 = pintarCantidadDa("Shaber", "5209", $IDejercicio);
$D_C54 = pintarCantReglaNeg("Sdebe", "1211", $IDejercicio);
$D_C55 = pintarCantidadDa("Shaber", "5210", $IDejercicio);
$D_C56 = pintarCantReglaNeg("Sdebe", "1212", $IDejercicio);
$D_C57 = pintarCantidadDa("Shaber", "5211", $IDejercicio);
$D_C58 = pintarCantReglaNeg("Sdebe", "1213", $IDejercicio);
$D_C59 = pintarCantidadDa("Shaber", "5212", $IDejercicio);
$D_C60 = pintarCantReglaNeg("Sdebe", "1214", $IDejercicio);
$D_C61 = pintarCantidadDa("Shaber", "5213", $IDejercicio);
$D_C62 = pintarCantReglaNeg("Sdebe", "1215", $IDejercicio);
$D_C63 = pintarCantidadDa("Shaber", "5214", $IDejercicio);
$D_C64 = pintarCantReglaNeg("Sdebe", "1216", $IDejercicio);
$D_C65 = pintarCantidadDa("Shaber", "5215", $IDejercicio);

$D_D7 = pintarCantReglaNeg("Sdebe", "1101", $IDejercicio);
$D_D8 = pintarCantReglaNeg("Sdebe", "1102", $IDejercicio);
$D_D9 = pintarCantReglaNeg("Sdebe", "1103", $IDejercicio);
$D_D10 = pintarCantidadDa("Sdebe", "1104", $IDejercicio);
$D_D11 = pintarCantReglaNeg("Sdebe", "1105", $IDejercicio);
$D_D13 = $D_C12 - $D_C13;
$D_D14 = pintarCantReglaNeg("Sdebe", "1107", $IDejercicio);
$D_D15 = pintarCantReglaNeg("Sdebe", "1108", $IDejercicio);
$D_D16 = pintarCantReglaNeg("Sdebe", "1109", $IDejercicio);
$D_D17 = pintarCantReglaNeg("Sdebe", "1110", $IDejercicio);
$D_D18 = pintarCantReglaNeg("Sdebe", "1111", $IDejercicio);
$D_D19 = pintarCantReglaNeg("Sdebe", "1112", $IDejercicio);
$D_D20 = pintarCantReglaNeg("Sdebe", "1113", $IDejercicio);
$D_D21 = pintarCantReglaNeg("Sdebe", "1114", $IDejercicio);
// ATENCIÓN
switch($metodo){ // dependiendo del metodo será la clave
	case "1": $cmet = "A1115";
	break;
	case "2": $cmet = "G1115";
	break;
	case "3": $cmet = "P1115";
	break;	
}
$D_D22 = pintarCantReglaNeg("Sdebe", $cmet, $IDejercicio);
//
$D_D23 = pintarCantReglaNeg("Sdebe", "1116", $IDejercicio);
$D_D24 = pintarCantReglaNeg("Sdebe", "1117", $IDejercicio);
$D_D25 = pintarCantReglaNeg("Sdebe", "1118", $IDejercicio);
$D_D26 = pintarCantReglaNeg("Sdebe", "1119", $IDejercicio);
$D_D27 = pintarCantReglaNeg("Sdebe", "1120", $IDejercicio);
$D_D28 = pintarCantReglaNeg("Sdebe", "1121", $IDejercicio);
$D_D29 = pintarCantReglaNeg("Sdebe", "1122", $IDejercicio);
$D_D30 = pintarCantReglaNeg("Sdebe", "1123", $IDejercicio);
$D_D31 = pintarCantReglaNeg("Sdebe", "1124", $IDejercicio);
$D_D34 = pintarCantReglaNeg("Sdebe", "1201", $IDejercicio);
$D_D36 = $D_C35 - $D_C36;
$D_D38 = $D_C37 - $D_C38;
$D_D40 = $D_C39 - $D_C40;
$D_D42 = $D_C41 - $D_C42;
$D_D44 = $D_C43 - $D_C44;
$D_D46 = $D_C45 - $D_C46;
$D_D48 = $D_C47 - $D_C48;
$D_D51 = $D_C50 - $D_C51;
$D_D53 = $D_C52 - $D_C53;
$D_D55 = $D_C54 - $D_C55;
$D_D57 = $D_C56 - $D_C57;
$D_D59 = $D_C58 - $D_C59;
$D_D61 = $D_C60 - $D_C61;
$D_D63 = $D_C62 - $D_C63;
$D_D65 = $D_C64 - $D_C65;
$D_D67 = pintarCantReglaNeg("Sdebe", "1217", $IDejercicio);
$D_D68 = pintarCantReglaNeg("Sdebe", "1218", $IDejercicio);
$D_D69 = pintarCantReglaNeg("Sdebe", "1219", $IDejercicio);
$D_D70 = pintarCantReglaNeg("Sdebe", "1220", $IDejercicio);
$D_D71 = pintarCantReglaNeg("Sdebe", "1221", $IDejercicio);
$D_D72 = pintarCantReglaNeg("Sdebe", "1222", $IDejercicio);

$D_E31 = $D_D7+$D_D8+$D_D9+$D_D10+$D_D11+$D_D13+$D_D14+$D_D15+$D_D16+$D_D17+$D_D18+$D_D19+$D_D20+$D_D21+$D_D22+$D_D23+$D_D24+$D_D25+$D_D26+$D_D27+$D_D28+$D_D29+$D_D30+$D_D31;
$D_E48 = $D_D34+$D_D36+$D_D38+$D_D40+$D_D42+$D_D44+$D_D46+$D_D48;
$D_E65 = $D_D51+$D_D53+$D_D55+$D_D57+$D_D59+$D_D61+$D_D63+$D_D65;
$D_E72 = $D_D67+$D_D68+$D_D69+$D_D70+$D_D71+$D_D72;
$D_E73 = $D_E31+$D_E48+$D_E65+$D_E72;

$H_H7 = pintarCantidadDa("Shaber", "1104", $IDejercicio);
$H_H8 = pintarCantReglaNeg("Shaber", "2101", $IDejercicio);
$H_H9 = pintarCantReglaNeg("Shaber", "2102", $IDejercicio);
$H_H10 = pintarCantReglaNeg("Shaber", "2103", $IDejercicio);
$H_H11 = pintarCantReglaNeg("Shaber", "2104", $IDejercicio);
$H_H12 = pintarCantReglaNeg("Shaber", "2105", $IDejercicio);
$H_H13 = pintarCantReglaNeg("Shaber", "2106", $IDejercicio);
$H_H14 = pintarCantReglaNeg("Shaber", "2107", $IDejercicio);
$H_H15 = pintarCantReglaNeg("Shaber", "2108", $IDejercicio);
$H_H16 = pintarCantReglaNeg("Shaber", "2109", $IDejercicio);
$H_H17 = pintarCantReglaNeg("Shaber", "2110", $IDejercicio);
$H_H18 = pintarCantReglaNeg("Shaber", "2111", $IDejercicio);
$H_H19 = pintarCantReglaNeg("Shaber", "2112", $IDejercicio);
$H_H20 = pintarCantReglaNeg("Shaber", "2113", $IDejercicio);
$H_H21 = pintarCantReglaNeg("Shaber", "2114", $IDejercicio);
$H_H22 = pintarCantReglaNeg("Shaber", "2115", $IDejercicio);
$H_H23 = pintarCantReglaNeg("Shaber", "2116", $IDejercicio);
$H_H25 = pintarCantReglaNeg("Shaber", "2201", $IDejercicio);
$H_H26 = pintarCantReglaNeg("Shaber", "2202", $IDejercicio);
$H_H27 = pintarCantReglaNeg("Shaber", "2203", $IDejercicio);
$H_H28 = pintarCantReglaNeg("Shaber", "2204", $IDejercicio);
$H_H29 = pintarCantReglaNeg("Shaber", "2205", $IDejercicio);
$H_H30 = pintarCantReglaNeg("Shaber", "2206", $IDejercicio);
$H_H33 = pintarCantReglaNeg("Shaber", "3101", $IDejercicio);
$H_H34 = pintarCantReglaNeg("Shaber", "3102", $IDejercicio);
$H_H35 = pintarCantReglaNeg("Shaber", "3103", $IDejercicio);
$H_H36 = pintarCantReglaNeg("Shaber", "3104", $IDejercicio);
$H_H38 = pintarCantReglaNeg("Shaber", "3201", $IDejercicio);

// Corrección a pérdida neta del ejercicio
// $H_H39 = (pintarCantidadDa("Sdebe", "3202", $IDejercicio))*-1;
$H_H39 = pintarCantReglaNegReverse("Sdebe", "3202", $IDejercicio);

$H_H40 = pintarCantReglaNeg("Shaber", "3203", $IDejercicio);

// Corrección a pérdida acumulada
// $H_H41 = (pintarCantidadDa("Sdebe", "3204", $IDejercicio))*-1;
$H_H41 = pintarCantReglaNegReverse("Sdebe", "3204", $IDejercicio);

$H_H42 = pintarCantReglaNeg("Shaber", "3205", $IDejercicio);
$H_H43 = pintarCantReglaNeg("Shaber", "3206", $IDejercicio);
$H_H44 = pintarCantReglaNeg("Shaber", "3207", $IDejercicio);

$H_I23 = $H_H7+$H_H8+$H_H9+$H_H10+$H_H11+$H_H12+$H_H13+$H_H14+$H_H15+$H_H16+$H_H17+$H_H18+$H_H19+$H_H20+$H_H21+$H_H22+$H_H23;
$H_I30 = $H_H25+$H_H26+$H_H27+$H_H28+$H_H29+$H_H30;
$H_I36 = $H_H33+$H_H34+$H_H35+$H_H36;
$H_I44 = $H_H38+$H_H39+$H_H40+$H_H41+$H_H42+$H_H43+$H_H44;

$H_J30 = $H_I23+$H_I30;
$H_J45 = $H_I36+$H_I44;
$H_J73 = $H_J30+$H_J45;

$C18 = $D_C35+$D_C37+$D_C39+$D_C41+$D_C43+$D_C45+$D_C47;
$C19 = $D_C36+$D_C38+$D_C40+$D_C42+$D_C44+$D_C46+$D_C48;
$C21 = $D_C50+$D_C52+$D_C54+$D_C56+$D_C58+$D_C60+$D_C62+$D_C64;
$C22 = $D_C51+$D_C53+$D_C55+$D_C57+$D_C59+$D_C61+$D_C63+$D_C65;

$D7 = $D_D7+$D_D8+$D_D9+$D_D10;
$D8 = $D_D11;
$D9 = $D_D13+$D_D14+$D_D15+$D_D16+$D_D17+$D_D18+$D_D19+$D_D20+$D_D21;
$D10 = $D_D22;
$D11 = $D_D23;
$D12 = $D_D24;
$D13 = $D_D25+$D_D26+$D_D27+$D_D28+$D_D29+$D_D30;
$D14 = $D_D31;
$D17 = $D_D34;
$D19 = $C18 - $C19;
$D22 = $C21-$C22;
$D24 = $D_D67+$D_D68+$D_D69+$D_D70+$D_D71+$D_D72;

$E14 = $D7+$D8+$D9+$D10+$D11+$D12+$D13+$D14;
$E24 = $D17+$D19+$D22+$D24;
$E30 = $E14+$E24;

$H7 = $H_H7;
$H8 = $H_H8;
$H9 = $H_H9+$H_H10+$H_H11+$H_H12+$H_H20+$H_H21+$H_H22+$H_H23;
$H10 = $H_H13+$H_H14+$H_H15+$H_H16+$H_H17;
$H11 = $H_H18;
$H12 = $H_H19;
$H14 = $H_H25+$H_H29+$H_H30;
$H15 = $H_H26;
$H16 = $H_H27;
$H17 = $H_H28;
$H20 = $H_H33;
$H21 = $H_H34;
$H22 = $H_H35;
$H23 = $H_H36;
$H25 = $H_H38;
$H26 = $H_H39;
$H27 = $H_H40;
$H28 = $H_H41;
$H29 = $H_H42+$H_H43+$H_H44;

$I12 = $H7+$H8+$H9+$H10+$H11+$H12;
$I17 = $H14+$H15+$H16+$H17;
$I23 = $H20+$H21+$H22+$H23;
$I29 = $H25+$H26+$H27+$H28+$H29;

$J17 = $I12+$I17;
$J29 = $I23+$I29;
$J30 = $J17+$J29;


?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sistema Contable Romero</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript" src="libreria/js_principal.js"></script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Balance general sintético</h1>
    </div>
    <div id="divTitularMenu"><a href="portada.php">Inicio</a> | <a href="libreria/calculadora/calculadora.html" target="_blank" onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | <a href="diarioFeAs.php?IDejercicio=<?php echo $IDejercicio; ?>&amp;metodo=<?php echo $metodo; ?>">Asientos</a> | <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenidoBal">
<div class="divNavPlan">
  <form id="navPlan" name="navPlan" method="post" action="<?php $PHP_SELF; ?>">
    Navegación por plantillas:
    <select name="planilla" class="campoSelectPlanilla" id="planilla">
      <option selected="selected">Seleccione una...</option>
      <?php
	  do {
		  echo '<option value="'. $rowP['link'] .'">'. utf8_encode($rowP['Nplanilla']) .'</option>';
	  } while ($rowP = mysql_fetch_assoc($resultP));
	  ?>
    </select>
    <input name="IDejercicio" type="hidden" id="IDejercicio" value="<?php echo $IDejercicio; ?>" />
    <input name="metodo" type="hidden" id="metodo" value="<?php echo $metodo; ?>" />
    <input name="asiento" type="hidden" id="asiento" value="<?php echo $asiento; ?>" />
<input type="submit" name="subNav" id="subNav" value="ir" />
  </form>
</div>
<div id="divSupCuerpo"><strong>Ejercicio:</strong>  <span class="divContCuerpo">
  <?php pintarNejercicio($IDejercicio); ?>
</span><br />
  <strong>ESTADO DE POSICIÓN FINANCIERA</strong> al 
  <?php arregloFechaAs(fechasAs($IDejercicio)); ?>.</div>
<div class="divContCuerpo"><a href="verImp_balanceSin.php?IDejercicio=<?php echo $IDejercicio; ?>&amp;metodo=<?php echo $metodo; ?>&amp;asiento=<?php echo $asiento; ?>" target="_blank"><img src="imagenes/verImp.gif" alt="Versión imprimible" width="150" height="25" border="0" title="Versión imprimible" /></a></div>
<?php if($msgError) { ?>
<div class="divContCuerpoInfo"><?php echo $msgError; ?></div>
<?php } ?>
<?php if(!$msgError) { ?>
<div class="divContCuerpo">
  <table width="900" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="440" align="left" valign="top"><table cellspacing="1" cellpadding="0">
        <tr>
          <td colspan="4" align="center" valign="middle" class="celdaListaDatBalD"><strong>- ACTIVO -</strong></td>
        </tr>
        <tr>
          <td width="200" align="center" valign="middle" class="celdaListaDatBalD"><strong> Circulante</strong></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalD">Efectivo y equivalentes de    efectivo</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D7,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalD">Instrumentos    financieros</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D8,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalD">Cuentas por    cobrar</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D9,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalD"><?php
          switch ($metodo) {
              case 1:
                  echo "Inventarios";
                  break;
              case 2:
                  echo "Inventarios";
                  break;
              case 3:
                  echo "Almacén";
                  break;
          }
          ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format(($D10 + $D11 + $D12),2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalD">Pagos anticipados</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D13,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalD">Otros</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D14,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($E14,2); ?></td>
        </tr>
        <tr>
          <td width="125" align="center" valign="middle" class="celdaListaDatBalD"><strong>No circulante</strong></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="center" valign="top" class="celdaListaDatBalD"><strong> Propiedades,    planta y equipo</strong></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalD">Terrenos</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D17,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalD">Propiedades, planta y equipo</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($C18,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalD">Depreciación    acumulada</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($C19,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D19,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="center" valign="top" class="celdaListaDatBalD"><strong>Intangibles</strong></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalD">Intangibles</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($C21,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalD">Amortización    acumulada</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($C22,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D22,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="center" valign="top" class="celdaListaDatBalD"><strong>Otros</strong></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalD">Otros</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D24,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($E24,2); ?></td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalD"><strong>Total activo</strong></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD"><strong>$ <?php echo number_format($E30,2); ?></strong></td>
        </tr>
      </table></td>
      <td width="20" align="left" valign="top">&nbsp;</td>
      <td width="440" align="left" valign="top"><table cellspacing="1" cellpadding="0">
        <tr>
          <td colspan="4" align="center" valign="top" class="celdaListaDatBalH"><strong>- PASIVO -</strong></td>
        </tr>
        <tr>
          <td width="200" align="center" valign="top" class="celdaListaDatBalH"><strong>A corto plazo</strong></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalH">Bancos, saldo    acreedor</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H7,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalH">Proveedores</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H8,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalH">Otras cuentas    por pagar</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H9,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalH">Impuestos por    pagar</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H10,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalH">ISR por pagar</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H11,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalH">PTU por pagar</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H12,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($I12,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="center" valign="top" class="celdaListaDatBalH"><strong>A largo plazo</strong></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalH">Otras cuentas    por pagar a largo plazo</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H14,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalH">Acreedores    bancarios a largo plazo</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H15,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalH">Acreedores    hipotecarios a largo plazo</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H16,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalH">Obligaciones    en circulación a largo plazo</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H17,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($I17,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($J17,2); ?></td>
        </tr>
        <tr>
          <td width="125" align="center" valign="top" class="celdaListaDatBalH"><strong>Capital contable</strong></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="center" valign="top" class="celdaListaDatBalH"><strong> Capital    contribuido</strong></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalH">Capital social</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H20,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalH">Capital </td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H21,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalH">Aportaciones para futuros aumentos de capital</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H22,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalH">Prima en venta de acciones</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H23,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($I23,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="center" valign="top" class="celdaListaDatBalH"><strong> Capital ganado    (déficit)</strong></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalH">Utilidad neta del ejercicio</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H25,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalH">Pérdida neta del ejercicio</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H26,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalH">Utilidades acumuladas</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H27,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalH">Pérdidas acumuladas</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H28,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalH">Reservas</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H29,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($I29,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($J29,2); ?></td>
        </tr>
        <tr>
          <td width="125" align="left" valign="top" class="celdaListaDatBalH"><strong>Total pasivo    más capital contable</strong></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH"><strong>$ <?php echo number_format($J30,2); ?></strong></td>
        </tr>
      </table></td>
    </tr>
  </table>
</div>
<?php } ?>
<div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
</body>
</html>