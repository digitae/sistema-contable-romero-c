<?php
include("libreria/principal.php");

esUsuario(); // será usuario registrado?

###################
## SCRIPT GENERAL ##

// Navegación por planillas
if(isset($_POST['subNav'])){
	$planilla = $_POST['planilla'];
	$IDejercicio = $_POST['IDejercicio'];
	$metodo = $_POST['metodo'];
	$asiento = $_POST['asiento'];
	
	header("Location: $planilla?IDejercicio=$IDejercicio&metodo=$metodo&asiento=$asiento");
	exit;		
}

// comprobar que han iniciado un ejercicio
if(isset($_GET['IDejercicio']) && isset($_GET['metodo']) && isset($_GET['asiento'])){
	$IDejercicio = $_GET['IDejercicio'];
	$metodo = $_GET['metodo'];
	$asiento = $_GET['asiento'];
} else {
	header("Location: portada.php?ejercicio=false");
	exit;	
}
//fin

###################

// Extraer fecha del asiento
$queryFecha = "SELECT Fasiento, redaccion, status FROM rom_asiento WHERE asientoR = '$asiento' AND Easiento = '$IDejercicio'";
$resultFecha = mysql_query($queryFecha);
$rowFecha = mysql_fetch_assoc($resultFecha);
$fechaAsiento = $rowFecha['Fasiento'];
$statusAsiento = $rowFecha['status']; // status del asiento ** IMPORTANTE **

// pintar navegación de plantillas
$queryP		= "SELECT * FROM rom_planillas WHERE Mplanilla = 4 OR Mplanilla = '$metodo' ORDER BY IDplanilla ASC";
$resultP	= mysql_query($queryP) or die (mysql_error());
$rowP		= mysql_fetch_assoc($resultP);

// extraer datos para llenar la balanza
// NOTA: asumimos que todos los asientos están cerrados
$queryCuentas = "SELECT DISTINCT cuenta FROM rom_cantidades
				WHERE subcuenta = '' AND ejercicio = '$IDejercicio'
				ORDER BY cuenta ASC";
$resulCuentas = mysql_query($queryCuentas);
$datosCuentas = mysql_fetch_assoc($resulCuentas);

// extraer total
$totalMd = 0;
$totalMh = 0;
$totalSd = 0;
$totalSh = 0;

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sistema Contable Romero</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="libreria/js_principal.js"></script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Balanza antes de ajustes</h1>
    </div>
    <div id="divTitularMenu"><a href="portada.php">Inicio</a> | <a href="libreria/calculadora/calculadora.html" target="_blank" onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | <a href="diarioFeAs.php?IDejercicio=<?php echo $IDejercicio; ?>&metodo=<?php echo $metodo; ?>">Asientos</a> | <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido">
<div class="divNavPlan">
  <form id="navPlan" name="navPlan" method="post" action="<?php $PHP_SELF; ?>">
    Navegación por plantillas:
    <select name="planilla" class="campoSelectPlanilla" id="planilla">
      <option selected="selected">Seleccione una...</option>
      <?php
	  do {
		  echo '<option value="'. $rowP['link'] .'">'. utf8_encode($rowP['Nplanilla']) .'</option>';
	  } while ($rowP = mysql_fetch_assoc($resultP));
	  ?>
    </select>
    <input name="IDejercicio" type="hidden" id="IDejercicio" value="<?php echo $IDejercicio; ?>" />
    <input name="metodo" type="hidden" id="metodo" value="<?php echo $metodo; ?>" />
    <input name="asiento" type="hidden" id="asiento" value="<?php echo $asiento; ?>" />
<input type="submit" name="subNav" id="subNav" value="ir" />
  </form>
</div>
<div class="divContCuerpo" id="divSupCuerpo"><strong>Fecha</strong>: <?php arregloFecha($fechaAsiento); ?>.<strong> Método</strong>: <?php pintarMetodo($metodo); ?>. <strong>Status</strong>: <?php echo ($statusAsiento==0)?"Abierto":"Cerrado"; ?> 
  <strong>Ejercicio</strong>: <?php pintarNejercicio($IDejercicio); ?><br />
  [ <a href="diarioFeAs.php?IDejercicio=<?php echo $IDejercicio; ?>&metodo=<?php echo $metodo; ?>">Crear un nuevo asiento | ver asientos anteriores</a> ]</div>  
<div class="divContCuerpo"><a href="verImp_balanza_aa.php?IDejercicio=<?php echo $IDejercicio; ?>&metodo=<?php echo $metodo; ?>&asiento=<?php echo $asiento; ?>" target="_blank"><img src="imagenes/verImp.gif" alt="Versión imprimible" width="150" height="25" border="0" title="Versión imprimible" /></a></div>
<div class="divContCuerpo">
    <fieldset class="mostrar-modulos">
  <legend>Registros del ejercicio</legend>
  <table border="0" cellpadding="0" cellspacing="3" class="tablaLista">
      <tr>
        <td colspan="2" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="center" valign="middle"><strong>Movimientos</strong></td>
        <td colspan="2" align="center" valign="middle"><strong>Saldos</strong></td>
      </tr>
      <tr>
        <td align="center" valign="middle" class="celdaListaTit"><strong>Clave</strong></td>
        <td align="center" valign="middle" class="celdaListaTit"><strong>Cuenta</strong></td>
        <td align="center" valign="middle" class="celdaListaDatA"><strong>Debe</strong></td>
        <td align="center" valign="middle" class="celdaListaDatV"><strong>Haber</strong></td>
        <td align="center" valign="middle" class="celdaListaDatA"><strong>Debe</strong></td>
        <td align="center" valign="middle" class="celdaListaDatV"><strong>Haber</strong></td>
      </tr><?php do {
				$cuenta = $datosCuentas['cuenta']; // cuenta de la iteración
				// DEBE
				$queryMd = "SELECT SUM(cantidad) AS Md FROM rom_cantidades
							WHERE tipo = 'd' AND cuenta = '$cuenta' AND ejercicio = '$IDejercicio'";
				$resulMd = mysql_query($queryMd);
				$datosMd = mysql_fetch_array($resulMd);
				$Md = $datosMd['Md']; // movimiento DEBE
				// HABER
				$queryMh = "SELECT SUM(cantidad) AS Mh FROM rom_cantidades
							WHERE tipo = 'h' AND cuenta = '$cuenta' AND ejercicio = '$IDejercicio'";
				$resulMh = mysql_query($queryMh);
				$datosMh = mysql_fetch_array($resulMh);
				$Mh = $datosMh['Mh']; // movimiento HABER
				// SALDOS
				if($Md > $Mh){
					$Sd = $Md - $Mh;
					$Sh = 0;
				} else {
					$Sh = $Mh - $Md;
					$Sd = 0;
				}
				// totales
				$totalMd += $Md;
				$totalMh += $Mh;
				$totalSd += $Sd;
				$totalSh += $Sh;
				?>
      <tr>
        <td align="left" valign="top" class="celdaListaDat"><?php echo $cuenta; ?></td>
        <td align="left" valign="top" class="celdaListaDat"><?php mostrarNombre($cuenta); ?></td>
        <td align="left" valign="top" class="celdaListaDatA">$ <?php echo number_format($Md,2) ?></td>
        <td align="left" valign="top" class="celdaListaDatV">$ <?php echo number_format($Mh,2) ?></td>
        <td align="left" valign="top" class="celdaListaDatA">$ <?php echo number_format($Sd,2) ?></td>
        <td align="left" valign="top" class="celdaListaDatV">$ <?php echo number_format($Sh,2) ?></td>
      </tr><?php } while($datosCuentas = mysql_fetch_assoc($resulCuentas)); ?>
	  <tr>
	    <td colspan="2" align="right" valign="top">&nbsp;</td>
	    <td align="left" valign="top">&nbsp;</td>
	    <td align="left" valign="top">&nbsp;</td>
	    <td align="left" valign="top">&nbsp;</td>
	    <td align="left" valign="top">&nbsp;</td>
      </tr>
	  <tr>
        <td colspan="2" align="right" valign="middle" class="celdaListaDat"><strong>Sumas iguales:</strong></td>
        <td align="left" valign="top" class="celdaListaDatA"><strong>$ <?php echo number_format($totalMd,2); ?></strong></td>
        <td align="left" valign="top" class="celdaListaDatV"><strong>$ <?php echo number_format($totalMh,2); ?>
		</strong></td>
        <td align="left" valign="top" class="celdaListaDatA"><strong>$ <?php echo number_format($totalSd,2); ?></strong></td>
        <td align="left" valign="top" class="celdaListaDatV"><strong>$ <?php echo number_format($totalSh,2); ?></strong></td>
	  </tr>
    </table>
  </fieldset></div>
<div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
</body>
</html>