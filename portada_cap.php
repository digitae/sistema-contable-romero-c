<?php
include("libreria/principal.php");

esUsuario(); // será usuario registrado?

$IDusuario = $_SESSION['userID'];

/**
 * AGREGAR UN NUEVO EJERCICIO COMPLETO
 */
$submit = filter_input(INPUT_POST, 'submit');
if(isset($submit))
{
    $Nejercicio = strtolower(utf8_decode(filter_input(INPUT_POST, 'Nejercicio',
            FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_LOW)));
    $Mejercicio = filter_input(INPUT_POST, 'Mejercicio',
            FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_HIGH);
    switch ($Mejercicio) {
        case 'global':
            $m = 2;
            break;
        case 'perpetuo':
            $m = 3;
            break;
        default :
            $m = 1;
            break;
    }
    $query = "INSERT INTO rom_ejercicio_pruebas "
            . "(Mejercicio,Nejercicio,Fejercicio,IDusuario) VALUES "
            . "($m,'$Nejercicio',NOW(),$IDusuario)";
    if(mysql_query($query) or die(mysql_error()))
    {
        header("Location: portada_cap.php?ejeAgregado=true");
        exit;
    }
}
// fin

/**
 * EXTRAER EJERCICIOS COMPLETOS
 * 
 * Función para extraer los ejercicios completos
 * del usuario. Mostrarlos en un campo <code><select></code>.
 * Y si no hay ejercicios, no mostrar el campo.
 * @author Fernando Magrosoto V.
 * @copyright (c) 2014, Fernando Magrosoto
 * @category Sistema Romero C
 * @param int $IDusuario El ID del usuario conectado
 */
function mostrarEjercicios($IDusuario)
{
    $lista = "";
    $query = "SELECT IDejercicio, Mejercicio, Nejercicio "
            . "FROM rom_ejercicio_pruebas "
            . "WHERE IDusuario = $IDusuario "
            . "ORDER BY Nejercicio ASC";
    $result = mysql_query($query) or die(mysql_error());
    $num = mysql_num_rows($result);
    
    if($num != 0)
    {
        $opciones = "";
        while($data = mysql_fetch_assoc($result))
        {
            $valor = $data['IDejercicio']."|".$data['Mejercicio'];
            $opciones .= "<option value='".$valor."'>"
                    . utf8_encode(ucwords($data['Nejercicio']))
                    ."</option>".PHP_EOL;
        }
        $lista .= <<<LISTA
                <div style="margin-bottom: 10px;">
                  <select name="ejercicio" onchange="abrirEjercicio(this.value);">
                      <option value="false">Seleccionar un ejercicio...</option>
                      {$opciones}
                  </select>
              </div>
LISTA;
    }
    return $lista;
}
// fin función

$ejeAgregado = filter_input(INPUT_GET, 'ejeAgregado',
        FILTER_VALIDATE_BOOLEAN,FILTER_NULL_ON_FAILURE);

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sistema Contable Romero</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="libreria/js_principal.js"></script>
<script>
    var flipEjercicio = function()
    {
        var boton = document.getElementById('bot-nuevo-ejercicio');
        var form = document.getElementById('nuevo-ejercicio');
        var estadoForm = form.style.display;
        if(estadoForm === 'none')
        {
            form.style.display = 'block';
            boton.style.display = 'none';
            document.forms['nuevo-ejercicio']['nombre'].focus();
        } else {
            form.style.display = 'none';
            boton.style.display = 'block';
            document.forms['nuevo-ejercicio']['nombre'].value = "";
        }
    };
    var Validar = function(theForm)
    {
        if(theForm.Nejercicio.value === '')
        {
            alert('El campo NOMBRE está vacío.');
            theForm.Nejercicio.focus();
            return (false);
        }
        var m = document.querySelector('input[name="Mejercicio"]:checked').value;
        console.log(m);
        if(m !== 'analitico' && m !== 'global' && m !== 'perpetuo')
        {
            alert('El MÉTODO no es válido.');
            return (false);
        }
        return (true);
    };
    
    var abrirEjercicio = function(valor)
    {
        var parametros = valor.split('|');
        var id = parametros[0];
        var me = parametros[1];
        window.location = "diarioFeAs_pruebas.php?IDejercicio="+id+"&metodo="+me;
    };
</script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Portada</h1>
    </div>
    <div id="divTitularMenu">
        <a href="portada.php">Inicio</a> | 
        <a href="libreria/calculadora/calculadora.html" 
           target="_blank" 
           onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | 
        <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido">
  <div class="divContCuerpo">
      <div style="float: left; margin: 25px 0;">
          <img class="img-portada" src="imagenes/porPrincipiosConta.jpg" alt="" height="375" />
      </div>
      <div style="float: left; margin-left: 50px; margin-top: 25px; width: 400px">
          
          <h3 class="titulo-principal">Capítulos 11 y 12</h3>
          <div class="migas">
              <ul>
                  <li><a href="portada.php">Inicio</a></li>
                  <li><a href="javascript:history.back()">Página anterior</a></li>
              </ul>
          </div>
          
          <fieldset class="mostrar-modulos" style="margin-top: 20px; font-size: 16px;">
              <legend>Ejercicios de prácticas</legend>
              <?php echo mostrarEjercicios($IDusuario) ?>
              <div id="bot-nuevo-ejercicio">
                  <ul>
                      <li><a onclick="flipEjercicio();" 
                             href="javascript:void(0);">
                              Crear una nueva práctica</a></li>
                  </ul>
              </div>
              <div id="nuevo-ejercicio" style="margin-top: 20px; display: none;">
                  <form class="form-2014" name="nuevo-ejercicio" 
                        action="portada_cap.php" method="POST" 
                        onsubmit="return Validar(this);" autocomplete="off">
                      <fieldset>
                          <legend>Nueva práctica</legend>
                          <div>
                              <input type="text" 
                                     name="Nejercicio" 
                                     required 
                                     placeholder="Nombre del ejercicio" />
                          </div>
                          <div>
                              <label for="mA">Analítico</label>
                              <input type="radio" 
                                     checked 
                                     name="Mejercicio" 
                                     value="analitico"
                                     id="mA" /><br />
                              <label for="mG">Global</label>
                              <input type="radio" 
                                     name="Mejercicio" 
                                     value="global"
                                     id="mG" /><br />
                              <label for="mP">Perpétuo</label>
                              <input type="radio" 
                                     name="Mejercicio" 
                                     value="perpetuo"
                                     id="mP" />
                          </div>
                          <div>
                              <button type="submit" name="submit">Agregar ejercicio</button>
                              <a style="margin-left: 10px" 
                                 onclick="flipEjercicio();" 
                                 href="javascript:void(0);">Cancelar</a>
                          </div>
                      </fieldset>
                  </form>
              </div>
          </fieldset>
          
      </div>
  </div>
  <!-- -->
  <div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
<!-- msg -->
<?php
if($ejeAgregado)
{
    echo "<div id='msg-done'>Ejercicio agregado favorablemente.</div>".PHP_EOL;
    echo <<<SCRIPT
    <script>
    window.setTimeout(function(){
            var elemento = document.getElementById('msg-done');
            elemento.parentNode.removeChild(elemento);
        },3000);
    </script>
SCRIPT;
    echo PHP_EOL;
}
?>
<!-- fin -->
</body>
</html>