<?php
include("libreria/principal.php");

esUsuario(); // será usuario registrado?

###################
## SCRIPT GENERAL ##

// Navegación por planillas
if(isset($_POST['subNav'])){
	$planilla = $_POST['planilla'];
	$IDejercicio = $_POST['IDejercicio'];
	$metodo = $_POST['metodo'];
	$asiento = $_POST['asiento'];
	
	header("Location: $planilla?IDejercicio=$IDejercicio&metodo=$metodo&asiento=$asiento");
	exit;		
}

// comprobar que han iniciado un ejercicio
if(isset($_GET['IDejercicio']) && isset($_GET['metodo']) && isset($_GET['asiento'])){
	$IDejercicio = $_GET['IDejercicio'];
	$metodo = $_GET['metodo'];
	$asiento = $_GET['asiento'];
} else {
	header("Location: portada.php?ejercicio=false");
	exit;	
}
//fin
###################

## IMPORTANTE ##
// saber si hay algún ASIENTO DE AJUSTES realizado
$queryAjuste = "SELECT COUNT(IDajuste) AS total FROM rom_ajustes WHERE ejercicioAj = '$IDejercicio'";
$resultAjuste = mysql_query($queryAjuste);
$datoAjuste = mysql_fetch_array($resultAjuste);
//

if($datoAjuste['total'] != 0){ // si ya hay...
## VIENE LO BUENO ##
// El reto es: pasar la tabla de balanza_aa
// a la nueva tabla de balanza_da
// pero agregarle las nuevas cuentas que se hubieran afectado
// en ASIENTO DE AJUSTES.
// Y de una vez pasar los SALDOS de balanza_aa
// a las primeras columnas de balanza_ba
// y también de una vez calcular los saldos con respecto a
// los ajustes y ponerlos en las columnas de la derecha

	// PRIMERO revisar si ya hay un registro en balanza_da del ejercicio
	$queryExistente = "SELECT COUNT(IDbalanza_da) AS numDatos FROM rom_balanza_da WHERE ejercicio = '$IDejercicio'";
	$resulExistente = mysql_query($queryExistente) or die(mysql_error());
	$datosExistente = mysql_fetch_array($resulExistente);
	$numDatos = $datosExistente['numDatos'];
	
	if($numDatos == 0){ // si no hay datos...
		// primero pasamos tal cual la tabla de balanza_aa a una nueva en balanza_da
		// luego los editamos
		$queryBaa = "SELECT clave, Sdebe, Shaber FROM rom_balanza_aa WHERE ejercicio = '$IDejercicio'";
		$resulBaa = mysql_query($queryBaa) or die(mysql_error());
		$datosBaa = mysql_fetch_array($resulBaa);
		
		do {
			// metemos cada fila en balanza_da por cada iteracion
			// pero ya con todo y saldo
			$clave = $datosBaa['clave'];
			$Mdebe = $datosBaa['Sdebe'];
			$Mhaber = $datosBaa['Shaber'];
			if($Mdebe > $Mhaber){
				$Mdebe = $Mdebe - $Mhaber;
				$Mhaber = 0;
			} else {
				$Mhaber = $Mhaber - $Mdebe;
				$Mdebe = 0;
			}
			// SALDOS
			if($Mdebe > $Mhaber) {
				$Sdebe = $Mdebe - $Mhaber;
				$Shaber = 0;
			} else {
				$Sdebe = 0;
				$Shaber = $Mhaber - $Mdebe;
			}
			$queryNew = "INSERT INTO rom_balanza_da
						(clave, Mdebe, Mhaber, Sdebe, Shaber, ejercicio)
						VALUES
						('$clave', '$Mdebe', '$Mhaber', '$Sdebe', '$Shaber', '$IDejercicio')";
			mysql_query($queryNew) or die(mysql_error());
		} while($datosBaa = mysql_fetch_array($resulBaa));
		## hasta aquí, todo bien ##
		
		// luego, calculamos los saldos con AJUSTES,
		// primero recorremos AJUSTES para saber que cuentas hay...
		$qCuentasAjustes = "SELECT DISTINCT cuentaAj FROM rom_ajustes WHERE ejercicioAj = '$IDejercicio'";
		$rCuentasAjustes = mysql_query($qCuentasAjustes) or die(mysql_error());
		$dCuentasAjustes = mysql_fetch_array($rCuentasAjustes);
		do {
			// y por cada iteración comprobamos que hay coincidencias en balanza_da
			$cuenta = $dCuentasAjustes['cuentaAj'];
			$bCoinBda = "SELECT Mdebe, Mhaber FROM rom_balanza_da WHERE clave = '$cuenta' AND ejercicio = '$IDejercicio'";
			$rCoinBda = mysql_query($bCoinBda) or die(mysql_error());
			$dCoinBda = mysql_fetch_assoc($rCoinBda);
			$nCoinBda = mysql_num_rows($rCoinBda);
			
			if($nCoinBda == 0) { // si no hay coincidencias, entonces metemos un nuevo registro a balanza_da
				// sacamos las sumas de Debe en AJUSTES
				$qSumaD = "SELECT SUM(cantidadAj) AS totalD FROM rom_ajustes WHERE cuentaAj = '$cuenta' AND tipoAj = 'd' AND ejercicioAj = '$IDejercicio'";
				$rSumadD = mysql_query($qSumaD) or die(mysql_error());
				$dSumaD = mysql_fetch_array($rSumadD);
				$totalD = $dSumaD['totalD']; // totalD
				// sacamos las sumas de Haber en AJUSTES
				$qSumaH = "SELECT SUM(cantidadAj) AS totalH FROM rom_ajustes WHERE cuentaAj = '$cuenta' AND tipoAj = 'h' AND ejercicioAj = '$IDejercicio'";
				$rSumadH = mysql_query($qSumaH) or die(mysql_error());
				$dSumaH = mysql_fetch_array($rSumadH);
				$totalH = $dSumaH['totalH']; // totalH
				// sacamos el saldo
				if($totalD > $totalH){
					$saldoD = $totalD - $totalH;
					$saldoH = 0;
				} else {
					$saldoD = 0;
					$saldoH = $totalH - $totalD;
				}
				// insertamos el nuevo registro en balanza_da
				$nuevo = "INSERT INTO rom_balanza_da (clave, Mdebe, Mhaber, Sdebe, Shaber, ejercicio)
							VALUES ('$cuenta', '$totalD', '$totalH', '$saldoD', '$saldoH', '$IDejercicio')";
				mysql_query($nuevo) or die(mysql_error());
				// listo!
				## hasta aquí, todo bien ##
						
			} else { // ahora, si sí hay coincidencias...
				// sacamos los movimientos de balanza_da
				$qMovBda = "SELECT Mdebe, Mhaber FROM rom_balanza_da WHERE clave = '$cuenta' AND ejercicio = '$IDejercicio'";
				$rMovBda = mysql_query($qMovBda) or die(mysql_error());
				$dMovBda = mysql_fetch_array($rMovBda);
				$Mdebe = $dMovBda['Mdebe']; // saldos iniciales
				$Mhaber = $dMovBda['Mhaber']; // saldos iniciales
				
				// sacamos las sumas de Debe en AJUSTES
				$qSumaD = "SELECT SUM(cantidadAj) AS totalD FROM rom_ajustes WHERE cuentaAj = '$cuenta' AND tipoAj = 'd' AND ejercicioAj = '$IDejercicio'";
				$rSumadD = mysql_query($qSumaD) or die(mysql_error());
				$dSumaD = mysql_fetch_array($rSumadD);
				$totalD = $dSumaD['totalD']; // totalD
				
				// sacamos las sumas de Haber en AJUSTES
				$qSumaH = "SELECT SUM(cantidadAj) AS totalH FROM rom_ajustes WHERE cuentaAj = '$cuenta' AND tipoAj = 'h' AND ejercicioAj = '$IDejercicio'";
				$rSumadH = mysql_query($qSumaH) or die(mysql_error());
				$dSumaH = mysql_fetch_array($rSumadH);
				$totalH = $dSumaH['totalH']; // totalH
				
				// CASO 1 - Si el saldo de balanza_da es DEBE
				if($Mdebe > 0){
					$Mdebe = $Mdebe + $totalD;
					$Mhaber = $totalH;
					// de una vez, sacamos saldos
					if($Mdebe > $Mhaber){
						$Sdebe = $Mdebe - $Mhaber;
						$Shaber = 0;
					} else {
						$Sdebe = 0;
						$Shaber = $Mhaber - $Mdebe;
					}
				} elseif($Mhaber > 0) { // CASO 2 - Si el saldo de balanza_da es HABER
					$Mdebe = $totalD;
					$Mhaber = $Mhaber + $totalH;
					// de una vez, sacamos los saldos
					if($Mdebe > $Mhaber){
						$Sdebe = $Mdebe - $Mhaber;
						$Shaber = 0;
					} else {
						$Sdebe = 0;
						$Shaber = $Mhaber - $Mdebe;
					}
				} elseif($Mdebe == 0 || $Mhaber == 0){ // CASO 3 - PARA LOS NÚMEROS 0
					$Mdebe = $totalD;
					$Mhaber = $totalH;
					if($Mdebe > $Mhaber){
						$Sdebe = $Mdebe - $Mhaber;
						$Shaber = 0;
					} else {
						$Sdebe = 0;
						$Shaber = $Mhaber - $Mdebe;
					}
				
				} // fin de los casos
				// ahora, los editamos en la balanza_da
				$editar = "UPDATE rom_balanza_da SET
							Mdebe = '$Mdebe',
							Mhaber = '$Mhaber',
							Sdebe = '$Sdebe',
							Shaber = '$Shaber'
							WHERE clave = '$cuenta' AND ejercicio = '$IDejercicio'";
				mysql_query($editar) or die(mysql_error());				
			}
			// listo!			
		} while($dCuentasAjustes = mysql_fetch_array($rCuentasAjustes));
		// fin del loop
		
		// finalmente, sacamos la lista de registro
		$queryBda = "SELECT * FROM rom_balanza_da WHERE clave NOT LIKE '4208' AND ejercicio = '$IDejercicio' ORDER BY clave ASC";
		$resulBda = mysql_query($queryBda) or die(mysql_error());
		$datosBda = mysql_fetch_array($resulBda);
		
	} else { // si sí hay balanza_da...
		// sacamos la lista de registros
		$queryBda = "SELECT * FROM rom_balanza_da WHERE clave NOT LIKE '4208' AND ejercicio = '$IDejercicio' ORDER BY clave ASC";
		$resulBda = mysql_query($queryBda) or die(mysql_error());
		$datosBda = mysql_fetch_array($resulBda);
	}

// fin
} // si no hay ASIENTO DE AJUSTES, no hace nada

// Extraer fecha del asiento
$queryFecha = "SELECT Fasiento, redaccion, status FROM rom_asiento WHERE asientoR = '$asiento' AND Easiento = '$IDejercicio'";
$resultFecha = mysql_query($queryFecha);
$rowFecha = mysql_fetch_assoc($resultFecha);
$fechaAsiento = $rowFecha['Fasiento'];
$statusAsiento = $rowFecha['status']; // status del asiento ** IMPORTANTE **

// pintar navegación de plantillas
$queryP		= "SELECT * FROM rom_planillas WHERE Mplanilla = 4 OR Mplanilla = '$metodo' ORDER BY IDplanilla ASC";
$resultP	= mysql_query($queryP) or die (mysql_error());
$rowP		= mysql_fetch_assoc($resultP);

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sistema Contable Romero</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="libreria/js_principal.js"></script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Balanza después de ajustes</h1>
    </div>
    <div id="divTitularMenu"><a href="portada.php">Inicio</a> | <a href="libreria/calculadora/calculadora.html" target="_blank" onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | <a href="diarioFeAs.php?IDejercicio=<?php echo $IDejercicio; ?>&metodo=<?php echo $metodo; ?>">Asientos</a> | <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido">
<div class="divNavPlan">
  <form id="navPlan" name="navPlan" method="post" action="<?php $PHP_SELF; ?>">
    Navegación por plantillas:
    <select name="planilla" class="campoSelectPlanilla" id="planilla">
      <option selected="selected">Seleccione una...</option>
      <?php
	  do {
		  echo '<option value="'. $rowP['link'] .'">'. utf8_encode($rowP['Nplanilla']) .'</option>';
	  } while ($rowP = mysql_fetch_assoc($resultP));
	  ?>
    </select>
    <input name="IDejercicio" type="hidden" id="IDejercicio" value="<?php echo $IDejercicio; ?>" />
    <input name="metodo" type="hidden" id="metodo" value="<?php echo $metodo; ?>" />
    <input name="asiento" type="hidden" id="asiento" value="<?php echo $asiento; ?>" />
<input type="submit" name="subNav" id="subNav" value="ir" />
  </form>
</div><?php if($datoAjuste['total'] == 0) { ?>
<div class="divContCuerpoInfo"><strong>Actualmente no hay ASIENTOS DE AJUSTE elaborados.</strong><br />
Favor de crear uno.</div>
<?php } ?>
<div class="divContCuerpo" id="divSupCuerpo"><strong>Fecha</strong>: 
  <?php arregloFechaAs(fechasAs($IDejercicio)); ?>
  . <strong>Método</strong>: <?php pintarMetodo($metodo); ?>. <strong>Status</strong>: <?php
    if($statusAsiento==0) {
		echo "abierto";
		} else {
		echo "cerrado";	
		}
		
		?> 
  <strong>Ejercicio</strong>: <?php pintarNejercicio($IDejercicio); ?></div>  
<?php if($datoAjuste['total'] != 0) { ?>
<div class="divContCuerpo"><fieldset class="mostrar-modulos">
  <legend>Registros del ejercicio</legend>
  <table border="0" cellpadding="0" cellspacing="3" class="tablaLista">
      <tr>
        <td colspan="2" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="center" valign="middle"><strong>Movimientos</strong></td>
        <td colspan="2" align="center" valign="middle"><strong>Saldos</strong></td>
      </tr>
      <tr>
        <td align="center" valign="middle" class="celdaListaTit"><strong>Clave</strong></td>
        <td align="center" valign="middle" class="celdaListaTit"><strong>Cuenta</strong></td>
        <td align="center" valign="middle" class="celdaListaDatA"><strong>Debe</strong></td>
        <td align="center" valign="middle" class="celdaListaDatV"><strong>Haber</strong></td>
        <td align="center" valign="middle" class="celdaListaDatA"><strong>Debe</strong></td>
        <td align="center" valign="middle" class="celdaListaDatV"><strong>Haber</strong></td>
      </tr><?php do { ?>
      <tr>
        <td align="left" valign="top" class="celdaListaDat"><?php echo $datosBda['clave']; ?></td>
        <td align="left" valign="top" class="celdaListaDat"><?php mostrarNombre($datosBda['clave']); ?></td>
        <td align="left" valign="top" class="celdaListaDatA">$ <?php echo number_format($datosBda['Mdebe'],2); ?></td>
        <td align="left" valign="top" class="celdaListaDatV">$ <?php echo number_format($datosBda['Mhaber'],2); ?></td>
        <td align="left" valign="top" class="celdaListaDatA">$ <?php echo number_format($datosBda['Sdebe'],2); ?></td>
        <td align="left" valign="top" class="celdaListaDatV">$ <?php echo number_format($datosBda['Shaber'],2); ?></td>
      </tr>
      <?php } while($datosBda = mysql_fetch_array($resulBda));
	  // ahora, extraer la cuenta 4208, que debe de ser la ultima...
	  $queryPGN = "SELECT * FROM rom_balanza_da WHERE clave = 4208 AND ejercicio = '$IDejercicio'";
	  $resulPGN = mysql_query($queryPGN);
	  $datosPGN = mysql_fetch_assoc($resulPGN);
	  $numPGN = mysql_num_rows($resulPGN);
	  if($numPGN != 0) { ?>
	        <tr>
        <td align="left" valign="top" class="celdaListaDat"><?php echo $datosPGN['clave']; ?></td>
        <td align="left" valign="top" class="celdaListaDat"><?php mostrarNombre($datosPGN['clave']); ?></td>
        <td align="left" valign="top" class="celdaListaDatA">$ <?php echo number_format($datosPGN['Mdebe'],2); ?></td>
        <td align="left" valign="top" class="celdaListaDatV">$ <?php echo number_format($datosPGN['Mhaber'],2); ?></td>
        <td align="left" valign="top" class="celdaListaDatA">$ <?php echo number_format($datosPGN['Sdebe'],2); ?></td>
        <td align="left" valign="top" class="celdaListaDatV">$ <?php echo number_format($datosPGN['Shaber'],2); ?></td>
      </tr>
	  <?php } ?>
	  <tr>
	    <td colspan="2" align="right" valign="top">&nbsp;</td>
	    <td align="left" valign="top">&nbsp;</td>
	    <td align="left" valign="top">&nbsp;</td>
	    <td align="left" valign="top">&nbsp;</td>
	    <td align="left" valign="top">&nbsp;</td>
      </tr>
	  <tr>
        <td colspan="2" align="right" valign="middle" class="celdaListaDat"><strong>Sumas iguales:</strong></td>
        <td align="left" valign="top" class="celdaListaDatA"><strong>$ 
          <?php mostrarTotalAllBda("Mdebe", $IDejercicio); ?>
        </strong></td>
        <td align="left" valign="top" class="celdaListaDatV"><strong>$ </strong><strong>
          <?php mostrarTotalAllBda("Mhaber", $IDejercicio); ?>
        </strong></td>
        <td align="left" valign="top" class="celdaListaDatA"><strong>$ 
          <?php mostrarTotalAllBda("Sdebe", $IDejercicio); ?>
        </strong></td>
        <td align="left" valign="top" class="celdaListaDatV"><strong>$ 
          <?php mostrarTotalAllBda("Shaber", $IDejercicio); ?>
        </strong></td>
	  </tr>
    </table>
  </fieldset></div>
  <?php } ?>
  <div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
</body>
</html>