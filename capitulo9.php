<?php
include 'libreria/principal.php';
include 'libreria/evaluaciones.php';

esUsuario(); // será usuario registrado?
$IDusuario = $_SESSION['userID'];
$capitulo = 9;
$registro = verificarCuestionario($capitulo, $IDusuario,  utf8_decode($Fecha));


?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> Principios de Contabilidad. Cuestionario de evaluación. Capítulo 9</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="libreria/js_principal.js"></script>
<script src="libreria/vendor/jquery/jquery-1.11.0.min.js"></script>
<script src="libreria/calCap.js"></script>
<script src="libreria/rc9.js"></script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Cuestionario de autoevaluación</h1>
    </div>
    <div id="divTitularMenu">
        <a href="portada.php">Inicio</a> | 
        <a href="libreria/calculadora/calculadora.html" 
           target="_blank" 
           onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | 
        <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido" style="width: 900px">
  <div class="divContCuerpo">
      
      <h3 class="titulo-principal">Capítulo 9</h3>
      <div class="migas">
          <ul>
              <li><a href="portada.php">Portada</a></li>
              <li><a href="javascript:history.back();">Página anterior</a></li>
              <li><img src="imagenes/print.png" 
                       alt="" 
                       height="25" 
                       style="float: left; margin-right: 5px;" />
                  <a href="javascript:print();">Imprimir página</a></li>
          </ul>
      </div>
      
      <table class="mis-datos">
          <tr>
              <td>Universidad o Institución:</td>
              <td>
                  <input oninput="cCamCues('unidad',this.value,<?php echo $registro ?>);" 
                         name="unidad" 
                         id='unidad' 
                         required 
                         value="<?php echo xCamCues('unidad', $registro) ?>" />
              </td>
              <td>Escuela o facultad:</td>
              <td>
                  <input oninput="cCamCues('facultad',this.value,<?php echo $registro ?>);" 
                         name="facultad" 
                         id='facultad' 
                         required 
                         value="<?php echo xCamCues('facultad', $registro) ?>" />
              </td>
          </tr>
          <tr>
              <td>Asignatura:</td>
              <td>
                  <input oninput="cCamCues('asignatura',this.value,<?php echo $registro ?>);" 
                         name="asignatura" 
                         id='asignatura' 
                         required 
                         value="<?php echo xCamCues('asignatura', $registro) ?>" />
              </td>
              <td>Alumno:</td>
              <td>
                  <?php echo nombreUsuario($IDusuario) ?>
              </td>
          </tr>
          <tr>
              <td>Maestro:</td>
              <td>
                  <input oninput="cCamCues('maestro',this.value,<?php echo $registro ?>);" 
                         name="maestro" 
                         id='maestro' 
                         required 
                         value="<?php echo xCamCues('maestro', $registro) ?>" />
              </td>
              <td>Fecha:</td>
              <td>
                  <input oninput="cCamCues('fecha',this.value,<?php echo $registro ?>);" 
                         name="fecha" 
                         id='fecha' 
                         required 
                         value="<?php echo xCamCues('fecha', $registro) ?>" />
              </td>
          </tr>
      </table>
      
      <h4 class="subtit-4">OBJETIVOS DE APRENDIZAJE</h4>
      
      <div class="objetivos">
          <p>Al finalizar este capítulo, el alumno será capaz de:</p>
          <ol>
              <li>Conocer sus antecedentes y definirlo.</li>
              <li>Comprender su estructura básica.</li>
              <li>Distinguir las actividades de operación, inversión y financiamiento.</li>
              <li>Conocer las normas  de presentación.</li>
          </ol>
      </div>
      
      <h4 class="subtit-4">EJERCICIOS</h4>
      
      <h5>I. RESUELVA LO SIGUIENTE</h5>
      
      <div class="pregunta-libre">
          <p><span>1</span> ¿En qué fecha entró en vigor la NIF B-2?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib01" 
                    id="prelib01"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib01'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>2</span> ¿Cómo se define el estado de flujos de efectivo?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib02" 
                    id="prelib02"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib02'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>3</span> ¿Qué son los flujos de efectivo?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib03" 
                    id="prelib03"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib03'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>4</span> ¿Qué son las entradas de efectivo?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib04" 
                    id="prelib04"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib04'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>5</span> ¿Qué son las salidas de efectivo?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib05" 
                    id="prelib05"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib05'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>6</span> Señale cómo se clasifican y presentan los flujos de efectivo.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib06" 
                    id="prelib06"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib06'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>7</span> Defina actividades de operación.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib07" 
                    id="prelib07"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib07'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>8</span> Defina actividades de inversión.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib08" 
                    id="prelib08"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib08'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>9</span> Defina actividades de financiamiento.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib09" 
                    id="prelib09"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib09'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>10</span> Indique cuáles son los métodos para determinar y 
              presentar las actividades de operación.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib10" 
                    id="prelib10"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib10'); ?></textarea>
      </div>
      
      <h5>II. EJERCICIOS DE OPCIÓN MÚLTIPLE</h5>
      
      <table class="opcion-multiple-radio" id="opradio">
          <tr>
              <th colspan="2">1. Diga cuál de las siguientes operaciones es una 
                  entrada de efectivo.</th>
          </tr>
          <tr>
              <td id="opmulR_1a_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_1" value="a" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_1')==='a')?' checked':'' ?> />
              </td>
              <td>
                  a) Venta de mercancías a crédito.
              </td>
          </tr>
          <tr>
              <td id="opmulR_1b_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_1" value="b" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_1')==='b')?' checked':'' ?> />
              </td>
              <td>
                  b) Compra de edificios a crédito
              </td>
          </tr>
          <tr>
              <td id="opmulR_1c_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_1" value="c" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_1')==='c')?' checked':'' ?> />
              </td>
              <td>
                  c) Cobro a clientes
              </td>
          </tr>
          <tr>
              <td id="opmulR_1d_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_1" value="d" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_1')==='d')?' checked':'' ?> />
              </td>
              <td>
                  d) Pago a proveedores
              </td>
          </tr>
          
          <tr>
              <th colspan="2">2. Diga cuál de las siguientes operaciones es 
                  una salida de efectivo.</th>
          </tr>
          <tr>
              <td id="opmulR_2a_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_2" value="a" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_2')==='a')?' checked':'' ?> />
              </td>
              <td>
                  a) Venta de edificios a crédito.
              </td>
          </tr>
          <tr>
              <td id="opmulR_2b_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_2" value="b" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_2')==='b')?' checked':'' ?> />
              </td>
              <td>
                  b) Obtención de un préstamo a largo plazo.
              </td>
          </tr>
          <tr>
              <td id="opmulR_2c_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_2" value="c" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_2')==='c')?' checked':'' ?> />
              </td>
              <td>
                  c) Pago de gastos de venta.
              </td>
          </tr>
          <tr>
              <td id="opmulR_2d_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_2" value="d" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_2')==='d')?' checked':'' ?> />
              </td>
              <td>
                  d) Depreciación de equipo de reparto.
              </td>
          </tr>
          
          
          
          <tr>
              <th colspan="2">3. Señale cuál de las siguientes operaciones no 
                  es una entrada de efectivo.</th>
          </tr>
          <tr>
              <td id="opmulR_3a_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_3" value="a" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_3')==='a')?' checked':'' ?> />
              </td>
              <td>
                  a) Cobro de deudores.
              </td>
          </tr>
          <tr>
              <td id="opmulR_3b_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_3" value="b" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_3')==='b')?' checked':'' ?> />
              </td>
              <td>
                  b) Venta de equipo de cómputo al contado.
              </td>
          </tr>
          <tr>
              <td id="opmulR_3c_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_3" value="c" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_3')==='c')?' checked':'' ?> />
              </td>
              <td>
                  c) Cobro de intereses.
              </td>
          </tr>
          <tr>
              <td id="opmulR_3d_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_3" value="d" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_3')==='d')?' checked':'' ?> />
              </td>
              <td>
                  d) Estimación de cuentas de cobro dudoso de clientes.
              </td>
          </tr>
          
          
          
          <tr>
              <th colspan="2">4. Señale cuál de las siguientes operaciones no 
                  es una salida de efectivo.</th>
          </tr>
          <tr>
              <td id="opmulR_4a_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_4" value="a" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_4')==='a')?' checked':'' ?> />
              </td>
              <td>
                  a) Amortización de gastos de constitución.
              </td>
          </tr>
          <tr>
              <td id="opmulR_4b_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_4" value="b" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_4')==='b')?' checked':'' ?> />
              </td>
              <td>
                  b) Pago de gastos de operación.
              </td>
          </tr>
          <tr>
              <td id="opmulR_4c_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_4" value="c" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_4')==='c')?' checked':'' ?> />
              </td>
              <td>
                  c) Pago de intereses.
              </td>
          </tr>
          <tr>
              <td id="opmulR_4d_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_4" value="d" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_4')==='d')?' checked':'' ?> />
              </td>
              <td>
                  d) Pago a socios.
              </td>
          </tr>
          
          
          
          <tr>
              <th colspan="2">5. Diga cuál de las siguientes operaciones no es 
                  una actividad de operación.</th>
          </tr>
          <tr>
              <td id="opmulR_5a_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_5" value="a" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_5')==='a')?' checked':'' ?> />
              </td>
              <td>
                  a) Utilidad neta del ejercicio.
              </td>
          </tr>
          <tr>
              <td id="opmulR_5b_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_5" value="b" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_5')==='b')?' checked':'' ?> />
              </td>
              <td>
                  b) Compra de mercancías - aumento de inventarios.
              </td>
          </tr>
          <tr>
              <td id="opmulR_5c_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_5" value="c" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_5')==='c')?' checked':'' ?> />
              </td>
              <td>
                  c) Venta de terrenos.
              </td>
          </tr>
          <tr>
              <td id="opmulR_5d_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_5" value="d" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_5')==='d')?' checked':'' ?> />
              </td>
              <td>
                  d) Disminución de clientes - cobro a clientes -.
              </td>
          </tr>
          
          
          <tr>
              <th colspan="2">6. Diga cuál de las siguientes operaciones no es 
                  una actividad de operación.</th>
          </tr>
          <tr>
              <td id="opmulR_6a_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_6" value="a" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_6')==='a')?' checked':'' ?> />
              </td>
              <td>
                  a) Pérdida neta del ejercicio.
              </td>
          </tr>
          <tr>
              <td id="opmulR_6b_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_6" value="b" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_6')==='b')?' checked':'' ?> />
              </td>
              <td>
                  b) Obtención de préstamos a largo plazo.
              </td>
          </tr>
          <tr>
              <td id="opmulR_6c_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_6" value="c" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_6')==='c')?' checked':'' ?> />
              </td>
              <td>
                  c) Disminuciones de ISR y PTU por pagar (pago de ISR y PTU)
              </td>
          </tr>
          <tr>
              <td id="opmulR_6d_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_6" value="d" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_6')==='d')?' checked':'' ?> />
              </td>
              <td>
                  d) Disminuciones de proveedores (pago a proveedores)
              </td>
          </tr>
          
          
          <tr>
              <th colspan="2">7. De las siguientes operaciones indique cuál no 
                  debe clasificarse como actividad de inversión.</th>
          </tr>
          <tr>
              <td id="opmulR_7a_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_7" value="a" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_7')==='a')?' checked':'' ?> />
              </td>
              <td>
                  a) Compra de propiedades, planta y equipo (aumento de propiedades, planta y equipo).
              </td>
          </tr>
          <tr>
              <td id="opmulR_7b_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_7" value="b" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_7')==='b')?' checked':'' ?> />
              </td>
              <td>
                  b) Venta de intangibles (disminución de intangibles).
              </td>
          </tr>
          <tr>
              <td id="opmulR_7c_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_7" value="c" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_7')==='c')?' checked':'' ?> />
              </td>
              <td>
                  c)  Disminuciones de deudores (cobro a deudores).
              </td>
          </tr>
          <tr>
              <td id="opmulR_7d_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_7" value="d" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_7')==='d')?' checked':'' ?> />
              </td>
              <td>
                  d) Aumentos de capital social (aportaciones de socios).
              </td>
          </tr>
          
          
          <tr>
              <th colspan="2">8. De las siguientes operaciones indique cuál no 
                  debe clasificarse como actividad de inversión.</th>
          </tr>
          <tr>
              <td id="opmulR_8a_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_8" value="a" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_8')==='a')?' checked':'' ?> />
              </td>
              <td>
                  a) Disminución de documentos por pagar a largo plazo 
                  (pago de documentos por pagar a largo plazo).
              </td>
          </tr>
          <tr>
              <td id="opmulR_8b_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_8" value="b" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_8')==='b')?' checked':'' ?> />
              </td>
              <td>
                  b) Venta de propiedades, planta y equipo.
              </td>
          </tr>
          <tr>
              <td id="opmulR_8c_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_8" value="c" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_8')==='c')?' checked':'' ?> />
              </td>
              <td>
                  c) Préstamos a deudores (aumentos de deudores).
              </td>
          </tr>
          <tr>
              <td id="opmulR_8d_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_8" value="d" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_8')==='d')?' checked':'' ?> />
              </td>
              <td>
                  d) Compras o desarrollos de intangibles (aumentos de intangibles).
              </td>
          </tr>
          
          
          <tr>
              <th colspan="2">9. Señale cuál de las siguientes operaciones no 
                  es una actividad de financiamiento.</th>
          </tr>
          <tr>
              <td id="opmulR_9a_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_9" value="a" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_9')==='a')?' checked':'' ?> />
              </td>
              <td>
                  a) Incremento de la reserva legal.
              </td>
          </tr>
          <tr>
              <td id="opmulR_9b_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_9" value="b" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_9')==='b')?' checked':'' ?> />
              </td>
              <td>
                  b) Obtención de créditos a corto plazo, diferentes a 
                  operaciones con proveedores y acreedores (aumentos de pasivo a 
                  corto plazo).
              </td>
          </tr>
          <tr>
              <td id="opmulR_9c_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_9" value="c" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_9')==='c')?' checked':'' ?> />
              </td>
              <td>
                  c) Pagos por retiro de socios (reembolsos de capital).
              </td>
          </tr>
          <tr>
              <td id="opmulR_9d_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_9" value="d" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_9')==='d')?' checked':'' ?> />
              </td>
              <td>
                  d) Pago de intereses.
              </td>
          </tr>
          
          
          <tr>
              <th colspan="2">10. Señale cuál de las siguientes operaciones no 
                  es una actividad de financiamiento.</th>
          </tr>
          <tr>
              <td id="opmulR_10a_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_10" value="a" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_10')==='a')?' checked':'' ?> />
              </td>
              <td>
                  a) Aportaciones de socios (aumentos de capital contribuido).
              </td>
          </tr>
          <tr>
              <td id="opmulR_10b_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_10" value="b" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_10')==='b')?' checked':'' ?> />
              </td>
              <td>
                  b) Cobro de intereses.
              </td>
          </tr>
          <tr>
              <td id="opmulR_10c_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_10" value="c" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_10')==='c')?' checked':'' ?> />
              </td>
              <td>
                  c) Compra de mercancías.
              </td>
          </tr>
          <tr>
              <td id="opmulR_10d_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_10" value="d" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_10')==='d')?' checked':'' ?> />
              </td>
              <td>
                  d) Pagos de créditos a corto y largo plazos, sin incluir 
                  intereses diferentes a operaciones con proveedores y acreedores.
              </td>
          </tr>
          
          
          <tr>
              <th colspan="2">&nbsp;</th>
          </tr>
          <tr>
              <td>
                  <button onclick="calificarOpMulRadio('opmulR',10,rc9_omr,'opradio');">CALIFICAR</button>
              </td>
              <td id="opmulR_cal"></td>
          </tr>
      </table>
      
      <h5>III. RELACIONA LAS COLUMNAS</h5>
      
      En el siguiente cuadro, indique si la operación corresponde a una 
      aplicación o a un origen de efectivo.
      
      <table class="verdadero-falso" id="vf_simple01">
          <tr>
              <th>Operaciones</th>
              <th>Aplicación de efectivo</th>
              <th>Origen de efectivo</th>
          </tr>
          
          <!-- tanda -->
          <tr>
              <td>1. Pagos por retiro de socios – reembolsos de capital-, pago 
                  de dividendos (disminuciones de capital contribuido)</td>
              <td id="vf_simple01_verfal_1v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_1" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_1')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_1f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_1" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_1')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>2. Pagos de créditos a corto y largo plazos, sin incluir 
                  intereses  —diferentes a operaciones con proveedores y 
                  acreedores— (disminuciones  de pasivo a corto y largo plazos)</td>
              <td id="vf_simple01_verfal_2v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_2" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_2')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_2f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_2" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_2')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>3. Pago de intereses</td>
              <td id="vf_simple01_verfal_3v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_3" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_3')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_3f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_3" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_3')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>4. Ventas de propiedades, planta y equipo 
                  (disminución de activo no circulante)</td>
              <td id="vf_simple01_verfal_4v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_4" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_4')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_4f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_4" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_4')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>5. Ventas de intangibles (disminución de activo no circulante)</td>
              <td id="vf_simple01_verfal_5v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_5" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_5')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_5f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_5" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_5')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>6. Disminuciones de deudores  —cobro  a deudores, sin incluir 
                  intereses (disminución de activo)— </td>
              <td id="vf_simple01_verfal_6v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_6" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_6')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_6f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_6" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_6')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>7. Compras de propiedades, planta y equipo  (aumento de 
                  activo no circulante)</td>
              <td id="vf_simple01_verfal_7v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_7" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_7')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_7f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_7" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_7')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>8. Compras o desarrollos de intangibles (aumento de 
                  activo no circulante)</td>
              <td id="vf_simple01_verfal_8v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_8" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_8')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_8f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_8" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_8')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>9. Préstamos efectuados a deudores (aumentos de activo)</td>
              <td id="vf_simple01_verfal_9v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_9" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_9')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_9f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_9" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_9')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>10. Aportaciones de socios (aumentos de capital contribuido)</td>
              <td id="vf_simple01_verfal_10v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_10" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_10')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_10f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_10" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_10')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>11. Obtención de créditos a corto y largo plazos –diferentes 
                  a operaciones con proveedores y acreedores— emisión de 
                  obligaciones (aumentos de pasivo a corto y largo plazos)</td>
              <td id="vf_simple01_verfal_11v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_11" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_11')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_11f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_11" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_11')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>12. Cobro de intereses</td>
              <td id="vf_simple01_verfal_12v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_12" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_12')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_12f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_12" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_12')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>13. Pérdida neta del ejercicio</td>
              <td id="vf_simple01_verfal_13v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_13" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_13')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_13f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_13" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_13')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>14. Compras de mercancías –aumento de inventarios— aumento 
                  de activo circulante</td>
              <td id="vf_simple01_verfal_14v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_14" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_14')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_14f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_14" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_14')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>15. Disminución de proveedores —pago a proveedores— 
                  (disminuciones de pasivo a corto plazo)</td>
              <td id="vf_simple01_verfal_15v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_15" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_15')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_15f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_15" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_15')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>16. Pago de gastos de operación (disminución de la utilidad 
                  neta del ejercicio)</td>
              <td id="vf_simple01_verfal_16v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_16" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_16')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_16f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_16" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_16')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>17. Las disminuciones de ISR y PTU por pagar –pago de ISR y 
                  PTU- (disminución de pasivo a corto plazo)</td>
              <td id="vf_simple01_verfal_17v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_17" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_17')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_17f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_17" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_17')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>18. Utilidad neta del ejercicio</td>
              <td id="vf_simple01_verfal_18v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_18" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_18')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_18f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_18" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_18')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>19. Ventas de mercancías</td>
              <td id="vf_simple01_verfal_19v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_19" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_19')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_19f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_19" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_19')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>20. Disminuciones de clientes –cobro clientes (disminución de 
                  activo circulante) —</td>
              <td id="vf_simple01_verfal_20v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_20" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_20')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_20f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_20" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_20')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <tr>
              <td colspan="2" id="vf_simple01_verfal_cal"></td>
              <td>
                  <button onclick="calificarVerFalSimple('verfal',20,rc9_simpleA,'vf_simple01');">CALIFICAR</button>
              </td>
          </tr>
      </table>
      
      
  </div>
  <!-- -->
  <div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
<script>
    var tc;
    var cCamCues = function(campo,valor,registro)
    {
      clearTimeout(tc);
      tc = window.setTimeout(function(){
          $.post('libreria/cCamCues.php',{
              modDatos: true,
              elcampo: campo,
              elvalor: valor,
              elregistro: registro
          },function(respuesta){
              if(respuesta === 'modificado')
              {
                  // Modificado
              } else {
                  // Error
                  console.log('-> '+respuesta);
              }
          });
      },1000);
    };
    var cResCues = function(cues,usu,cap,preg,resp)
    {
      clearTimeout(tc);
      tc = window.setTimeout(function(){
          $.post('libreria/cCamCues.php',{
              modRespuestas: true,
              cuestionario: cues,
              usuario: usu,
              capitulo: cap,
              pregunta: preg,
              respuesta:resp
          },function(respuesta){
              if(respuesta === 'modificado')
              {
                  // Modificado
              } else {
                  // Error
                  console.log('-> '+respuesta);
              }
          });
      },500);
    };
    
    window.onload = function(){
        protegerPLibres();
        protegerOpMulRadio('opradio');
        protegerVerFalSimple('vf_simple01');
    };
</script>
</body>
</html>