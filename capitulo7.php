<?php
include 'libreria/principal.php';
include 'libreria/evaluaciones.php';

esUsuario(); // será usuario registrado?
$IDusuario = $_SESSION['userID'];
$capitulo = 7;
$registro = verificarCuestionario($capitulo, $IDusuario,  utf8_decode($Fecha));


?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> Principios de Contabilidad. Cuestionario de evaluación. Capítulo 7</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="libreria/js_principal.js"></script>
<script src="libreria/vendor/jquery/jquery-1.11.0.min.js"></script>
<script src="libreria/calCap.js"></script>
<script src="libreria/rc7.js"></script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Cuestionario de autoevaluación</h1>
    </div>
    <div id="divTitularMenu">
        <a href="portada.php">Inicio</a> | 
        <a href="libreria/calculadora/calculadora.html" 
           target="_blank" 
           onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | 
        <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido" style="width: 900px">
  <div class="divContCuerpo">
      
      <h3 class="titulo-principal">Capítulo 7</h3>
      <div class="migas">
          <ul>
              <li><a href="portada.php">Portada</a></li>
              <li><a href="javascript:history.back();">Página anterior</a></li>
              <li><img src="imagenes/print.png" 
                       alt="" 
                       height="25" 
                       style="float: left; margin-right: 5px;" />
                  <a href="javascript:print();">Imprimir página</a></li>
          </ul>
      </div>
      
      <table class="mis-datos">
          <tr>
              <td>Universidad o Institución:</td>
              <td>
                  <input oninput="cCamCues('unidad',this.value,<?php echo $registro ?>);" 
                         name="unidad" 
                         id='unidad' 
                         required 
                         value="<?php echo xCamCues('unidad', $registro) ?>" />
              </td>
              <td>Escuela o facultad:</td>
              <td>
                  <input oninput="cCamCues('facultad',this.value,<?php echo $registro ?>);" 
                         name="facultad" 
                         id='facultad' 
                         required 
                         value="<?php echo xCamCues('facultad', $registro) ?>" />
              </td>
          </tr>
          <tr>
              <td>Asignatura:</td>
              <td>
                  <input oninput="cCamCues('asignatura',this.value,<?php echo $registro ?>);" 
                         name="asignatura" 
                         id='asignatura' 
                         required 
                         value="<?php echo xCamCues('asignatura', $registro) ?>" />
              </td>
              <td>Alumno:</td>
              <td>
                  <?php echo nombreUsuario($IDusuario) ?>
              </td>
          </tr>
          <tr>
              <td>Maestro:</td>
              <td>
                  <input oninput="cCamCues('maestro',this.value,<?php echo $registro ?>);" 
                         name="maestro" 
                         id='maestro' 
                         required 
                         value="<?php echo xCamCues('maestro', $registro) ?>" />
              </td>
              <td>Fecha:</td>
              <td>
                  <input oninput="cCamCues('fecha',this.value,<?php echo $registro ?>);" 
                         name="fecha" 
                         id='fecha' 
                         required 
                         value="<?php echo xCamCues('fecha', $registro) ?>" />
              </td>
          </tr>
      </table>
      
      <h4 class="subtit-4">OBJETIVOS DE APRENDIZAJE</h4>
      
      <div class="objetivos">
          <p>Al finalizar este capítulo, el alumno será capaz de:</p>
          <ol>
              <li>Identificar el estado de situación financiera como un  estado 
                  que comunica información sobre los recursos que posee la entidad 
                  y la manera en que los utiliza para alcanzar sus fines.</li>
              <li>Analizar la definición, distinguir sus características y 
                  objetivos.</li>
              <li>Distinguir sus formas de presentación y la clasificación de sus 
                  elementos integrantes.</li>
              <li>Elaborar estados de situación financiera.</li>
          </ol>
      </div>
      
      <h4 class="subtit-4">EJERCICIOS</h4>
      
      <h5>I. RESUELVA LO SIGUIENTE</h5>
      
      <div class="pregunta-libre">
          <p><span>1</span> ¿Qué afirma Hernández Robles del balance general?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib01" 
                    id="prelib01"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib01'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>2</span> ¿Qué información comunica el estado de situación 
              financiera?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib02" 
                    id="prelib02"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib02'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>3</span> ¿Qué uso puede darle el usuario al balance general?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib03" 
                    id="prelib03"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib03'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>4</span> Por medio de su análisis e interpretación, ¿qué 
              información se puede conocer?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib04" 
                    id="prelib04"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib04'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>5</span> A partir de qué fecha es vigente la NIF B-6.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib05" 
                    id="prelib05"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib05'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>6</span> ¿A qué tipo de empresas es aplicable la NIF B-6?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib06" 
                    id="prelib06"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib06'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>7</span> ¿De qué partes se conforma el estado de situación 
              financiera?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib07" 
                    id="prelib07"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib07'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>8</span> ¿Qué se anota en cada una de las partes mencionadas 
              en la pregunta anterior?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib08" 
                    id="prelib08"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib08'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>9</span> ¿Cuáles son las formas de presentación del balance 
              general?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib09" 
                    id="prelib09"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib09'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>10</span> ¿En qué fórmulas descansan las presentaciones del 
              balance general?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib10" 
                    id="prelib10"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib10'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>11</span> De conformidad con la NIF B-6, ¿cómo pueden 
              presentarse clasificados los activos y pasivos?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib11" 
                    id="prelib11"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib11'); ?></textarea>
      </div>
      
      <h5>II. COMPLETE LAS LÍNEAS EN BLANCO</h5>
      
      <div class="completar-lineas">
          <span class="num">1</span>
          Según la NIF B-6, el estado de situación financiera, también llamado 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_1" 
                    name="comLin_1" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_1'); ?>" /> general o  estado de 
          posición financiera, muestra información relativa a los 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_2" 
                    name="comLin_2" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_2'); ?>" /> y 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_3" 
                    name="comLin_3" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_3'); ?>" /> financieros de la 
          entidad en una fecha <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_4" 
                    name="comLin_4" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_4'); ?>" />.
      </div>
      
      <div class="completar-lineas">
          <span class="num">2</span>
          Según el autor, el balance general es un estado financiero que muestra, 
          en una fecha determinada, los <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_5" 
                    name="comLin_5" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_5'); ?>" /> de que dispone la 
          entidad para la alcanzar sus fines y las <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_6" 
                    name="comLin_6" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_6'); ?>" /> externas e internas 
          de dichos recursos. De su <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_7" 
                    name="comLin_7" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_7'); ?>" /> e interpretación 
          podemos conocer, entre otros aspectos, la <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_8" 
                    name="comLin_8" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_8'); ?>" /> financiera, 
          solvencia, <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_9" 
                    name="comLin_9" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_9'); ?>" />, rentabilidad, etcétera.
      </div>
      
      <div class="completar-lineas">
          <span class="num">3</span>
          Los renglones se identifican como <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_10" 
                    name="comLin_10" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_10'); ?>" /> o niveles.
      </div>
      
      <div class="completar-lineas">
          <span class="num">4</span>
          Clases. Definen los distintos tipos de <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_11" 
                    name="comLin_11" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_11'); ?>" />, pasivos y capital 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_12" 
                    name="comLin_12" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_12'); ?>" /> que integran cada 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_13" 
                    name="comLin_13" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_13'); ?>" />, cuyo contenido 
          depende del tipo o <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_14" 
                    name="comLin_14" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_14'); ?>" /> de entidad.
      </div>
      
      <div class="completar-lineas">
          <span class="num">5</span>
          Partidas. Son unidades <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_15" 
                    name="comLin_15" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_15'); ?>" /> de cada 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_16" 
                    name="comLin_16" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_16'); ?>" /> de activos, 
          pasivos o capital contable.
      </div>
      
      <div class="completar-lineas">
          <span class="num">6</span>
          Componente. Son partes con características <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_17" 
                    name="comLin_17" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_17'); ?>" /> del resto de 
          otras partes que componen una <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_18" 
                    name="comLin_18" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_18'); ?>" />.
      </div>
      
      <div class="completar-lineas">
          <span class="num">7</span>
          Este formato se presenta <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_19" 
                    name="comLin_19" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_19'); ?>" />, en primer lugar 
          se presenta el <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_20" 
                    name="comLin_20" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_20'); ?>" />, en segundo lugar 
          se incluye el pasivo y en el tercero o último lugar se presenta el 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_21" 
                    name="comLin_21" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_21'); ?>" /> contable. En este 
          formato el capital contable es igual a la <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_22" 
                    name="comLin_22" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_22'); ?>" /> entre el activo 
          menos el <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_23" 
                    name="comLin_23" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_23'); ?>" />.
      </div>
      
      <div class="completar-lineas">
          <span class="num">8</span>
          Esta es la presentación más utilizada; tradicionalmente en ella se 
          muestran, <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_24" 
                    name="comLin_24" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_24'); ?>" />, del lado 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_25" 
                    name="comLin_25" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_25'); ?>" /> el activo y del 
          lado derecho el <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_26" 
                    name="comLin_26" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_26'); ?>" /> y el capital 
          contable o se muestran en este orden en forma vertical. En este 
          formato el activo es igual a la <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_27" 
                    name="comLin_27" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_27'); ?>" /> del pasivo y del 
          capital contable.
      </div>
      
      <div class="completar-lineas">
          <span class="num">9</span>
          Los activos y pasivos a corto (<input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_28" 
                    name="comLin_28" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_28'); ?>" />) y 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_29" 
                    name="comLin_29" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_29'); ?>" /> (no circulantes) 
          plazos <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_30" 
                    name="comLin_30" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_30'); ?>" /> presentarse como 
          categorías <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_31" 
                    name="comLin_31" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_31'); ?>" /> en el estado de 
          situación financiera, <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_32" 
                    name="comLin_32" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_32'); ?>" /> cuando una 
          presentación basada en el grado de <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_33" 
                    name="comLin_33" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_33'); ?>" /> proporcione 
          una información confiable que sea más relevante.
      </div>
      
      <div class="completar-lineas">
          <button onclick="calificarCompletar('comLin',33,rc7_cl);">CALIFICAR</button>
          <span id="comLin_cal"></span>
      </div>
      
  </div>
  <!-- -->
  <div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
<script>
    var tc;
    var cCamCues = function(campo,valor,registro)
    {
      clearTimeout(tc);
      tc = window.setTimeout(function(){
          $.post('libreria/cCamCues.php',{
              modDatos: true,
              elcampo: campo,
              elvalor: valor,
              elregistro: registro
          },function(respuesta){
              if(respuesta === 'modificado')
              {
                  // Modificado
              } else {
                  // Error
                  console.log('-> '+respuesta);
              }
          });
      },1000);
    };
    var cResCues = function(cues,usu,cap,preg,resp)
    {
      clearTimeout(tc);
      tc = window.setTimeout(function(){
          $.post('libreria/cCamCues.php',{
              modRespuestas: true,
              cuestionario: cues,
              usuario: usu,
              capitulo: cap,
              pregunta: preg,
              respuesta:resp
          },function(respuesta){
              if(respuesta === 'modificado')
              {
                  // Modificado
              } else {
                  // Error
                  console.log('-> '+respuesta);
              }
          });
      },500);
    };
    
    window.onload = function(){
        protegerPLibres();
        protegerCompletar();
    };
</script>
</body>
</html>