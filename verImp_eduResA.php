<?php
include("libreria/principal.php");

// comprobar que han iniciado un ejercicio
if(isset($_GET['IDejercicio']) && isset($_GET['metodo']) && isset($_GET['asiento'])){
	$IDejercicio = $_GET['IDejercicio'];
	$metodo = $_GET['metodo'];
	$asiento = $_GET['asiento'];
} else {
	header("Location: portada.php?ejercicio=false");
	exit;	
}
//fin

// variables
$C10 = pintarCantidad("Sdebe", "A4105", $IDejercicio);
$C11 = pintarCantidad("Sdebe", "A4106", $IDejercicio);
$C13 = pintarCantidad("Shaber", "A4203", $IDejercicio);
$C14 = pintarCantidad("Shaber", "A4204", $IDejercicio);
$C15 = pintarCantidad("Shaber", "A4205", $IDejercicio);

$D5 = pintarCantidad("Sdebe", "A4102", $IDejercicio);
$D6 = pintarCantidad("Sdebe", "A4103", $IDejercicio);
$D7 = pintarCantidad("Sdebe", "A4104", $IDejercicio);
$D12 = $C10+$C11;
$D15 = $C13+$C14+$C15;

$E4 = pintarCantidad("Shaber", "A4202", $IDejercicio);
$E7 = ($D5+$D6+$D7);
$E9 = pintarCantidad("Mdebe", "A1115", $IDejercicio);
$E16 = ($D12-$D15);
$E17 = ($E9+$E16);
$E18 = pintarCantidadDa("Sdebe", "A1115", $IDejercicio);
$E22 = (pintarCantidadDa("Mdebe", "4107", $IDejercicio))*-1;
$E23 = (pintarCantidadDa("Mdebe", "4108", $IDejercicio))*-1;
$E26 = pintarCantidadDa("Mhaber", "4206", $IDejercicio);
$E27 = (pintarCantidadDa("Mdebe", "4109", $IDejercicio))*-1;
$E30 = pintarCantidadDa("Mhaber", "4207", $IDejercicio);
$E31 = (pintarCantidadDa("Mdebe", "4110", $IDejercicio))*-1;

$F8 = $E4-$E7;
$F19 = $E17-$E18;
$F20 = $F8-$F19;
$F21 = $E22+$E23;
$F24 = $F20+$F21;
$F25 = $E26+$E27;
$F28 = $F24+$F25;
$F29 = $E30+$E31;
$F32 = $F28+$F29;
$F33 = ($F32>0)?$F32*0.3:0;
$F34 = $F32-$F33;
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Balanza antes de ajustes</title>
<link href="css/imprimible.css" rel="stylesheet" type="text/css" />

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<div id="header">
  <h1>Sistema Contable Romero</h1>
</div>
<h2>Estado de resultados</h2>
<div id="info">Ejercicio: <span class="divContCuerpo">
<?php pintarNejercicio($IDejercicio); ?>
</span><br />
ESTADO DE RESULTADOS del
<?php arregloFecha(fechasPU("ASC", $IDejercicio)); ?>
 al 
 <?php arregloFechaAs(fechasAs($IDejercicio)); ?>.</div>
<div>
  <table align="center" cellpadding="0" cellspacing="1">
    <tr height="20">
      <td height="20" class="celdaEduRes">Ventas    totales</td>
      <td rowspan="7" valign="bottom" class="celdaEduRes">$ <?php echo number_format($C10,2); ?></td>
      <td rowspan="2" valign="bottom" class="celdaEduRes">$ <?php echo number_format($D5,2); ?></td>
      <td class="celdaEduRes">$ <?php echo number_format($E4,2); ?></td>
      <td rowspan="5" valign="bottom" class="celdaEduRes">$ <?php echo number_format($F8,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Devoluciones    sobre venta</td>
      <td rowspan="3" valign="bottom" class="celdaEduRes">$ <?php echo number_format($E7,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Rebajas    sobre venta</td>
      <td class="celdaEduRes">$ <?php echo number_format($D6,2); ?></td>
    </tr>
    <tr height="21">
      <td height="21" class="celdaEduRes">Descuentos    sobre venta</td>
      <td class="celdaEduRes">$ <?php echo number_format($D7,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Ventas netas</td>
      <td rowspan="5" valign="bottom" class="celdaEduRes">$ <?php echo number_format($D12,2); ?></td>
      <td rowspan="2" valign="bottom" class="celdaEduRes">$ <?php echo number_format($E9,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Inventario inicial</td>
      <td rowspan="11" valign="bottom" class="celdaEduRes">$ <?php echo number_format($F19,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Compras</td>
      <td rowspan="7" valign="bottom" class="celdaEduRes">$ <?php echo number_format($E16,2); ?></td>
    </tr>
    <tr height="21">
      <td height="21" class="celdaEduRes">Gastos de compra</td>
      <td class="celdaEduRes">$ <?php echo number_format($C11,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Compras totales</td>
      <td rowspan="2" valign="bottom" class="celdaEduRes">$ <?php echo number_format($C13,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Devoluciones sobre compra</td>
      <td rowspan="3" valign="bottom" class="celdaEduRes">$ <?php echo number_format($D15,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Rebajas sobre compra</td>
      <td class="celdaEduRes">$ <?php echo number_format($C14,2); ?></td>
    </tr>
    <tr height="21">
      <td height="21" class="celdaEduRes">Descuentos sobre compra</td>
      <td class="celdaEduRes">$ <?php echo number_format($C15,2); ?></td>
    </tr>
    <tr height="21">
      <td height="21" class="celdaEduRes">Compras netas</td>
      <td rowspan="19" class="celdaEduRes">&nbsp;</td>
      <td rowspan="19" class="celdaEduRes">&nbsp;</td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Mercancías disponibles</td>
      <td class="celdaEduRes">$ <?php echo number_format($E17,2); ?></td>
    </tr>
    <tr height="21">
      <td height="21" class="celdaEduRes">Inventario final</td>
      <td class="celdaEduRes">$ <?php echo number_format($E18,2); ?></td>
    </tr>
    <tr height="21">
      <td height="21" class="celdaEduRes">Costo de ventas</td>
      <td rowspan="4" valign="bottom" class="celdaEduRes">$ <?php echo number_format($E22,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Utilidad bruta</td>
      <td class="celdaEduRes">$ <?php echo number_format($F20,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Gastos de operación</td>
      <td class="celdaEduRes">$ <?php echo number_format($F21,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Gastos de venta</td>
      <td rowspan="3" valign="bottom" class="celdaEduRes">$ <?php echo number_format($F24,2); ?></td>
    </tr>
    <tr height="21">
      <td height="21" class="celdaEduRes">Gastos de administración</td>
      <td class="celdaEduRes">$ <?php echo number_format($E23,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Utilidad (pérdida) de    operación</td>
      <td rowspan="3" valign="bottom" class="celdaEduRes">$ <?php echo number_format($E26,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Otros ingresos y gastos</td>
      <td class="celdaEduRes">$ <?php echo number_format($F25,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Ingresos</td>
      <td rowspan="3" valign="bottom" class="celdaEduRes">$ <?php echo number_format($F28,2); ?></td>
    </tr>
    <tr height="21">
      <td height="21" class="celdaEduRes">Gastos</td>
      <td class="celdaEduRes">$ <?php echo number_format($E27,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">&nbsp;</td>
      <td rowspan="3" valign="bottom" class="celdaEduRes">$ <?php echo number_format($E30,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Resultado integral de    financiamiento</td>
      <td class="celdaEduRes">$ <?php echo number_format($F29,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">A favor</td>
      <td rowspan="3" valign="bottom" class="celdaEduRes">$ <?php echo number_format($F32,2); ?></td>
    </tr>
    <tr height="21">
      <td height="21" class="celdaEduRes">A cargo</td>
      <td class="celdaEduRes">$ <?php echo number_format($E31,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Utilidad antes de impuestos</td>
      <td rowspan="3" class="celdaEduRes">&nbsp;</td>
    </tr>
    <tr height="21">
      <td height="21" class="celdaEduRes">ISR</td>
      <td class="celdaEduRes">$ <?php echo number_format($F33,2); ?></td>
    </tr>
    <tr height="21">
      <td height="21" class="celdaEduRes">Utilidad (pérdida) neta del    ejercicio</td>
      <td class="celdaEduRes">$ <?php echo number_format($F34,2); ?></td>
    </tr>
  </table>
</div>
<div id="footer">&copy;2011 - Sistema Contable Romero | Todos los Derechos Reservados.</div>
</body>
</html>
