<?php
include("libreria/principal.php");

esUsuario(); // será usuario registrado?
$IDusuario = $_SESSION['userID'];
$IDpractica = filter_input(INPUT_GET, 'IDpractica',
        FILTER_VALIDATE_INT);
$tabla = "rom_edo_cantidades";
if(!isset($IDpractica) || empty($IDpractica) || !validarPractica($IDpractica, 'rom_edo_practicas'))
{
    header("Location: portada.php");
    exit;
}

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sistema Contable Romero</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="libreria/js_principal.js"></script>
<script src="libreria/vendor/jquery/jquery-1.11.0.min.js"></script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Estado de resultado integral, NIF B-3</h1>
    </div>
    <div id="divTitularMenu">
        <a href="portada.php">Inicio</a> | 
        <a href="libreria/calculadora/calculadora.html" 
           target="_blank" 
           onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | 
        <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido" style="width: 1100px">
  <div class="divContCuerpo">
      <h3 class="titulo-principal">Estado de resultado integral</h3>
      <div class="migas">
          <ul>
              <li><a href="portada.php">Portada</a></li>
              <li><a href="portada_edo.php">Prácticas</a></li>
              <li><img src="imagenes/print.png" 
                       alt="" 
                       height="25" 
                       style="float: left; margin-right: 5px;" />
                  <a href="javascript:print();">Imprimir página</a></li>
              <li><a href="concentrado_edo.php?IDpractica=<?php echo $IDpractica ?>">Concentrado</a></li>
          </ul>
      </div>
      <div style="margin-bottom: 25px;"></div>
      <table class='resultado'>
          <tr>
              <td class="resaltado" style="width: 200px;">
                  <span class="etiqueta">Universidad o Institución:</span><br />
                  <input oninput="ajaxCambiarFecha(this.id,<?php echo $IDpractica ?>,'rom_edo_practicas');" 
                         id="unidad" class="campo-agno" 
                         placeholder="Unidad o Institución" 
                         type="text" name="unidad" 
                         value="<?php echo extraerFechaPracticas($IDpractica, 'rom_edo_practicas', 'unidad') ?>" />
              </td>
              <td colspan="5" class="resaltado">
                  <span class="etiqueta">Escuela o Facultad:</span><br />
                  <input oninput="ajaxCambiarFecha(this.id,<?php echo $IDpractica ?>,'rom_edo_practicas');" 
                         id="facultad" class="campo-agno" 
                         placeholder="Escuela o Facultad" 
                         type="text" name="facultad" 
                         value="<?php echo extraerFechaPracticas($IDpractica, 'rom_edo_practicas', 'facultad') ?>" />
              </td>
              <td colspan="5" class="resaltado">
                  <span class="etiqueta">Asignatura:</span><br />
                  <input oninput="ajaxCambiarFecha(this.id,<?php echo $IDpractica ?>,'rom_edo_practicas');" 
                         id="asignatura" class="campo-agno" 
                         placeholder="Asignatura" 
                         type="text" name="asignatura" 
                         value="<?php echo extraerFechaPracticas($IDpractica, 'rom_edo_practicas', 'asignatura') ?>" />
              </td>
          </tr>
          <tr>
              <td class="resaltado">
                  <span class="etiqueta">Alumno:</span> <br />
                  <span class="datos"><?php echo nombreUsuario($IDusuario) ?></span>
              </td>
              <td colspan="5" class="resaltado">
                  <span class="etiqueta">Maestro:</span><br />
                  <input oninput="ajaxCambiarFecha(this.id,<?php echo $IDpractica ?>,'rom_edo_practicas');" 
                         id="maestro" class="campo-agno" 
                         placeholder="Maestro" 
                         type="text" name="maestro" 
                         value="<?php echo extraerFechaPracticas($IDpractica, 'rom_edo_practicas', 'maestro') ?>" />
              </td>
              <td colspan="5" class="resaltado">
                  <span class="etiqueta">Fecha de realización:</span><br />
                  <input oninput="ajaxCambiarFecha(this.id,<?php echo $IDpractica ?>,'rom_edo_practicas');" 
                         id="fechaTarea" class="campo-agno" 
                         placeholder="Fecha" 
                         type="text" name="fechaTarea" 
                         value="<?php echo extraerFechaPracticas($IDpractica, 'rom_edo_practicas', 'fechaTarea') ?>" />
              </td>
          </tr>
          <tr>
              <td class="resaltado" style="font-weight: bold;"></td>
              <td colspan="5" class="resaltado" style="font-weight: bold;">Año actual</td>
              <td colspan="5" class="resaltado" style="font-weight: bold;">Año anterior</td>
          </tr>
          <tr>
              <td class="resaltado">&nbsp;</td>
              <td colspan="5" class="resaltado" style="font-weight: bold;">Estado de resultados:<br />  
              del <input oninput="ajaxCambiarFecha(this.id,<?php echo $IDpractica ?>,'rom_edo_practicas');" 
                         id="aactual" class="campo-agno" 
                         placeholder="Ej: Diciembre 31 de 2014" 
                         type="text" name="aactual" value="<?php echo extraerFechaPracticas($IDpractica, 'rom_edo_practicas', 'aactual') ?>" />
              al <input oninput="ajaxCambiarFecha(this.id,<?php echo $IDpractica ?>,'rom_edo_practicas');" 
                         id="aactualB" class="campo-agno" 
                         placeholder="Ej: Diciembre 31 de 2014" 
                         type="text" name="aactualB" value="<?php echo extraerFechaPracticas($IDpractica, 'rom_edo_practicas', 'aactualB') ?>" />
              </td>
              <td colspan="5" class="resaltado" style="font-weight: bold;">Estado de resultados:<br />  
              del <input oninput="ajaxCambiarFecha(this.id,<?php echo $IDpractica ?>,'rom_edo_practicas');" 
                         id="aanterior" class="campo-agno" 
                         placeholder="Ej: Diciembre 31 de 2014" 
                         type="text" name="aanterior" value="<?php echo extraerFechaPracticas($IDpractica, 'rom_edo_practicas', 'aanterior') ?>" />
              al <input oninput="ajaxCambiarFecha(this.id,<?php echo $IDpractica ?>,'rom_edo_practicas');" 
                         id="aanteriorB" class="campo-agno" 
                         placeholder="Ej: Diciembre 31 de 2014" 
                         type="text" name="aanteriorB" value="<?php echo extraerFechaPracticas($IDpractica, 'rom_edo_practicas', 'aanteriorB') ?>" />
              </td>
          </tr>
          <tr>
              <td class="titulo">Ventas totales</td>
              <td style="width: 90px;">&nbsp;</td>
              <td style="width: 90px;">&nbsp;</td>
              <td class="campo" style="width: 90px;">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="e5" 
                           class="campo-edo" 
                           type="text" 
                           name="e5" 
                           value="<?php echo extraerValorCampo('e5', $IDpractica, $tabla) ?>" />
              </td>
              <td style="width: 90px;">&nbsp;</td>
              <td class="azulF" style="width: 90px;">% integrales</td>
              <td style="width: 90px;">&nbsp;</td>
              <td style="width: 90px;">&nbsp;</td>
              <td class="campo" style="width: 90px;">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="k5" 
                           class="campo-edo" 
                           type="text" 
                           name="k5" 
                           value="<?php echo extraerValorCampo('k5', $IDpractica, $tabla) ?>" />
              </td>
              <td style="width: 90px;">&nbsp;</td>
              <td id="m5" class="azulF" style="width: 90px;">% integrales</td>
          </tr>
          <tr>
              <td class="titulo">Devoluciones sobre venta</td>
              <td>&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="d6" 
                           class="campo-edo" 
                           type="text" 
                           name="d6" 
                           value="<?php echo extraerValorCampo('d6', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
              <td>&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="j6" 
                           class="campo-edo" 
                           type="text" 
                           name="j6" 
                           value="<?php echo extraerValorCampo('j6', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">Rebajas sobre venta</td>
              <td>&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="d7" 
                           class="campo-edo" 
                           type="text" 
                           name="d7" 
                           value="<?php echo extraerValorCampo('d7', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
              <td>&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="j7" 
                           class="campo-edo" 
                           type="text" 
                           name="j7" 
                           value="<?php echo extraerValorCampo('j7', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">Descuentos sobre venta</td>
              <td>&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="d8" 
                           class="campo-edo" 
                           type="text" 
                           name="d8" 
                           value="<?php echo extraerValorCampo('d8', $IDpractica, $tabla) ?>" />
              </td>
              <td id="e8">&nbsp;</td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
              <td>&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="j8" 
                           class="campo-edo" 
                           type="text" 
                           name="j8" 
                           value="<?php echo extraerValorCampo('j8', $IDpractica, $tabla) ?>" />
              </td>
              <td id="k8">&nbsp;</td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">Ventas netas</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td id="f9">&nbsp;</td>
              <td id="g9" class="azulF">100%</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td id="l9">&nbsp;</td>
              <td id="m9" class="azulF">100%</td>
          </tr>
          <tr>
              <td class="titulo">Inventario inicial</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="e10" 
                           class="campo-edo" 
                           type="text" 
                           name="e10" 
                           value="<?php echo extraerValorCampo('e10', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="k10" 
                           class="campo-edo" 
                           type="text" 
                           name="k10" 
                           value="<?php echo extraerValorCampo('k10', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">Compras</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="c11" 
                           class="campo-edo" 
                           type="text" 
                           name="c11" 
                           value="<?php echo extraerValorCampo('c11', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="i11" 
                           class="campo-edo" 
                           type="text" 
                           name="i11" 
                           value="<?php echo extraerValorCampo('i11', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">Gastos de compra</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="c12" 
                           class="campo-edo" 
                           type="text" 
                           name="c12" 
                           value="<?php echo extraerValorCampo('c12', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="i12" 
                           class="campo-edo" 
                           type="text" 
                           name="i12" 
                           value="<?php echo extraerValorCampo('i12', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">Compras totales</td>
              <td>&nbsp;</td>
              <td id="d13">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
              <td>&nbsp;</td>
              <td id="j13">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">Devoluciones sobre compra</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="c14" 
                           class="campo-edo" 
                           type="text" 
                           name="c14" 
                           value="<?php echo extraerValorCampo('c14', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="i14" 
                           class="campo-edo" 
                           type="text" 
                           name="i14" 
                           value="<?php echo extraerValorCampo('i14', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">Rebajas sobre compra</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="c15" 
                           class="campo-edo" 
                           type="text" 
                           name="c15" 
                           value="<?php echo extraerValorCampo('c15', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="i15" 
                           class="campo-edo" 
                           type="text" 
                           name="i15" 
                           value="<?php echo extraerValorCampo('i15', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">Descuentos sobre compra</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="c16" 
                           class="campo-edo" 
                           type="text" 
                           name="c16" 
                           value="<?php echo extraerValorCampo('c16', $IDpractica, $tabla) ?>" />
              </td>
              <td id="d16">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="i16" 
                           class="campo-edo" 
                           type="text" 
                           name="i16" 
                           value="<?php echo extraerValorCampo('i16', $IDpractica, $tabla) ?>" />
              </td>
              <td id="j16">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">Compras netas</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td id="e17">&nbsp;</td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td id="k17">&nbsp;</td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">Mercancías disponibles</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td id="e18">&nbsp;</td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td id="k18">&nbsp;</td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">Inventario final</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="e19" 
                           class="campo-edo" 
                           type="text" 
                           name="e19" 
                           value="<?php echo extraerValorCampo('e19', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="k19" 
                           class="campo-edo" 
                           type="text" 
                           name="k19" 
                           value="<?php echo extraerValorCampo('k19', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">Costo de ventas</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td id="f20">&nbsp;</td>
              <td id="g20" class="azulF">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td id="l20">&nbsp;</td>
              <td id="m20" class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">Utilidad bruta</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td id="f21">&nbsp;</td>
              <td id="g21" class="azulF">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td id="l21">&nbsp;</td>
              <td id="m21" class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">Gastos de operación</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td id="f22">&nbsp;</td>
              <td id="g22" class="azulF">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td id="l22">&nbsp;</td>
              <td id="m22" class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">Gastos de venta</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="e23" 
                           class="campo-edo" 
                           type="text" 
                           name="e23" 
                           value="<?php echo extraerValorCampo('e23', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="k23" 
                           class="campo-edo" 
                           type="text" 
                           name="k23" 
                           value="<?php echo extraerValorCampo('k23', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">Gastos de administración</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="e24" 
                           class="campo-edo" 
                           type="text" 
                           name="e24" 
                           value="<?php echo extraerValorCampo('e24', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="k24" 
                           class="campo-edo" 
                           type="text" 
                           name="k24" 
                           value="<?php echo extraerValorCampo('k24', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">Gastos de investigación</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="e25" 
                           class="campo-edo" 
                           type="text" 
                           name="e25" 
                           value="<?php echo extraerValorCampo('e25', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="k25" 
                           class="campo-edo" 
                           type="text" 
                           name="k25" 
                           value="<?php echo extraerValorCampo('k25', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">Utilidad (pérdida) de operación</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td id="f26">&nbsp;</td>
              <td id="g26" class="azulF">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td id="l26">&nbsp;</td>
              <td id="m26" class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">Otros ingresos y gastos</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td id="f27">&nbsp;</td>
              <td id="g27" class="azulF">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td id="l27">&nbsp;</td>
              <td id="m27" class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">Ingresos</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="e28" 
                           class="campo-edo" 
                           type="text" 
                           name="e28" 
                           value="<?php echo extraerValorCampo('e28', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="k28" 
                           class="campo-edo" 
                           type="text" 
                           name="k28" 
                           value="<?php echo extraerValorCampo('k28', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">Gastos</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="e29" 
                           class="campo-edo" 
                           type="text" 
                           name="e29" 
                           value="<?php echo extraerValorCampo('e29', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="k29" 
                           class="campo-edo" 
                           type="text" 
                           name="k29" 
                           value="<?php echo extraerValorCampo('k29', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td id="f30">&nbsp;</td>
              <td id="g30" class="azulF">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td id="l30">&nbsp;</td>
              <td id="m30" class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">Resultado integral de financiamiento (RIF)</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td id="f31">&nbsp;</td>
              <td id="g31" class="azulF">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td id="l31">&nbsp;</td>
              <td id="m31" class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">RIF a favor</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="e32" 
                           class="campo-edo" 
                           type="text" 
                           name="e32" 
                           value="<?php echo extraerValorCampo('e32', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="k32" 
                           class="campo-edo" 
                           type="text" 
                           name="k32" 
                           value="<?php echo extraerValorCampo('k32', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">RIF a cargo</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="e33" 
                           class="campo-edo" 
                           type="text" 
                           name="e33" 
                           value="<?php echo extraerValorCampo('e33', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="campo">
                  $ <input oninput="ajaxCambiaValorEdo(this.value,this.id,<?php echo $IDpractica ?>,'rom_edo_cantidades')" 
                           id="k33" 
                           class="campo-edo" 
                           type="text" 
                           name="k33" 
                           value="<?php echo extraerValorCampo('k33', $IDpractica, $tabla) ?>" />
              </td>
              <td>&nbsp;</td>
              <td class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">Utilidad antes de impuestos</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td id="f34">&nbsp;</td>
              <td id="g34" class="azulF">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td id="l34">&nbsp;</td>
              <td id="m34" class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">ISR</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td id="f35">&nbsp;</td>
              <td id="g35" class="azulF">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td id="l35">&nbsp;</td>
              <td id="m35" class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="titulo">Utilidad (pérdida) neta del ejercicio</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td id="f36">&nbsp;</td>
              <td id="g36" class="azulF">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td id="l36">&nbsp;</td>
              <td id="m36" class="azulF">&nbsp;</td>
          </tr>
      </table>
      
  </div>
  <!-- -->
  <div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
<script src="libreria/accionesAjaxPracticas.js"></script>
<script>
    window.onload = function(){
        /* Una vez cargada la página
         * hacemos los cálculos de cada campo
         */
        calculosEdo();
    };
</script>
</body>
</html>