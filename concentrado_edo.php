<?php
include("libreria/principal.php");

esUsuario(); // será usuario registrado?
$IDusuario = $_SESSION['userID'];
$IDpractica = filter_input(INPUT_GET, 'IDpractica',
        FILTER_VALIDATE_INT);
$tabla = "rom_edo_cantidades";
if(!isset($IDpractica) || empty($IDpractica) || !validarPractica($IDpractica, 'rom_edo_practicas'))
{
    header("Location: portada.php");
    exit;
}


?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sistema Contable Romero</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="libreria/js_principal.js"></script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Estado de resultado integral, NIF B-3</h1>
    </div>
    <div id="divTitularMenu">
        <a href="portada.php">Inicio</a> | 
        <a href="libreria/calculadora/calculadora.html" 
           target="_blank" 
           onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | 
        <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido">
  <div class="divContCuerpo">
      <h3 class="titulo-principal">Concentrado</h3>
      <div class="migas">
          <ul>
              <li><a href="portada.php">Portada</a></li>
              <li><a href="portada_edo.php">Prácticas</a></li>
              <li><img src="imagenes/print.png" 
                       alt="" 
                       height="25" 
                       style="float: left; margin-right: 5px;" />
                  <a href="javascript:print();">Imprimir página</a></li>
              <li><a href="practicas_edo.php?IDpractica=<?php echo $IDpractica ?>">Estado de resultado</a></li>
          </ul>
      </div>
      <div style="margin-bottom: 25px;"></div>
      <table class="resultado">
          <tr>
              <td class="resaltado" colspan="2">
                  <span class="etiqueta">Universidad:</span> <br />
                  <span class="datos"><?php echo extraInfo('rom_edo_practicas', 'unidad', $IDpractica) ?></span>
              </td>
              <td class="resaltado" colspan="3">
                  <span class="etiqueta">Escuela o Facultad:</span> <br />
                  <span class="datos"><?php echo extraInfo('rom_edo_practicas', 'facultad', $IDpractica) ?></span>
              </td>
          </tr>
          <tr>
              <td class="resaltado" colspan="2">
                  <span class="etiqueta">Asignatura:</span> <br />
                  <span class="datos"><?php echo extraInfo('rom_edo_practicas', 'asignatura', $IDpractica) ?></span>
              </td>
              <td class="resaltado" colspan="3">
                  <span class="etiqueta">Maestro:</span> <br />
                  <span class="datos"><?php echo extraInfo('rom_edo_practicas', 'maestro', $IDpractica) ?></span>
              </td>
          </tr>
          <tr>
              <td class="resaltado" colspan="2">
                  <span class="etiqueta">Alumno:</span> <br />
                  <span class="datos"><?php echo nombreUsuario($IDusuario) ?></span>
              </td>
              <td class="resaltado" colspan="3">
                  <span class="etiqueta">Fecha de realización:</span> <br />
                  <span class="datos"><?php echo extraInfo('rom_edo_practicas', 'fechaTarea', $IDpractica) ?></span>
              </td>
          </tr>
          <tr>
              <td class="resaltado" colspan="5">
                  Nombre de la entidad: <span class="datos"><?php echo nombreEntidad($IDpractica, 'rom_edo_practicas') ?></span>
              </td>
          </tr>
          <tr>
              <td class="resaltado">&nbsp;</td>
              <td class="resaltado" colspan="2" style="font-weight: bold;">Año actual</td>
              <td class="resaltado" colspan="2" style="font-weight: bold;">Año anterior</td>
          </tr>
          <tr>
              <td class="resaltado">&nbsp;</td>
              <td class="resaltado" colspan="2" style="font-weight: bold;">Estado de resultados:<br />
              del <?php echo extraInfo('rom_edo_practicas', 'aactual', $IDpractica) ?><br />
              al <?php echo extraInfo('rom_edo_practicas', 'aactualB', $IDpractica) ?></td>
              <td class="resaltado" colspan="2" style="font-weight: bold;">Estado de resultados:<br />
                  del <?php echo extraInfo('rom_edo_practicas', 'aanterior', $IDpractica) ?><br />
              al <?php echo extraInfo('rom_edo_practicas', 'aanteriorB', $IDpractica) ?></td>
          </tr>
          <tr>
              <td class="resaltado" style="width: 250px; text-align: left">Ventas netas</td>
              <td id="r5">&nbsp;</td>
              <td id="s5" class="azulF">100%</td>
              <td id="u5">&nbsp;</td>
              <td id="v5" class="azulF">100%</td>
          </tr>
          <tr>
              <td class="resaltado" style="width: 250px; text-align: left">Costo de ventas</td>
              <td id="r6">&nbsp;</td>
              <td id="s6" class="azulF">&nbsp;</td>
              <td id="u6">&nbsp;</td>
              <td id="v6" class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="resaltado" style="width: 250px; text-align: left">Utilidad bruta</td>
              <td id="r7">&nbsp;</td>
              <td id="s7" class="azulF">&nbsp;</td>
              <td id="u7">&nbsp;</td>
              <td id="v7" class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="resaltado" style="width: 250px; text-align: left">Gastos de operación</td>
              <td id="r8">&nbsp;</td>
              <td id="s8" class="azulF">&nbsp;</td>
              <td id="u8">&nbsp;</td>
              <td id="v8" class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="resaltado" style="width: 250px; text-align: left">Utilidad (pérdida) de operación</td>
              <td id="r9">&nbsp;</td>
              <td id="s9" class="azulF">&nbsp;</td>
              <td id="u9">&nbsp;</td>
              <td id="v9" class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="resaltado" style="width: 250px; text-align: left">Otros ingresos y gastos</td>
              <td id="r10">&nbsp;</td>
              <td id="s10" class="azulF">&nbsp;</td>
              <td id="u10">&nbsp;</td>
              <td id="v10" class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="resaltado" style="width: 250px; text-align: left">&nbsp;</td>
              <td id="r11">&nbsp;</td>
              <td id="s11" class="azulF">&nbsp;</td>
              <td id="u11">&nbsp;</td>
              <td id="v11" class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="resaltado" style="width: 250px; text-align: left">Resultado integral de financiamiento (RIF)</td>
              <td id="r12">&nbsp;</td>
              <td id="s12" class="azulF">&nbsp;</td>
              <td id="u12">&nbsp;</td>
              <td id="v12" class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="resaltado" style="width: 250px; text-align: left">Utilidad antes de impuestos</td>
              <td id="r13">&nbsp;</td>
              <td id="s13" class="azulF">&nbsp;</td>
              <td id="u13">&nbsp;</td>
              <td id="v13" class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="resaltado" style="width: 250px; text-align: left">ISR</td>
              <td id="r14">&nbsp;</td>
              <td id="s14" class="azulF">&nbsp;</td>
              <td id="u14">&nbsp;</td>
              <td id="v14" class="azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class="resaltado" style="width: 250px; text-align: left">Utilidad (pérdida) neta del ejercicio</td>
              <td id="r15">&nbsp;</td>
              <td id="s15" class="azulF">&nbsp;</td>
              <td id="u15">&nbsp;</td>
              <td id="v15" class="azulF">&nbsp;</td>
          </tr>
      </table>
  </div>
  <!-- -->
  <div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
<script>
    window.onload = function(){
        var r5 = parseFloat(sessionStorage.r5);
        var r6 = parseFloat(sessionStorage.r6);
        var r7 = parseFloat(sessionStorage.r7);
        var r8 = parseFloat(sessionStorage.r8);
        var r9 = parseFloat(sessionStorage.r9);
        var r10 = parseFloat(sessionStorage.r10);
        var r11 = parseFloat(sessionStorage.r11);
        var r12 = parseFloat(sessionStorage.r12);
        var r13 = parseFloat(sessionStorage.r13);
        var r14 = parseFloat(sessionStorage.r14);
        var r15 = parseFloat(sessionStorage.r15);
        var u5 = parseFloat(sessionStorage.u5);
        var u6 = parseFloat(sessionStorage.u6);
        var u7 = parseFloat(sessionStorage.u7);
        var u8 = parseFloat(sessionStorage.u8);
        var u9 = parseFloat(sessionStorage.u9);
        var u10 = parseFloat(sessionStorage.u10);
        var u11 = parseFloat(sessionStorage.u11);
        var u12 = parseFloat(sessionStorage.u12);
        var u13 = parseFloat(sessionStorage.u13);
        var u14 = parseFloat(sessionStorage.u14);
        var u15 = parseFloat(sessionStorage.u15);
        
        document.getElementById('r5').innerHTML = '$ ' + r5.toFixed(2);
        document.getElementById('r6').innerHTML = '$ ' + r6.toFixed(2);
        document.getElementById('r7').innerHTML = '$ ' + r7.toFixed(2);
        document.getElementById('r8').innerHTML = '$ ' + r8.toFixed(2);
        document.getElementById('r9').innerHTML = '$ ' + r9.toFixed(2);
        document.getElementById('r10').innerHTML = '$ ' + r10.toFixed(2);
        document.getElementById('r11').innerHTML = '$ ' + r11.toFixed(2);
        document.getElementById('r12').innerHTML = '$ ' + r12.toFixed(2);
        document.getElementById('r13').innerHTML = '$ ' + r13.toFixed(2);
        document.getElementById('r14').innerHTML = '$ ' + r14.toFixed(2);
        document.getElementById('r15').innerHTML = '$ ' + r15.toFixed(2);
        document.getElementById('u5').innerHTML = '$ ' + u5.toFixed(2);
        document.getElementById('u6').innerHTML = '$ ' + u6.toFixed(2);
        document.getElementById('u7').innerHTML = '$ ' + u7.toFixed(2);
        document.getElementById('u8').innerHTML = '$ ' + u8.toFixed(2);
        document.getElementById('u9').innerHTML = '$ ' + u9.toFixed(2);
        document.getElementById('u10').innerHTML = '$ ' + u10.toFixed(2);
        document.getElementById('u11').innerHTML = '$ ' + u11.toFixed(2);
        document.getElementById('u12').innerHTML = '$ ' + u12.toFixed(2);
        document.getElementById('u13').innerHTML = '$ ' + u13.toFixed(2);
        document.getElementById('u14').innerHTML = '$ ' + u14.toFixed(2);
        document.getElementById('u15').innerHTML = '$ ' + u15.toFixed(2);
        
        document.getElementById('s6').innerHTML = ((r6 / r5)*100).toFixed(2) +'%';
        document.getElementById('s7').innerHTML = ((r7 / r5)*100).toFixed(2) +'%';
        document.getElementById('s8').innerHTML = ((r8 / r5)*100).toFixed(2) +'%';
        document.getElementById('s9').innerHTML = ((r9 / r5)*100).toFixed(2) +'%';
        document.getElementById('s10').innerHTML = ((r10 / r5)*100).toFixed(2) +'%';
        document.getElementById('s11').innerHTML = ((r11 / r5)*100).toFixed(2) +'%';
        document.getElementById('s12').innerHTML = ((r12 / r5)*100).toFixed(2) +'%';
        document.getElementById('s13').innerHTML = ((r13 / r5)*100).toFixed(2) +'%';
        document.getElementById('s14').innerHTML = ((r14 / r5)*100).toFixed(2) +'%';
        document.getElementById('s15').innerHTML = ((r15 / r5)*100).toFixed(2) +'%';
        document.getElementById('v6').innerHTML = ((u6 / u5)*100).toFixed(2) +'%';
        document.getElementById('v7').innerHTML = ((u7 / u5)*100).toFixed(2) +'%';
        document.getElementById('v8').innerHTML = ((u8 / u5)*100).toFixed(2) +'%';
        document.getElementById('v9').innerHTML = ((u9 / u5)*100).toFixed(2) +'%';
        document.getElementById('v10').innerHTML = ((u10 / u5)*100).toFixed(2) +'%';
        document.getElementById('v11').innerHTML = ((u11 / u5)*100).toFixed(2) +'%';
        document.getElementById('v12').innerHTML = ((u12 / u5)*100).toFixed(2) +'%';
        document.getElementById('v13').innerHTML = ((u13 / u5)*100).toFixed(2) +'%';
        document.getElementById('v14').innerHTML = ((u14 / u5)*100).toFixed(2) +'%';
        document.getElementById('v15').innerHTML = ((u15 / u5)*100).toFixed(2) +'%';
    };
</script>
</body>
</html>