<?php
include("libreria/principal.php");

esUsuario(); // será usuario registrado?

// Agregar nuevo asiento
$submitNasiento = filter_input(INPUT_POST, 'submitNasiento');
if(isset($submitNasiento)){
	
	$Fasiento = filter_input(INPUT_POST, 'Fasiento', 
                FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
	$Easiento = filter_input(INPUT_POST, 'Easiento', FILTER_SANITIZE_NUMBER_INT);
	$IDejercicio = filter_input(INPUT_POST, 'IDejercicio', FILTER_SANITIZE_NUMBER_INT);
	$metodo = filter_input(INPUT_POST, 'metodo', FILTER_SANITIZE_NUMBER_INT);
	$asientoR = filter_input(INPUT_POST, 'asientoR', FILTER_SANITIZE_NUMBER_INT);
	
	$queryAg = "INSERT INTO rom_asiento (asientoR, Fasiento, Easiento) VALUES ('$asientoR', '$Fasiento', '$Easiento')";
	mysql_query($queryAg) or die('Error, insert query failed');
	
	header("Location: diarioFeAs.php?IDejercicio=$IDejercicio&metodo=$metodo");
	exit;
}

// comprobar que han iniciado un ejercicio
$IDejercicio = filter_input(INPUT_GET, 'IDejercicio',FILTER_SANITIZE_NUMBER_INT);
$metodo = filter_input(INPUT_GET, 'metodo',FILTER_SANITIZE_NUMBER_INT);
$asiento = filter_input(INPUT_GET, 'asiento',FILTER_SANITIZE_NUMBER_INT);
if(!isset($IDejercicio) || !isset($metodo))
{
    header("Location: portada.php?ejercicio=false");
    exit;
}

// Pintar asientos anteriores
$queryAsientos		= "SELECT * FROM rom_asiento WHERE Easiento = $IDejercicio ORDER BY asientoR ASC";
$resultAsientos		= mysql_query($queryAsientos) or die (mysql_error());
$rowAsientos		= mysql_fetch_assoc($resultAsientos);
$totalAsientos		= mysql_num_rows($resultAsientos);

// mostrar el último asientoR de la tabla
$queryLast = "SELECT asientoR FROM rom_asiento
			WHERE Easiento = $IDejercicio
			ORDER BY asientoR DESC limit 0,1";
$resultLast = mysql_query($queryLast) or die (mysql_error());
$rowLast = mysql_fetch_assoc($resultLast);
$totalLast = mysql_num_rows($resultLast);
if ($totalLast == 0) {
	$proxAsiento = 1;	
} else {
	$proxAsiento = $rowLast['asientoR'] + 1;
}

// comprobar que ya haya una balanza_aa para este ejercicio
$queryBaa = "SELECT COUNT(IDbalanza_aa) AS regBaa FROM rom_balanza_aa WHERE ejercicio = $IDejercicio";
$resulBaa = mysql_query($queryBaa) or die(mysql_error());
$datoBaa = mysql_fetch_array($resulBaa);
$regBaa = $datoBaa['regBaa'];

/**
 * MOSTRAR NOMBRE DE EJERCICIO
 * 
 * Función para mostrar el nombre del ejercicio
 * pasado como parámetro (int).
 * @author Fernando Magrosoto V.
 * @category Romero C
 * @copyright 2014 Fernando Magrosoto V.
 * @param int $IDejercicio El ID del ejercicio
 */
function mostrarNombreEjercicio($IDejercicio)
{
    $query = "SELECT Nejercicio FROM rom_ejercicio "
            . "WHERE IDejercicio = $IDejercicio";
    $result = mysql_query($query);
    $data = mysql_fetch_row($result);
    return ucwords(utf8_encode($data[0]));
}
// fin función

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sistema Contable Romero</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="libreria/calendario/calendar_db.js"></script>
<script language="javascript" src="libreria/js_principal.js"></script>
<link rel="stylesheet" href="libreria/calendario/calendar.css">
<script language="javascript" type="text/javascript">
function Validador(theForm)
{
  if (theForm.Fasiento.value === "")
  {
    alert("El campo fecha está vacío.");
    return (false);
  }
  return (true);
}
function VerificarBaa()
{
	var Baa = <?php echo $regBaa; ?>;
	if(Baa !== 0){
		document.getElementById("submitNasiento").value = "Ejercicio cerrado";
		document.getElementById("submitNasiento").disabled = true;
                document.getElementById('abrir').innerHTML = " <a onclick='abrirEjercicio();' href='javascript:void(0);'>¿Deseas abrir el ejercicio?</a>";
	}
}

function abrirEjercicio(){
    document.getElementById("submitNasiento").value = "Continuar";
    document.getElementById("submitNasiento").disabled = false;
    document.getElementById('abrir').innerHTML = "";
}

</script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body onload="VerificarBaa();">
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Crear y seleccionar asientos</h1>
    </div>
    <div id="divTitularMenu"><a href="portada.php">Inicio</a> | <a href="libreria/calculadora/calculadora.html" target="_blank" onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido">
    <div class='migas'>
        <ul>
            <li><a href='portada.php'>Portada</a></li>
        </ul>
    </div>
    
<?php if(isset($_GET['accion'])) {
	if($_GET['accion'] == "eliminado") {
	?>
<div class="divContCuerpoInfo">El asiento <?php echo $asiento; ?> y todas sus entradas ha sido eliminados favorablemente.</div>
<?php }
if($_GET['accion'] == "actualizar"){
?>
<div class="divContCuerpoInfo">El asiento <?php echo $asiento; ?> ha quedado cerrado favorablemente.</div>
<?php } } ?>
<div class="divContCuerpo">
<fieldset class="mostrar-modulos">
  <legend>Fechas y asientos</legend>
  
  <div class="divContCuerpoIzq">
  <form class="form-2014" action="diarioFeAs.php" 
        method="post" id="Nasiento" 
        onsubmit="return Validador(this)">
<fieldset>
  <legend>Nuevo Asiento</legend>
  <div>
      Método: <strong><?php pintarMetodo($metodo); ?></strong>
  </div>
  <div>
      Ejercicio: <strong><?php echo mostrarNombreEjercicio($IDejercicio) ?></strong>
  </div>
  <div>
      Próximo asiento: <strong><?php echo $proxAsiento; ?></strong>
  </div>
  <div>
      <input name="Fasiento" type="text" id="Fasiento" size="10" />
      <input name="Easiento" 
             type="hidden" 
             id="Easiento" value="<?php echo $IDejercicio; ?>" />
      	<script language="JavaScript" type="text/javascript">
	new tcal ({
		// form name
		'formname': 'Nasiento',
		// input name
		'controlname': 'Fasiento'
	});

	</script>
  </div>
  <div>
      <input name="IDejercicio" type="hidden" id="IDejercicio" value="<?php echo $IDejercicio; ?>" />
      <input name="metodo" type="hidden" id="metodo" value="<?php echo $metodo; ?>" />
      <input name="asientoR" type="hidden" id="asientoR" value="<?php echo $proxAsiento; ?>" />
      <button type="submit" name="submitNasiento" id="submitNasiento">Continuar</button><br />
      <span id='abrir'></span>
  </div>
</fieldset>
</form>
  </div>
  
  <div class="divContCuerpoIzq">
      <div class="divNavPlan">Utilice la tabla de <strong>ASIENTOS CARGADOS</strong> 
    para poder capturar entradas.</div>
  <fieldset class="mostrar-modulos" style="font-size: 11px;">
      <legend>Asientos cargados</legend>
  <table border="0" cellpadding="0" cellspacing="3" class="tablaLista">
  <tr>
    <td colspan="5" class="celdaFormT">Seleccione un asiento anterior:</td>
  </tr>
  <tr>
    <td class="celdaListaTit"><strong>Fecha:</strong></td>
    <td class="celdaListaTit"><strong>Asiento:</strong></td>
    <td class="celdaListaTit"><strong>Acciones:</strong></td>
    <td class="celdaListaTit"><strong>Status:</strong></td>
    <td class="celdaListaTit"><strong>Eliminar</strong></td>
  </tr>
  <?php
  if($totalAsientos == 0) {
  ?>
  <tr>
    <td colspan="5" class="celdaForm">No hay asientos para este ejercicio.</td>
    </tr>
    <?php } else {
		do {
			// sólo si hubo un ASIENTO modificado
			if(isset($_GET['accion']) && $_GET['accion']=="actualizar" && $_GET['asiento']==$rowAsientos['IDasiento']){
			if($rowAsientos['status']==0){ $class = "celdaForm"; } else { $class = "celdaFormAct"; }
			}
			else {
				$class = "celdaForm";
			}
			?>
  <tr>
    <td class="<?php echo $class; ?>"><?php arregloFecha($rowAsientos['Fasiento']); ?></td>
    <td class="<?php echo $class; ?>"><?php echo $rowAsientos['asientoR']; ?></td>
    <td align="center" valign="middle" class="<?php echo $class; ?>"><a href="diario.php?asiento=<?php echo $rowAsientos['asientoR']; ?>&IDejercicio=<?php echo $IDejercicio; ?>&metodo=<?php echo $metodo; ?>"><?php mostrarAccion($rowAsientos['status']); ?></a></td>
    <td class="<?php echo $class; ?>"><?php
    if($rowAsientos['status']==0) {
		echo "[ abierto ]";
		} else {
		echo "<strong>[ cerrado ]</strong>";	
		}		
		?></td>
    <td align="center" valign="middle" class="<?php echo $class; ?>">
    <?php if($regBaa == 0) { ?>
    <a href="libreria/principal.php?accion=eliminar&asiento=<?php echo $rowAsientos['asientoR']; ?>&IDejercicio=<?php echo $IDejercicio; ?>&metodo=<?php echo $metodo; ?>" onclick="return confirm('¿Está seguro de eliminar este asiento junto con todas sus entradas?');"><img src="imagenes/action_delete.gif" alt="Eliminar asiento" width="16" height="16" border="0" title="Eliminar asiento" /></a>
    <?php } else {
        if($rowAsientos['status'] == 0){
            ?>
    <a href="libreria/principal.php?accion=eliminar&asiento=<?php echo $rowAsientos['asientoR']; ?>&IDejercicio=<?php echo $IDejercicio; ?>&metodo=<?php echo $metodo; ?>" onclick="return confirm('¿Está seguro de eliminar este asiento junto con todas sus entradas?');"><img src="imagenes/action_delete.gif" alt="Eliminar asiento" width="16" height="16" border="0" title="Eliminar asiento" /></a>
            <?php
        } else {
            ?>
    <img src="imagenes/action_delete_bg.gif" alt="No se puede eliminar porque el ejercicio está cerrado" title="No se puede eliminar porque el ejercicio y el asiento está cerrado" />
            <?php
        }
    } ?>
    </td>
  </tr>
  <?php } while($rowAsientos = mysql_fetch_assoc($resultAsientos)); }?>
  </table>

  </fieldset>
  </div>
</fieldset>
</div>
  <div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
</body>
</html>