<?php
include("libreria/principal.php");

esUsuario(); // será usuario registrado?
$msgError = FALSE;

###################
## SCRIPT GENERAL ##

// Navegación por planillas
if(isset($_POST['subNav'])){
	$planilla = $_POST['planilla'];
	$IDejercicio = $_POST['IDejercicio'];
	$metodo = $_POST['metodo'];
	$asiento = $_POST['asiento'];
	
	header("Location: $planilla?IDejercicio=$IDejercicio&metodo=$metodo&asiento=$asiento");
	exit;		
}

// comprobar que han iniciado un ejercicio
if(isset($_GET['IDejercicio']) && isset($_GET['metodo']) && isset($_GET['asiento'])){
	$IDejercicio = $_GET['IDejercicio'];
	$metodo = $_GET['metodo'];
	$asiento = $_GET['asiento'];
} else {
	header("Location: portada.php?ejercicio=false");
	exit;	
}
//fin
###################

// Extraer fecha del asiento
$queryFecha = "SELECT Fasiento, redaccion, status FROM rom_asiento WHERE asientoR = '$asiento' AND Easiento = '$IDejercicio'";
$resultFecha = mysql_query($queryFecha);
$rowFecha = mysql_fetch_assoc($resultFecha);
$fechaAsiento = $rowFecha['Fasiento'];
$statusAsiento = $rowFecha['status']; // status del asiento ** IMPORTANTE **

// pintar navegación de plantillas
$queryP		= "SELECT * FROM rom_planillas WHERE Mplanilla = 4 OR Mplanilla = '$metodo' ORDER BY IDplanilla ASC";
$resultP	= mysql_query($queryP) or die (mysql_error());
$rowP		= mysql_fetch_assoc($resultP);

// verificar si hay una balanza_da y balanza_aa
$queryBaa = "SELECT COUNT(IDbalanza_da) as bda FROM rom_balanza_da WHERE ejercicio = '$IDejercicio'";
$resulBaa = mysql_query($queryBaa);
$datoBaa = mysql_fetch_assoc($resulBaa);
$bda = $datoBaa['bda'];

if($bda == 0) { // no hay balanza_da
	$msgError = "<strong>Advertencia:</strong> No hay BALANZA DESPUES DE AJUSTES. Cree una y vuelva a intentar.";
}

$D_C12 = pintarCantReglaNeg("Sdebe", "1106", $IDejercicio);
$D_C13 = pintarCantidadDa("Shaber", "5101", $IDejercicio);
$D_C35 = pintarCantReglaNeg("Sdebe", "1202", $IDejercicio);
$D_C36 = pintarCantidadDa("Shaber", "5201", $IDejercicio);
$D_C37 = pintarCantReglaNeg("Sdebe", "1203", $IDejercicio);
$D_C38 = pintarCantidadDa("Shaber", "5202", $IDejercicio);
$D_C39 = pintarCantReglaNeg("Sdebe", "1204", $IDejercicio);
$D_C40 = pintarCantidadDa("Shaber", "5203", $IDejercicio);
$D_C41 = pintarCantReglaNeg("Sdebe", "1205", $IDejercicio);
$D_C42 = pintarCantidadDa("Shaber", "5204", $IDejercicio);
$D_C43 = pintarCantReglaNeg("Sdebe", "1206", $IDejercicio);
$D_C44 = pintarCantidadDa("Shaber", "5205", $IDejercicio);
$D_C45 = pintarCantReglaNeg("Sdebe", "1207", $IDejercicio);
$D_C46 = pintarCantidadDa("Shaber", "5206", $IDejercicio);
$D_C47 = pintarCantReglaNeg("Sdebe", "1208", $IDejercicio);
$D_C48 = pintarCantidadDa("Shaber", "5207", $IDejercicio);
$D_C50 = pintarCantReglaNeg("Sdebe", "1209", $IDejercicio);
$D_C51 = pintarCantidadDa("Shaber", "5208", $IDejercicio);
$D_C52 = pintarCantReglaNeg("Sdebe", "1210", $IDejercicio);
$D_C53 = pintarCantidadDa("Shaber", "5209", $IDejercicio);
$D_C54 = pintarCantReglaNeg("Sdebe", "1211", $IDejercicio);
$D_C55 = pintarCantidadDa("Shaber", "5210", $IDejercicio);
$D_C56 = pintarCantReglaNeg("Sdebe", "1212", $IDejercicio);
$D_C57 = pintarCantidadDa("Shaber", "5211", $IDejercicio);
$D_C58 = pintarCantReglaNeg("Sdebe", "1213", $IDejercicio);
$D_C59 = pintarCantidadDa("Shaber", "5212", $IDejercicio);
$D_C60 = pintarCantReglaNeg("Sdebe", "1214", $IDejercicio);
$D_C61 = pintarCantidadDa("Shaber", "5213", $IDejercicio);
$D_C62 = pintarCantReglaNeg("Sdebe", "1215", $IDejercicio);
$D_C63 = pintarCantidadDa("Shaber", "5214", $IDejercicio);
$D_C64 = pintarCantReglaNeg("Sdebe", "1216", $IDejercicio);
$D_C65 = pintarCantidadDa("Shaber", "5215", $IDejercicio);

$D_D7 = pintarCantReglaNeg("Sdebe", "1101", $IDejercicio);
$D_D8 = pintarCantReglaNeg("Sdebe", "1102", $IDejercicio);
$D_D9 = pintarCantReglaNeg("Sdebe", "1103", $IDejercicio);
$D_D10 = pintarCantidadDa("Sdebe", "1104", $IDejercicio);
$D_D11 = pintarCantReglaNeg("Sdebe", "1105", $IDejercicio);
$D_D13 = $D_C12 - $D_C13;
$D_D14 = pintarCantReglaNeg("Sdebe", "1107", $IDejercicio);
$D_D15 = pintarCantReglaNeg("Sdebe", "1108", $IDejercicio);
$D_D16 = pintarCantReglaNeg("Sdebe", "1109", $IDejercicio);
$D_D17 = pintarCantReglaNeg("Sdebe", "1110", $IDejercicio);
$D_D18 = pintarCantReglaNeg("Sdebe", "1111", $IDejercicio);
$D_D19 = pintarCantReglaNeg("Sdebe", "1112", $IDejercicio);
$D_D20 = pintarCantReglaNeg("Sdebe", "1113", $IDejercicio);
$D_D21 = pintarCantReglaNeg("Sdebe", "1114", $IDejercicio);
// ATENCIÓN
switch($metodo){ // dependiendo del metodo será la clave
	case "1": $cmet = "A1115";
	break;
	case "2": $cmet = "G1115";
	break;
	case "3": $cmet = "P1115";
	break;	
}
$D_D22 = pintarCantReglaNeg("Sdebe", $cmet, $IDejercicio);
//
$D_D23 = pintarCantReglaNeg("Sdebe", "1116", $IDejercicio);
$D_D24 = pintarCantReglaNeg("Sdebe", "1117", $IDejercicio);
$D_D25 = pintarCantReglaNeg("Sdebe", "1118", $IDejercicio);
$D_D26 = pintarCantReglaNeg("Sdebe", "1119", $IDejercicio);
$D_D27 = pintarCantReglaNeg("Sdebe", "1120", $IDejercicio);
$D_D28 = pintarCantReglaNeg("Sdebe", "1121", $IDejercicio);
$D_D29 = pintarCantReglaNeg("Sdebe", "1122", $IDejercicio);
$D_D30 = pintarCantReglaNeg("Sdebe", "1123", $IDejercicio);
$D_D31 = pintarCantReglaNeg("Sdebe", "1124", $IDejercicio);
$D_D34 = pintarCantReglaNeg("Sdebe", "1201", $IDejercicio);
$D_D36 = $D_C35 - $D_C36;
$D_D38 = $D_C37 - $D_C38;
$D_D40 = $D_C39 - $D_C40;
$D_D42 = $D_C41 - $D_C42;
$D_D44 = $D_C43 - $D_C44;
$D_D46 = $D_C45 - $D_C46;
$D_D48 = $D_C47 - $D_C48;
$D_D51 = $D_C50 - $D_C51;
$D_D53 = $D_C52 - $D_C53;
$D_D55 = $D_C54 - $D_C55;
$D_D57 = $D_C56 - $D_C57;
$D_D59 = $D_C58 - $D_C59;
$D_D61 = $D_C60 - $D_C61;
$D_D63 = $D_C62 - $D_C63;
$D_D65 = $D_C64 - $D_C65;
$D_D67 = pintarCantReglaNeg("Sdebe", "1217", $IDejercicio);
$D_D68 = pintarCantReglaNeg("Sdebe", "1218", $IDejercicio);
$D_D69 = pintarCantReglaNeg("Sdebe", "1219", $IDejercicio);
$D_D70 = pintarCantReglaNeg("Sdebe", "1220", $IDejercicio);
$D_D71 = pintarCantReglaNeg("Sdebe", "1221", $IDejercicio);
$D_D72 = pintarCantReglaNeg("Sdebe", "1222", $IDejercicio);

$D_E31 = $D_D7+$D_D8+$D_D9+$D_D10+$D_D11+$D_D13+$D_D14+$D_D15+$D_D16+$D_D17+$D_D18+$D_D19+$D_D20+$D_D21+$D_D22+$D_D23+$D_D24+$D_D25+$D_D26+$D_D27+$D_D28+$D_D29+$D_D30+$D_D31;
$D_E48 = $D_D34+$D_D36+$D_D38+$D_D40+$D_D42+$D_D44+$D_D46+$D_D48;
$D_E65 = $D_D51+$D_D53+$D_D55+$D_D57+$D_D59+$D_D61+$D_D63+$D_D65;
$D_E72 = $D_D67+$D_D68+$D_D69+$D_D70+$D_D71+$D_D72;
$D_E73 = $D_E31+$D_E48+$D_E65+$D_E72;

$H_H7 = pintarCantidadDa("Shaber", "1104", $IDejercicio);
$H_H8 = pintarCantReglaNeg("Shaber", "2101", $IDejercicio);
$H_H9 = pintarCantReglaNeg("Shaber", "2102", $IDejercicio);
$H_H10 = pintarCantReglaNeg("Shaber", "2103", $IDejercicio);
$H_H11 = pintarCantReglaNeg("Shaber", "2104", $IDejercicio);
$H_H12 = pintarCantReglaNeg("Shaber", "2105", $IDejercicio);
$H_H13 = pintarCantReglaNeg("Shaber", "2106", $IDejercicio);
$H_H14 = pintarCantReglaNeg("Shaber", "2107", $IDejercicio);
$H_H15 = pintarCantReglaNeg("Shaber", "2108", $IDejercicio);
$H_H16 = pintarCantReglaNeg("Shaber", "2109", $IDejercicio);
$H_H17 = pintarCantReglaNeg("Shaber", "2110", $IDejercicio);
$H_H18 = pintarCantReglaNeg("Shaber", "2111", $IDejercicio);
$H_H19 = pintarCantReglaNeg("Shaber", "2112", $IDejercicio);
$H_H20 = pintarCantReglaNeg("Shaber", "2113", $IDejercicio);
$H_H21 = pintarCantReglaNeg("Shaber", "2114", $IDejercicio);
$H_H22 = pintarCantReglaNeg("Shaber", "2115", $IDejercicio);
$H_H23 = pintarCantReglaNeg("Shaber", "2116", $IDejercicio);
$H_H25 = pintarCantReglaNeg("Shaber", "2201", $IDejercicio);
$H_H26 = pintarCantReglaNeg("Shaber", "2202", $IDejercicio);
$H_H27 = pintarCantReglaNeg("Shaber", "2203", $IDejercicio);
$H_H28 = pintarCantReglaNeg("Shaber", "2204", $IDejercicio);
$H_H29 = pintarCantReglaNeg("Shaber", "2205", $IDejercicio);
$H_H30 = pintarCantReglaNeg("Shaber", "2206", $IDejercicio);
$H_H33 = pintarCantReglaNeg("Shaber", "3101", $IDejercicio);
$H_H34 = pintarCantReglaNeg("Shaber", "3102", $IDejercicio);
$H_H35 = pintarCantReglaNeg("Shaber", "3103", $IDejercicio);
$H_H36 = pintarCantReglaNeg("Shaber", "3104", $IDejercicio);
$H_H38 = pintarCantReglaNeg("Shaber", "3201", $IDejercicio);

// Corrección para pérdida neta del ejercicio
// $H_H39 = (pintarCantidadDa("Sdebe", "3202", $IDejercicio))*-1;
$H_H39 = pintarCantReglaNegReverse("Sdebe", "3202", $IDejercicio);

$H_H40 = pintarCantReglaNeg("Shaber", "3203", $IDejercicio);

// Corrección paara pérdida acumulada
// $H_H41 = (pintarCantidadDa("Sdebe", "3204", $IDejercicio))*-1;
$H_H41 = pintarCantReglaNegReverse("Sdebe", "3204", $IDejercicio);

$H_H42 = pintarCantReglaNeg("Shaber", "3205", $IDejercicio);
$H_H43 = pintarCantReglaNeg("Shaber", "3206", $IDejercicio);
$H_H44 = pintarCantReglaNeg("Shaber", "3207", $IDejercicio);

$H_I23 = $H_H7+$H_H8+$H_H9+$H_H10+$H_H11+$H_H12+$H_H13+$H_H14+$H_H15+$H_H16+$H_H17+$H_H18+$H_H19+$H_H20+$H_H21+$H_H22+$H_H23;
$H_I30 = $H_H25+$H_H26+$H_H27+$H_H28+$H_H29+$H_H30;
$H_I36 = $H_H33+$H_H34+$H_H35+$H_H36;
$H_I44 = $H_H38+$H_H39+$H_H40+$H_H41+$H_H42+$H_H43+$H_H44;

$H_J30 = $H_I23+$H_I30;
$H_J45 = $H_I36+$H_I44;
$H_J73 = $H_J30+$H_J45;


?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sistema Contable Romero</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript" 
src="libreria/js_principal.js"></script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Balance general detallado</h1>
    </div>
    <div id="divTitularMenu"><a href="portada.php">Inicio</a> | <a href="libreria/calculadora/calculadora.html" target="_blank" onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | <a href="diarioFeAs.php?IDejercicio=<?php echo $IDejercicio; ?>&amp;metodo=<?php echo $metodo; ?>">Asientos</a> | <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenidoBal">
<div class="divNavPlan">
  <form id="navPlan" name="navPlan" method="post" action="<?php $PHP_SELF; ?>">
    Navegación por plantillas:
    <select name="planilla" class="campoSelectPlanilla" id="planilla">
      <option selected="selected">Seleccione una...</option>
      <?php
	  do {
		  echo '<option value="'. $rowP['link'] .'">'. utf8_encode($rowP['Nplanilla']) .'</option>';
	  } while ($rowP = mysql_fetch_assoc($resultP));
	  ?>
    </select>
    <input name="IDejercicio" type="hidden" id="IDejercicio" value="<?php echo $IDejercicio; ?>" />
    <input name="metodo" type="hidden" id="metodo" value="<?php echo $metodo; ?>" />
    <input name="asiento" type="hidden" id="asiento" value="<?php echo $asiento; ?>" />
<input type="submit" name="subNav" id="subNav" value="ir" />
  </form>
</div>
<div id="divSupCuerpo"><strong>Ejercicio:</strong>  <span class="divContCuerpo">
  <?php pintarNejercicio($IDejercicio); ?>
</span><br />
<strong>ESTADO DE POSICIÓN FINANCIERA</strong> al
<?php arregloFechaAs(fechasAs($IDejercicio)); ?>.</div>
<div class="divContCuerpo"><a href="verImp_balanceDet.php?IDejercicio=<?php echo $IDejercicio; ?>&amp;metodo=<?php echo $metodo; ?>&amp;asiento=<?php echo $asiento; ?>" target="_blank"><img src="imagenes/verImp.gif" alt="Versión imprimible" width="150" height="25" border="0" title="Versión imprimible" /></a></div>
<?php if($msgError) { ?>
<div class="divContCuerpoInfo"><?php echo $msgError; ?></div>
<?php } ?>
<?php if(!$msgError) { ?>
<div class="divContCuerpo">
  <table width="900" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="440" align="left" valign="top"><table cellspacing="1" cellpadding="0">
        <tr>
          <td colspan="4" align="center" valign="middle" class="celdaListaDatBalD"><strong>- ACTIVO -</strong></td>
          </tr>
        <tr>
          <td width="150" align="center" valign="top" class="celdaListaDatBalD"><strong>Circulante</strong></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Caja</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D7,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Fondo fijo de caja chica</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D8,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Fondo variable de caja chica</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D9,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Bancos</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D10,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Instrumentos financieros</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D11,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Clientes</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C12,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Estimación para cuentas de cobro dudoso</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C13,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D13,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Documentos por cobrar</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D14,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Documentos protestados</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D15,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Deudores</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D16,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Funcionarios y empleados</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D17,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">IVA acreditable</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D18,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">IVA pendiente de acreditar</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D19,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">IVA a favor</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D20,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Anticipo de impuestos</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D21,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD"><?php
          switch ($metodo) {
              case 1:
                  echo "Inventarios";
                  break;
              case 2:
                  echo "Inventarios";
                  break;
              case 3:
                  echo "Almacén";
                  break;
          }
          ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D22,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Mercancías en tránsito</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D23,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Anticipo a proveedores</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D24,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Papelería y útiles</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D25,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Propaganda y publicidad</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D26,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Muestras y literatura médica</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D27,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Primas de seguros y fianzas</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D28,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Rentas pagadas por anticipado</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D29,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Intereses pagados por anticipados</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D30,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Otros </td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D31,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_E31,2); ?></td>
        </tr>
        <tr>
          <td width="150" align="center" valign="top" class="celdaListaDatBalD"><strong>No circulante</strong></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="center" valign="top" class="celdaListaDatBalD"><strong>Propiedades, planta y equipo</strong></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Terrenos</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D34,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Edificios</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C35,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Depreciación acumulada</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C36,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D36,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Maquinaria</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C37,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Depreciación acumulada</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C38,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D38,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Mobiliario y equipo de oficina</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C39,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Depreciación acumulada</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C40,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D40,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Muebles y enseres</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C41,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Depreciación acumulada</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C42,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D42,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Equipo de transporte</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C43,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Depreciación acumulada</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C44,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D44,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Equipo de entrega y reparto</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C45,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Depreciación acumulada</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C46,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D46,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Equipo de cómputo</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C47,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Depreciación acumulada</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C48,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D48,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_E48,2); ?></td>
        </tr>
        <tr>
          <td width="150" align="center" valign="top" class="celdaListaDatBalD"><strong>Intangibles</strong></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Derechos de autor</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C50,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Amortización acumulada</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C51,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D51,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Patentes</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C52,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Amortización acumulada</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C53,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D53,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Marcas y nombres comerciales</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C54,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Amortización acumulada</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C55,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D55,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Crédito mercantil</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C56,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Amortización acumulada</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C57,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D57,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Gastos pre operativos</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C58,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Amortización acumulada</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C59,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D59,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Franquicias y licencias</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C60,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Amortización acumulada</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C61,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D61,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Gastos de instalación</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C62,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Amortización acumulada</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C63,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D63,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Gastos de constitución</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C64,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Amortización acumulada</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_C65,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D65,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_E65,2); ?></td>
        </tr>
        <tr>
          <td width="150" align="center" valign="top" class="celdaListaDatBalD"><strong>Otros</strong></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Fondos a largo plazo</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D67,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Cuentas por cobrar a largo plazo</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D68,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Pagos anticipados a largo plazo</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D69,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Inmuebles no utilizados</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D70,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Depósitos en garantía</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D71,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalD">Otros </td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_D72,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalD">$ <?php echo number_format($D_E72,2); ?></td>
        </tr>
        <tr>
          <td width="150" align="center" valign="top" class="celdaListaDatBalD"><strong>Total activo</strong></td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalD"><strong>$ <?php echo number_format($D_E73,2); ?></strong></td>
        </tr>
      </table></td>
      <td width="20" align="left" valign="top">&nbsp;</td>
      <td width="440" align="left" valign="top"><table cellspacing="1" cellpadding="0">
        <tr>
          <td colspan="4" align="center" valign="middle" class="celdaListaDatBalH"><strong>- PASIVO -</strong></td>
          </tr>
        <tr>
          <td width="150" align="center" valign="top" class="celdaListaDatBalH"><strong>A corto plazo</strong></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Bancos, saldo    acreedor</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H7,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Proveedores</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H8,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Acreedores</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H9,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Documentos por pagar</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H10,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Anticipo de clientes</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H11,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Dividendos por pagar</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H12,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">IVA causado</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H13,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">IVA pendiente de causar</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H14,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">IVA por pagar</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H15,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Impuestos y derechos por pagar</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H16,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Impuestos y derechos retenidos por enterar</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H17,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">ISR por pagar</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H18,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">PTU por pagar</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H19,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Rentas cobradas por anticipado</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H20,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Intereses cobrados por anticipado</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H21,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Acreedores bancarios</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H22,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Acreedores hipotecarios</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H23,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_I23,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="center" valign="top" class="celdaListaDatBalH"><strong> A largo plazo</strong></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Documentos por    pagar a largo plazo</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H25,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Acreedores bancarios a largo plazo</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H26,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Acreedores    hipotecarios a largo plazo</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H27,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Obligaciones en circulación a largo plazo</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H28,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Rentas cobradas por anticipado a largo plazo</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H29,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Intereses cobrados por anticipado a largo plazo</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H30,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_I30,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_J30,2); ?></td>
        </tr>
        <tr>
          <td width="150" align="center" valign="top" class="celdaListaDatBalH"><strong>Capital contable</strong></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="center" valign="top" class="celdaListaDatBalH"><strong>Capital    contribuido</strong></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Capital social</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H33,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Capital </td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H34,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Aportaciones para futuros aumentos de capital</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H35,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Prima en venta de acciones</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H36,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_I36,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="center" valign="top" class="celdaListaDatBalH"><strong>Capital ganado    (déficit)</strong></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Utilidad neta del ejercicio</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H38,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Pérdida neta del ejercicio</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H39,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Utilidades acumuladas</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H40,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Pérdidas    acumuladas</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H41,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Reserva legal</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H42,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Reserva    contractual</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H43,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">Reserva    estatutaria</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_H44,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_I44,2); ?></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="center" valign="top" class="celdaListaDatBalH"><strong>Total capital    contable</strong></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">$ <?php echo number_format($H_J45,2); ?></td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="left" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
        </tr>

        <tr>
          <td width="150" align="center" valign="top" class="celdaListaDatBalH"><strong>Total pasivo    más capital contable</strong></td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH">&nbsp;</td>
          <td align="right" valign="top" class="celdaListaDatBalH"><strong>$ <?php echo number_format($H_J73,2); ?></strong></td>
        </tr>
      </table></td>
    </tr>
  </table>
</div>
<?php } ?>
<div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
</body>
</html>