<?php
include("libreria/principal.php");

esUsuario(); // será usuario registrado?
$IDusuario = $_SESSION['userID'];
$pagina = $_SERVER['PHP_SELF'];
$IDpractica = filter_input(INPUT_GET, 'IDpractica',
        FILTER_VALIDATE_INT);
$tabla = "rom_cambios_cantidades";
if(!isset($IDpractica) || empty($IDpractica) || !validarPractica($IDpractica, 'rom_cambios_practicas'))
{
    header("Location: portada.php");
    exit;
}

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sistema Contable Romero</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="libreria/js_principal.js"></script>
<script src="libreria/vendor/jquery/jquery-1.11.0.min.js"></script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Estado de cambios en el capital contable, NIF B-4</h1>
    </div>
    <div id="divTitularMenu">
        <a href="portada.php">Inicio</a> | 
        <a href="libreria/calculadora/calculadora.html" 
           target="_blank" 
           onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | 
        <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido">
  <div class="divContCuerpo">
      <h3 class="titulo-principal">Estado de cambios en el capital contable</h3>
      <div class="migas">
          <ul>
              <li><a href="portada.php">Portada</a></li>
              <li><img src="imagenes/print.png" 
                       alt="" 
                       height="25" 
                       style="float: left; margin-right: 5px;" />
                  <a href="javascript:print();">Imprimir página</a></li>
              <li><a href="portada_cambios.php">Prácticas</a></li>
          </ul>
      </div>
      <div style="margin-bottom: 25px;"></div>
      
      <table class='cambios'>
          <tr>
              <td colspan="2" class="resaltado">
                  <span class="etiqueta">Universidad o Institución:</span><br />
                  <input oninput="ajaxCambiarFecha(this.id,<?php echo $IDpractica ?>,'rom_cambios_practicas');" 
                         id="unidad" class="campo-agno" 
                         placeholder="Unidad o Institución" 
                         type="text" name="unidad" 
                         value="<?php echo extraerFechaPracticas($IDpractica, 'rom_cambios_practicas', 'unidad') ?>" />
              </td>
              <td colspan="2" class="resaltado">
                  <span class="etiqueta">Escuela o Facultad:</span><br />
                  <input oninput="ajaxCambiarFecha(this.id,<?php echo $IDpractica ?>,'rom_cambios_practicas');" 
                         id="facultad" class="campo-agno" 
                         placeholder="Escuela o Facultad" 
                         type="text" name="facultad" 
                         value="<?php echo extraerFechaPracticas($IDpractica, 'rom_cambios_practicas', 'facultad') ?>" />
              </td>
              <td colspan="2" class="resaltado">
                  <span class="etiqueta">Asignatura:</span><br />
                  <input oninput="ajaxCambiarFecha(this.id,<?php echo $IDpractica ?>,'rom_cambios_practicas');" 
                         id="asignatura" class="campo-agno" 
                         placeholder="Asignatura" 
                         type="text" name="asignatura" 
                         value="<?php echo extraerFechaPracticas($IDpractica, 'rom_cambios_practicas', 'asignatura') ?>" />
              </td>
          </tr>
          <tr>
              <td colspan="2" class="resaltado">
                  <span class="etiqueta">Alumno:</span> <br />
                  <span class="datos"><?php echo nombreUsuario($IDusuario) ?></span>
              </td>
              <td colspan="2" class="resaltado">
                  <span class="etiqueta">Maestro:</span><br />
                  <input oninput="ajaxCambiarFecha(this.id,<?php echo $IDpractica ?>,'rom_cambios_practicas');" 
                         id="maestro" class="campo-agno" 
                         placeholder="Maestro" 
                         type="text" name="maestro" 
                         value="<?php echo extraerFechaPracticas($IDpractica, 'rom_cambios_practicas', 'maestro') ?>" />
              </td>
              <td colspan="2" class="resaltado">
                  <span class="etiqueta">Fecha de realización:</span><br />
                  <input oninput="ajaxCambiarFecha(this.id,<?php echo $IDpractica ?>,'rom_cambios_practicas');" 
                         id="fechaTarea" class="campo-agno" 
                         placeholder="Fecha" 
                         type="text" name="fechaTarea" 
                         value="<?php echo extraerFechaPracticas($IDpractica, 'rom_cambios_practicas', 'fechaTarea') ?>" />
              </td>
          </tr>
          <tr>
              <td colspan="6" class="resaltado">
                  Nombre de la entidad: <span class="datos"><?php echo nombreEntidad($IDpractica, 'rom_cambios_practicas') ?></span>
              </td>
          </tr>
          <tr>
              <td colspan="6">Estados de cambios en el capital contable al:  
              <input oninput="ajaxCambiarFecha(this.id,<?php echo $IDpractica ?>,'rom_cambios_practicas');" 
                         id="aactual" class="campo-agno" 
                         placeholder="Ej: Diciembre 31 de 2014" 
                         type="text" name="aactual" value="<?php echo extraerFechaPracticas($IDpractica, 'rom_cambios_practicas', 'aactual') ?>" />
              </td>
          </tr>
          <tr>
              <th>&nbsp;</th>
              <th style='width: 100px; background: #00ccff;'>Capital social</th>
              <th style='width: 100px; background: #00ccff;'>Utilidad neta</th>
              <th style='width: 100px; background: #00ccff;'>Utilidades acumuladas</th>
              <th style='width: 100px; background: #00ccff;'>Reserva legal</th>
              <th class='azulF' style='width: 100px'>Total capital contable</th>
          </tr>
          <tr>
              <td class='izquierda'>Saldos al 
                  <input oninput="ajaxCambiarFecha(this.id,<?php echo $IDpractica ?>,'rom_cambios_practicas');" 
                         id="ab5" class="campo-agno" 
                         placeholder="Ej: Diciembre 31 de 2014" 
                         type="text" name="ab5" value="<?php echo extraerFechaPracticas($IDpractica, 'rom_cambios_practicas', 'ab5') ?>" />
                  previamente reportados
              </td>
              <td>$ <input oninput="ajaxCambiaValorPR(this.value,this.id,<?php echo $IDpractica ?>,'rom_cambios_cantidades')" 
                                         id="c5" 
                                         class="campo-cambios" 
                                         type="text" 
                                         name="c5" 
                                         value="<?php echo extraerValorCampo('c5', $IDpractica, $tabla) ?>" />
              </td>
              <td>$ <input oninput="ajaxCambiaValorPR(this.value,this.id,<?php echo $IDpractica ?>,'rom_cambios_cantidades')" 
                                         id="d5" 
                                         class="campo-cambios" 
                                         type="text" 
                                         name="d5" 
                                         value="<?php echo extraerValorCampo('d5', $IDpractica, $tabla) ?>" />
              </td>
              <td>$ <input oninput="ajaxCambiaValorPR(this.value,this.id,<?php echo $IDpractica ?>,'rom_cambios_cantidades')" 
                                         id="e5" 
                                         class="campo-cambios" 
                                         type="text" 
                                         name="e5" 
                                         value="<?php echo extraerValorCampo('e5', $IDpractica, $tabla) ?>" />
              </td>
              <td>$ <input oninput="ajaxCambiaValorPR(this.value,this.id,<?php echo $IDpractica ?>,'rom_cambios_cantidades')" 
                                         id="f5" 
                                         class="campo-cambios" 
                                         type="text" 
                                         name="f5" 
                                         value="<?php echo extraerValorCampo('f5', $IDpractica, $tabla) ?>" />
              </td>
              <td id='g5' class='total azulF'>&nbsp;</td>
          </tr>
          <tr>
              <td class='izquierda'>Ajustes retrospectivos por corrección de errores</td>
              <td>$ <input oninput="ajaxCambiaValorPR(this.value,this.id,<?php echo $IDpractica ?>,'rom_cambios_cantidades')" 
                                         id="c6" 
                                         class="campo-cambios" 
                                         type="text" 
                                         name="c6" 
                                         value="<?php echo extraerValorCampo('c6', $IDpractica, $tabla) ?>" />
              </td>
              <td>$ <input oninput="ajaxCambiaValorPR(this.value,this.id,<?php echo $IDpractica ?>,'rom_cambios_cantidades')" 
                                         id="d6" 
                                         class="campo-cambios" 
                                         type="text" 
                                         name="d6" 
                                         value="<?php echo extraerValorCampo('d6', $IDpractica, $tabla) ?>" />
              </td>
              <td>$ <input oninput="ajaxCambiaValorPR(this.value,this.id,<?php echo $IDpractica ?>,'rom_cambios_cantidades')" 
                                         id="e6" 
                                         class="campo-cambios" 
                                         type="text" 
                                         name="e6" 
                                         value="<?php echo extraerValorCampo('e6', $IDpractica, $tabla) ?>" />
              </td>
              <td>$ <input oninput="ajaxCambiaValorPR(this.value,this.id,<?php echo $IDpractica ?>,'rom_cambios_cantidades')" 
                                         id="f6" 
                                         class="campo-cambios" 
                                         type="text" 
                                         name="f6" 
                                         value="<?php echo extraerValorCampo('f6', $IDpractica, $tabla) ?>" />
              </td>
              <td id="g6" class="total azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class='izquierda'>Saldos al <span id="ab7" class="aPractica"></span> ajustados</td>
              <td id='c7' class='total'>&nbsp;</td>
              <td id='d7' class='total'>&nbsp;</td>
              <td id='e7' class='total'>&nbsp;</td>
              <td id='f7' class='total'>&nbsp;</td>
              <td id='g7' class='total azulF'>&nbsp;</td>
          </tr>
          <tr>
              <td class='izquierda'>Dividendos decretados</td>
              <td>$ <input oninput="ajaxCambiaValorPR(this.value,this.id,<?php echo $IDpractica ?>,'rom_cambios_cantidades')" 
                                         id="c8" 
                                         class="campo-cambios" 
                                         type="text" 
                                         name="c8" 
                                         value="<?php echo extraerValorCampo('c8', $IDpractica, $tabla) ?>" />
              </td>
              <td>$ <input oninput="ajaxCambiaValorPR(this.value,this.id,<?php echo $IDpractica ?>,'rom_cambios_cantidades')" 
                                         id="d8" 
                                         class="campo-cambios" 
                                         type="text" 
                                         name="d8" 
                                         value="<?php echo extraerValorCampo('d8', $IDpractica, $tabla) ?>" />
              </td>
              <td>$ <input oninput="ajaxCambiaValorPR(this.value,this.id,<?php echo $IDpractica ?>,'rom_cambios_cantidades')" 
                                         id="e8" 
                                         class="campo-cambios" 
                                         type="text" 
                                         name="e8" 
                                         value="<?php echo extraerValorCampo('e8', $IDpractica, $tabla) ?>" />
              </td>
              <td>$ <input oninput="ajaxCambiaValorPR(this.value,this.id,<?php echo $IDpractica ?>,'rom_cambios_cantidades')" 
                                         id="f8" 
                                         class="campo-cambios" 
                                         type="text" 
                                         name="f8" 
                                         value="<?php echo extraerValorCampo('f8', $IDpractica, $tabla) ?>" />
              </td>
              <td id='g8' class='total azulF'>&nbsp;</td>
          </tr>
          <tr>
              <td class='izquierda'>Reserva legal</td>
              <td>$ <input oninput="ajaxCambiaValorPR(this.value,this.id,<?php echo $IDpractica ?>,'rom_cambios_cantidades')" 
                                         id="c9" 
                                         class="campo-cambios" 
                                         type="text" 
                                         name="c9" 
                                         value="<?php echo extraerValorCampo('c9', $IDpractica, $tabla) ?>" />
              </td>
              <td>$ <input oninput="ajaxCambiaValorPR(this.value,this.id,<?php echo $IDpractica ?>,'rom_cambios_cantidades')" 
                                         id="d9" 
                                         class="campo-cambios" 
                                         type="text" 
                                         name="d9" 
                                         value="<?php echo extraerValorCampo('d9', $IDpractica, $tabla) ?>" />
              </td>
              <td>$ <input oninput="ajaxCambiaValorPR(this.value,this.id,<?php echo $IDpractica ?>,'rom_cambios_cantidades')" 
                                         id="e9" 
                                         class="campo-cambios" 
                                         type="text" 
                                         name="e9" 
                                         value="<?php echo extraerValorCampo('e9', $IDpractica, $tabla) ?>" />
              </td>
              <td>$ <input oninput="ajaxCambiaValorPR(this.value,this.id,<?php echo $IDpractica ?>,'rom_cambios_cantidades')" 
                                         id="f9" 
                                         class="campo-cambios" 
                                         type="text" 
                                         name="f9" 
                                         value="<?php echo extraerValorCampo('f9', $IDpractica, $tabla) ?>" />
              </td>
              <td id='g9' class='total azulF'>&nbsp;</td>
          </tr>
          <tr>
              <td class='izquierda'>Utilidades acumuladas</td>
              <td>$ <input oninput="ajaxCambiaValorPR(this.value,this.id,<?php echo $IDpractica ?>,'rom_cambios_cantidades')" 
                                         id="c10" 
                                         class="campo-cambios" 
                                         type="text" 
                                         name="c10" 
                                         value="<?php echo extraerValorCampo('c10', $IDpractica, $tabla) ?>" />
              </td>
              <td>$ <input oninput="ajaxCambiaValorPR(this.value,this.id,<?php echo $IDpractica ?>,'rom_cambios_cantidades')" 
                                         id="d10" 
                                         class="campo-cambios" 
                                         type="text" 
                                         name="d10" 
                                         value="<?php echo extraerValorCampo('d10', $IDpractica, $tabla) ?>" />
              </td>
              <td>$ <input oninput="ajaxCambiaValorPR(this.value,this.id,<?php echo $IDpractica ?>,'rom_cambios_cantidades')" 
                                         id="e10" 
                                         class="campo-cambios" 
                                         type="text" 
                                         name="e10" 
                                         value="<?php echo extraerValorCampo('e10', $IDpractica, $tabla) ?>" />
              </td>
              <td>$ <input oninput="ajaxCambiaValorPR(this.value,this.id,<?php echo $IDpractica ?>,'rom_cambios_cantidades')" 
                                         id="f10" 
                                         class="campo-cambios" 
                                         type="text" 
                                         name="f10" 
                                         value="<?php echo extraerValorCampo('f10', $IDpractica, $tabla) ?>" />
              </td>
              <td id='g10' class='total azulF'>&nbsp;</td>
          </tr>
          <tr>
              <td class='izquierda'>Saldos al 
              <input oninput="ajaxCambiarFecha(this.id,<?php echo $IDpractica ?>,'rom_cambios_practicas');" 
                         id="ab11" class="campo-agno" 
                         placeholder="Ej: Diciembre 31 de 2014" 
                         type="text" name="ab11" value="<?php echo extraerFechaPracticas($IDpractica, 'rom_cambios_practicas', 'ab11') ?>" />
              </td>
              <td id='c11' class='total'>&nbsp;</td>
              <td id='d11' class='total'>&nbsp;</td>
              <td id='e11' class='total'>&nbsp;</td>
              <td id='f11' class='total'>&nbsp;</td>
              <td id='g11' class='total azulF'>&nbsp;</td>
          </tr>
          <tr>
              <td class='izquierda letrero'>Cambios en el capital en 
                  <input oninput="ajaxCambiarFecha(this.id,<?php echo $IDpractica ?>,'rom_cambios_practicas');" 
                         id="ab12" class="campo-agno" 
                         placeholder="Ej: Diciembre 31 de 2014" 
                         type="text" name="ab12" value="<?php echo extraerFechaPracticas($IDpractica, 'rom_cambios_practicas', 'ab12') ?>" />
              </td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="total azulF">&nbsp;</td>
          </tr>
          <tr>
              <td class='izquierda'>Capital emitido</td>
              <td>$ <input oninput="ajaxCambiaValorPR(this.value,this.id,<?php echo $IDpractica ?>,'rom_cambios_cantidades')" 
                                         id="c13" 
                                         class="campo-cambios" 
                                         type="text" 
                                         name="c13" 
                                         value="<?php echo extraerValorCampo('c13', $IDpractica, $tabla) ?>" />
              </td>
              <td>$ <input oninput="ajaxCambiaValorPR(this.value,this.id,<?php echo $IDpractica ?>,'rom_cambios_cantidades')" 
                                         id="d13" 
                                         class="campo-cambios" 
                                         type="text" 
                                         name="d13" 
                                         value="<?php echo extraerValorCampo('d13', $IDpractica, $tabla) ?>" />
              </td>
              <td>$ <input oninput="ajaxCambiaValorPR(this.value,this.id,<?php echo $IDpractica ?>,'rom_cambios_cantidades')" 
                                         id="e13" 
                                         class="campo-cambios" 
                                         type="text" 
                                         name="e13" 
                                         value="<?php echo extraerValorCampo('e13', $IDpractica, $tabla) ?>" />
              </td>
              <td>$ <input oninput="ajaxCambiaValorPR(this.value,this.id,<?php echo $IDpractica ?>,'rom_cambios_cantidades')" 
                                         id="f13" 
                                         class="campo-cambios" 
                                         type="text" 
                                         name="f13" 
                                         value="<?php echo extraerValorCampo('f13', $IDpractica, $tabla) ?>" />
              </td>
              <td id='g13' class='total azulF'>&nbsp;</td>
          </tr>
          <tr>
              <td class='izquierda'>Utilidad neta</td>
              <td>$ <input oninput="ajaxCambiaValorPR(this.value,this.id,<?php echo $IDpractica ?>,'rom_cambios_cantidades')" 
                                         id="c14" 
                                         class="campo-cambios" 
                                         type="text" 
                                         name="c14" 
                                         value="<?php echo extraerValorCampo('c14', $IDpractica, $tabla) ?>" />
              </td>
              <td>$ <input oninput="ajaxCambiaValorPR(this.value,this.id,<?php echo $IDpractica ?>,'rom_cambios_cantidades')" 
                                         id="d14" 
                                         class="campo-cambios" 
                                         type="text" 
                                         name="d14" 
                                         value="<?php echo extraerValorCampo('d14', $IDpractica, $tabla) ?>" />
              </td>
              <td>$ <input oninput="ajaxCambiaValorPR(this.value,this.id,<?php echo $IDpractica ?>,'rom_cambios_cantidades')" 
                                         id="e14" 
                                         class="campo-cambios" 
                                         type="text" 
                                         name="e14" 
                                         value="<?php echo extraerValorCampo('e14', $IDpractica, $tabla) ?>" />
              </td>
              <td>$ <input oninput="ajaxCambiaValorPR(this.value,this.id,<?php echo $IDpractica ?>,'rom_cambios_cantidades')" 
                                         id="f14" 
                                         class="campo-cambios" 
                                         type="text" 
                                         name="f14" 
                                         value="<?php echo extraerValorCampo('f14', $IDpractica, $tabla) ?>" />
              </td>
              <td id='g14' class='total azulF'>&nbsp;</td>
          </tr>
          <tr>
              <td class='izquierda'>Saldos al <span id="ab15" class="aPractica"></span></td>
              <td id='c15' class='total'>&nbsp;</td>
              <td id='d15' class='total'>&nbsp;</td>
              <td id='e15' class='total'>&nbsp;</td>
              <td id='f15' class='total'>&nbsp;</td>
              <td id='g15' class='total azulF'>&nbsp;</td>
          </tr>
      </table>
  </div>
  <!-- -->
  <div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
<script src="libreria/accionesAjaxPracticas.js"></script>
<script>
    window.onload = function(){
        calcularTotalesCambios();
        extraerLaFecha(<?php echo $IDpractica ?>,'ab5','ab7');
        extraerLaFecha(<?php echo $IDpractica ?>,'ab12','ab15');

        // Poner oyentes
        var fechab5 = document.getElementById('ab5');
        var fechab12 = document.getElementById('ab12');
        fechab5.addEventListener('input',function(){
            var tiempo;
            clearTimeout(tiempo);
            tiempot = window.setTimeout(function(){
                extraerLaFecha(<?php echo $IDpractica ?>,'ab5','ab7');
            },1000);
        });
        fechab12.addEventListener('input',function(){
            var tiempo;
            clearTimeout(tiempo);
            tiempot = window.setTimeout(function(){
                extraerLaFecha(<?php echo $IDpractica ?>,'ab12','ab15');
            },1000);
        });
    };
</script>
</body>
</html>