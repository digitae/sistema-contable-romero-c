<?php
include("libreria/principal.php");

esUsuario(); // será usuario registrado?
$msgAbiertos = FALSE;

if(isset($_POST['Submit'])){
    
    /*
     * ELIMINAR rom_ajustes, rom_balanza_aa y rom_balanza_da
     * 
     * Si quieren modificar el ajuste de asientos entonces hay  que eliminar
     * el contenido de rom_ajustes, rom_balanza_aa y rom_balanza_da
     * para que empiece de cero
     * 
     */
    if($_POST['actualizar'] == 'si'){
        $ejercicio = $_POST['IDejercicio'];
        $queryAjustes = "DELETE FROM rom_ajustes WHERE ejercicioAJ = '$ejercicio'";
        mysql_query($queryAjustes) or die(mysql_error());
        $queryBalanza = "DELETE FROM rom_balanza_aa WHERE ejercicio = '$ejercicio'";
        mysql_query($queryBalanza) or die(mysql_error());
        $queryBalanzaDa = "DELETE FROM rom_balanza_da WHERE ejercicio = '$ejercicio'";
        mysql_query($queryBalanzaDa) or die(mysql_error());
    }
	
	$letras = array("a","b","c","e","f","g","h","i","j","k","l","m");
	$ejercicio = $_POST['IDejercicio'];
	$metodo = $_POST['metodo'];
	$asiento = $_POST['asiento'];
	$fechaAj = $_POST['fechaAj'];
	
	// recorremos el ARRAY $letras y números impar
	foreach ($letras as $valor){		
		$campo = $valor."1";
		$cuenta = $valor."3";
		$tipo = "d";
		
		if($_POST[$campo] != 0){ // si el campo NO está vacío...
			$cantidadAj = $_POST[$campo];
			$cuentaAj = $_POST[$cuenta];
			$ejercicioAj = $ejercicio;
			// insertamos el registro
			$query = "INSERT INTO rom_ajustes (cuentaAj, cantidadAj, tipoAj, ejercicioAj, fechaAj, campoAj)
						VALUES ('$cuentaAj', '$cantidadAj', '$tipo', '$ejercicioAj', '$fechaAj', '$campo')";
			mysql_query($query) or die(mysql_error());
		}
	} // fin del recorrido del array
	
	// recorremos el ARRAY $letras y número par
	foreach ($letras as $valor){		
		$campo = $valor."2";
		$cuenta = $valor."4";
		$tipo = "h";
		
		if($_POST[$campo] != 0){ // si el campo NO está vacío...
			$cantidadAj = $_POST[$campo];
			$cuentaAj = $_POST[$cuenta];
			$ejercicioAj = $ejercicio;
			// insertamos el registro
			$query = "INSERT INTO rom_ajustes (cuentaAj, cantidadAj, tipoAj, ejercicioAj, fechaAj, campoAj)
						VALUES ('$cuentaAj', '$cantidadAj', '$tipo', '$ejercicioAj','$fechaAj', '$campo')";
			mysql_query($query) or die(mysql_error());
		}
	} // fin del recorrido del array
	
	// Ahora, llenar la tabla de balanza_aa
	$queryCuentas = "SELECT DISTINCT cuenta FROM rom_cantidades
							WHERE subcuenta = '' AND ejercicio = '$ejercicio'
							ORDER BY cuenta ASC";
			$resulCuentas = mysql_query($queryCuentas);
			$datosCuentas = mysql_fetch_assoc($resulCuentas);
			// luego recorremos el ARRAY y por cada
			// iteración conseguimos los totales en DEBE y HABER
			do{
				$cuenta = $datosCuentas['cuenta']; // cuenta de la iteración
				// DEBE
				$queryMd = "SELECT SUM(cantidad) AS Md FROM rom_cantidades
							WHERE tipo = 'd' AND cuenta = '$cuenta' AND ejercicio = '$ejercicio'";
				$resulMd = mysql_query($queryMd);
				$datosMd = mysql_fetch_array($resulMd);
				$Md = $datosMd['Md']; // movimiento DEBE
				// HABER
				$queryMh = "SELECT SUM(cantidad) AS Mh FROM rom_cantidades
							WHERE tipo = 'h' AND cuenta = '$cuenta' AND ejercicio = '$ejercicio'";
				$resulMh = mysql_query($queryMh);
				$datosMh = mysql_fetch_array($resulMh);
				$Mh = $datosMh['Mh']; // movimiento HABER
				// SALDOS
				if($Md > $Mh){
					$Sd = $Md - $Mh;
					$Sh = 0;
				} else {
					$Sh = $Mh - $Md;
					$Sd = 0;
				}
				// Insertamos los datos en rom_balanza_aa
				$queryNew = "INSERT INTO rom_balanza_aa 
							(clave, Mdebe, Mhaber, Sdebe, Shaber, ejercicio)
							VALUES ('$cuenta', '$Md', '$Mh', '$Sd', '$Sh', '$ejercicio')";
				mysql_query($queryNew) or die(mysql_error());
				// queda grabado				
			} while($datosCuentas = mysql_fetch_assoc($resulCuentas));
	
	// finalmente...
	header("Location: ajustesP.php?IDejercicio=$ejercicio&metodo=$metodo&asiento=$asiento&accion=enviado");
	exit;
}

###################
## SCRIPT GENERAL ##

// Navegación por planillas
if(isset($_POST['subNav'])){
	$planilla = $_POST['planilla'];
	$IDejercicio = $_POST['IDejercicio'];
	$metodo = $_POST['metodo'];
	$asiento = $_POST['asiento'];
	
	header("Location: $planilla?IDejercicio=$IDejercicio&metodo=$metodo&asiento=$asiento");
	exit;		
}

// comprobar que han iniciado un ejercicio
if(isset($_GET['IDejercicio']) && isset($_GET['metodo']) && isset($_GET['asiento'])){
	$IDejercicio = $_GET['IDejercicio'];
	$metodo = $_GET['metodo'];
	$asiento = $_GET['asiento'];
} else {
	header("Location: portada.php?ejercicio=false");
	exit;	
}
//fin

###################

// Extraer fecha del asiento
$queryFecha = "SELECT Fasiento, redaccion, status FROM rom_asiento WHERE asientoR = '$asiento' AND Easiento = '$IDejercicio'";
$resultFecha = mysql_query($queryFecha);
$rowFecha = mysql_fetch_assoc($resultFecha);
$fechaAsiento = $rowFecha['Fasiento'];
$statusAsiento = $rowFecha['status']; // status del asiento ** IMPORTANTE **

// pintar navegación de plantillas
$queryP		= "SELECT * FROM rom_planillas WHERE Mplanilla = 4 OR Mplanilla = '$metodo' ORDER BY IDplanilla ASC";
$resultP	= mysql_query($queryP) or die (mysql_error());
$rowP		= mysql_fetch_assoc($resultP);

// Saber si hay una BALANZA ANTES DE AJUSTES
$queryBaa = "SELECT COUNT(IDbalanza_aa) AS baa FROM rom_balanza_aa WHERE ejercicio = '$IDejercicio'";
$resulBaa = mysql_query($queryBaa);
$datosBaa = mysql_fetch_array($resulBaa);

if($datosBaa['baa'] == 0){ // No hay balanza antes de ajuste
	$subtitulo = "IMPORTANTE: Al momento de llenar el ASIENTO DE AJUSTES, no se podrá crear ni eliminar algún asiento en el ejercicio. 
				Por favor, téngalo presente antes de enviar éste formulario.";
} else {
	// Saber si ya hay un AJUSTE del ejercicio en la BD
	$buscarRegistros = "SELECT COUNT(IDajuste) AS registros FROM rom_ajustes WHERE ejercicioAj = '$IDejercicio'";
	$resultadoBuscar = mysql_query($buscarRegistros) or die(mysql_error());
	$datosResultado = mysql_fetch_array($resultadoBuscar);
	// cuenta de los registros
	$registros = $datosResultado['registros'];
	
	// subtítulo
	if($registros == 0) {
		$subtitulo = "Llene el siguiente formulario:";
	} else {
		$subtitulo = "Los AJUSTES ya han sido realizados";
		// suma de cantidades
		$querySumaD = "SELECT SUM(cantidadAj) AS total FROM rom_ajustes WHERE tipoAJ = 'd' AND ejercicioAj = '$IDejercicio'";
		$resultSumaD = mysql_query($querySumaD);
		$datosSumaD = mysql_fetch_array($resultSumaD);
		$totalSumaD = number_format($datosSumaD['total'],2);
		
		$querySumaH = "SELECT SUM(cantidadAj) AS total FROM rom_ajustes WHERE tipoAJ = 'h' AND ejercicioAj = '$IDejercicio'";
		$resultSumaH = mysql_query($querySumaH);
		$datosSumaH = mysql_fetch_array($resultSumaH);
		$totalSumaH = number_format($datosSumaH['total'],2);
	}
}

// saber si todos los asientos están CERRADOS
$queryAbierto = "SELECT COUNT(IDasiento) AS abierto
						FROM rom_asiento WHERE Easiento = '$IDejercicio' AND status = 0";
		$resulAbierto = mysql_query($queryAbierto);
		$datosAbierto = mysql_fetch_assoc($resulAbierto);
		$abiertos = $datosAbierto['abierto'];
		if($abiertos != 0) {
			$msgAbiertos = true;
		}

function fechaAj($ejercicio){
	$query = "SELECT fechaAj FROM rom_ajustes WHERE ejercicioAj = '$ejercicio'";
	$result = mysql_query($query);
	$dato = mysql_fetch_assoc($result);
        $num = mysql_num_rows($result);
        
        if($num == 0) {
            $fechaAJ = date('Y-m-d');
        } else {
            $fechaAJ = $dato['fechaAj'];
        }
        
	// return arregloFechaAs($fechaAj);
        echo $fechaAJ;
}

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sistema Contable Romero</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="libreria/js_principal.js" type="text/javascript"></script>
<link rel="stylesheet" href="libreria/calendario/calendar.css">
<script language="JavaScript" src="libreria/calendario/calendar_db.js"></script>
<script language="javascript" type="text/javascript">
    
var actualizarAA = function(){
        
    document.getElementById("Submit").value = "¡Enviar formulario!";
    document.getElementById("Submit").disabled = false;
    document.getElementById('actualizar').value = 'si';
    document.getElementById('actualizarAjustes').innerHTML = "";

};
    
function mostrarTxtBoton()
{
	var texto;
	var registro;
	registro = "<?php echo $registros; ?>";
	if(registro == 0)
	{
		texto = "¡Enviar formulario!";
	} else {
		texto = "¡Se han realizado los AJUSTES!";
		document.getElementById("Submit").disabled = "true";
		var totalSumaD = "<?php echo $totalSumaD; ?>";
		var totalSumaH = "<?php echo $totalSumaH; ?>";		
		document.getElementById("totalD").value = totalSumaD;
		document.getElementById("totalH").value = totalSumaH;
                document.getElementById('actualizarAjustes').innerHTML = "<a onclick='actualizarAA();' href='javascript:void(0);'>¿Deseas atualizar el asiento de ajustes?</a>";
	}
	document.getElementById("Submit").value = texto;
}

// sumar todos los campos DEBE
function sumatoria()
{
	var sumaD; // variable PRIVADA de la suma
	sumaD = 0; // incializar la variable
	sumaD = parseFloat(sumaD);
	var sumaH; // variable PRIVADA de la suma
	sumaH = 0; // incializar la variable
	sumaH = parseFloat(sumaH);
	var campos = new Array("a","b","c","e","f","g","h","i","j","k","l","m");
	var iteraciones = campos.length;
	for(i=0;i<iteraciones;i++)
	{
		cantidadD = document.getElementById(campos[i]+1).value;
		cantidadD = parseFloat(cantidadD);
		sumaD += cantidadD;
		
		cantidadH = document.getElementById(campos[i]+2).value;
		cantidadH = parseFloat(cantidadH);
		sumaH += cantidadH;
	}
	if("<?php echo $registros; ?>" == 0)
	{
		document.getElementById("totalD").value = sumaD;
		document.getElementById("totalH").value = sumaH;
	}
}
function Validador(theForm)
{
  if (theForm.fechaAj.value == "")
  {
    alert("El campo fecha está vacío.");
    return (false);
  }
  return (true);
}
</script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body onload="mostrarTxtBoton();">
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Asientos de ajuste</h1>
    </div>
    <div id="divTitularMenu"><a href="portada.php">Inicio</a> | <a href="libreria/calculadora/calculadora.html" target="_blank" onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | <a href="diarioFeAs.php?IDejercicio=<?php echo $IDejercicio; ?>&metodo=<?php echo $metodo; ?>">Asientos</a> | <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido">
<div class="divNavPlan">
  <form id="navPlan" name="navPlan" method="post" action="<?php $PHP_SELF; ?>">
    Navegación por plantillas:
    <select name="planilla" class="campoSelectPlanilla" id="planilla">
      <option selected="selected">Seleccione una...</option>
      <?php
	  do {
		  echo '<option value="'. $rowP['link'] .'">'. utf8_encode($rowP['Nplanilla']) .'</option>';
	  } while ($rowP = mysql_fetch_assoc($resultP));
	  ?>
    </select>
    <input name="IDejercicio" type="hidden" id="IDejercicio" value="<?php echo $IDejercicio; ?>" />
    <input name="metodo" type="hidden" id="metodo" value="<?php echo $metodo; ?>" />
    <input name="asiento" type="hidden" id="asiento" value="<?php echo $asiento; ?>" />
<input type="submit" name="subNav" id="subNav" value="ir" />
  </form>
</div>
<div class="divContCuerpo" id="divSupCuerpo"><strong>Fecha</strong>: <?php arregloFecha($fechaAsiento); ?>.<strong> Método</strong>: <?php pintarMetodo($metodo); ?>. <strong>Status</strong>: <?php
    if($statusAsiento==0) {
		echo "abierto";
		} else {
		echo "cerrado";	
		}
		
		?> 
  <strong>Ejercicio</strong>: <?php pintarNejercicio($IDejercicio); ?></div>
  <?php if (isset($_GET['accion']) && $_GET['accion'] == "enviado") { ?>
<div class="divContCuerpoInfo">Los AJUSTES han sido dados de alta favorablemente.</div>
<?php } ?>
<div class="celdaSubtit"><?php echo $subtitulo; ?></div>
<?php if($msgAbiertos == true) { ?>
<div class="divContCuerpoInfo"><strong>ATENCIÓN:</strong> Actualmente hay asientos <strong>ABIERTOS</strong> en este ejercicio. Por favor, <strong>cierre los asientos</strong> y vuélvalo a intentar.<br />
Para saber qué asientos están abiertos: <a href="diarioFeAs.php?IDejercicio=<?php echo $IDejercicio; ?>&amp;metodo=<?php echo $metodo; ?>">mostrar lista de asientos</a>.</div>
<?php } ?>
<div class="divContCuerpo">
    <form action="ajustesP.php" class="form-2014"
          method="post" enctype="multipart/form-data" name="ajustesA" id="ajustesA" onsubmit="return Validador(this)">
  <fieldset>
  <legend>Registros del asiento</legend>
  <table border="0" cellpadding="0" cellspacing="3" class="tablaLista">
      <tr>
        <td colspan="2" align="center" valign="middle" class="celdaListaDat">Fecha del ajuste: 
          <input name="fechaAj" type="text" id="fechaAj" size="10" value="<?php fechaAj($IDejercicio); ?>" />
          <script language="JavaScript" type="text/javascript">
	new tcal ({
		// form name
		'formname': 'ajustesA',
		// input name
		'controlname': 'fechaAj'
	});
	</script>
          </td>
        <td align="left" valign="middle">&nbsp;</td>
        <td align="center" valign="middle">&nbsp;</td>
        <td align="center" valign="middle">&nbsp;</td>
      </tr>
      <tr>
        <td align="center" valign="middle">&nbsp;</td>
        <td align="left" valign="middle">&nbsp;</td>
        <td align="left" valign="middle">&nbsp;</td>
        <td align="center" valign="middle">&nbsp;</td>
        <td align="center" valign="middle">&nbsp;</td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle" class="celdaListaTit"><strong>Clave</strong></td>
        <td width="175" align="left" valign="middle">&nbsp;</td>
        <td width="175" align="left" valign="middle">&nbsp;</td>
        <td align="center" valign="middle" class="celdaListaDatA"><strong>Debe</strong></td>
        <td align="center" valign="middle" class="celdaListaDatV"><strong>Haber</strong></td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="left" valign="middle" class="celdaListaTit"><strong>A</strong></td>
        <td align="center" valign="middle" class="celdaListaDatA">&nbsp;</td>
        <td align="center" valign="middle" class="celdaListaDatV">&nbsp;</td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle" class="celdaListaDat"><label>P4202</label></td>
        <td width="175" align="left" valign="middle" class="celdaListaDat">Ventas </td>
        <td width="175" align="center" valign="middle">&nbsp;</td>
        <td align="center" valign="middle" class="celdaListaDatA">$
          <input name="a1" type="text" id="a1" onchange="sumatoria();" value="<?php echo ponerValor("a1",$IDejercicio); ?>" size="12" /></td>
        <td align="center" valign="middle" class="celdaListaDatV"><input name="a3" type="hidden" id="a3" value="P4202" /></td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle" class="celdaListaDat"><label>P4101</label></td>
        <td width="175" align="left" valign="top">&nbsp;</td>
        <td width="175" align="left" valign="top" class="celdaListaDat">Costo de ventas </td>
        <td align="left" valign="top" class="celdaListaDatA"><input name="a4" type="hidden" id="a4" value="P4101" />        </td>
        <td align="left" valign="top" class="celdaListaDatV">$ 
          <input name="a2" type="text" id="a2" onchange="sumatoria();" value="<?php echo ponerValor("a2",$IDejercicio); ?>" size="12" /></td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="left" valign="top">Traspaso para determinar la utilidad bruta </td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="left" valign="top" class="celdaListaTit"><strong>B</strong></td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle" class="celdaListaDat">P4202</td>
        <td width="175" align="left" valign="middle" class="celdaListaDat">Ventas </td>
        <td width="175" align="center" valign="middle">&nbsp;</td>
        <td align="center" valign="middle" class="celdaListaDatA">$ 
          <input name="b1" type="text" id="b1" onchange="sumatoria();" value="<?php echo ponerValor("b1",$IDejercicio); ?>" size="12" /></td>
        <td align="center" valign="middle" class="celdaListaDatV"><input name="b3" type="hidden" id="b3" value="P4202" /></td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle" class="celdaListaDat">4208</td>
        <td width="175" align="left" valign="top">&nbsp;</td>
        <td width="175" align="left" valign="top" class="celdaListaDat">Pérdidas y ganancias </td>
        <td align="left" valign="top" class="celdaListaDatA">          <input name="b4" type="hidden" id="b4" value="4208" />        </td>
        <td align="left" valign="top" class="celdaListaDatV">$ 
          <input name="b2" type="text" id="b2" onchange="sumatoria();" value="<?php echo ponerValor("b2",$IDejercicio); ?>" size="12" /></td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="left" valign="top">Traspaso de la utilidad bruta a pérdidas y ganancias </td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="left" valign="top" class="celdaListaTit"><strong>C</strong></td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle" class="celdaListaDat">4208</td>
        <td width="175" align="left" valign="middle" class="celdaListaDat">Pérdidas y ganancias </td>
        <td width="175" align="center" valign="middle">&nbsp;</td>
        <td align="center" valign="middle" class="celdaListaDatA">$ 
          <input name="c1" type="text" id="c1" onchange="sumatoria();" value="<?php echo ponerValor("c1",$IDejercicio); ?>" size="12" /></td>
        <td align="center" valign="middle" class="celdaListaDatV"><input name="c3" type="hidden" id="c3" value="4208" /></td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle" class="celdaListaDat">P4202</td>
        <td width="175" align="left" valign="top">&nbsp;</td>
        <td width="175" align="left" valign="top" class="celdaListaDat">Ventas</td>
        <td align="left" valign="top" class="celdaListaDatA">          <input name="c4" type="hidden" id="c4" value="P4202" />        </td>
        <td align="left" valign="top" class="celdaListaDatV">$ 
          <input name="c2" type="text" id="c2" onchange="sumatoria();" value="<?php echo ponerValor("c2",$IDejercicio); ?>" size="12" /></td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="left" valign="top">Traspaso de la pérdida bruta a pérdidas y ganancias </td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
      </tr>
      <!--
      <tr>
        <td width="75" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="left" valign="top" class="celdaListaTit"><strong>D</strong></td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle" class="celdaListaDat">4109</td>
        <td width="175" align="left" valign="middle" class="celdaListaDat">Otros gastos </td>
        <td width="175" align="center" valign="middle">&nbsp;</td>
        <td align="center" valign="middle" class="celdaListaDatA">$ 
          <input name="d1" type="text" id="d1" onchange="sumatoria();" value="<?php echo ponerValor("d1",$IDejercicio); ?>" size="12" /></td>
        <td align="center" valign="middle" class="celdaListaDatV"><input name="d3" type="hidden" id="d3" value="4109" /></td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle" class="celdaListaDat">2112</td>
        <td width="175" align="left" valign="top">&nbsp;</td>
        <td width="175" align="left" valign="top" class="celdaListaDat">PTU por pagar </td>
        <td align="left" valign="top" class="celdaListaDatA">          <input name="d4" type="hidden" id="d4" value="2112" />        </td>
        <td align="left" valign="top" class="celdaListaDatV">$ 
          <input name="d2" type="text" id="d2" onchange="sumatoria();" value="<?php echo ponerValor("d2",$IDejercicio); ?>" size="12" /></td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="left" valign="top">Creación de  pasivo por la PTU del ejercicio </td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
      </tr>
      -->
      <tr>
        <td width="75" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="left" valign="top" class="celdaListaTit"><strong>D</strong></td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle" class="celdaListaDat">4208</td>
        <td width="175" align="left" valign="middle" class="celdaListaDat">Pérdidas y ganancias </td>
        <td width="175" align="center" valign="middle">&nbsp;</td>
        <td align="center" valign="middle" class="celdaListaDatA">$ 
          <input name="e1" type="text" id="e1" onchange="sumatoria();" value="<?php echo ponerValor("e1",$IDejercicio); ?>" size="12" /></td>
        <td align="center" valign="middle" class="celdaListaDatV"><input name="e3" type="hidden" id="e3" value="4208" /></td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle" class="celdaListaDat">4107</td>
        <td width="175" align="left" valign="top">&nbsp;</td>
        <td width="175" align="left" valign="top" class="celdaListaDat">Gastos de venta </td>
        <td align="left" valign="top" class="celdaListaDatA">          <input name="e4" type="hidden" id="e4" value="4107" />        </td>
        <td align="left" valign="top" class="celdaListaDatV">$ 
          <input name="e2" type="text" id="e2" onchange="sumatoria();" value="<?php echo ponerValor("e2",$IDejercicio); ?>" size="12" /></td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="left" valign="top"><p>Traspaso a pérdidas y ganancias para determinar la utilidad<br />
          (pérdida) de operación</p>          </td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="left" valign="top" class="celdaListaTit"><strong>E</strong></td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle" class="celdaListaDat">4208</td>
        <td width="175" align="left" valign="middle" class="celdaListaDat">Pérdidas y ganancias </td>
        <td width="175" align="center" valign="middle">&nbsp;</td>
        <td align="center" valign="middle" class="celdaListaDatA">$ 
          <input name="f1" type="text" id="f1" onchange="sumatoria();" value="<?php echo ponerValor("f1",$IDejercicio); ?>" size="12" /></td>
        <td align="center" valign="middle" class="celdaListaDatV"><input name="f3" type="hidden" id="f3" value="4208" /></td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle" class="celdaListaDat">4108</td>
        <td width="175" align="left" valign="top">&nbsp;</td>
        <td width="175" align="left" valign="top" class="celdaListaDat">Gastos de administración </td>
        <td align="left" valign="top" class="celdaListaDatA">          <input name="f4" type="hidden" id="f4" value="4108" />        </td>
        <td align="left" valign="top" class="celdaListaDatV">$ 
          <input name="f2" type="text" id="f2" onchange="sumatoria();" value="<?php echo ponerValor("f2",$IDejercicio); ?>" size="12" /></td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="left" valign="top">Traspaso a pérdidas y ganancias para determinar la utilidad<br />
          (pérdida) de operación</td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="left" valign="top" class="celdaListaTit"><strong>F</strong></td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle" class="celdaListaDat">4206</td>
        <td width="175" align="left" valign="middle" class="celdaListaDat">Otros ingresos </td>
        <td width="175" align="center" valign="middle">&nbsp;</td>
        <td align="center" valign="middle" class="celdaListaDatA">$ 
          <input name="g1" type="text" id="g1" onchange="sumatoria();" value="<?php echo ponerValor("g1",$IDejercicio); ?>" size="12" /></td>
        <td align="center" valign="middle" class="celdaListaDatV"><input name="g3" type="hidden" id="g3" value="4206" /></td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle" class="celdaListaDat">4208</td>
        <td width="175" align="left" valign="top">&nbsp;</td>
        <td width="175" align="left" valign="top" class="celdaListaDat">Pérdidas y ganancias </td>
        <td align="left" valign="top" class="celdaListaDatA">          <input name="g4" type="hidden" id="g4" value="4208" />        </td>
        <td align="left" valign="top" class="celdaListaDatV">$ 
          <input name="g2" type="text" id="g2" onchange="sumatoria();" value="<?php echo ponerValor("g2",$IDejercicio); ?>" size="12" /></td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="left" valign="top">Traspaso a pérdidas y ganancias</td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="left" valign="top" class="celdaListaTit"><strong>G</strong></td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle" class="celdaListaDat">4208</td>
        <td width="175" align="left" valign="middle" class="celdaListaDat">Pérdidas y ganancias </td>
        <td width="175" align="center" valign="middle">&nbsp;</td>
        <td align="center" valign="middle" class="celdaListaDatA">$          
          <input name="h1" type="text" id="h1" onchange="sumatoria();" value="<?php echo ponerValor("h1",$IDejercicio); ?>" size="12" /></td>
        <td align="center" valign="middle" class="celdaListaDatV"><input name="h3" type="hidden" id="h3" value="4208" /></td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle" class="celdaListaDat">4109</td>
        <td width="175" align="left" valign="top">&nbsp;</td>
        <td width="175" align="left" valign="top" class="celdaListaDat">Otros gastos </td>
        <td align="left" valign="top" class="celdaListaDatA"><input name="h4" type="hidden" id="h4" value="4109" /></td>
        <td align="left" valign="top" class="celdaListaDatV">$ 
          <input name="h2" type="text" id="h2" onchange="sumatoria();" value="<?php echo ponerValor("h2",$IDejercicio); ?>" size="12" /></td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="left" valign="top">Traspaso a pérdidas y ganancias </td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="left" valign="top" class="celdaListaTit"><strong>H</strong></td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle" class="celdaListaDat">4207</td>
        <td width="175" align="left" valign="middle" class="celdaListaDat">Resultado integral de financiamiento a favor </td>
        <td width="175" align="center" valign="middle">&nbsp;</td>
        <td align="center" valign="middle" class="celdaListaDatA">$ 
          <input name="i1" type="text" id="i1" onchange="sumatoria();" value="<?php echo ponerValor("i1",$IDejercicio); ?>" size="12" /></td>
        <td align="center" valign="middle" class="celdaListaDatV"><input name="i3" type="hidden" id="i3" value="4207" /></td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle" class="celdaListaDat">4208</td>
        <td width="175" align="left" valign="top">&nbsp;</td>
        <td width="175" align="left" valign="top" class="celdaListaDat">Pérdidas y ganancias </td>
        <td align="left" valign="top" class="celdaListaDatA">          <input name="i4" type="hidden" id="i4" value="4208" />        </td>
        <td align="left" valign="top" class="celdaListaDatV">$ 
          <input name="i2" type="text" id="i2" onchange="sumatoria();" value="<?php echo ponerValor("i2",$IDejercicio); ?>" size="12" /></td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="left" valign="top">Traspaso a pérdidas y ganancias</td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="left" valign="top" class="celdaListaTit"><strong>I</strong></td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle" class="celdaListaDat">4208</td>
        <td width="175" align="left" valign="middle" class="celdaListaDat">Pérdidas y ganancias </td>
        <td width="175" align="center" valign="middle">&nbsp;</td>
        <td align="center" valign="middle" class="celdaListaDatA">$ 
          <input name="j1" type="text" id="j1" onchange="sumatoria();" value="<?php echo ponerValor("j1",$IDejercicio); ?>" size="12" /></td>
        <td align="center" valign="middle" class="celdaListaDatV"><input name="j3" type="hidden" id="j3" value="4208" /></td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle" class="celdaListaDat">4110</td>
        <td width="175" align="left" valign="top">&nbsp;</td>
        <td width="175" align="left" valign="top" class="celdaListaDat">Resultado integral de financiamiento a cargo </td>
        <td align="left" valign="top" class="celdaListaDatA">          <input name="j4" type="hidden" id="j4" value="4110" />        </td>
        <td align="left" valign="top" class="celdaListaDatV">$ 
          <input name="j2" type="text" id="j2" onchange="sumatoria();" value="<?php echo ponerValor("j2",$IDejercicio); ?>" size="12" /></td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="left" valign="top">Traspaso a pérdidas y ganancias para determinar la utilidad<br />
          antes de impuestos o, la pérdida neta del ejercicio</td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="left" valign="top" class="celdaListaTit"><strong>J</strong></td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle" class="celdaListaDat">4208</td>
        <td width="175" align="left" valign="middle" class="celdaListaDat">Pérdidas y ganancias </td>
        <td width="175" align="center" valign="middle">&nbsp;</td>
        <td align="center" valign="middle" class="celdaListaDatA">$ 
          <input name="k1" type="text" id="k1" onchange="sumatoria();" value="<?php echo ponerValor("k1",$IDejercicio); ?>" size="12" /></td>
        <td align="center" valign="middle" class="celdaListaDatV"><input name="k3" type="hidden" id="k3" value="4208" /></td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle" class="celdaListaDat">2111</td>
        <td width="175" align="left" valign="top">&nbsp;</td>
        <td width="175" align="left" valign="top" class="celdaListaDat">ISR por pagar </td>
        <td align="left" valign="top" class="celdaListaDatA">          <input name="k4" type="hidden" id="k4" value="2111" />        </td>
        <td align="left" valign="top" class="celdaListaDatV">$ 
          <input name="k2" type="text" id="k2" onchange="sumatoria();" value="<?php echo ponerValor("k2",$IDejercicio); ?>" size="12" /></td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="left" valign="top">Creación del pasivo por el ISR del ejercicio</td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="left" valign="top" class="celdaListaTit"><strong>K</strong></td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle" class="celdaListaDat">4208</td>
        <td width="175" align="left" valign="middle" class="celdaListaDat">Pérdidas y ganancias </td>
        <td width="175" align="center" valign="middle">&nbsp;</td>
        <td align="center" valign="middle" class="celdaListaDatA">$ 
          <input name="l1" type="text" id="l1" onchange="sumatoria();" value="<?php echo ponerValor("l1",$IDejercicio); ?>" size="12" /></td>
        <td align="center" valign="middle" class="celdaListaDatV"><input name="l3" type="hidden" id="l3" value="4208" /></td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle" class="celdaListaDat">3201</td>
        <td width="175" align="left" valign="top">&nbsp;</td>
        <td width="175" align="left" valign="top" class="celdaListaDat">Utilidad neta del ejercicio </td>
        <td align="left" valign="top" class="celdaListaDatA">          <input name="l4" type="hidden" id="l4" value="3201" />        </td>
        <td align="left" valign="top" class="celdaListaDatV">$ 
          <input name="l2" type="text" id="l2" onchange="sumatoria();" value="<?php echo ponerValor("l2",$IDejercicio); ?>" size="12" /></td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="left" valign="top">Traspaso de la utilidad neta del ejercicio</td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="left" valign="top" class="celdaListaTit"><strong>L</strong></td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle" class="celdaListaDat">3202</td>
        <td width="175" align="left" valign="middle" class="celdaListaDat">Pérdida neta del ejercicio </td>
        <td width="175" align="center" valign="middle">&nbsp;</td>
        <td align="center" valign="middle" class="celdaListaDatA">$ 
          <input name="m1" type="text" id="m1" onchange="sumatoria();" value="<?php echo ponerValor("m1",$IDejercicio); ?>" size="12" /></td>
        <td align="center" valign="middle" class="celdaListaDatV"><input name="m3" type="hidden" id="m3" value="3202" /></td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle" class="celdaListaDat">4208</td>
        <td width="175" align="left" valign="top">&nbsp;</td>
        <td width="175" align="left" valign="top" class="celdaListaDat">Pérdidas y ganancias </td>
        <td align="left" valign="top" class="celdaListaDatA"><input name="m4" type="hidden" id="m4" value="4208" />        </td>
        <td align="left" valign="top" class="celdaListaDatV">$ 
          <input name="m2" type="text" id="m2" onchange="sumatoria();" value="<?php echo ponerValor("m2",$IDejercicio); ?>" size="12" /></td>
      </tr>
      <tr>
        <td width="75" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="left" valign="top">Traspaso de la pérdida neta del ejercicio</td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
      </tr>
	  <tr>
        <td width="75" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="right" valign="middle" class="celdaListaTit"><strong>Sumas iguales:</strong></td>
        <td align="left" valign="top" class="celdaListaDatA">$ 
          <input name="totalD" type="text" id="totalD" size="12" /></td>
        <td align="left" valign="top" class="celdaListaDatV">$
        <input name="totalH" type="text" id="totalH" size="12" /></td>
	  </tr>
	  <tr>
	    <td width="75" align="center" valign="middle">&nbsp;</td>
	    <td width="175" align="right" valign="top">&nbsp;</td>
	    <td width="175" align="right" valign="top">&nbsp;</td>
	    <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
	    <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
	    </tr>
	  <tr>
	    <td width="75" align="center" valign="middle">&nbsp;</td>
	    <td width="175" align="right" valign="top">&nbsp;</td>
	    <td width="175" align="right" valign="top">&nbsp;</td>
	    <td colspan="2" align="center" valign="top" class="celdaListaTit">
	      <input name="IDejercicio" type="hidden" id="IDejercicio" value="<?php echo $IDejercicio; ?>" />
	      <input name="metodo" type="hidden" id="metodo" value="<?php echo $metodo; ?>" />
	      <input name="asiento" type="hidden" id="asiento" value="<?php echo $asiento; ?>" />
              <input type="hidden" name="actualizar" id="actualizar" value="no" />
	      <input type="submit" name="Submit" id="Submit" value="Enviar formulario" onclick="return confirm('¿Está seguro enviar el formulario de AJUSTES?');" />
              <br /><span id="actualizarAjustes"></span>
	    </td>
	    </tr>
    </table>
  </fieldset></form></div>
<div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
</body>
</html>