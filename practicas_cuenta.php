<?php
include("libreria/principal.php");

esUsuario(); // será usuario registrado?
$IDusuario = $_SESSION['userID'];
$pagina = $_SERVER['PHP_SELF'];
$IDpractica = filter_input(INPUT_GET, 'IDpractica',
        FILTER_VALIDATE_INT);
$tabla = "rom_cuenta_cantidades";
if(!isset($IDpractica) || empty($IDpractica) || !validarPractica($IDpractica, 'rom_cuenta_practicas'))
{
    header("Location: portada.php");
    exit;
}

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sistema Contable Romero</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="libreria/js_principal.js"></script>
<script src="libreria/vendor/jquery/jquery-1.11.0.min.js"></script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Estado de situación financiera, NIF B-6, cuenta</h1>
    </div>
    <div id="divTitularMenu">
        <a href="portada.php">Inicio</a> | 
        <a href="libreria/calculadora/calculadora.html" 
           target="_blank" 
           onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | 
        <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido" style="width: 1200px;">
  <div class="divContCuerpo">
      <h3 class="titulo-principal">Estado de situación financiera</h3>
      <div class="migas">
          <ul>
              <li><a href="portada.php">Portada</a></li>
              <li><a href="portada_cuenta.php">Prácticas</a></li>
              <li><img src="imagenes/print.png" 
                       alt="" 
                       height="25" 
                       style="float: left; margin-right: 5px;" />
                  <a href="javascript:print();">Imprimir página</a></li>
              <li><a href="razones_cuenta.php?IDpractica=<?php echo $IDpractica ?>">Razones Financieras</a></li>
          </ul>
      </div>
      <div style="margin-bottom: 25px;"></div>
      
      <table class="practicas-balance" style="font-size: 11px;">
          <tr>
              <td colspan="4" class="resaltado">
                  <span class="etiqueta">Universidad o Institución:</span><br />
                  <input oninput="ajaxCambiarFecha(this.id,<?php echo $IDpractica ?>,'rom_cuenta_practicas');" 
                         id="unidad" class="campo-agno" 
                         placeholder="Unidad o Institución" 
                         type="text" name="unidad" 
                         value="<?php echo extraerFechaPracticas($IDpractica, 'rom_cuenta_practicas', 'unidad') ?>" />
              </td>
              <td colspan="4" class="resaltado">
                  <span class="etiqueta">Escuela o Facultad:</span><br />
                  <input oninput="ajaxCambiarFecha(this.id,<?php echo $IDpractica ?>,'rom_cuenta_practicas');" 
                         id="facultad" class="campo-agno" 
                         placeholder="Escuela o Facultad" 
                         type="text" name="facultad" 
                         value="<?php echo extraerFechaPracticas($IDpractica, 'rom_cuenta_practicas', 'facultad') ?>" />
              </td>
              <td colspan="4" class="resaltado">
                  <span class="etiqueta">Asignatura:</span><br />
                  <input oninput="ajaxCambiarFecha(this.id,<?php echo $IDpractica ?>,'rom_cuenta_practicas');" 
                         id="asignatura" class="campo-agno" 
                         placeholder="Asignatura" 
                         type="text" name="asignatura" 
                         value="<?php echo extraerFechaPracticas($IDpractica, 'rom_cuenta_practicas', 'asignatura') ?>" />
              </td>
          </tr>
          <tr>
              <td colspan="4" class="resaltado">
                  <span class="etiqueta">Alumno:</span> <br />
                  <span class="datos"><?php echo nombreUsuario($IDusuario) ?></span>
              </td>
              <td colspan="4" class="resaltado">
                  <span class="etiqueta">Maestro:</span><br />
                  <input oninput="ajaxCambiarFecha(this.id,<?php echo $IDpractica ?>,'rom_cuenta_practicas');" 
                         id="maestro" class="campo-agno" 
                         placeholder="Maestro" 
                         type="text" name="maestro" 
                         value="<?php echo extraerFechaPracticas($IDpractica, 'rom_cuenta_practicas', 'maestro') ?>" />
              </td>
              <td colspan="4" class="resaltado">
                  <span class="etiqueta">Fecha de realización:</span><br />
                  <input oninput="ajaxCambiarFecha(this.id,<?php echo $IDpractica ?>,'rom_cuenta_practicas');" 
                         id="fechaTarea" class="campo-agno" 
                         placeholder="Fecha" 
                         type="text" name="fechaTarea" 
                         value="<?php echo extraerFechaPracticas($IDpractica, 'rom_cuenta_practicas', 'fechaTarea') ?>" />
              </td>
          </tr>
          <tr>
              <td colspan="12" class="resaltado">
                  Nombre de la entidad: <span class="datos"><?php echo nombreEntidad($IDpractica, 'rom_cuenta_practicas') ?></span>
              </td>
          </tr>
          <tr>
              <td>&nbsp;</td>
              <td colspan="2" class="resaltado">Año actual</td>
              <td colspan="2" class="resaltado">Año anterior</td>
              <td>&nbsp;</td>
              <td colspan="3" class="resaltado">Año actual</td>
              <td colspan="3" class="resaltado">Año anterior</td>
          </tr>
          <tr>
              <td>&nbsp;</td>
              <td colspan="2" class="resaltado">Estado de situación financiera al 
                  <input oninput="ajaxCambiarFecha(this.id,<?php echo $IDpractica ?>,'rom_cuenta_practicas');" 
                         id="aactual" class="campo-agno" 
                         placeholder="Ej: Diciembre 31 de 2014" 
                         type="text" name="aactual" value="<?php echo extraerFechaPracticas($IDpractica, 'rom_cuenta_practicas', 'aactual') ?>" />
              </td>
              <td colspan="2" class="resaltado">Estado de situación financiera al 
                  <input oninput="ajaxCambiarFecha(this.id,<?php echo $IDpractica ?>,'rom_cuenta_practicas');" 
                         id="aanterior" class="campo-agno" 
                         placeholder="Ej: Diciembre 31 de 2015" 
                         type="text" name="aanterior" value="<?php echo extraerFechaPracticas($IDpractica, 'rom_cuenta_practicas', 'aanterior') ?>" />
              </td>
              <td>&nbsp;</td>
              <td colspan="3" class="resaltado">Estado de situación financiera al <span id="factual"></span></td>
              <td colspan="3" class="resaltado">Estado de situación financiera al <span id="fanterior"></span> </td>
          </tr>
          <tr>
              <td class="resaltado verde" style="width: 100px">Activo</td>
              <td class="verde" style="width: 95px">&nbsp;</td>
              <td class="verde" style="width: 95px">&nbsp;</td>
              <td class="verde" style="width: 95px">&nbsp;</td>
              <td class="verde" style="width: 95px">&nbsp;</td>
              <td class="resaltado amarillo" style="width: 100px">Pasivo</td>
              <td class="amarillo" style="width: 95px">&nbsp;</td>
              <td class="amarillo" style="width: 95px">&nbsp;</td>
              <td class="amarillo" style="width: 95px">&nbsp;</td>
              <td class="amarillo" style="width: 95px">&nbsp;</td>
              <td class="amarillo" style="width: 95px">&nbsp;</td>
              <td class="amarillo" style="width: 95px">&nbsp;</td>
          </tr>
          <tr>
              <td class="resaltado verde tSecion">Circulante</td>
              <td class="verde">&nbsp;</td>
              <td class="verde">&nbsp;</td>
              <td class="verde">&nbsp;</td>
              <td class="verde">&nbsp;</td>
              <td class="resaltado amarillo tSecion">A corto plazo</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Caja</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c7" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c7" 
                                         value="<?php echo extraerValorCampo('c7', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f7" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f7" 
                                         value="<?php echo extraerValorCampo('f7', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">Proveedores</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c57" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c57" 
                                         value="<?php echo extraerValorCampo('c57', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f57" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f57" 
                                         value="<?php echo extraerValorCampo('f57', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Fondo fijo de caja chica</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c8" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c8" 
                                         value="<?php echo extraerValorCampo('c8', $IDpractica, $tabla) ?>" />                  
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f8" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f8" 
                                         value="<?php echo extraerValorCampo('f8', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">Acreedores</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c58" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c58" 
                                         value="<?php echo extraerValorCampo('c58', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f58" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f58" 
                                         value="<?php echo extraerValorCampo('f58', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Bancos</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c9" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c9" 
                                         value="<?php echo extraerValorCampo('c9', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f9" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f9" 
                                         value="<?php echo extraerValorCampo('f9', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">Documentos por pagar</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c59" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c59" 
                                         value="<?php echo extraerValorCampo('c59', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f59" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f59" 
                                         value="<?php echo extraerValorCampo('f59', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Instrumentos financieros</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c10" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c10" 
                                         value="<?php echo extraerValorCampo('c10', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f10" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f10" 
                                         value="<?php echo extraerValorCampo('f10', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">Anticipo de clientes</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c60" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c60" 
                                         value="<?php echo extraerValorCampo('c60', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f60" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f60" 
                                         value="<?php echo extraerValorCampo('f60', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Clientes</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c11" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c11" 
                                         value="<?php echo extraerValorCampo('c11', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f11" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f11" 
                                         value="<?php echo extraerValorCampo('f11', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">Acreedores bancarios</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c61" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c61" 
                                         value="<?php echo extraerValorCampo('c61', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f61" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f61" 
                                         value="<?php echo extraerValorCampo('f61', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Documentos por cobrar</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c12" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c12" 
                                         value="<?php echo extraerValorCampo('c12', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f12" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f12" 
                                         value="<?php echo extraerValorCampo('f12', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">Acreedores hipotecarios</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c62" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c62" 
                                         value="<?php echo extraerValorCampo('c62', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f62" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f62" 
                                         value="<?php echo extraerValorCampo('f62', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Deudores</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c13" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c13" 
                                         value="<?php echo extraerValorCampo('c13', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f13" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f13" 
                                         value="<?php echo extraerValorCampo('f13', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">Dividendos por pagar</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c63" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c63" 
                                         value="<?php echo extraerValorCampo('c63', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f63" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f63" 
                                         value="<?php echo extraerValorCampo('f63', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Funcionarios y empledos</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c14" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c14" 
                                         value="<?php echo extraerValorCampo('c14', $IDpractica, $tabla) ?>" /></td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f14" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f14" 
                                         value="<?php echo extraerValorCampo('f14', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">IVA causado</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c64" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c64" 
                                         value="<?php echo extraerValorCampo('c64', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f64" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f64" 
                                         value="<?php echo extraerValorCampo('f64', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">IVA acreditable</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c15" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c15" 
                                         value="<?php echo extraerValorCampo('c15', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f15" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f15" 
                                         value="<?php echo extraerValorCampo('f15', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">IVA pendiente de causar</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c65" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c65" 
                                         value="<?php echo extraerValorCampo('c65', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f65" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f65" 
                                         value="<?php echo extraerValorCampo('f65', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">IVA pendiente de acreditar</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c16" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c16" 
                                         value="<?php echo extraerValorCampo('c16', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f16" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f16" 
                                         value="<?php echo extraerValorCampo('f16', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">IVA por pagar</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c66" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c66" 
                                         value="<?php echo extraerValorCampo('c66', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f66" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f66" 
                                         value="<?php echo extraerValorCampo('f66', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">IVA a favor</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c17" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c17" 
                                         value="<?php echo extraerValorCampo('c17', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f17" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f17" 
                                         value="<?php echo extraerValorCampo('f17', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">Impuestos y derechos por pagar</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c67" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c67" 
                                         value="<?php echo extraerValorCampo('c67', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f67" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f67" 
                                         value="<?php echo extraerValorCampo('f67', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Anticipo de impuestos</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c18" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c18" 
                                         value="<?php echo extraerValorCampo('c18', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f18" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f18" 
                                         value="<?php echo extraerValorCampo('f18', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">Impuestos y derechos retenidos por enterar</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c68" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c68" 
                                         value="<?php echo extraerValorCampo('c68', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f68" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f68" 
                                         value="<?php echo extraerValorCampo('f68', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Inventarios</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c19" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c19" 
                                         value="<?php echo extraerValorCampo('c19', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f19" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f19" 
                                         value="<?php echo extraerValorCampo('f19', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">ISR por pagar</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c69" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c69" 
                                         value="<?php echo extraerValorCampo('c69', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f69" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f69" 
                                         value="<?php echo extraerValorCampo('f69', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Mercancías en tránsito</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c20" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c20" 
                                         value="<?php echo extraerValorCampo('c20', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f20" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f20" 
                                         value="<?php echo extraerValorCampo('f20', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">PTU por pagar</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c70" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c70" 
                                         value="<?php echo extraerValorCampo('c70', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f70" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f70" 
                                         value="<?php echo extraerValorCampo('f70', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Anticipo a proveedores</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c21" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c21" 
                                         value="<?php echo extraerValorCampo('c21', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f21" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f21" 
                                         value="<?php echo extraerValorCampo('f21', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">Rentas cobradas por anticipado</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c71" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c71" 
                                         value="<?php echo extraerValorCampo('c71', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f71" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f71" 
                                         value="<?php echo extraerValorCampo('f71', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Papelería y útiles</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c22" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c22" 
                                         value="<?php echo extraerValorCampo('c22', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f22" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f22" 
                                         value="<?php echo extraerValorCampo('f22', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">Intereses cobrados por anticipado</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c72" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c72" 
                                         value="<?php echo extraerValorCampo('c72', $IDpractica, $tabla) ?>" />
              </td>
              <td id="d72" class="amarillo total">$ </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f72" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f72" 
                                         value="<?php echo extraerValorCampo('f72', $IDpractica, $tabla) ?>" />
              </td>
              <td id="g72" class="amarillo total">$ </td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Propaganda y publicidad</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c23" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c23" 
                                         value="<?php echo extraerValorCampo('c23', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f23" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f23" 
                                         value="<?php echo extraerValorCampo('f23', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="resaltado amarillo tSecion">A largo plazo</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Muestras y literatura médica</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c24" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c24" 
                                         value="<?php echo extraerValorCampo('c24', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f24" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f24" 
                                         value="<?php echo extraerValorCampo('f24', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">Documentos por pagar a largo plazo</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c74" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c74" 
                                         value="<?php echo extraerValorCampo('c74', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f74" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f74" 
                                         value="<?php echo extraerValorCampo('f74', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Primas de seguros y fianzas</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c25" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c25" 
                                         value="<?php echo extraerValorCampo('c25', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f25" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f25" 
                                         value="<?php echo extraerValorCampo('f25', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">Acreedores hipotecarios</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c75" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c75" 
                                         value="<?php echo extraerValorCampo('c75', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f75" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f75" 
                                         value="<?php echo extraerValorCampo('f75', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Rentas pagadas por anticipado</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c26" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c26" 
                                         value="<?php echo extraerValorCampo('c26', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f26" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f26" 
                                         value="<?php echo extraerValorCampo('f26', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">Acreedores bancarios</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c76" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c76" 
                                         value="<?php echo extraerValorCampo('c76', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f76" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f76" 
                                         value="<?php echo extraerValorCampo('f76', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Intereses pagados por anticipado</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c27" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c27" 
                                         value="<?php echo extraerValorCampo('c27', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f27" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f27" 
                                         value="<?php echo extraerValorCampo('f27', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">Obligaciones en circulación</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c77" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c77" 
                                         value="<?php echo extraerValorCampo('c77', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f77" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f77" 
                                         value="<?php echo extraerValorCampo('f77', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Otros activos circulantes</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c28" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c28" 
                                         value="<?php echo extraerValorCampo('c28', $IDpractica, $tabla) ?>" />
              </td>
              <td id="d28" class="verde total">$ </td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f28" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f28" 
                                         value="<?php echo extraerValorCampo('f28', $IDpractica, $tabla) ?>" />
              </td>
              <td id="g28" class="verde total">$ </td>
              <td class="amarillo">Rentas cobradas por anticipado a largo plazo</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c78" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c78" 
                                         value="<?php echo extraerValorCampo('c78', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f78" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f78" 
                                         value="<?php echo extraerValorCampo('f78', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="resaltado verde">No circulante</td>
              <td class="verde">&nbsp;</td>
              <td class="verde">&nbsp;</td>
              <td class="verde">&nbsp;</td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">Intereses cobrados por anticipado a largo plazo</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c79" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c79" 
                                         value="<?php echo extraerValorCampo('c79', $IDpractica, $tabla) ?>" />
              </td>
              <td id="d79" class="amarillo total">$ </td>
              <td id="e79" class="amarillo total">$ </td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f79" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f79" 
                                         value="<?php echo extraerValorCampo('f79', $IDpractica, $tabla) ?>" />
              </td>
              <td id="g79" class="amarillo total">$ </td>
              <td id="h79" class="amarillo total">$ </td>
          </tr>
          <tr>
              <td class="resaltado verde tSecion">Propiedades, planta y equipo</td>
              <td class="verde">&nbsp;</td>
              <td class="verde">&nbsp;</td>
              <td class="verde">&nbsp;</td>
              <td class="verde">&nbsp;</td>
              <td class="resaltado amarillo">Capital contable</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Terrenos</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c31" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c31" 
                                         value="<?php echo extraerValorCampo('c31', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f31" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f31" 
                                         value="<?php echo extraerValorCampo('f31', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="resaltado amarillo tSecion">Capital contribuido</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Edificios</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c32" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c32" 
                                         value="<?php echo extraerValorCampo('c32', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f32" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f32" 
                                         value="<?php echo extraerValorCampo('f32', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">Capital social</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c82" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c82" 
                                         value="<?php echo extraerValorCampo('c82', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f82" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f82" 
                                         value="<?php echo extraerValorCampo('f82', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Maquinaria</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c33" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c33" 
                                         value="<?php echo extraerValorCampo('c33', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f33" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f33" 
                                         value="<?php echo extraerValorCampo('f33', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">Aportaciones para futuros aumentos de capital</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c83" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c83" 
                                         value="<?php echo extraerValorCampo('c83', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f83" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f83" 
                                         value="<?php echo extraerValorCampo('f83', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Mobiliario y equipo de oficina</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c34" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c34" 
                                         value="<?php echo extraerValorCampo('c34', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f34" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f34" 
                                         value="<?php echo extraerValorCampo('f34', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">Prima en venta de acciones</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c84" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c84" 
                                         value="<?php echo extraerValorCampo('c84', $IDpractica, $tabla) ?>" />
              </td>
              <td id="d84" class="amarillo total">$ </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f84" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f84" 
                                         value="<?php echo extraerValorCampo('f84', $IDpractica, $tabla) ?>" />
              </td>
              <td id="g84" class="amarillo total">$ </td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Muebles y enseres</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c35" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c35" 
                                         value="<?php echo extraerValorCampo('c35', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f35" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f35" 
                                         value="<?php echo extraerValorCampo('f35', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="resaltado amarillo tSecion">Capital ganado</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Equipo de transporte</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c36" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c36" 
                                         value="<?php echo extraerValorCampo('c36', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f36" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f36" 
                                         value="<?php echo extraerValorCampo('f36', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">Utilidad neta del ejercicio</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c86" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c86" 
                                         value="<?php echo extraerValorCampo('c86', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f86" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f86" 
                                         value="<?php echo extraerValorCampo('f86', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Equipo de entrega y reparto</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c37" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c37" 
                                         value="<?php echo extraerValorCampo('c37', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f37" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f37" 
                                         value="<?php echo extraerValorCampo('f37', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">Pérdida neta del ejercicio</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c87" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c87" 
                                         value="<?php echo extraerValorCampo('c87', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f87" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f87" 
                                         value="<?php echo extraerValorCampo('f87', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Equipo de cómputo</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c38" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c38" 
                                         value="<?php echo extraerValorCampo('c38', $IDpractica, $tabla) ?>" />
              </td>
              <td id="d38" class="verde total">$ </td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f38" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f38" 
                                         value="<?php echo extraerValorCampo('f38', $IDpractica, $tabla) ?>" />
              </td>
              <td id="g38" class="verde total">$ </td>
              <td class="amarillo">Utilidades acumuladas</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c88" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c88" 
                                         value="<?php echo extraerValorCampo('c88', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f88" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f88" 
                                         value="<?php echo extraerValorCampo('f88', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="resaltado verde tSecion">Intangibles</td>
              <td class="verde">&nbsp;</td>
              <td class="verde">&nbsp;</td>
              <td class="verde">&nbsp;</td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">Pérdidas acumuladas</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c89" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c89" 
                                         value="<?php echo extraerValorCampo('c89', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f89" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f89" 
                                         value="<?php echo extraerValorCampo('f89', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Derechos de autor</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c40" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c40" 
                                         value="<?php echo extraerValorCampo('c40', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f40" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f40" 
                                         value="<?php echo extraerValorCampo('f40', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">Reserva legal</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c90" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c90" 
                                         value="<?php echo extraerValorCampo('c90', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f90" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f90" 
                                         value="<?php echo extraerValorCampo('f90', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Patentes</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c41" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c41" 
                                         value="<?php echo extraerValorCampo('c41', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f41" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f41" 
                                         value="<?php echo extraerValorCampo('f41', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">Reserva contractual</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c91" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c91" 
                                         value="<?php echo extraerValorCampo('c91', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f91" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f91" 
                                         value="<?php echo extraerValorCampo('f91', $IDpractica, $tabla) ?>" />
              </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Marcas y nombres comerciales</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c42" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c42" 
                                         value="<?php echo extraerValorCampo('c42', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f42" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f42" 
                                         value="<?php echo extraerValorCampo('f42', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">Reserva estatutaria</td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="c92" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c92" 
                                         value="<?php echo extraerValorCampo('c92', $IDpractica, $tabla) ?>" />
              </td>
              <td id="d92" class="amarillo total">$ </td>
              <td id="e92" class="amarillo total">$ </td>
              <td class="amarillo">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                            id="f92" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f92" 
                                         value="<?php echo extraerValorCampo('f92', $IDpractica, $tabla) ?>" />
              </td>
              <td id="g92" class="amarillo total">$ </td>
              <td id="h92" class="amarillo total">$ </td>
          </tr>
          <tr>
              <td class="verde">Crédito mercantil</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c43" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c43" 
                                         value="<?php echo extraerValorCampo('c43', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f43" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f43" 
                                         value="<?php echo extraerValorCampo('f43', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Gastos pre operativos</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c44" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c44" 
                                         value="<?php echo extraerValorCampo('c44', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f44" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f44" 
                                         value="<?php echo extraerValorCampo('f44', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Franquicias</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c45" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c45" 
                                         value="<?php echo extraerValorCampo('c45', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f45" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f45" 
                                         value="<?php echo extraerValorCampo('f45', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Gastos de instalación</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c46" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c46" 
                                         value="<?php echo extraerValorCampo('c46', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f46" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f46" 
                                         value="<?php echo extraerValorCampo('f46', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Gastos de constitución</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c47" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c47" 
                                         value="<?php echo extraerValorCampo('c47', $IDpractica, $tabla) ?>" />
              </td>
              <td id="d47" class="verde total">$ </td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f47" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f47" 
                                         value="<?php echo extraerValorCampo('f47', $IDpractica, $tabla) ?>" />
              </td>
              <td id="g47" class="verde total">$ </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="resaltado verde tSecion">Otros</td>
              <td class="verde">&nbsp;</td>
              <td class="verde">&nbsp;</td>
              <td class="verde">&nbsp;</td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Fondos a largo plazo</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c49" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c49" 
                                         value="<?php echo extraerValorCampo('c49', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f49" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f49" 
                                         value="<?php echo extraerValorCampo('f49', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Cuentas por cobrar a largo plazo</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c50" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c50" 
                                         value="<?php echo extraerValorCampo('c50', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f50" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f50" 
                                         value="<?php echo extraerValorCampo('f50', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Pagos anticipados a largo plazo</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c51" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c51" 
                                         value="<?php echo extraerValorCampo('c51', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f51" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f51" 
                                         value="<?php echo extraerValorCampo('f51', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Inmuebles no utilizados</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c52" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c52" 
                                         value="<?php echo extraerValorCampo('c52', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f52" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f52" 
                                         value="<?php echo extraerValorCampo('f52', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Depósitos en garantía</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c53" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c53" 
                                         value="<?php echo extraerValorCampo('c53', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f53" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f53" 
                                         value="<?php echo extraerValorCampo('f53', $IDpractica, $tabla) ?>" />
              </td>
              <td class="verde">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde">Otros activos no circulantes</td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="c54" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="c54" 
                                         value="<?php echo extraerValorCampo('c54', $IDpractica, $tabla) ?>" /></td>
              <td id="d54" class="verde total">$ </td>
              <td class="verde">$ <input oninput="ajaxCambiaValorCambios(this.value,this.id,<?php echo $IDpractica ?>,'rom_cuenta_cantidades')" 
                                         id="f54" 
                                         class="campo-cuenta" 
                                         type="text" 
                                         name="f54" 
                                         value="<?php echo extraerValorCampo('f54', $IDpractica, $tabla) ?>" /></td>
              <td id="g54" class="verde total">$ </td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
          </tr>
          <tr>
              <td class="separador">&nbsp;</td>
              <td class="separador">&nbsp;</td>
              <td class="separador">&nbsp;</td>
              <td class="separador">&nbsp;</td>
              <td class="separador">&nbsp;</td>
              <td class="separador">&nbsp;</td>
              <td class="separador">&nbsp;</td>
              <td class="separador">&nbsp;</td>
              <td class="separador">&nbsp;</td>
              <td class="separador">&nbsp;</td>
              <td class="separador">&nbsp;</td>
              <td class="separador">&nbsp;</td>
          </tr>
          <tr>
              <td class="verde resaltado tSecion">Total activo</td>
              <td class="verde">&nbsp;</td>
              <td id="totA1" class="verde total">$</td>
              <td class="verde">&nbsp;</td>
              <td id="totA2" class="verde total">$</td>
              <td colspan="2" class="amarillo resaltado tSecion">Total pasivo más capital contable</td>
              <td class="amarillo">&nbsp;</td>
              <td id="totP1" class="amarillo total">$</td>
              <td class="amarillo">&nbsp;</td>
              <td class="amarillo">&nbsp;</td>
              <td id="totP2" class="amarillo total"></td>
          </tr>
          <tr>
              <td style="background: transparent;">&nbsp;</td>
              <td style="background: transparent;">&nbsp;</td>
              <td style="background: transparent;">&nbsp;</td>
              <td style="background: transparent;">&nbsp;</td>
              <td style="background: transparent;">&nbsp;</td>
              <td style="background: transparent;">&nbsp;</td>
              <td style="background: transparent;">&nbsp;</td>
              <td style="background: transparent;">&nbsp;</td>
              <td style="background: transparent;">&nbsp;</td>
              <td style="background: transparent;">&nbsp;</td>
              <td style="background: transparent;">&nbsp;</td>
          </tr>
          <tr>
              <td colspan="6" style="background: transparent;"><a class='bActualizar' href="practicas_cuenta.php?IDpractica=<?php echo $IDpractica ?>">Actualizar página</a></td>
              <td id="sumA" colspan="2"></td>
              <td id="difA" class="mosSuma"></td>
              <td id="sumB" colspan="2"></td>
              <td id="difB" class="mosSuma"></td>
          </tr>
      </table>
  </div>
    <div id="e54" style="display: none"></div>
    <div id="h54" style="display: none"></div>
  <!-- -->
  <div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
<script src="libreria/accionesAjaxPracticas.js"></script>
<script src="libreria/sumarColumnasCuenta.js"></script>
<script>
    window.onload = function(){
            
            // Sumar Activos
            sumarActivos();
            // Sumar Pasivos
            
    };
    
    // oyente
    var loscampos = document.querySelectorAll("input[type='text']");
    var numCampos = loscampos.length;
    
    extraerLaFechaCuenta(<?php echo $IDpractica ?>,'aactual','factual');
    extraerLaFechaCuenta(<?php echo $IDpractica ?>,'aanterior','fanterior');
    // Poner oyentes
    var aactual = document.getElementById('aactual');
    var aanterior = document.getElementById('aanterior');
    aactual.addEventListener('input',function(){
        var tiempo;
        clearTimeout(tiempo);
        tiempot = window.setTimeout(function(){
            extraerLaFechaCuenta(<?php echo $IDpractica ?>,'aactual','factual');
        },1000);
    });
    aanterior.addEventListener('input',function(){
        var tiempo;
        clearTimeout(tiempo);
        tiempot = window.setTimeout(function(){
            extraerLaFechaCuenta(<?php echo $IDpractica ?>,'aanterior','fanterior');
        },1000);
    });
    
    // Tabla final
    var ponerComparativo = function()
        {
            var d54 = limpiarCantidad(document.getElementById('totA1').innerHTML);
            var j54 = limpiarCantidad(document.getElementById('totP1').innerHTML);
            var f54 = limpiarCantidad(document.getElementById('totA2').innerHTML);
            var m54 = limpiarCantidad(document.getElementById('totP2').innerHTML);
            
            if(d54 === j54)
            {
                document.getElementById('sumA').innerHTML = "Sumas iguales";
                document.getElementById('sumA').className = "siguales";
            } else 
            {
                document.getElementById('sumA').innerHTML = "Sumas diferentes";
                document.getElementById('sumA').className = "sdiferentes";
            }
            
            if(f54 === m54)
            {
                document.getElementById('sumB').innerHTML = "Sumas iguales";
                document.getElementById('sumB').className = "siguales";
            } else 
            {
                document.getElementById('sumB').innerHTML = "Sumas iguales";
                document.getElementById('sumB').className = "siguales";
            }
            
            document.getElementById('difA').innerHTML = "$ "+(d54 - j54);
            document.getElementById('difB').innerHTML = "$ "+(f54 - m54);
        };
        
</script>
</body>
</html>