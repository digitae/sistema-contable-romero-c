<?php
include("libreria/principal.php");

esUsuario(); // será usuario registrado?


#######################################
## agregar nuevo registro al asiento ##

// Datos del formulario
if(isset($_POST['submitNcantidad'])) {
	
	$cuenta = $_POST['cuenta'];
	$cantidad = $_POST['cantidad'];
	$tipo = $_POST['tipo'];
	$asiento = $_POST['asiento'];
	$IDejercicio = $_POST['IDejercicio'];
	$metodo = $_POST['metodo'];
	$subcuenta = $_POST['subcuenta'];
	
	// primer paso
	if($subcuenta == ""){ // si no hay SUBCUENTA o NO es ACUMULATIVA..
		$queryNR = "INSERT INTO rom_cantidades (cantidad, tipo, asiento, cuenta, ejercicio, fechareg)
		VALUES ('$cantidad', '$tipo', '$asiento', '$cuenta', '$IDejercicio', NOW())";
		mysql_query($queryNR) or die(mysql_error());
		$ultimo = mysql_insert_id();
		
	// segundo paso
	} else { // si sí es ACUMULATIVA...
	
		// primero agregamos la cuenta detalle
		$subcantidad = $cantidad;
		$queryNR = "INSERT INTO rom_cantidades (tipo, asiento, cuenta, subcuenta, subcantidad, ejercicio, fechareg)
		VALUES ('$tipo', '$asiento', '$cuenta', '$subcuenta', '$subcantidad', '$IDejercicio', NOW())";
		mysql_query($queryNR) or die(mysql_error());
		$ultimo = mysql_insert_id();
		
	// tercer paso
		// buscar si ya hay un registro con la misma cuenta acumulativa...
		$queryB =	"SELECT IDcantidad, cantidad, tipo FROM rom_cantidades
					WHERE cuenta = '$cuenta'
					AND ejercicio = '$IDejercicio'
					AND asiento = '$asiento'
					AND subcuenta = ''";
		$resultB =	mysql_query($queryB) or die (mysql_error());
		$datoB =	mysql_fetch_assoc($resultB);
		$numDatos =	mysql_num_rows($resultB);
		
		if($numDatos == 0){ // ... si no hay, entonces metemos el primero...
			$queryNC = "INSERT INTO rom_cantidades (cantidad, tipo, asiento, cuenta, ejercicio, acumulable, fechareg)
			VALUES ('$cantidad', '$tipo', '$asiento', '$cuenta', '$IDejercicio', 1, NOW())";
			mysql_query($queryNC) or die(mysql_error());
			
		} else { // si sí hay elementos...
			// sacamos la info del registro
			$oldcantidad = $datoB['cantidad'];
			$oldtipo = $datoB['tipo'];
			$IDcantidad = $datoB['IDcantidad'];
			
				// si la cantidad tiene el mismo tipo
				// entonces que se vaya sumando
				// y el nuevo tipo que sea el tipo anterior
				if($tipo == $oldtipo){
					$newcantidad = $oldcantidad + $subcantidad;
					$nuevoTipo = $oldtipo;
				}
				// si es diferente, entonces...
				else {
					if($oldtipo == "d" && $tipo == "h"){
						$newcantidad = $oldcantidad - $subcantidad;
						if($newcantidad > 0){
							$nuevoTipo = "d";
						} else {
							$nuevoTipo = "h";
							$newcantidad = $newcantidad * -1;
						}
					} elseif($oldtipo == "h" && $tipo == "d"){
						$newcantidad = $oldcantidad - $subcantidad;
						if($newcantidad > 0){
							$nuevoTipo = "h";
						} else {
							$nuevoTipo = "d";
							$newcantidad = $newcantidad * -1;
						}
					}
				} // fin de else
				
			// actualizamos el registro
			$query = "UPDATE rom_cantidades SET cantidad = '$newcantidad', tipo = '$nuevoTipo'
			WHERE IDcantidad = '$IDcantidad'";
			mysql_query($query) or die(mysql_error()); 
		}
	}
		// último paso
		header("Location: diario.php?accion=insertado&ultimo=$ultimo&asiento=$asiento&IDejercicio=$IDejercicio&metodo=$metodo");
		exit;
}

##		fin							##
######################################


###################
## SCRIPT GENERAL ##

// Navegación por planillas
if(isset($_POST['subNav'])){
	$planilla = $_POST['planilla'];
	$IDejercicio = $_POST['IDejercicio'];
	$metodo = $_POST['metodo'];
	$asiento = $_POST['asiento'];
	
	header("Location: $planilla?IDejercicio=$IDejercicio&metodo=$metodo&asiento=$asiento");
	exit;		
}

// comprobar que han iniciado un ejercicio
if(isset($_GET['IDejercicio']) && isset($_GET['metodo']) && isset($_GET['asiento'])){
	$IDejercicio = $_GET['IDejercicio'];
	$metodo = $_GET['metodo'];
	$asiento = $_GET['asiento'];
	if(isset($_GET['ultimo'])){ // si se trata de una nueva entra, es el id del nuevo registro
		$ultimo = $_GET['ultimo'];
	}
} else {
	header("Location: portada.php?ejercicio=false");
	exit;	
}
//fin
###################

// Extraer fecha del asiento
$queryFecha = "SELECT Fasiento, redaccion, status FROM rom_asiento WHERE asientoR = '$asiento' AND Easiento = '$IDejercicio'";
$resultFecha = mysql_query($queryFecha);
$rowFecha = mysql_fetch_assoc($resultFecha);
$fechaAsiento = $rowFecha['Fasiento'];
$statusAsiento = $rowFecha['status']; // status del asiento ** IMPORTANTE **

// pintar navegación de plantillas
$queryP		= "SELECT * FROM rom_planillas WHERE Mplanilla = 4 OR Mplanilla = '$metodo' ORDER BY IDplanilla ASC";
$resultP	= mysql_query($queryP) or die (mysql_error());
$rowP		= mysql_fetch_assoc($resultP);

// pintar lista de registros del asiento
$queryListA = "SELECT IDcantidad, cantidad, tipo, asiento, cuenta, rom_cantidades.subcuenta, subcantidad, Ncuenta
				FROM rom_cantidades
				LEFT OUTER JOIN rom_cuentas ON rom_cantidades.cuenta = rom_cuentas.clave
				WHERE asiento = '$asiento' AND ejercicio = '$IDejercicio'
				ORDER BY fechareg ASC, cuenta ASC";
$resultListA = mysql_query($queryListA) or die (mysql_error());
$rowListA = mysql_fetch_assoc($resultListA);
$totalListA = mysql_num_rows($resultListA);

##############################
## EMPIEZA EL MODELO		##
## AJAX PARA LAS CUENTAS	##
##############################

// PRIMERO pintar los grupos
$queryCuentas = "SELECT * FROM rom_tcuenta
				ORDER BY IDtcuenta ASC";
$resultCuentas = mysql_query($queryCuentas);
$rowCuentas = mysql_fetch_assoc($resultCuentas);
// continua en el JAVASCRIPT de esa misma página
// fin modelo AJAX

######################
## CALCULAR TOTALES ##
######################

// Calcular totales DEBE
$queryTD = "SELECT cantidad FROM rom_cantidades WHERE tipo = 'd' AND asiento = '$asiento' AND ejercicio = '$IDejercicio'";
$resultTD = mysql_query($queryTD);
$rowTD = mysql_fetch_assoc($resultTD);
$totalDebe = 0;

do{
	$totalDebe = $totalDebe+$rowTD['cantidad'];
} while ($rowTD = mysql_fetch_assoc($resultTD));

// Calcular totales DEBE subcuenta
$querySTD = "SELECT subcantidad FROM rom_cantidades WHERE tipo = 'd' AND asiento = '$asiento' AND ejercicio = '$IDejercicio'";
$resultSTD = mysql_query($querySTD);
$rowSTD = mysql_fetch_assoc($resultSTD);
$totalSDebe = 0;

do{
	$totalSDebe = $totalSDebe+$rowSTD['subcantidad'];
} while ($rowSTD = mysql_fetch_assoc($resultSTD));

// Calcular totales HABER
$queryTH = "SELECT cantidad FROM rom_cantidades WHERE tipo = 'h' AND asiento = '$asiento' AND ejercicio = '$IDejercicio'";
$resultTH = mysql_query($queryTH);
$rowTH = mysql_fetch_assoc($resultTH);
$totalHaber = 0;

do{
	$totalHaber = $totalHaber+$rowTH['cantidad'];
} while ($rowTH = mysql_fetch_assoc($resultTH));

// Calcular totales HABER subcuenta
$querySTH = "SELECT subcantidad FROM rom_cantidades WHERE tipo = 'h' AND asiento = '$asiento' AND ejercicio = '$IDejercicio'";
$resultSTH = mysql_query($querySTH);
$rowSTH = mysql_fetch_assoc($resultSTH);
$totalSHaber = 0;

do{
	$totalSHaber = $totalSHaber+$rowSTH['subcantidad'];
} while ($rowSTH = mysql_fetch_assoc($resultSTH));

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sistema Contable Romero</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="libreria/ajaxIni.js"></script>
<script language="javascript" src="libreria/js_principal.js"></script>
<script language="javascript">
function verifAsiento()
{
	var estado
	estado = <?php echo $statusAsiento; ?>;
	if(estado == 1){
	document.getElementById("subNav").value = "Ir";
	document.getElementById("subNav").disabled = false;
	document.getElementById("grupocuenta").disabled = true;
	document.getElementById("submitNcantidad").value = "Este asiento está CERRADO.";
	document.getElementById("submitNcantidad").disabled = true;
        document.getElementById("abrirDiario").innerHTML = "<a href='diario.php?accion=abrirDiario&asiento=<?php echo $asiento; ?>&IDejercicio=<?php echo $IDejercicio; ?>&metodo=<?php echo $metodo; ?>'>¿Deseas modificar el asiento?</a>"
	}
}
function blurCantidad()
{
	document.getElementById("cantidad").focus();
}

function Validador(theForm)
{

  if (theForm.cuenta.value == 0)
  {
    alert("El campo CUENTA está vacío.");
    theForm.cuenta.focus();
    return (false);
  }
    if (theForm.cantidad.value == "")
  {
    alert("El campo CANTIDAD está vacío.");
    theForm.cantidad.focus();
    return (false);
  }
    if (theForm.tipo[0].checked == false && theForm.tipo[1].checked == false)
  {
    alert("El campo TIPO está vacío.");
    theForm.tipo[0].focus();
    return (false);
  }
  if(theForm.subcuenta.value == 'seleccionar'){
      alert("El campo SUBCUENTA está vacío.");
    theForm.subcuenta.focus();
    return (false);
  }
  return (true);
}

function ValidarRed(theForm)
{

  if (theForm.redaccion.value == 0)
  {
    alert("El campo REDACCIÓN está vacío.");
    theForm.redaccion.focus();
    return (false);
  }
  return (true);
}
</script>
<style>
    a {
        color: blue;
        text-decoration: none;
    }
    a:hover {
        text-decoration: underline;
    }
</style>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>
<body onload="verifAsiento();">
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Diario</h1>
    </div>
    <div id="divTitularMenu"><a href="portada.php">Inicio</a> | <a href="libreria/calculadora/calculadora.html" target="_blank" onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | <a href="diarioFeAs.php?IDejercicio=<?php echo $IDejercicio; ?>&metodo=<?php echo $metodo; ?>">Asientos</a> | <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido">
<div class="divNavPlan">
  <form id="navPlan" name="navPlan" method="post" action="<?php $PHP_SELF; ?>">
    Navegación por plantillas:
    <select name="planilla" class="campoSelectPlanilla" id="planilla">
      <option selected="selected">Seleccione una...</option>
      <?php
	  do {
		  echo '<option value="'. $rowP['link'] .'">'. utf8_encode($rowP['Nplanilla']) .'</option>';
	  } while ($rowP = mysql_fetch_assoc($resultP));
	  ?>
    </select>
    <input name="IDejercicio" type="hidden" id="IDejercicio" value="<?php echo $IDejercicio; ?>" />
    <input name="metodo" type="hidden" id="metodo" value="<?php echo $metodo; ?>" />
    <input name="asiento" type="hidden" id="asiento" value="<?php echo $asiento; ?>" />
<input type="submit" name="subNav" id="subNav" value="Es necesario cerrar el asiento para continuar" disabled="disabled" />
  </form>
</div>
<div class="divContCuerpo" id="divSupCuerpo"><strong>Fecha</strong>: <?php arregloFecha($fechaAsiento); ?>. <strong>Asiento</strong>: <?php echo $asiento; ?>. <strong>Método</strong>: <?php pintarMetodo($metodo); ?>. <strong>Status</strong>: <?php
    if($statusAsiento==0) {
		echo "abierto";
		} else {
		echo "cerrado";	
		}
		
		?> 
  <strong>Ejercicio</strong>: <?php pintarNejercicio($IDejercicio); ?><br />
  [ <a href="diarioFeAs.php?IDejercicio=<?php echo $IDejercicio; ?>&metodo=<?php echo $metodo; ?>">Crear un nuevo asiento | ver asientos anteriores</a> ]</div>
<div class="celdaSubtit">Complete el siguiente formulario para alimentar esta plantilla:</div>
<div class="divContCuerpo">
  <div class="divContCuerpoIzq">
  <form id="Ncantidad" name="Ncantidad" method="post" action="diario.php" onsubmit="return Validador(this);">
    <fieldset><legend>Nueva entrada</legend>
      <table border="0" cellpadding="0" cellspacing="3" class="tablaForm">
        <tr>
          <td class="celdaForm"><table border="0" cellspacing="3" cellpadding="0">
            <tr>
              <td class="celdaFormT">Grupo de cuenta:</td>
              <td><select name="grupocuenta" id="grupocuenta" onchange="buscarTipo();">
                <option value="0" selected="selected">Seleccione una ...</option>
                <?php do {
					echo '<option value="'. $rowCuentas['IDtcuenta'] .'">'. $rowCuentas['Ntcuenta'].'</option>';
				} while ($rowCuentas = mysql_fetch_assoc($resultCuentas)); ?>
              </select></td>
            </tr>
            <tr>
              <td class="celdaFormT">Tipo de cuenta:</td>
              <td><select name="tipocuenta" id="tipocuenta" onchange="buscarCuenta();">
              </select></td>
            </tr>
            <tr>
              <td class="celdaFormT">Cuenta:</td>
              <td><select name="cuenta" id="cuenta" onchange="buscarDetalle();">
              </select></td>
            </tr>
            <tr>
              <td class="celdaFormT">Subcuenta:</td>
              <td><select name="subcuenta" id="subcuenta" onchange="blurCantidad();">
              </select></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td class="celdaForm"><table border="0" cellspacing="3" cellpadding="0">
            <tr>
              <td align="left" valign="middle" class="celdaFormT">Cantidad:</td>
              <td align="left" valign="middle">$
                <input name="cantidad" type="text" id="cantidad" size="10" /></td>
              <td align="left" valign="middle" class="celdaFormT">Tipo:</td>
              <td align="left" valign="middle"><input name="tipo" type="radio" id="radio" value="d" />
Debe
  <input type="radio" name="tipo" id="radio2" value="h" />
Haber</td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td align="right" class="celdaForm"><input name="asiento" type="hidden" id="asiento" value="<?php echo $asiento; ?>" />
            <input name="IDejercicio" type="hidden" id="IDejercicio" value="<?php echo $IDejercicio; ?>" />
            <input name="metodo" type="hidden" id="metodo" value="<?php echo $metodo; ?>" />
            <span id="abrirDiario"></span>
            <input type="submit" name="submitNcantidad" id="submitNcantidad" value="Agregar" /></td>
        </tr>
      </table>
      </fieldset>
    </form>
</div>
  <div class="divContCuerpoIzq"><?php
  if($totalListA != 0) {
if($totalDebe == $totalHaber){ ?>
    <form action="libreria/principal.php" method="post" enctype="multipart/form-data" name="cerrarAsiento" id="cerrarAsiento" onsubmit="return ValidarRed(this)">
      <fieldset>
        <legend><?php if($statusAsiento == 0) {
		echo "Cerrar asiento";
		} elseif ($statusAsiento == 1){
		echo "Asiento cerrado";
		}
		?>
		</legend>
        <table border="0" cellpadding="0" cellspacing="3" class="tablaForm">
  <tr>
    <td class="celdaFormT">Redacción del asiento:</td>
  </tr>
  <tr>
    <td class="celdaForm"><textarea name="redaccion" id="redaccion" cols="45" rows="2"><?php
	if($rowFecha['status'] == 1){
    $redaccion = $rowFecha['redaccion'];
	} else {
		$redaccion = "";
	}
	echo $redaccion;
	?></textarea></td>
  </tr>
  <tr>
    <td class="celdaForm"><input name="asiento" type="hidden" id="asiento" value="<?php echo $asiento; ?>" />
      <input name="status" type="hidden" id="status" value="1" />
      <input name="IDejercicio" type="hidden" id="IDejercicio" value="<?php echo $IDejercicio; ?>" />
      <input name="metodo" type="hidden" id="metodo" value="<?php echo $metodo; ?>" />
      <input name="SubmitCeAs" type="hidden" id="SubmitCeAs" value="1" />
      <?php if($statusAsiento == 0) { ?>
      <input name="boton" type="image" id="boton" value="Cerrar Asiento" src="imagenes/botCerrarA.png" /></td>
<?php } ?>
  </tr>
        </table>
      </fieldset>
    </form>
    <?php } } ?>
  </div>
</div>
<?php if(isset($_GET['accion'])){
	if($_GET['accion'] == "insertado"){
	?>
<div class="divContCuerpoInfo"><strong>La entrada ha sido dada de alta favorablemente.</strong></div>
<?php }
if($_GET['accion'] == "eliminado"){
?>
<div class="divContCuerpoInfo"><strong>La entrada ha sido eliminada favorablemente.</strong></div>
<?php } } ?>
<div class="divContCuerpo">
<fieldset>
  <legend>Registros del asiento</legend>
  <table border="0" cellpadding="0" cellspacing="3" class="tablaLista">
      <tr>
        <td colspan="8" align="left" valign="middle" class="celdaListaDatV"><strong>Fecha</strong>:
        <?php arregloFecha($fechaAsiento); ?> | <strong>Ejercicio</strong>:
        <?php pintarNejercicio($IDejercicio); ?></td>
      </tr>
      <tr>
        <td colspan="3" align="center" valign="middle">&nbsp;</td>
        <td colspan="2" align="center" valign="middle" class="celdaListaTit"><strong>Parciales</strong></td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="center" valign="middle">&nbsp;</td>
      </tr>
      <tr>
        <td align="center" valign="middle" class="celdaListaTit"><strong>Asiento</strong></td>
        <td align="center" valign="middle" class="celdaListaTit"><strong>Clave</strong></td>
        <td align="center" valign="middle" class="celdaListaTit"><strong>Cuenta</strong></td>
        <td align="center" valign="top" class="celdaListaDatA"><strong>Debe</strong></td>
        <td align="center" valign="top" class="celdaListaDatV"><strong>Haber</strong></td>
        <td align="center" valign="top" class="celdaListaDatA"><strong>Debe</strong></td>
        <td align="center" valign="top" class="celdaListaDatV"><strong>Haber</strong></td>
        <td align="center" valign="middle" class="celdaListaTit"><strong>Acciones</strong></td>
      </tr>
      <?php if($totalListA == 0 ) { ?>
      <tr>
        <td align="left" valign="top" class="celdaListaDat">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDat">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDat">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDat">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" valign="top" class="celdaListaDat">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDat">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDat">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDat">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" valign="top" class="celdaListaDat">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDat">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDat">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDat">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" valign="top" class="celdaListaDat">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDat">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDat">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDat">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" valign="top" class="celdaListaDat">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDat">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDat">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDat">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" valign="top" class="celdaListaDat">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDat">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDat">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatA">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDatV">&nbsp;</td>
        <td align="left" valign="top" class="celdaListaDat">&nbsp;</td>
      </tr><?php } else {  do { ?>
      <tr>
        <td align="left" valign="top" class="celdaListaDat"><?php echo $rowListA['asiento']; ?></td>
        <td align="left" valign="top" class="celdaListaDat"><?php
        if($rowListA['subcuenta'] != ""){
		echo $rowListA['subcuenta'];
		} else {
		echo $rowListA['cuenta'];
		}
		?></td>
        <td align="left" valign="top" class="celdaListaDat"><?php
        if(isset($_GET['ultimo'])){ // si se ha dado de alta un nuevo registro... entonces resaltarlo
			if($ultimo == $rowListA['IDcantidad']){
				echo "<strong>".utf8_encode($rowListA['Ncuenta'])."</strong>";
			} else {
				echo utf8_encode($rowListA['Ncuenta']);
				}
		} else { // sino... entonces ponerlo normal
			echo utf8_encode($rowListA['Ncuenta']);
		}
		?></td>
        <td align="left" valign="top" class="celdaListaDatA"><?php if($rowListA['tipo'] == "d" && $rowListA['cantidad']== "0.00") { echo "$ ".number_format($rowListA['subcantidad'],2); } else { echo ""; }?></td>
        <td align="left" valign="top" class="celdaListaDatV"><?php if($rowListA['tipo'] == "h" && $rowListA['cantidad']== "0.00") { echo "$ ".number_format($rowListA['subcantidad'],2); } else { echo ""; }?></td>
        <td align="left" valign="top" class="celdaListaDatA"><?php if($rowListA['tipo'] == "d" && $rowListA['subcantidad']== "0.00") { echo "$ ".number_format($rowListA['cantidad'],2); } else { echo ""; }?></td>
        <td align="left" valign="top" class="celdaListaDatV"><?php if($rowListA['tipo'] == "h" && $rowListA['subcantidad']== "0.00") { echo "$ ".number_format($rowListA['cantidad'],2); } else { echo ""; }?></td>
        <td align="center" valign="middle" class="celdaListaDat"><?php
		if($statusAsiento == 0) {
        ?>
		<a href="libreria/principal.php?IDejercicio=<?php echo $IDejercicio; ?>&asiento=<?php echo $asiento; ?>&metodo=<?php echo $metodo; ?>&IDcantidad=<?php echo $rowListA['IDcantidad']; ?>&accion=eliminarEntrada" onclick="return confirm('¿Está seguro de eliminar esta entrada?');"><img src="imagenes/action_delete.gif" alt="borrar" width="16" height="16" border="0" title="borrar" /></a>
		<?php } ?>		</td>
      </tr>
      <?php } while($rowListA = mysql_fetch_assoc($resultListA)); ?>
      <tr>
        <td colspan="3" align="right" valign="top" class="celdaListaDat"><strong>Totales:</strong></td>
        <td align="left" valign="top" class="celdaListaDatA"><strong>$ <?php echo number_format($totalSDebe,2); ?></strong></td>
        <td align="left" valign="top" class="celdaListaDatV"><strong>$ <?php echo number_format($totalSHaber,2); ?></strong></td>
        <td align="left" valign="top" class="celdaListaDatA"><strong>$ <?php echo number_format($totalDebe,2); ?></strong></td>
        <td align="left" valign="top" class="celdaListaDatV"><strong>$ <?php echo number_format($totalHaber,2); ?></strong></td>
        <td align="center" valign="middle">&nbsp;</td>
      </tr>
      <?php }
	  	if($totalListA != 0) {		  
		  if($totalDebe != $totalHaber) {
		  ?>          
      <tr><td colspan="3" align="right" valign="top">&nbsp;</td>
        <td colspan="4" align="center" valign="middle" class="celdaListaDatW"><strong>Las sumas no son iguales. (Dif: $ <?php diferencia($totalDebe, $totalHaber); ?>)</strong></td>
        <td align="center" valign="middle">&nbsp;</td>
        </tr>
      <?php } else {
	  if($statusAsiento == 0){
	  ?>
        <tr>
        <td colspan="3" align="right" valign="top">&nbsp;</td>
        <td colspan="4" align="center" valign="middle" class="celdaListaDatG"><strong>Ahora se puede cerrar el asiento.</strong></td>
        <td align="center" valign="middle">&nbsp;</td>
        </tr>
        <?php } else { ?>
		<tr>
        <td colspan="3" align="right" valign="top">&nbsp;</td>
        <td colspan="4" align="center" valign="middle" class="celdaListaDatG"><strong>El asiento está CERRADO.</strong></td>
        <td align="center" valign="middle">&nbsp;</td>
        </tr>
		<?php } } } ?>
    </table>
  </fieldset></div>
  <div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
</body>
</html>