<?php
include 'libreria/principal.php';
include 'libreria/evaluaciones.php';

esUsuario(); // será usuario registrado?
$IDusuario = $_SESSION['userID'];
$capitulo = 10;
$registro = verificarCuestionario($capitulo, $IDusuario,  utf8_decode($Fecha));


?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> Principios de Contabilidad. Cuestionario de evaluación. Capítulo 10</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="libreria/js_principal.js"></script>
<script src="libreria/vendor/jquery/jquery-1.11.0.min.js"></script>
<script src="libreria/calCap.js"></script>
<script src="libreria/rc10.js"></script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Cuestionario de autoevaluación</h1>
    </div>
    <div id="divTitularMenu">
        <a href="portada.php">Inicio</a> | 
        <a href="libreria/calculadora/calculadora.html" 
           target="_blank" 
           onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | 
        <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido" style="width: 900px">
  <div class="divContCuerpo">
      
      <h3 class="titulo-principal">Capítulo 10</h3>
      <div class="migas">
          <ul>
              <li><a href="portada.php">Portada</a></li>
              <li><a href="javascript:history.back();">Página anterior</a></li>
              <li><img src="imagenes/print.png" 
                       alt="" 
                       height="25" 
                       style="float: left; margin-right: 5px;" />
                  <a href="javascript:print();">Imprimir página</a></li>
          </ul>
      </div>
      
      <table class="mis-datos">
          <tr>
              <td>Universidad o Institución:</td>
              <td>
                  <input oninput="cCamCues('unidad',this.value,<?php echo $registro ?>);" 
                         name="unidad" 
                         id='unidad' 
                         required 
                         value="<?php echo xCamCues('unidad', $registro) ?>" />
              </td>
              <td>Escuela o facultad:</td>
              <td>
                  <input oninput="cCamCues('facultad',this.value,<?php echo $registro ?>);" 
                         name="facultad" 
                         id='facultad' 
                         required 
                         value="<?php echo xCamCues('facultad', $registro) ?>" />
              </td>
          </tr>
          <tr>
              <td>Asignatura:</td>
              <td>
                  <input oninput="cCamCues('asignatura',this.value,<?php echo $registro ?>);" 
                         name="asignatura" 
                         id='asignatura' 
                         required 
                         value="<?php echo xCamCues('asignatura', $registro) ?>" />
              </td>
              <td>Alumno:</td>
              <td>
                  <?php echo nombreUsuario($IDusuario) ?>
              </td>
          </tr>
          <tr>
              <td>Maestro:</td>
              <td>
                  <input oninput="cCamCues('maestro',this.value,<?php echo $registro ?>);" 
                         name="maestro" 
                         id='maestro' 
                         required 
                         value="<?php echo xCamCues('maestro', $registro) ?>" />
              </td>
              <td>Fecha:</td>
              <td>
                  <input oninput="cCamCues('fecha',this.value,<?php echo $registro ?>);" 
                         name="fecha" 
                         id='fecha' 
                         required 
                         value="<?php echo xCamCues('fecha', $registro) ?>" />
              </td>
          </tr>
      </table>
      
      <h4 class="subtit-4">OBJETIVOS DE APRENDIZAJE</h4>
      
      <div class="objetivos">
          <p>Al finalizar este capítulo, el alumno será capaz de:</p>
          <ol>
              <li>Analizar su concepto y objetivos.</li>
              <li>Distinguir los elementos que lo integran.</li>
              <li>Elaborar el estado de cambios en el capital contable.</li>
          </ol>
      </div>
      
      <h4 class="subtit-4">EJERCICIOS</h4>
      
      <h5>I. RESUELVA LO SIGUIENTE</h5>
      
      <div class="pregunta-libre">
          <p><span>1</span> ¿De qué le sirve al propietario el estado de 
              cambios en el capital contable?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib01" 
                    id="prelib01"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib01'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>2</span> ¿Qué elementos básicos debe incluir el estado de 
              cambios en el capital contable?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib02" 
                    id="prelib02"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib02'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>3</span> ¿En qué fecha entró en vigor la NIF B-4?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib03" 
                    id="prelib03"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib03'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>4</span> ¿Qué rubros conforman el capital contribuido?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib04" 
                    id="prelib04"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib04'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>5</span> ¿Qué rubros conforman el capital ganado?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib05" 
                    id="prelib05"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib05'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>6</span> Señale qué importes deben presentarse por separado, 
              por cada periodo por los que se presente el estado de cambios en 
              el capital contable.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib06" 
                    id="prelib06"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib06'); ?></textarea>
      </div>
      
      <h5>II. RELACIONE LAS COLUMNAS 1 Y 2</h5>
      
      <table class='correlaciones'>
          <tr>
              <th>Columna 1</th>
              <th>Columna 2</th>
          </tr>
          <tr>
              <td>
                  <span class="num">1</span>
                  <p>Aportación de capital</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_1' id='corr1_1'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>8</option>
                  </select>
                  <span id='corr1_1-parcial'></span>
              </td>
              <td rowspan="8">
                  <ol>
                      <li>Distribuciones de las utilidades netas a los 
                          propietarios de la entidad; estos disminuyen el 
                          capital contable.</li>
                      <li>En estos renglones se incluyen los movimientos 
                          efectuados por los propietarios, en relación con su 
                          inversión en dicha entidad. </li>
                      <li>Se incluyen en este renglón los importes que 
                          representan aumentos o disminuciones de las reservas 
                          de capital. Las reservas de capital son importes de 
                          resultados acumulados segregados por disposiciones 
                          legales o por los propietarios para cumplir con fines 
                          específicos.</li>
                      <li>Recursos entregados por la entidad a los propietarios, 
                          producto de la devolución de sus aportaciones, por lo 
                          que representan disminuciones de su inversión y del 
                          capital contable.</li>
                      <li>a) utilidad o pérdida neta; b) otros resultados 
                          integrales (ORI); y c) la participación en los ORI 
                          de otras entidades (tales como asociadas o inversiones 
                          conjuntas).</li>
                      <li>Son modificaciones en la participación de las 
                          subsidiarias consolidadas.</li>
                      <li>Recursos entregados a una entidad por sus propietarios, 
                          que representan aumentos de su inversión y, 
                          consecuentemente, del capital contable.</li>
                      <li>Son asignaciones al capital social provenientes de 
                          otros conceptos de capital contable, como la prima 
                          pagada en colocación de acciones. Estos movimientos 
                          son traspasos entre rubros del capital contable, 
                          por lo que no modifican su valor total.</li>
                  </ol>
              </td>
          </tr>
          <tr>
              <td>
                  <span class="num">2</span>
                  <p>Capitalizaciones</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_2' id='corr1_2'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>6</option>
                  <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>8</option>
                  </select>
                  <span id='corr1_2-parcial'></span>
              </td>
          </tr>
          
          <tr>
              <td>
                  <span class="num">3</span>
                  <p>Resultado integral. Este renglón se desglosa en los 
                      siguientes componentes:</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_3' id='corr1_3'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>6</option>
                  <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>8</option>
                  </select>
                  <span id='corr1_3-parcial'></span>
              </td>
          </tr>
          
          <tr>
              <td>
                  <span class="num">4</span>
                  <p>Cambios en la participación controladora que no implican pérdida de control.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_4' id='corr1_4'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>8</option>
                  </select>
                  <span id='corr1_4-parcial'></span>
              </td>
          </tr>
          
          <tr>
              <td>
                  <span class="num">5</span>
                  <p>Reembolsos de capital:</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_5' id='corr1_5'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>8</option>
                  </select>
                  <span id='corr1_5-parcial'></span>
              </td>
          </tr>
          
          <tr>
              <td>
                  <span class="num">6</span>
                  <p>Movimientos de reservas</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_6' id='corr1_6'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>8</option>
                  </select>
                  <span id='corr1_6-parcial'></span>
              </td>
          </tr>
          
          <tr>
              <td>
                  <span class="num">7</span>
                  <p>Decretos de dividendos:</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_7' id='corr1_7'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>8</option>
                  </select>
                  <span id='corr1_7-parcial'></span>
              </td>
          </tr>
          
          <tr>
              <td>
                  <span class="num">8</span>
                  <p>Movimientos de propietarios.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_8' id='corr1_8'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>8</option>
                  </select>
                  <span id='corr1_8-parcial'></span>
              </td>
          </tr>
          
          <tr>
              <td id='corr1_cal'></td>
              <td><button onclick="calificarCorrelacion('corr1',8,corr10);" 
                          name='cal_corr1'>CALIFICAR</button></td>
          </tr>
      </table>
      
  </div>
  <!-- -->
  <div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
<script>
    var tc;
    var cCamCues = function(campo,valor,registro)
    {
      clearTimeout(tc);
      tc = window.setTimeout(function(){
          $.post('libreria/cCamCues.php',{
              modDatos: true,
              elcampo: campo,
              elvalor: valor,
              elregistro: registro
          },function(respuesta){
              if(respuesta === 'modificado')
              {
                  // Modificado
              } else {
                  // Error
                  console.log('-> '+respuesta);
              }
          });
      },1000);
    };
    var cResCues = function(cues,usu,cap,preg,resp)
    {
      clearTimeout(tc);
      tc = window.setTimeout(function(){
          $.post('libreria/cCamCues.php',{
              modRespuestas: true,
              cuestionario: cues,
              usuario: usu,
              capitulo: cap,
              pregunta: preg,
              respuesta:resp
          },function(respuesta){
              if(respuesta === 'modificado')
              {
                  // Modificado
              } else {
                  // Error
                  console.log('-> '+respuesta);
              }
          });
      },500);
    };
    
    window.onload = function(){
        protegerPLibres();
        protegerCorrelaciones();
    };
</script>
</body>
</html>