<?php
include("libreria/principal.php");

esUsuario(); // será usuario registrado?
$msgError = FALSE;

###################
## SCRIPT GENERAL ##

// Navegación por planillas
if(isset($_POST['subNav'])){
	$planilla = $_POST['planilla'];
	$IDejercicio = $_POST['IDejercicio'];
	$metodo = $_POST['metodo'];
	$asiento = $_POST['asiento'];
	
	header("Location: $planilla?IDejercicio=$IDejercicio&metodo=$metodo&asiento=$asiento");
	exit;		
}

// comprobar que han iniciado un ejercicio
if(isset($_GET['IDejercicio']) && isset($_GET['metodo']) && isset($_GET['asiento'])){
	$IDejercicio = $_GET['IDejercicio'];
	$metodo = $_GET['metodo'];
	$asiento = $_GET['asiento'];
} else {
	header("Location: portada.php?ejercicio=false");
	exit;	
}
//fin
###################

// Extraer fecha del asiento
$queryFecha = "SELECT Fasiento, redaccion, status FROM rom_asiento WHERE asientoR = '$asiento' AND Easiento = '$IDejercicio'";
$resultFecha = mysql_query($queryFecha);
$rowFecha = mysql_fetch_assoc($resultFecha);
$fechaAsiento = $rowFecha['Fasiento'];
$statusAsiento = $rowFecha['status']; // status del asiento ** IMPORTANTE **

// pintar navegación de plantillas
$queryP		= "SELECT * FROM rom_planillas WHERE Mplanilla = 4 OR Mplanilla = '$metodo' ORDER BY IDplanilla ASC";
$resultP	= mysql_query($queryP) or die (mysql_error());
$rowP		= mysql_fetch_assoc($resultP);

// verificar si hay una balanza_da y balanza_aa
$queryBaa = "SELECT COUNT(IDbalanza_aa) as baa FROM rom_balanza_aa WHERE ejercicio = '$IDejercicio'";
$resulBaa = mysql_query($queryBaa);
$datoBaa = mysql_fetch_assoc($resulBaa);
$baa = $datoBaa['baa'];

if($baa == 0) { // no hay balanza_aa
	$msgError = "<strong>Advertencia:</strong> No hay BALANZA ANTES DE AJUSTES, ni ASIENTOS DE AJUSTE. Cree una y vuelva a intentar.";
}


// variables
$C8 = (pintarCantidadDa("Mdebe", "4107", $IDejercicio))*-1;
$C9 = (pintarCantidadDa("Mdebe", "4108", $IDejercicio))*-1;
$C12 = pintarCantidad("Shaber", "4206", $IDejercicio);
$C13 = (pintarCantidadDa("Mdebe", "4109", $IDejercicio))*-1;
$C16 = pintarCantidadDa("Mhaber", "4207", $IDejercicio);
$C17 = (pintarCantidadDa("Mdebe", "4110", $IDejercicio))*-1;
$newISR = (pintarCantidadDa("Shaber", "2111", $IDejercicio))*-1;

$D4 = pintarCantidad("Shaber", "P4202", $IDejercicio);
$D5 = pintarCantidad("Sdebe", "P4101", $IDejercicio);
$D6 = $D4-$D5;
$D7 = $C8+$C9;
$D10 = $D6+$D7;
$D11 = $C12+$C13;
$D14 = $D10+$D11;
$D15 = $C16+$C17;
$D18 = $D14+$D15;
// $D19 = ($D18>0)?$D18*0.3:0;
$D19 = ($newISR<0)?$newISR*-1:$newISR;
$D20 = $D18-$D19;

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sistema Contable Romero</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="libreria/js_principal.js"></script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Estado de resultados</h1>
    </div>
    <div id="divTitularMenu"><a href="portada.php">Inicio</a> | <a href="libreria/calculadora/calculadora.html" target="_blank" onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | <a href="diarioFeAs.php?IDejercicio=<?php echo $IDejercicio; ?>&metodo=<?php echo $metodo; ?>">Asientos</a> | <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido">
<div class="divNavPlan">
  <form id="navPlan" name="navPlan" method="post" action="<?php $PHP_SELF; ?>">
    Navegación por plantillas:
    <select name="planilla" class="campoSelectPlanilla" id="planilla">
      <option selected="selected">Seleccione una...</option>
      <?php
	  do {
		  echo '<option value="'. $rowP['link'] .'">'. utf8_encode($rowP['Nplanilla']) .'</option>';
	  } while ($rowP = mysql_fetch_assoc($resultP));
	  ?>
    </select>
    <input name="IDejercicio" type="hidden" id="IDejercicio" value="<?php echo $IDejercicio; ?>" />
    <input name="metodo" type="hidden" id="metodo" value="<?php echo $metodo; ?>" />
    <input name="asiento" type="hidden" id="asiento" value="<?php echo $asiento; ?>" />
<input type="submit" name="subNav" id="subNav" value="ir" />
  </form>
</div>
<div id="divSupCuerpo"><strong>Ejercicio</strong>: 
  <?php pintarNejercicio($IDejercicio); ?>
<br />
<strong>ESTADO DE RESULTADOS</strong> del <?php arregloFecha(fechasPU("ASC", $IDejercicio)); ?>
 al 
 <?php arregloFechaAs(fechasAs($IDejercicio)); ?> 
 .</div>
<div class="divContCuerpo"><a href="verImp_eduResP.php?IDejercicio=<?php echo $IDejercicio; ?>&amp;metodo=<?php echo $metodo; ?>&amp;asiento=<?php echo $asiento; ?>" target="_blank"><img src="imagenes/verImp.gif" alt="Versión imprimible" width="150" height="25" border="0" title="Versión imprimible" /></a></div>
<?php if($msgError) { ?>
<div class="divContCuerpoInfo"><?php echo $msgError; ?></div>
<?php } ?>
<?php if(!$msgError) { ?>
<div class="divContCuerpo">
  <table align="center" cellpadding="0" cellspacing="1">
    <tr height="20">
      <td height="20" class="celdaEduRes">Ventas    netas</td>
      <td rowspan="5" valign="bottom" class="celdaEduRes">$ <?php echo number_format($C8,2); ?></td>
      <td class="celdaEduRes">$ <?php echo number_format($D4,2); ?></td>
    </tr>
    <tr height="21">
      <td height="21" class="celdaEduRes">Costo de ventas</td>
      <td class="celdaEduRes">$ <?php echo number_format($D5,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Utilidad bruta</td>
      <td class="celdaEduRes">$ <?php echo number_format($D6,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Gastos de operación</td>
      <td class="celdaEduRes">$ <?php echo number_format($D7,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Gastos de venta</td>
      <td rowspan="3" valign="bottom" class="celdaEduRes">$ <?php echo number_format($D10,2); ?></td>
    </tr>
    <tr height="21">
      <td height="21" class="celdaEduRes">Gastos de administración</td>
      <td class="celdaEduRes">$ <?php echo number_format($C9,2); ?></td>
      </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Utilidad (pérdida) de    operación</td>
      <td rowspan="3" valign="bottom" class="celdaEduRes">$ <?php echo number_format($C12,2); ?></td>
      </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Otros ingresos y gastos</td>
      <td class="celdaEduRes">$ <?php echo number_format($D11,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Ingresos</td>
      <td rowspan="3" valign="bottom" class="celdaEduRes">$ <?php echo number_format($D14,2); ?></td>
    </tr>
    <tr height="21">
      <td height="21" class="celdaEduRes">Gastos</td>
      <td class="celdaEduRes">$ <?php echo number_format($C13,2); ?></td>
      </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">&nbsp;</td>
      <td rowspan="3" valign="bottom" class="celdaEduRes">$ <?php echo number_format($C16,2); ?></td>
      </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Resultado integral de    financiamiento</td>
      <td class="celdaEduRes">$ <?php echo number_format($D15,2); ?></td>
    </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">A favor</td>
      <td rowspan="3" valign="bottom" class="celdaEduRes">$ <?php echo number_format($D18,2); ?></td>
    </tr>
    <tr height="21">
      <td height="21" class="celdaEduRes">A cargo</td>
      <td class="celdaEduRes">$ <?php echo number_format($C17,2); ?></td>
      </tr>
    <tr height="20">
      <td height="20" class="celdaEduRes">Utilidad antes de impuestos</td>
      <td rowspan="3" class="celdaEduRes">&nbsp;</td>
      </tr>
    <tr height="21">
      <td height="21" class="celdaEduRes">ISR</td>
      <td class="celdaEduRes">$ <?php echo number_format($D19,2); ?></td>
    </tr>
    <tr height="21">
      <td height="21" class="celdaEduRes">Utilidad neta del ejercicio</td>
      <td class="celdaEduRes">$ <?php echo number_format($D20,2); ?></td>
    </tr>
  </table>
</div>
<?php } ?>
<div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
</body>
</html>