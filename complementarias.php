<?php
include("libreria/principal.php");

esUsuario(); // será usuario registrado?

###################
## SCRIPT GENERAL ##

// Navegación por planillas
if(isset($_POST['subNav'])){
	$planilla = $_POST['planilla'];
	$IDejercicio = $_POST['IDejercicio'];
	$metodo = $_POST['metodo'];
	$asiento = $_POST['asiento'];
	
	header("Location: $planilla?IDejercicio=$IDejercicio&metodo=$metodo&asiento=$asiento");
	exit;		
}


// comprobar que han iniciado un ejercicio
if(isset($_GET['IDejercicio']) && isset($_GET['metodo']) && isset($_GET['asiento'])){
	$IDejercicio = $_GET['IDejercicio'];
	$metodo = $_GET['metodo'];
	$asiento = $_GET['asiento'];
} else {
	header("Location: portada.php?ejercicio=false");
	exit;	
}
//fin
###################

// Extraer fecha del asiento
$queryFecha = "SELECT Fasiento, redaccion, status FROM rom_asiento WHERE asientoR = '$asiento' AND Easiento = '$IDejercicio'";
$resultFecha = mysql_query($queryFecha);
$rowFecha = mysql_fetch_assoc($resultFecha);
$fechaAsiento = $rowFecha['Fasiento'];
$statusAsiento = $rowFecha['status']; // status del asiento ** IMPORTANTE **

// pintar lista de registros por ejercicio

if(isset($_GET['cuenta'])){
	$cuenta = $_GET['cuenta'];
	$queryLista = "SELECT cantidad, tipo, asiento, cuenta,
					rom_cantidades.subcuenta, subcantidad, Ncuenta
					FROM rom_cantidades
					LEFT OUTER JOIN rom_cuentas ON rom_cantidades.cuenta = rom_cuentas.clave
					INNER JOIN rom_asiento ON
					(rom_cantidades.ejercicio = rom_asiento.Easiento)
					AND
					(rom_cantidades.asiento = rom_asiento.asientoR)
					WHERE (Easiento = '$IDejercicio' AND status = 1)
					AND cuenta = '$cuenta' ORDER BY asiento ASC";
	$resultLista = mysql_query($queryLista) or die (mysql_error());
	$rowLista = mysql_fetch_assoc($resultLista);
	
######################
## CALCULAR TOTALES ##
######################

// Calcular totales DEBE

$queryTD = "SELECT cantidad FROM rom_cantidades
LEFT OUTER JOIN rom_asiento ON rom_cantidades.asiento = rom_asiento.IDasiento
WHERE tipo = 'd' AND cuenta = '$cuenta' AND ejercicio = '$IDejercicio' AND status = 1";
$resultTD = mysql_query($queryTD);
$rowTD = mysql_fetch_assoc($resultTD);
$totalDebe = 0;

do{
	$totalDebe = $totalDebe+$rowTD['cantidad'];
} while ($rowTD = mysql_fetch_assoc($resultTD));

$queryTH = "SELECT cantidad FROM rom_cantidades
LEFT OUTER JOIN rom_asiento ON rom_cantidades.asiento = rom_asiento.IDasiento
WHERE tipo = 'h' AND cuenta = '$cuenta' AND ejercicio = '$IDejercicio' AND status = 1";
$resultTH = mysql_query($queryTH);
$rowTH = mysql_fetch_assoc($resultTH);
$totalHaber = 0;

do{
	$totalHaber = $totalHaber+$rowTH['cantidad'];
} while ($rowTH = mysql_fetch_assoc($resultTH));
}

// pintar navegación de plantillas
$queryP		= "SELECT * FROM rom_planillas WHERE Mplanilla = 4 OR Mplanilla = '$metodo' ORDER BY IDplanilla ASC";
$resultP	= mysql_query($queryP) or die (mysql_error());
$rowP		= mysql_fetch_assoc($resultP);

// pintar cuentas - chan chan chan chan!!!!!
$tipoCuenta = 5;
$queryCuentas = "SELECT cuenta, Ncuenta FROM rom_cantidades
				LEFT OUTER JOIN rom_cuentas ON rom_cantidades.cuenta = rom_cuentas.clave
				INNER JOIN rom_asiento ON
					(rom_cantidades.ejercicio =  rom_asiento.Easiento)
					AND
					(rom_cantidades.asiento = rom_asiento.asientoR)
				WHERE (Easiento = '$IDejercicio' AND status = 1)
				AND Tcuenta = '$tipoCuenta'
				AND (Mcuenta = 4 OR Mcuenta = '$metodo')
				GROUP BY cuenta
				ORDER BY clave ASC";
$resultCuentas = mysql_query($queryCuentas);
$rowCuentas = mysql_fetch_assoc($resultCuentas);
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sistema Contable Romero</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="libreria/js_principal.js"></script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Complementarias</h1>
    </div>
    <div id="divTitularMenu"><a href="portada.php">Inicio</a> | <a href="libreria/calculadora/calculadora.html" target="_blank" onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | <a href="diarioFeAs.php?IDejercicio=<?php echo $IDejercicio; ?>&metodo=<?php echo $metodo; ?>">Asientos</a> | <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido">
<div class="divNavPlan">
  <form id="navPlan" name="navPlan" method="post" action="<?php $PHP_SELF; ?>">
    Navegación por plantillas:
    <select name="planilla" class="campoSelectPlanilla" id="planilla">
      <option selected="selected">Seleccione una...</option>
      <?php
	  do {
		  echo '<option value="'. $rowP['link'] .'">'. utf8_encode($rowP['Nplanilla']) .'</option>';
	  } while ($rowP = mysql_fetch_assoc($resultP));
	  ?>
    </select>
    <input name="IDejercicio" type="hidden" id="IDejercicio" value="<?php echo $IDejercicio; ?>" />
    <input name="metodo" type="hidden" id="metodo" value="<?php echo $metodo; ?>" />
    <input name="asiento" type="hidden" id="asiento" value="<?php echo $asiento; ?>" />
<input type="submit" name="subNav" id="subNav" value="ir" />
  </form>
</div>
<div class="divContCuerpo" id="divSupCuerpo"><strong>Fecha</strong>: <?php arregloFecha($fechaAsiento); ?>. <strong>Asiento</strong>: <?php echo $asiento; ?>. <strong>Método</strong>: <?php pintarMetodo($metodo); ?>. <strong>Status</strong>: <?php
    if($statusAsiento==0) {
		echo "abierto";
		} else {
		echo "cerrado";	
		}
		
		?> 
  <strong>Ejercicio</strong>: <?php pintarNejercicio($IDejercicio); ?><br />
  [ <a href="diarioFeAs.php?IDejercicio=<?php echo $IDejercicio; ?>&metodo=<?php echo $metodo; ?>">Crear un nuevo asiento | ver asientos anteriores</a> ]</div>
<div class="celdaSubtit">Seleccione las cuentas a mostrar:</div>
<div class="divContCuerpo">
  <div class="divContCuerpoIzq">
    <form id="listaCuentas" name="listaCuentas" method="get" action="complementarias.php">
      <fieldset>
        <legend>Selección de cuentas </legend>
        <table border="0" cellpadding="0" cellspacing="3" class="tablaForm">
          <tr>
            <td class="celdaForm"><table border="0" cellspacing="3" cellpadding="0">
              <tr>
                <td class="celdaFormT">Cuenta:</td>
                <td><select name="cuenta" id="cuenta">
                  <option>Seleccione una...</option>
                  <?php do {
					echo '<option value="'. $rowCuentas['cuenta'] .'">'. $rowCuentas['cuenta']. ' - '.utf8_encode($rowCuentas['Ncuenta']) .'</option>';
				} while ($rowCuentas = mysql_fetch_assoc($resultCuentas)); ?>
                </select>
                  <input name="IDejercicio" type="hidden" id="IDejercicio" value="<?php echo $IDejercicio; ?>" />
                  <input name="metodo" type="hidden" id="metodo" value="<?php echo $metodo; ?>" />
                  <input name="asiento" type="hidden" id="asiento" value="<?php echo $asiento; ?>" /></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td align="right" class="celdaForm"><input type="submit" name="submit" id="submit" value="Desplegar" /></td>
          </tr>
        </table>
      </fieldset>
    </form>
  </div>
</div>
<div class="divContCuerpo"><fieldset>
  <legend>Registros del asiento</legend>
  <table border="0" cellpadding="0" cellspacing="3" class="tablaLista">
    <tr>
      <td align="center" valign="middle" class="celdaListaTit"><strong>Asiento</strong></td>
      <td align="center" valign="middle" class="celdaListaTit"><strong>Fecha</strong></td>
      <td align="center" valign="middle" class="celdaListaTit"><strong>Clave</strong></td>
      <td align="center" valign="middle" class="celdaListaTit"><strong>Cuenta</strong></td>
      <td align="center" valign="middle" class="celdaListaDatA"><strong>Debe</strong></td>
      <td align="center" valign="middle" class="celdaListaDatV"><strong>Haber</strong></td>
    </tr>
    <?php if(!isset($_GET['cuenta'])) { ?>
    <tr>
      <td colspan="6" align="left" valign="top" class="celdaListaDat">Seleccione una cuenta.</td>
    </tr>
    <?php } else { do { ?>
    <tr>
      <td align="left" valign="top" class="celdaListaDat"><?php echo $rowLista['asiento']; ?></td>
      <td align="left" valign="top" class="celdaListaDat"><?php mostrarFechaAsiento($rowLista['asiento'],$IDejercicio); ?></td>
      <td align="left" valign="top" class="celdaListaDat"><?php echo $rowLista['cuenta']; ?></td>
      <td align="left" valign="top" class="celdaListaDat"><strong><?php echo utf8_encode($rowLista['Ncuenta']); ?></strong></td>
      <td align="left" valign="top" class="celdaListaDatA">$
        <?php if($rowLista['tipo'] == "d") { echo number_format($rowLista['cantidad'],2); } else { echo 0; }?></td>
      <td align="left" valign="top" class="celdaListaDatV">$
        <?php if($rowLista['tipo'] == "h") { echo number_format($rowLista['cantidad'],2); } else { echo 0; }?></td>
    </tr>
    <?php } while ($rowLista = mysql_fetch_assoc($resultLista)); ?>
    <tr>
      <td colspan="4" align="right" valign="top" class="celdaListaDat"><strong>Totales:</strong></td>
      <td align="left" valign="top" class="celdaListaDatA"><strong>$ <?php echo number_format($totalDebe,2); ?></strong></td>
      <td align="left" valign="top" class="celdaListaDatV"><strong>$ <?php echo number_format($totalHaber,2); ?></strong></td>
    </tr>
    <?php } ?>
  </table>
</fieldset></div>
  <div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
</body>
</html>