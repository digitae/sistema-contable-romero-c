<?php
include("libreria/principal.php");

esUsuario(); // será usuario registrado?

###################
## SCRIPT GENERAL ##

// Navegación por planillas
if(isset($_POST['subNav'])){
	$planilla = $_POST['planilla'];
	$IDejercicio = $_POST['IDejercicio'];
	$metodo = $_POST['metodo'];
	$asiento = $_POST['asiento'];
	
	header("Location: $planilla?IDejercicio=$IDejercicio&metodo=$metodo&asiento=$asiento");
	exit;		
}


// comprobar que han iniciado un ejercicio
if(isset($_GET['IDejercicio']) && isset($_GET['metodo']) && isset($_GET['asiento'])){
	$IDejercicio = $_GET['IDejercicio'];
	$metodo = $_GET['metodo'];
	$asiento = $_GET['asiento'];
} else {
	header("Location: portada.php?ejercicio=false");
	exit;	
}
//fin
###################

// Extraer fecha del asiento
$queryFecha = "SELECT Fasiento, redaccion, status FROM rom_asiento WHERE asientoR = '$asiento' AND Easiento = '$IDejercicio'";
$resultFecha = mysql_query($queryFecha);
$rowFecha = mysql_fetch_assoc($resultFecha);
$fechaAsiento = $rowFecha['Fasiento'];
$statusAsiento = $rowFecha['status']; // status del asiento ** IMPORTANTE **

// pintar lista de registros por ejercicio

if(isset($_GET['submit'])){
	$detalle = $_GET['detalle'];
	$queryLista = "SELECT tipo, asiento, rom_cantidades.subcuenta, subcantidad, ejercicio, Ndcuenta
					FROM rom_cantidades
					INNER JOIN rom_dcuenta ON
					(rom_cantidades.subcuenta = rom_dcuenta.claveD)
					AND
					(rom_cantidades.cuenta = rom_dcuenta.claveA)
					INNER JOIN rom_asiento ON
					(rom_cantidades.ejercicio =  rom_asiento.Easiento)
					AND
					(rom_cantidades.asiento = rom_asiento.asientoR)
					WHERE (Easiento = '$IDejercicio' AND status = 1)
					AND rom_cantidades.subcuenta = '$detalle'
					ORDER BY asiento ASC, Ndcuenta ASC";
	$resultLista = mysql_query($queryLista) or die (mysql_error());
	$rowLista = mysql_fetch_assoc($resultLista);
	
######################
## CALCULAR TOTALES ##
######################

// Calcular totales DEBE
$queryTD = "SELECT subcantidad
FROM rom_cantidades
INNER JOIN rom_asiento ON
(rom_cantidades.ejercicio =  rom_asiento.Easiento)
AND
(rom_cantidades.asiento = rom_asiento.asientoR)
WHERE (Easiento = '$IDejercicio' AND status = 1)
AND tipo = 'd' AND subcuenta = '$detalle'";
$resultTD = mysql_query($queryTD);
$rowTD = mysql_fetch_assoc($resultTD);
$totalDebe = 0;

do{
	$totalDebe = $totalDebe+$rowTD['subcantidad'];
} while ($rowTD = mysql_fetch_assoc($resultTD));

// Calcular totales HABER
$queryTH = "SELECT subcantidad
FROM rom_cantidades
INNER JOIN rom_asiento ON
(rom_cantidades.ejercicio =  rom_asiento.Easiento)
AND
(rom_cantidades.asiento = rom_asiento.asientoR)
WHERE (Easiento = '$IDejercicio' AND status = 1)
AND tipo = 'h' AND subcuenta = '$detalle'";
$resultTH = mysql_query($queryTH);
$rowTH = mysql_fetch_assoc($resultTH);
$totalHaber = 0;

do{
	$totalHaber = $totalHaber+$rowTH['subcantidad'];
} while ($rowTH = mysql_fetch_assoc($resultTH));
}

// pintar navegación de plantillas
$queryP		= "SELECT * FROM rom_planillas WHERE Mplanilla = 4 OR Mplanilla = '$metodo' ORDER BY IDplanilla ASC";
$resultP	= mysql_query($queryP) or die (mysql_error());
$rowP		= mysql_fetch_assoc($resultP);

// pintar cuentas - chan chan chan chan!!!!!
$tipoCuenta = 1;
$queryCuentas = "SELECT cuenta, Ncuenta FROM rom_cantidades
LEFT OUTER JOIN rom_cuentas ON rom_cantidades.cuenta = rom_cuentas.clave
LEFT OUTER JOIN rom_dcuenta ON rom_cantidades.subcuenta = rom_dcuenta.claveD
INNER JOIN rom_asiento ON
(rom_cantidades.ejercicio =  rom_asiento.Easiento)
AND
(rom_cantidades.asiento = rom_asiento.asientoR)
WHERE (Easiento = '$IDejercicio' AND status = 1)
AND Tcuenta = 4
AND (Mcuenta = 4 OR Mcuenta = '$metodo')
GROUP BY Ncuenta
ORDER BY rom_cuentas.clave ASC
";
$resultCuentas = mysql_query($queryCuentas);
$rowCuentas = mysql_fetch_assoc($resultCuentas);
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sistema Contable Romero</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="libreria/js_principal.js"></script>
<script language="javascript" src="libreria/ajaxIni.js"></script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Auxiliar resultados</h1>
    </div>
    <div id="divTitularMenu"><a href="portada.php">Inicio</a> | <a href="libreria/calculadora/calculadora.html" target="_blank" onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | <a href="diarioFeAs.php?IDejercicio=<?php echo $IDejercicio; ?>&metodo=<?php echo $metodo; ?>">Asientos</a> | <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido">
<div class="divNavPlan">
  <form id="navPlan" name="navPlan" method="post" action="<?php $PHP_SELF; ?>">
    Navegación por plantillas:
    <select name="planilla" class="campoSelectPlanilla" id="planilla">
      <option selected="selected">Seleccione una...</option>
      <?php
	  do {
		  echo '<option value="'. $rowP['link'] .'">'. utf8_encode($rowP['Nplanilla']) .'</option>';
	  } while ($rowP = mysql_fetch_assoc($resultP));
	  ?>
    </select>
    <input name="IDejercicio" type="hidden" id="IDejercicio" value="<?php echo $IDejercicio; ?>" />
    <input name="metodo" type="hidden" id="metodo" value="<?php echo $metodo; ?>" />
    <input name="asiento" type="hidden" id="asiento" value="<?php echo $asiento; ?>" />
<input type="submit" name="subNav" id="subNav" value="ir" />
  </form>
</div>
<div class="divContCuerpo" id="divSupCuerpo"><strong>Fecha</strong>: <?php arregloFecha($fechaAsiento); ?>. <strong>Asiento</strong>: <?php echo $asiento; ?>. <strong>Método</strong>: <?php pintarMetodo($metodo); ?>. <strong>Status</strong>: <?php
    if($statusAsiento==0) {
		echo "abierto";
		} else {
		echo "cerrado";	
		}
		
		?> 
  <strong>Ejercicio</strong>: <?php pintarNejercicio($IDejercicio); ?><br />
  [ <a href="diarioFeAs.php?IDejercicio=<?php echo $IDejercicio; ?>&metodo=<?php echo $metodo; ?>">Crear un nuevo asiento | ver asientos anteriores</a> ]</div>
<div class="divContCuerpo">
  <div class="divContCuerpoIzq">
    <form id="listaCuentas" class="form-2014"
          name="listaCuentas" 
          method="get" action="aux_r.php">
      <fieldset>
        <legend>Selección de cuentas </legend>
        <div>
            Cuenta acumulativa:
        </div>
        <div>
            <select name="acumulativa" id="acumulativa" onchange="buscarCuentasDetalle();">
              <option>Seleccione una...</option>
              <?php do {
					echo '<option value="'. $rowCuentas['cuenta'] .'">'. $rowCuentas['cuenta']. ' - '.utf8_encode($rowCuentas['Ncuenta']) .'</option>';
				} while ($rowCuentas = mysql_fetch_assoc($resultCuentas)); ?>
            </select>
        </div>
        <div>
            Cuenta detalle:
        </div>
        <div>
            <select name="detalle" id="detalle"></select>
        </div>
        <div>
            <input name="IDejercicio" type="hidden" id="IDejercicio" value="<?php echo $IDejercicio; ?>" />
            <input name="metodo" type="hidden" id="metodo" value="<?php echo $metodo; ?>" />
            <input name="asiento" type="hidden" id="asiento" value="<?php echo $asiento; ?>" />
            <button type="submit" name="submit" id="submit" disabled>Desplegar</button>
        </div>
      </fieldset>
    </form>
  </div>
</div>
<?php if(isset($_GET['submit'])) { ?>
<div class="divContCuerpoInfo">CLAVE: <?php echo $rowLista['subcuenta']; ?> | SUBCUENTA : 
  <?php mostrarNombreD($rowLista['subcuenta']); ?>
</div>
<?php } ?>
<div class="divContCuerpo">
    <fieldset class="mostrar-modulos">
  <legend>Registros del asiento</legend>
  <table border="0" cellpadding="0" cellspacing="3" class="tablaLista">
    <tr>
      <td align="center" valign="middle" class="celdaListaTit"><strong>Asiento</strong></td>
      <td align="center" valign="middle" class="celdaListaTit"><strong>Fecha</strong></td>
      <td align="center" valign="middle" class="celdaListaTit"><strong>Redacción</strong><strong></strong></td>
      <td align="center" valign="middle" class="celdaListaDatA"><strong>Debe</strong></td>
      <td align="center" valign="middle" class="celdaListaDatV"><strong>Haber</strong></td>
    </tr>
    <?php if(!isset($_GET['submit'])) { ?>
    <tr>
      <td colspan="5" align="left" valign="top" class="celdaListaDat">Seleccione una cuenta.</td>
    </tr>
    <?php } else {
		do {
			if($rowLista['Ndcuenta'] != ""){
			?>
    <tr>
      <td align="left" valign="top" class="celdaListaDat"><?php echo $rowLista['asiento']; ?></td>
      <td align="left" valign="top" class="celdaListaDat"><?php mostrarFechaAsiento($rowLista['asiento'],$IDejercicio); ?></td>
      <td align="left" valign="top" class="celdaListaDat"><?php mostrarRedaccion($rowLista['asiento'],$rowLista['ejercicio']); ?></td>
      <td align="left" valign="top" class="celdaListaDatA">$
        <?php if($rowLista['tipo'] == "d") { echo number_format($rowLista['subcantidad'],2); } else { echo 0; }?></td>
      <td align="left" valign="top" class="celdaListaDatV">$
        <?php if($rowLista['tipo'] == "h") { echo number_format($rowLista['subcantidad'],2); } else { echo 0; }?></td>
    </tr>
    <?php } } while ($rowLista = mysql_fetch_assoc($resultLista)); ?>
    <tr>
      <td colspan="3" align="right" valign="top" class="celdaListaDat"><strong>Saldo inicial:</strong></td>
      <td align="left" valign="top" class="celdaListaDatA"><strong>$ <?php mostrarSaldoInicial($detalle, $IDejercicio, "d"); ?></strong></td>
      <td align="left" valign="top" class="celdaListaDatV"><strong>$ <?php mostrarSaldoInicial($detalle, $IDejercicio, "h"); ?></strong></td>
    </tr>
    <tr>
      <td colspan="3" align="right" valign="top" class="celdaListaDat"><strong>Movimientos del periodo:</strong></td>
      <td align="left" valign="top" class="celdaListaDatA"><strong>$ <?php mostrarMovPer("d",$detalle,$IDejercicio); ?></strong></td>
      <td align="left" valign="top" class="celdaListaDatV"><strong>$ <?php mostrarMovPer("h",$detalle,$IDejercicio); ?></strong></td>
    </tr>
    <tr>
      <td colspan="3" align="right" valign="top" class="celdaListaDat"><strong>Movimientos:</strong></td>
      <td align="left" valign="top" class="celdaListaDatA"><strong>$ <?php echo number_format($totalDebe,2); ?></strong></td>
      <td align="left" valign="top" class="celdaListaDatV"><strong>$ <?php echo number_format($totalHaber,2); ?></strong></td>
    </tr>
    <tr>
      <td colspan="3" align="right" valign="top" class="celdaListaDat"><strong>Saldo:</strong></td>
      <td align="left" valign="top" class="celdaListaDatA"><strong>$ <?php mostrarSaldo($totalDebe, $totalHaber); ?></strong></td>
      <td align="left" valign="top" class="celdaListaDatV"><strong>$ <?php mostrarSaldo($totalHaber, $totalDebe); ?></strong></td>
    </tr>
    <?php } ?>
  </table>
</fieldset></div>
  <div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
</body>
</html>