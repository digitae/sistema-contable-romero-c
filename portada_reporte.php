<?php
include("libreria/principal.php");

esUsuario(); // será usuario registrado?
$IDusuario = $_SESSION['userID'];
$pagina = $_SERVER['PHP_SELF'];

/**
 * AGREGAR NUEVA PRÁCTICA
 */
$submit = filter_input(INPUT_POST, 'submit');
if(isset($submit))
{
    $Npractica = strtolower(filter_input(INPUT_POST, 'Npractica',
            FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW));
    $query = "INSERT INTO rom_reporte_practicas "
            . "(Npractica,Fpractica,IDusuario) "
            . "VALUES "
            . "('$Npractica',NOW(),$IDusuario)";
    if(mysql_query($query) or die(mysql_error()))
    {
        header("Location: $pagina?ejeAgregado=true");
        exit;
    }
}
// fin

/**
 * EXTRAER LISTA DE PRÁCTICAS
 * 
 * Función para extraer la lista de todas las 
 * pŕacticas de éste módulo a partir del ID de usuario 
 * pasado como parámetro.
 * En caso de no contar con ninguna práctica, entonces no mostrar
 * campos <code><select></code>
 * @author Fernando Magrosoto V.
 * @copyright (c) 2014, Fernando Magrosoto
 * @category Sistema Romero C
 * @param int $IDusuario El ID del usuario
 * @param string $tabla La tabla de la BD
 */
function mostrarPracticas($IDusuario,$tabla)
{
    $lista = "";
    $opciones = "";
    $query = "SELECT IDpractica, Npractica "
            . "FROM $tabla "
            . "WHERE IDusuario = $IDusuario";
    $result = mysql_query($query) or die(mysql_error());
    $num = mysql_numrows($result);
    if($num != 0)
    {
        while($data = mysql_fetch_assoc($result))
        {
            $opciones .= "<option value=".$data['IDpractica'].">"
                    .ucwords(utf8_encode($data['Npractica']))."</option>";
        }
        $lista .= <<<LISTA
                <div style="margin-bottom: 10px;">
                    <select name="ejercicio" onchange="abrirEjercicio(this.value);">
                      <option value="false">Seleccionar una práctica...</option>
                      {$opciones}
                  </select>
                </div>
LISTA;
    }
    return $lista;
}
// fin función

$ejeAgregado = filter_input(INPUT_GET, 'ejeAgregado',
        FILTER_VALIDATE_BOOLEAN,FILTER_NULL_ON_FAILURE);

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sistema Contable Romero</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="libreria/js_principal.js"></script>
<script>
    var flipEjercicio = function()
    {
        var boton = document.getElementById('bot-nuevo-ejercicio');
        var form = document.getElementById('nuevo-ejercicio');
        var estadoForm = form.style.display;
        if(estadoForm === 'none')
        {
            form.style.display = 'block';
            boton.style.display = 'none';
            document.forms['nuevo-ejercicio']['nombre'].focus();
        } else {
            form.style.display = 'none';
            boton.style.display = 'block';
            document.forms['nuevo-ejercicio']['nombre'].value = "";
        }
    };
    var Validar = function(theForm)
    {
        if(theForm.Npractica.value === '')
        {
            alert('El campo NOMBRE está vacío.');
            theForm.Npractica.focus();
            return (false);
        }
        return (true);
    };
    var abrirEjercicio = function(valor)
    {
        window.location = "practicas_reporte.php?IDpractica="+valor;
    }
</script>


<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Portada</h1>
    </div>
    <div id="divTitularMenu">
        <a href="portada.php">Inicio</a> | 
        <a href="libreria/calculadora/calculadora.html" 
           target="_blank" 
           onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | 
        <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido">
  <div class="divContCuerpo">
      <div style="float: left; margin: 25px 0;">
          <img class="img-portada" src="imagenes/porPrincipiosConta.jpg" alt="" height="375" />
      </div>
      <div style="float: left; margin-left: 50px; margin-top: 25px; width: 400px">
          
          <h3 class="titulo-principal">Capítulo 7 - Estado de situación 
              financiera, NIF B-6, reporte</h3>
          <div class="migas">
              <ul>
                  <li><a href="portada.php">Inicio</a></li>
                  <li><a href="javascript:history.back()">Página anterior</a></li>
              </ul>
          </div>
          
          <fieldset class="mostrar-modulos" style="margin-top: 20px; font-size: 16px;">
              <legend>Lista de prácticas</legend>
                  <?php echo mostrarPracticas($IDusuario, 'rom_reporte_practicas') ?>
              <div id="bot-nuevo-ejercicio">
                  <ul>
                      <li><a onclick="flipEjercicio();" 
                             href="javascript:void(0);">
                              Crear una nueva práctica</a></li>
                  </ul>
              </div>
              <div id="nuevo-ejercicio" style="margin-top: 20px; display: none;">
                  <form class="form-2014" name="nueva-practica" 
                        method="post" action="<?php echo $pagina ?>" 
                        onsubmit="return Validar(this);" autocomplete="off">
                      <fieldset>
                          <legend>Nueva práctica</legend>
                          <div>
                              <input type="text" 
                                     name="Npractica" 
                                     required 
                                     placeholder="Nombre de la entidad" />
                          </div>
                          <div>
                              <button type="submit" name="submit">Agregar práctica</button>
                              <a style="margin-left: 10px" 
                                 onclick="flipEjercicio();" 
                                 href="javascript:void(0);">Cancelar</a>
                          </div>
                      </fieldset>
                  </form>
              </div>
          </fieldset>
          
      </div>
  </div>
  <!-- -->
  <div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
<?php
if($ejeAgregado)
{
    echo "<div id='msg-done'>Práctica agregada favorablemente.</div>".PHP_EOL;
    echo <<<SCRIPT
    <script>
    window.setTimeout(function(){
            var elemento = document.getElementById('msg-done');
            elemento.parentNode.removeChild(elemento);
        },3000);
    </script>
SCRIPT;
    echo PHP_EOL;
}
?>
</body>
</html>