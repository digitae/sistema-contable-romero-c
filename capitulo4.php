<?php
include 'libreria/principal.php';
include 'libreria/evaluaciones.php';

esUsuario(); // será usuario registrado?
$IDusuario = $_SESSION['userID'];
$capitulo = 4;
$registro = verificarCuestionario($capitulo, $IDusuario,$Fecha);


?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> Principios de Contabilidad. Cuestionario de evaluación. Capítulo 4</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="libreria/js_principal.js"></script>
<script src="libreria/vendor/jquery/jquery-1.11.0.min.js"></script>
<script src="libreria/calCap.js"></script>
<script src="libreria/rc4.js"></script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Cuestionario de autoevaluación</h1>
    </div>
    <div id="divTitularMenu">
        <a href="portada.php">Inicio</a> | 
        <a href="libreria/calculadora/calculadora.html" 
           target="_blank" 
           onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | 
        <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido" style="width: 900px">
  <div class="divContCuerpo">
      
      <h3 class="titulo-principal">Capítulo 4</h3>
      <div class="migas">
          <ul>
              <li><a href="portada.php">Portada</a></li>
              <li><a href="javascript:history.back();">Página anterior</a></li>
              <li><img src="imagenes/print.png" 
                       alt="" 
                       height="25" 
                       style="float: left; margin-right: 5px;" />
                  <a href="javascript:print();">Imprimir página</a></li>
          </ul>
      </div>
      
      <table class="mis-datos">
          <tr>
              <td>Universidad o Institución:</td>
              <td>
                  <input oninput="cCamCues('unidad',this.value,<?php echo $registro ?>);" 
                         name="unidad" 
                         id='unidad' 
                         required 
                         value="<?php echo xCamCues('unidad', $registro) ?>" />
              </td>
              <td>Escuela o facultad:</td>
              <td>
                  <input oninput="cCamCues('facultad',this.value,<?php echo $registro ?>);" 
                         name="facultad" 
                         id='facultad' 
                         required 
                         value="<?php echo xCamCues('facultad', $registro) ?>" />
              </td>
          </tr>
          <tr>
              <td>Asignatura:</td>
              <td>
                  <input oninput="cCamCues('asignatura',this.value,<?php echo $registro ?>);" 
                         name="asignatura" 
                         id='asignatura' 
                         required 
                         value="<?php echo xCamCues('asignatura', $registro) ?>" />
              </td>
              <td>Alumno:</td>
              <td>
                  <?php echo nombreUsuario($IDusuario) ?>
              </td>
          </tr>
          <tr>
              <td>Maestro:</td>
              <td>
                  <input oninput="cCamCues('maestro',this.value,<?php echo $registro ?>);" 
                         name="maestro" 
                         id='maestro' 
                         required 
                         value="<?php echo xCamCues('maestro', $registro) ?>" />
              </td>
              <td>Fecha:</td>
              <td>
                  <input oninput="cCamCues('fecha',this.value,<?php echo $registro ?>);" 
                         name="fecha" 
                         id='fecha' 
                         required 
                         value="<?php echo xCamCues('fecha', $registro) ?>" />
              </td>
          </tr>
      </table>
      
      <h4 class="subtit-4">OBJETIVOS DE APRENDIZAJE</h4>
      
      <div class="objetivos">
          <p>Al finalizar este capítulo, el alumno será capaz de:</p>
          <ol>
              <li>Reconocer la necesidad e importancia de la convergencia de las 
                  normas nacionales e internacionales de información financiera.</li>
              <li>Revisar el marco de referencia y la evolución de la teoría 
                  contable en relación con las normas de información financiera.</li>
              <li>Comprender la importancia, concepto, conformación y división 
                  de las normas de información financiera.</li>
              <li>Comprender la definición de los postulados básicos.</li>
              <li>Identificar y aplicar los postulados básicos de la sustancia 
                  económica, de la entidad económica, de la entidad en marcha, 
                  de la devengación contable, de la asociación de costos y 
                  gastos con ingresos, de la valuación, de la dualidad 
                  económica y de la consistencia.</li>
          </ol>
      </div>
      
      <h4 class="subtit-4">EJERCICIOS</h4>
      
      <h5>I. RESUELVA LO SIGUIENTE</h5>
      
      <div class="pregunta-libre">
          <p><span>1</span> ¿Qué finalidad persigue la convergencia de las 
              normas de información financiera?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib01" 
                    id="prelib01"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib01'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>2</span> ¿En qué fecha los principios de contabilidad 
              generalmente aceptados (PCGA) se convirtieron en México en normas 
              de información financiera (NIF)?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib02" id="prelib02"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib02'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>3</span> ¿Cuál es el organismo responsable de la emisión de 
              las NIF? </p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib03" id="prelib03"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib03'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>4</span> A nivel mundial, ¿cuándo ocurrió el evento más 
              relevante a favor de la convergencia de normas contables y qué 
              organismos se vincularon?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib04" id="prelib04"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib04'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>5</span> ¿Qué proceso sigue la elaboración de los principios 
              de contabilidad y de las normas de información financiera en su 
              aspecto teórico?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib05" id="prelib05"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib05'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>6</span> ¿Qué proceso se emplea para el desarrollo de la 
              práctica contable?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib06" id="prelib06"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib06'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>7</span> ¿En qué radica la importancia de las NIF?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib07" id="prelib07"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib07'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>8</span> ¿Cómo se conforman las NIF?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib08" id="prelib08"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib08'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>9</span> Señale cuántos y cuáles son los  postulados básicos.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib09" id="prelib09"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib09'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>10</span> ¿Cúando se dice que una entidad es una unidad 
              identificable?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib10" id="prelib10"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib10'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>11</span> Diga qué es una entidad con propósitos lucrativos.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib11" id="prelib11"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib11'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>12</span> Indique qué es una entidad con propósitos no lucrativos.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib12" id="prelib12"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib12'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>13</span> ¿Qué características tienen las entidades con 
              propósitos no lucrativos?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib13" id="prelib13"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib13'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>14</span> Explique la diferencia entre los términos 
              devengado y realizado.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib14" id="prelib14"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib14'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>15</span> ¿Qué se entiende por transacciones según la NIF A-2?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib15" id="prelib15"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib15'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>16</span> Señale qué es una transacción recíproca y 
              una no recíproca.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib16" id="prelib16"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib16'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>17</span> Explique el concepto “periodo contable”.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib17" id="prelib17"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib17'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>18</span> Explique el postulado de asociación de costos y 
              gastos con ingresos.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib18" id="prelib18"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib18'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>19</span> Explique, en los términos de la NIF A-2, el 
              concepto cuantificación en términos monetarios.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib19" id="prelib19"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib19'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>20</span> Explique, en los términos de la NIF A-2, el  
              concepto valor económico más objetivo en el reconocimiento inicial.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib20" id="prelib20"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib20'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>21</span> Explique, en los términos de la NIF A-2, el 
              concepto valor económico más objetivo en el reconocimiento posterior.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib21" id="prelib21"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib21'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>22</span> ¿Qué sostiene John A. Tracy sobre el activo?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib22" id="prelib22"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib22'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>23</span> ¿Qué sostiene A. Lopes de Sá sobre el activo?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib23" id="prelib23"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib23'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>24</span> ¿Qué señala John A Tracy respecto de las fuentes 
              del capital, pasivo u origen de recursos?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib24" id="prelib24"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib24'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>25</span> ¿Qué señala A. Lopes de Sá respecto de las fuentes 
              del capital, pasivo u origen de recursos?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib25" id="prelib25"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib25'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>26</span> ¿Qué es el reconocimiento contable?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib26" id="prelib26"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib26'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>27</span> ¿Qué se entiende por reconocimiento inicial?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib27" id="prelib27"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib27'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>28</span> ¿Qué se entiende por reconocimiento posterior?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib28" id="prelib28"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib28'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>29</span> ¿A qué se refiere el concepto de presentación?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib29" id="prelib29"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib29'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>30</span> ¿Qué implica el concepto de revelación?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib30" id="prelib30"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib30'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>31</span> Mencione el objetivo de las normas particulares.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib31" id="prelib31"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib31'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>32</span> ¿Cómo se clasifican las normas particulares?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib32" id="prelib32"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib32'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>33</span> Indique a qué se refiere el juicio profesional en 
              la aplicación de las NIF.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib33" id="prelib33"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib33'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>34</span> ¿Con qué criterio o enfoque debe ejercerse el 
              juicio profesional? </p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib34" id="prelib34"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib34'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>35</span> Indique cuándo se emplea comúnmente el juicio 
              profesional.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib35" id="prelib35"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib35'); ?></textarea>
      </div>
      
      <h5>II. COMPLETE LAS LÍNEAS EN BLANCO</h5>
      
      <div class="completar-lineas">
          <span class="num">1</span>
          El término <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_1" 
                    name="comLin_1" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_1'); ?>" /> de información financiera se refiere al conjunto 
          de pronunciamientos normativos, <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_2" 
                    name="comLin_2" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_2'); ?>" /> y particulares, emitidos 
          por el <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_3" 
                    name="comLin_3" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_3'); ?>" /> o transferidos 
          al <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_4" 
                    name="comLin_4" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_4'); ?>" />, que regulan la información 
          financiera contenida en los <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_5" 
                    name="comLin_5" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_5'); ?>" /> financieros y sus notas, en un 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_6" 
                    name="comLin_6" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_6'); ?>" /> y fecha 
          determinados, que son <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_7" 
                    name="comLin_7" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_7'); ?>" /> de manera amplia y 
          generalizada por <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_8" 
                    name="comLin_8" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_8'); ?>" /> los usuarios de 
          la información financiera.
      </div>
      
      <div class="completar-lineas">
          <span class="num">2</span>
          Las NIF se conforman de cuatro grandes apartados: a) NIF conceptuales 
          o “<input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_9" 
                    name="comLin_9" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_9'); ?>" /> 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_10" 
                    name="comLin_10" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_10'); ?>" />”, b) Normas de 
          Información Financiera <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_11" 
                    name="comLin_11" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_11'); ?>" /> 
          o “NIF <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_12" 
                    name="comLin_12" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_12'); ?>" />”; c) 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_13" 
                    name="comLin_13" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_13'); ?>" /> a las NIF o 
          “<input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_14" 
                    name="comLin_14" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_14'); ?>" />” y, d) 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_15" 
                    name="comLin_15" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_15'); ?>" /> a las NIF o 
          “<input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_16" 
                    name="comLin_16" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_16'); ?>" />”.
      </div>
      
      <div class="completar-lineas">
          <span class="num">3</span>
          Los <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_17" 
                    name="comLin_17" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_17'); ?>" /> básicos son 
          fundamentos que configuran el sistema de 
          información contable y <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_18" 
                    name="comLin_18" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_18'); ?>" /> el ambiente en 
          que debe operar. Por lo 
          tanto, tienen influencia en todas las faces que comprenden dicho 
          sistema contable; esto es, inciden en la <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_19" 
                    name="comLin_19" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_19'); ?>" />, análisis, 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_20" 
                    name="comLin_20" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_20'); ?>" />, captación, 
          procesamiento y, finalmente, en el 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_21" 
                    name="comLin_21" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_21'); ?>" /> contable de las 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_22" 
                    name="comLin_22" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_22'); ?>" />, transformaciones 
          internas y de otros <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_23" 
                    name="comLin_23" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_23'); ?>" />, que lleva a cabo o que afectan 
          económicamente a una entidad.
      </div>
      
      <div class="completar-lineas">
          <span class="num">4</span>
          La <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_24" 
                    name="comLin_24" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_24'); ?>" /> económica debe 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_25" 
                    name="comLin_25" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_25'); ?>" /> en la delimitación 
          y operación del sistema de información contable, así como en el 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_26" 
                    name="comLin_26" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_26'); ?>" /> contable de las 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_27" 
                    name="comLin_27" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_27'); ?>" />, 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_28" 
                    name="comLin_28" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_28'); ?>" /> internas y otros 
          eventos que afectan <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_29" 
                    name="comLin_29" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_29'); ?>" /> a la entidad. 
      </div>
      
      <div class="completar-lineas">
          <span class="num">5</span>
          Para lograr la prevalencia de la sustancia económica sobre la 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_30" 
                    name="comLin_30" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_30'); ?>" /> 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_31" 
                    name="comLin_31" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_31'); ?>" />, el sistema 
          contable debe ser diseñado de tal forma que sea capaz de 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_32" 
                    name="comLin_32" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_32'); ?>" /> la 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_33" 
                    name="comLin_33" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_33'); ?>" /> económica de la 
          entidad emisora de <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_34" 
                    name="comLin_34" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_34'); ?>" /> financiera.
      </div>
      
      <div class="completar-lineas">
          <span class="num">6</span>
          La entidad económica es aquella unidad <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_35" 
                    name="comLin_35" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_35'); ?>" /> que realiza 
          actividades económicas y que está constituida por un conjunto 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_36" 
                    name="comLin_36" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_36'); ?>" /> de recursos 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_37" 
                    name="comLin_37" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_37'); ?>" />, materiales y 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_38" 
                    name="comLin_38" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_38'); ?>" /> (actividades 
          económicas y recursos) administrados por un centro de control 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_39" 
                    name="comLin_39" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_39'); ?>" /> que toma 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_40" 
                    name="comLin_40" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_40'); ?>" /> encaminadas al 
          cumplimiento de los fines específicos para los que fue creada.
      </div>
      
      <div class="completar-lineas">
          <span class="num">7</span>
          La <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_41" 
                    name="comLin_41" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_41'); ?>" /> de la entidad 
          económica es <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_42" 
                    name="comLin_42" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_42'); ?>" /> de la de sus 
          accionistas, propietarios o <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_43" 
                    name="comLin_43" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_43'); ?>" />.
      </div>
      
      <div class="completar-lineas">
          <span class="num">8</span>
          La entidad económica se presume en <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_44" 
                    name="comLin_44" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_44'); ?>" /> permanente, dentro 
          de un horizonte de tiempo <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_45" 
                    name="comLin_45" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_45'); ?>" />, salvo prueba en 
          contrario; por lo que las <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_46" 
                    name="comLin_46" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_46'); ?>" /> en el sistema de 
          información contable representan <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_47" 
                    name="comLin_47" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_47'); ?>" /> 
          sistemáticamente obtenidos con base en las NIF. En tanto prevalezcan 
          dichas condiciones, no deben determinarse valores <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_48" 
                    name="comLin_48" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_48'); ?>" /> 
          provenientes de la disposición o <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_49" 
                    name="comLin_49" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_49'); ?>" /> del conjunto de los 
          activos netos de la entidad.
      </div>
      
      <div class="completar-lineas">
          <span class="num">9</span>
          Los <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_50" 
                    name="comLin_50" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_50'); ?>" /> derivados de las 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_51" 
                    name="comLin_51" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_51'); ?>" /> que lleva a cabo 
          la entidad económica con otras entidades, de las transformaciones 
          internas y de otros eventos que la afectan económicamente, deben 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_52" 
                    name="comLin_52" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_52'); ?>" /> en su totalidad, 
          en el <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_53" 
                    name="comLin_53" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_53'); ?>" /> en que ocurren, 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_54" 
                    name="comLin_54" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_54'); ?>" /> de la fecha en que 
          se consideren <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_55" 
                    name="comLin_55" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_55'); ?>" /> para fines contables.
      </div>
      
      <div class="completar-lineas">
          <span class="num">10</span>
          Las transacciones, transformaciones <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_56" 
                    name="comLin_56" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_56'); ?>" /> y 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_57" 
                    name="comLin_57" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_57'); ?>" /> que afectan 
          económicamente a la entidad, susceptibles de ser 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_58" 
                    name="comLin_58" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_58'); ?>" />, se 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_59" 
                    name="comLin_59" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_59'); ?>" /> cuando se 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_60" 
                    name="comLin_60" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_60'); ?>" /> y deben 
          identificarse con el <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_61" 
                    name="comLin_61" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_61'); ?>" /> en 
          que ocurren. Por lo tanto, cualquier informe financiero debe indicar 
          claramente el <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_62" 
                    name="comLin_62" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_62'); ?>" /> contable a que se 
          refiere.
      </div>
      
      <div class="completar-lineas">
          <span class="num">11</span>
          Valuación es el proceso que atribuye un <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_63" 
                    name="comLin_63" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_63'); ?>" /> a los conceptos 
          específicos de los estados financieros, <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_64" 
                    name="comLin_64" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_64'); ?>" /> entre distintas 
          alternativas la base de <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_65" 
                    name="comLin_65" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_65'); ?>" /> más apropiada para los 
          elementos que mejor <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_66" 
                    name="comLin_66" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_66'); ?>" /> el tipo de 
          transacción o evento económico, atendiendo a su <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_67" 
                    name="comLin_67" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_67'); ?>" /> 
          y las circunstancias que los generaron.
      </div>
      
      <div class="completar-lineas">
          <span class="num">12</span>
          La <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_68" 
                    name="comLin_68" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_68'); ?>" /> económica o 
          estructura <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_69" 
                    name="comLin_69" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_69'); ?>" /> de una entidad está 
          constituida por los <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_70" 
                    name="comLin_70" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_70'); ?>" /> de los que dispone 
          para la consecución de sus fines y, por las fuentes para obtener 
          dichos recursos, ya sean <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_71" 
                    name="comLin_71" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_71'); ?>" /> o ajenas.
      </div>
      
      <div class="completar-lineas">
          <span class="num">13</span>
          El Boletín B-4, identificación de las fuentes y las aplicaciones de 
          recursos: los orígenes de recursos están representados por 
          disminuciones de <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_72" 
                    name="comLin_72" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_72'); ?>" />, aumentos de pasivo 
          y <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_73" 
                    name="comLin_73" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_73'); ?>" /> de capital. 
          Las aplicaciones de recursos están representadas por aumentos de 
          activos, disminuciones de pasivo y <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_74" 
                    name="comLin_74" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_74'); ?>" /> de capital contable.
      </div>
      
      <div class="completar-lineas">
          <span class="num">14</span>
          La <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_75" 
                    name="comLin_75" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_75'); ?>" /> implica que a 
          operaciones y eventos <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_76" 
                    name="comLin_76" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_76'); ?>" /> que afectan a una 
          entidad, debe corresponder un <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_77" 
                    name="comLin_77" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_77'); ?>" /> tratamiento 
          contable, el cual permanece a través del tiempo, mientras 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_78" 
                    name="comLin_78" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_78'); ?>" /> cambie su 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_79" 
                    name="comLin_79" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_79'); ?>" /> económica de las 
          transacciones.
      </div>
            
      <div class="completar-lineas">
          <button onclick="calificarCompletar('comLin',79,rc4_cl);">CALIFICAR</button>
          <span id="comLin_cal"></span>
      </div>
      
      <h5>III. CONTESTE VERDADERO O FALSO</h5>
      <p style="font-size: 12px; text-align: justify">En los siguientes casos 
          se muestran aplicaciones de los postulados básicos, en los cuales 
          algunos son respetados y otros no. Señale en la columna correspondiente 
          con una V si es verdadera o con una F si es falsa la aseveración; 
          en todos los casos, en la columna Postulado aplicable señale el 
          nombre del postulado que fundamenta la operación; asimismo, en el 
          supuesto de haberse violado el postulado en cuestión, independientemente 
          de que la aseveración sea verdadera o falsa, en la columna de 
          Observaciones señale cuál fue la violación y proponga la posible 
          corrección.</p>
      
      <table class="verdadero-falso">
          <tr>
              <th>Caso</th>
              <th>V</th>
              <th>F</th>
              <th>Postulado aplicable</th>
              <th>Observaciones</th>
          </tr>
          <tr>
              <td>
                  <span class="num">1</span>
                  La compañía Alpha, S.A. registra en su contabilidad los gastos 
                  de automóvil y viaje de la esposa e hijos del gerente. Se 
                  aplica correctamente el postulado.
              </td>
              <td id="verfal_1v_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_1" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_1')==='verdadero')?' checked':'' ?> />
              </td>
              <td id="verfal_1f_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_1" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_1')==='falso')?' checked':'' ?> />
              </td>
              <td id="verfalpa_1_parcial">
                  <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalpa_1" id="verfalpa_1"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalpa_1'); ?></textarea>
              </td>
              <td>
                  <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalob_1" id="verfalob_1"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalob_1'); ?></textarea>
              </td>
          </tr>
          <tr>
              <td>
                  <span class="num">2</span>
                  La profesora Bautista, maestra de escuela, tiene un negocio 
                  de salón de fiestas infantiles y una escuela particular. 
                  Maneja las operaciones de todo ello mediante una sola 
                  cuenta de cheques. Se viola el postulado.
              </td>
              <td id="verfal_2v_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_2" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_2')==='verdadero')?' checked':'' ?> />
              </td>
              <td id="verfal_2f_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_2" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_2')==='falso')?' checked':'' ?> />
              </td>
              <td id="verfalpa_2_parcial">
                  <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalpa_2" id="verfalpa_2"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalpa_2'); ?></textarea>
              </td>
              <td>
                  <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalob_2" id="verfalob_2"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalob_2'); ?></textarea>
              </td>
          </tr>
          <tr>
              <td>
                  <span class="num">3</span>
                  El gerente le indica al contador que el próximo año realizará 
                  una venta muy importante, que le ha sido asegurada por su 
                  cliente y le pide que la incluya en los registros y estados 
                  financieros de este año. Se viola el postulado.
              </td>
              <td id="verfal_3v_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_3" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_3')==='verdadero')?' checked':'' ?> />
              </td>
              <td id="verfal_3f_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_3" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_3')==='falso')?' checked':'' ?> />
              </td>
              <td id="verfalpa_3_parcial">
                  <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalpa_3" id="verfalpa_3"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalpa_3'); ?></textarea>
              </td>
              <td>
                  <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalob_3" id="verfalob_3"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalob_3'); ?></textarea>
              </td>
          </tr>
          <tr>
              <td>
                  <span class="num">4</span>
                  Como los gastos devengados de energía eléctrica, teléfonos y 
                  honorarios no serán pagados sino hasta el próximo ejercicio, 
                  el gerente no está de acuerdo en que se incluyan en este 
                  periodo, pero acepta la propuesta del contador de registrarlos 
                  en él. El contador aplica correctamente el postulado.
              </td>
              <td id="verfal_4v_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_4" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_4')==='verdadero')?' checked':'' ?> />
              </td>
              <td id="verfal_4f_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_4" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_4')==='falso')?' checked':'' ?> />
              </td>
              <td id="verfalpa_4_parcial">
                  <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalpa_4" id="verfalpa_4"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalpa_4'); ?></textarea>
              </td>
              <td>
                  <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalob_4" id="verfalob_4"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalob_4'); ?></textarea>
              </td>
          </tr>
          <tr>
              <td>
                  <span class="num">5</span>
                  A inicio de año se paga una póliza de seguros contra incendio 
                  que tiene una vigencia de tres años; el contador registra el 
                  importe total como un gasto del periodo. Se respeta el postulado.
              </td>
              <td id="verfal_5v_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_5" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_5')==='verdadero')?' checked':'' ?> />
              </td>
              <td id="verfal_5f_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_5" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_5')==='falso')?' checked':'' ?> />
              </td>
              <td id="verfalpa_5_parcial">
                  <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalpa_5" id="verfalpa_5"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalpa_5'); ?></textarea>
              </td>
              <td>
                  <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalob_5" id="verfalob_5"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalob_5'); ?></textarea>
              </td>
          </tr>
          <tr>
              <td>
                  <span class="num">6</span>
                  La compañía Delta adquirió un edificio hace dos años con un 
                  costo de $10 000 000; a la fecha de los estados financieros, 
                  según avalúo, su valor es de $15 000 000, importe que se 
                  presenta en los estados financieros, y las demás cuentas se 
                  muestran a valores históricos; la situación económica del país 
                  no es inflacionaria. Se aplica correctamente el postulado.
              </td>
              <td id="verfal_6v_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_6" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_6')==='verdadero')?' checked':'' ?> />
              </td>
              <td id="verfal_6f_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_6" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_6')==='falso')?' checked':'' ?> />
              </td>
              <td id="verfalpa_6_parcial">
                  <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalpa_6" id="verfalpa_6"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalpa_6'); ?></textarea>
              </td>
              <td>
                  <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalob_6" id="verfalob_6"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalob_6'); ?></textarea>
              </td>
          </tr>
          <tr>
              <td>
                  <span class="num">7</span>
                  La empresa del señor Gutiérrez presenta sus bienes valuados 
                  a precio de liquidación, aun cuando no se encuentra en esa 
                  situación. Se aplica correctamente el postulado.

              </td>
              <td id="verfal_7v_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_7" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_7')==='verdadero')?' checked':'' ?> />
              </td>
              <td id="verfal_7f_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="verfal_7" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfal_7')==='falso')?' checked':'' ?> />
              </td>
              <td id="verfalpa_7_parcial">
                  <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalpa_7" id="verfalpa_7"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalpa_7'); ?></textarea>
              </td>
              <td>
                  <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="verfalob_7" id="verfalob_7"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'verfalob_7'); ?></textarea>
              </td>
          </tr>
          <tr>
              <td colspan="4" id="verfal_cal"></td>
              <td>
                  <button onclick="calificarVerFal('verfal',7,rc4_verfalR,rc4_verfalI);">CALIFICAR</button>
              </td>
          </tr>
      </table>
      
  </div>
  <!-- -->
  <div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
<script>
    var tc;
    var cCamCues = function(campo,valor,registro)
    {
      clearTimeout(tc);
      tc = window.setTimeout(function(){
          $.post('libreria/cCamCues.php',{
              modDatos: true,
              elcampo: campo,
              elvalor: valor,
              elregistro: registro
          },function(respuesta){
              if(respuesta === 'modificado')
              {
                  // Modificado
              } else {
                  // Error
                  console.log('-> '+respuesta);
              }
          });
      },1000);
    };
    var cResCues = function(cues,usu,cap,preg,resp)
    {
      clearTimeout(tc);
      tc = window.setTimeout(function(){
          $.post('libreria/cCamCues.php',{
              modRespuestas: true,
              cuestionario: cues,
              usuario: usu,
              capitulo: cap,
              pregunta: preg,
              respuesta:resp
          },function(respuesta){
              if(respuesta === 'modificado')
              {
                  // Modificado
              } else {
                  // Error
                  console.log('-> '+respuesta);
              }
          });
      },500);
    };
    
    window.onload = function(){
        protegerPLibres();
        protegerCompletar();
        protegerVerFal();
    };
</script>
</body>
</html>