<?php
include("libreria/principal.php");

esUsuario(); // será usuario registrado?

###################
## SCRIPT GENERAL ##

// Navegación por planillas
if(isset($_POST['subNav'])){
	$planilla = $_POST['planilla'];
	$IDejercicio = $_POST['IDejercicio'];
	$metodo = $_POST['metodo'];
	$asiento = $_POST['asiento'];
	
	header("Location: $planilla?IDejercicio=$IDejercicio&metodo=$metodo&asiento=$asiento");
	exit;		
}


// comprobar que han iniciado un ejercicio
if(isset($_GET['IDejercicio']) && isset($_GET['metodo']) && isset($_GET['asiento'])){
	$IDejercicio = $_GET['IDejercicio'];
	$metodo = $_GET['metodo'];
	$asiento = $_GET['asiento'];
} else {
	header("Location: portada.php?ejercicio=false");
	exit;	
}
//fin
###################

// Extraer fecha del asiento
$queryFecha = "SELECT Fasiento, redaccion, status FROM rom_asiento WHERE asientoR = '$asiento' AND Easiento = '$IDejercicio'";
$resultFecha = mysql_query($queryFecha);
$rowFecha = mysql_fetch_assoc($resultFecha);
$fechaAsiento = $rowFecha['Fasiento'];
$statusAsiento = $rowFecha['status']; // status del asiento ** IMPORTANTE **

// pintar lista de registros por ejercicio

if(isset($_GET['cuenta'])){
	$cuenta = $_GET['cuenta'];
	$queryLista = "SELECT DISTINCT rom_cantidades.subcuenta, cuenta, Ncuenta, Ndcuenta
					FROM rom_cantidades
					LEFT OUTER JOIN rom_cuentas ON rom_cantidades.cuenta = rom_cuentas.clave
					LEFT OUTER JOIN rom_dcuenta ON rom_cantidades.subcuenta = rom_dcuenta.claveD
					INNER JOIN rom_asiento ON
					(rom_cantidades.ejercicio =  rom_asiento.Easiento)
					AND
					(rom_cantidades.asiento = rom_asiento.asientoR)
					WHERE (Easiento = '$IDejercicio' AND status = 1)
					AND cuenta = '$cuenta'
					AND rom_cantidades.subcuenta != ''
					ORDER BY Ndcuenta ASC";
	$resultLista = mysql_query($queryLista) or die (mysql_error());
	$rowLista = mysql_fetch_assoc($resultLista);
	
######################
## CALCULAR TOTALES ##
######################

// Calcular totales DEBE
$totalDebe = 0;
$SumaTotalSaldoD = 0;

// Calcular totales HABER
$totalHaber = 0;
$SumaTotalSaldoH = 0;

}

// pintar navegación de plantillas
$queryP		= "SELECT * FROM rom_planillas WHERE Mplanilla = 4 OR Mplanilla = '$metodo' ORDER BY IDplanilla ASC";
$resultP	= mysql_query($queryP) or die (mysql_error());
$rowP		= mysql_fetch_assoc($resultP);

// pintar cuentas - chan chan chan chan!!!!!
$tipoCuenta = 1;
$queryCuentas = "SELECT * FROM rom_cantidades
LEFT OUTER JOIN rom_cuentas ON rom_cantidades.cuenta = rom_cuentas.clave
LEFT OUTER JOIN rom_dcuenta ON rom_cantidades.subcuenta = rom_dcuenta.claveD
INNER JOIN rom_asiento ON
(rom_cantidades.ejercicio =  rom_asiento.Easiento)
AND
(rom_cantidades.asiento = rom_asiento.asientoR)
WHERE (Easiento = '$IDejercicio' AND status = 1)
AND Tcuenta = 2
AND (rom_dcuenta.subcuenta = 3 OR rom_dcuenta.subcuenta = 4)
AND (Mcuenta = 4 OR Mcuenta = '$metodo')
GROUP BY Ncuenta
ORDER BY rom_cuentas.clave ASC
";
$resultCuentas = mysql_query($queryCuentas);
$rowCuentas = mysql_fetch_assoc($resultCuentas);
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sistema Contable Romero</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="libreria/js_principal.js"></script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Relaciones auxiliares pasivo</h1>
    </div>
    <div id="divTitularMenu"><a href="portada.php">Inicio</a> | <a href="libreria/calculadora/calculadora.html" target="_blank" onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | <a href="diarioFeAs.php?IDejercicio=<?php echo $IDejercicio; ?>&metodo=<?php echo $metodo; ?>">Asientos</a> | <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido">
<div class="divNavPlan">
  <form id="navPlan" name="navPlan" method="post" action="<?php $PHP_SELF; ?>">
    Navegación por plantillas:
    <select name="planilla" class="campoSelectPlanilla" id="planilla">
      <option selected="selected">Seleccione una...</option>
      <?php
	  do {
		  echo '<option value="'. $rowP['link'] .'">'. utf8_encode($rowP['Nplanilla']) .'</option>';
	  } while ($rowP = mysql_fetch_assoc($resultP));
	  ?>
    </select>
    <input name="IDejercicio" type="hidden" id="IDejercicio" value="<?php echo $IDejercicio; ?>" />
    <input name="metodo" type="hidden" id="metodo" value="<?php echo $metodo; ?>" />
    <input name="asiento" type="hidden" id="asiento" value="<?php echo $asiento; ?>" />
<input type="submit" name="subNav" id="subNav" value="ir" />
  </form>
</div>
<div class="divContCuerpo" id="divSupCuerpo"><strong>Fecha</strong>: <?php arregloFecha($fechaAsiento); ?>. <strong>Asiento</strong>: <?php echo $asiento; ?>. <strong>Método</strong>: <?php pintarMetodo($metodo); ?>. <strong>Status</strong>: <?php
    if($statusAsiento==0) {
		echo "abierto";
		} else {
		echo "cerrado";	
		}
		
		?> 
  <strong>Ejercicio</strong>: <?php pintarNejercicio($IDejercicio); ?><br />
  [ <a href="diarioFeAs.php?IDejercicio=<?php echo $IDejercicio; ?>&metodo=<?php echo $metodo; ?>">Crear un nuevo asiento | ver asientos anteriores</a> ]</div>
<div class="divContCuerpo">
  <div class="divContCuerpoIzq">
    <form id="listaCuentas" class="form-2014"
          name="listaCuentas" method="get" action="rel_aux_pasivo.php">
      <fieldset>
        <legend>Selección de cuentas </legend>
        <div>Cuenta:</div>
        <div>
            <select name="cuenta" id="cuenta">
                  <option>Seleccione una...</option>
                  <?php do {
					echo '<option value="'. $rowCuentas['clave'] .'">'. $rowCuentas['clave']. ' - '.utf8_encode($rowCuentas['Ncuenta']) .'</option>';
				} while ($rowCuentas = mysql_fetch_assoc($resultCuentas)); ?>
                </select>
        </div>
        <div>
            <input name="IDejercicio" type="hidden" id="IDejercicio" value="<?php echo $IDejercicio; ?>" />
            <input name="metodo" type="hidden" id="metodo" value="<?php echo $metodo; ?>" />
            <input name="asiento" type="hidden" id="asiento" value="<?php echo $asiento; ?>" />
            <button type="submit" name="submit" id="submit">Desplegar</button>
        </div>
      </fieldset>
    </form>
  </div>
</div>
<div class="divContCuerpo">
    <fieldset class="mostrar-modulos">
  <legend>Registros del asiento</legend>
  <table border="0" cellpadding="0" cellspacing="3" class="tablaLista">
    <tr>
      <td rowspan="2" align="center" valign="middle" class="celdaListaTit"><strong>Clave</strong></td>
      <td rowspan="2" align="center" valign="middle" class="celdaListaTit"><strong>Cuenta</strong></td>
      <td rowspan="2" align="center" valign="middle" class="celdaListaTit"><strong>Subcuenta</strong></td>
      <td rowspan="2" align="center" valign="middle" class="celdaListaDatA"><strong>Debe</strong></td>
      <td rowspan="2" align="center" valign="middle" class="celdaListaDatV"><strong>Haber</strong></td>
      <td colspan="2" align="center" valign="middle"><strong>Saldo final</strong></td>
      </tr>
    <tr>
      <td align="center" valign="middle" class="celdaListaDatA"><strong>Deudor</strong></td>
      <td align="center" valign="middle" class="celdaListaDatV"><strong>Acreedor</strong></td>
    </tr>
    <?php if(!isset($_GET['cuenta'])) { ?>
    <tr>
      <td colspan="7" align="left" valign="top" class="celdaListaDat">Seleccione una cuenta.</td>
    </tr>
    <?php } else {
		do {
			if($rowLista['Ndcuenta'] != ""){
			$cantidadD = mostrarTotalRelMov("d",$rowLista['subcuenta'],$IDejercicio);
			$totalDebe += $cantidadD;
			$cantidadH = mostrarTotalRelMov("h",$rowLista['subcuenta'],$IDejercicio);
			$totalHaber += $cantidadH;
			$iteracionD = mostrarSaldoFinal($cantidadD,$cantidadH);
			$iteracionH = mostrarSaldoFinal($cantidadH,$cantidadD);
			$SumaTotalSaldoD += $iteracionD;
			$SumaTotalSaldoH += $iteracionH;
			?>
    <tr>
      <td align="left" valign="top" class="celdaListaDat"><?php echo $rowLista['subcuenta']; ?></td>
      <td align="left" valign="top" class="celdaListaDat"><strong><?php echo utf8_encode($rowLista['Ncuenta']); ?></strong></td>
      <td align="left" valign="top" class="celdaListaDat"><?php echo utf8_encode($rowLista['Ndcuenta']); ?></td>
      <td align="left" valign="top" class="celdaListaDatA">$ <?php mostrarTotalRel("d",$rowLista['subcuenta'],$IDejercicio); ?></td>
      <td align="left" valign="top" class="celdaListaDatV">$ <?php mostrarTotalRel("h",$rowLista['subcuenta'],$IDejercicio); ?></td>
      <td align="left" valign="top" class="celdaListaDatA">$ <?php echo number_format($iteracionD,2); ?></td>
      <td align="left" valign="top" class="celdaListaDatV">$ <?php echo number_format($iteracionH,2); ?></td>
    </tr>
    <?php } } while ($rowLista = mysql_fetch_assoc($resultLista)); ?>
    <tr>
      <td colspan="3" align="right" valign="top" class="celdaListaDat"><strong>Movimientos:</strong></td>
      <td align="left" valign="top" class="celdaListaDatA"><strong>$ <?php echo number_format($totalDebe,2); ?></strong></td>
      <td align="left" valign="top" class="celdaListaDatV"><strong>$ <?php echo number_format($totalHaber,2); ?></strong></td>
      <td align="left" valign="top" class="celdaListaDatA"><strong>$ <?php echo number_format($SumaTotalSaldoD,2); ?></strong></td>
      <td align="left" valign="top" class="celdaListaDatV"><strong>$ <?php echo number_format($SumaTotalSaldoH,2); ?></strong></td>
    </tr>
    <tr>
      <td colspan="3" align="right" valign="top" class="celdaListaDat"><strong>Saldo:</strong></td>
      <td align="left" valign="top" class="celdaListaDatA"><strong>$ <?php mostrarSaldo($totalDebe, $totalHaber); ?></strong></td>
      <td align="left" valign="top" class="celdaListaDatV"><strong>$ <?php mostrarSaldo($totalHaber, $totalDebe); ?></strong></td>
      <td align="left" valign="top" class="celdaListaDatA"><strong>$ 
        <?php mostrarSaldo($SumaTotalSaldoD, $SumaTotalSaldoH); ?>
      </strong></td>
      <td align="left" valign="top" class="celdaListaDatV"><strong>$ 
        <?php mostrarSaldo($SumaTotalSaldoH, $SumaTotalSaldoD); ?>
      </strong></td>
    </tr>
    <?php } ?>
  </table>
</fieldset></div>
  <div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
</body>
</html>