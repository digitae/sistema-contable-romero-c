<?php
include 'libreria/principal.php';
include 'libreria/evaluaciones.php';

esUsuario(); // será usuario registrado?
$IDusuario = $_SESSION['userID'];
$capitulo = 12;
$registro = verificarCuestionario($capitulo, $IDusuario,  utf8_decode($Fecha));


?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> Principios de Contabilidad. Cuestionario de evaluación. Capítulo 12</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="libreria/js_principal.js"></script>
<script src="libreria/vendor/jquery/jquery-1.11.0.min.js"></script>
<script src="libreria/calCap.js"></script>
<script src="libreria/rc12.js"></script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Cuestionario de autoevaluación</h1>
    </div>
    <div id="divTitularMenu">
        <a href="portada.php">Inicio</a> | 
        <a href="libreria/calculadora/calculadora.html" 
           target="_blank" 
           onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | 
        <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido" style="width: 900px">
  <div class="divContCuerpo">
      
      <h3 class="titulo-principal">Capítulo 12</h3>
      <div class="migas">
          <ul>
              <li><a href="portada.php">Portada</a></li>
              <li><a href="javascript:history.back();">Página anterior</a></li>
              <li><img src="imagenes/print.png" 
                       alt="" 
                       height="25" 
                       style="float: left; margin-right: 5px;" />
                  <a href="javascript:print();">Imprimir página</a></li>
          </ul>
      </div>
      
      <table class="mis-datos">
          <tr>
              <td>Universidad o Institución:</td>
              <td>
                  <input oninput="cCamCues('unidad',this.value,<?php echo $registro ?>);" 
                         name="unidad" 
                         id='unidad' 
                         required 
                         value="<?php echo xCamCues('unidad', $registro) ?>" />
              </td>
              <td>Escuela o facultad:</td>
              <td>
                  <input oninput="cCamCues('facultad',this.value,<?php echo $registro ?>);" 
                         name="facultad" 
                         id='facultad' 
                         required 
                         value="<?php echo xCamCues('facultad', $registro) ?>" />
              </td>
          </tr>
          <tr>
              <td>Asignatura:</td>
              <td>
                  <input oninput="cCamCues('asignatura',this.value,<?php echo $registro ?>);" 
                         name="asignatura" 
                         id='asignatura' 
                         required 
                         value="<?php echo xCamCues('asignatura', $registro) ?>" />
              </td>
              <td>Alumno:</td>
              <td>
                  <?php echo nombreUsuario($IDusuario) ?>
              </td>
          </tr>
          <tr>
              <td>Maestro:</td>
              <td>
                  <input oninput="cCamCues('maestro',this.value,<?php echo $registro ?>);" 
                         name="maestro" 
                         id='maestro' 
                         required 
                         value="<?php echo xCamCues('maestro', $registro) ?>" />
              </td>
              <td>Fecha:</td>
              <td>
                  <input oninput="cCamCues('fecha',this.value,<?php echo $registro ?>);" 
                         name="fecha" 
                         id='fecha' 
                         required 
                         value="<?php echo xCamCues('fecha', $registro) ?>" />
              </td>
          </tr>
      </table>
      
      <h4 class="subtit-4">OBJETIVOS DE APRENDIZAJE</h4>
      
      <div class="objetivos">
          <p>Al finalizar este capítulo, el alumno será capaz de:</p>
          <ol>
              <li>Identificar y explicar las reglas del cargo y del abono.</li>
              <li>Identificar y explicar los aumentos y disminuciones que 
                  muestran las cuentas del activo, pasivo y capital.</li>
              <li>Identificar y explicar la naturaleza y saldo de cuentas de 
                  activo, pasivo y capital.</li>
              <li>Aplicar y demostrar las reglas del cargo y del abono.</li>
          </ol>
      </div>
      
      <h4 class="subtit-4">EJERCICIOS</h4>
      
      <h5>I. RESUELVA LO SIGUIENTE</h5>
      
      <div class="pregunta-libre">
          <p><span>1</span> ¿Cuál es el punto de partida para determinar las 
              reglas del cargo y del abono?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib01" 
                    id="prelib01"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib01'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>2</span> ¿Qué estableció el Boletín B-4 respecto de la 
              identificación de las fuentes de recursos y su aplicación?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib02" 
                    id="prelib02"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib02'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>3</span> ¿Con qué tipo de registro empiezan las cuentas de activo?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib03" 
                    id="prelib03"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib03'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>4</span> ¿Cómo se registran los aumentos y las disminuciones de activo?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib04" 
                    id="prelib04"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib04'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>5</span> ¿Qué tipo de saldo (naturaleza) tienen las cuentas de activo?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib05" 
                    id="prelib05"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib05'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>6</span> ¿Con qué tipo de registro empiezan las cuentas de pasivo?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib06" 
                    id="prelib06"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib06'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>7</span> ¿Cómo se registran los aumentos y disminuciones del pasivo?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib07" 
                    id="prelib07"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib07'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>8</span> ¿Qué tipo de saldo (naturaleza) tienen las cuentas de pasivo?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib08" 
                    id="prelib08"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib08'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>9</span> ¿Con qué tipo de registro empiezan las cuentas de capital?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib09" 
                    id="prelib09"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib09'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>10</span> ¿Cómo se registran los aumentos y las disminuciones del capital?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib10" 
                    id="prelib10"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib10'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>11</span> ¿Qué tipo de saldo (naturaleza) tienen las cuentas de capital?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib11" 
                    id="prelib11"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib11'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>12</span> ¿Cómo se designa a las cuentas de capital que 
              registran los ingresos, productos, ganancias, utilidades, costos, gastos y pérdidas?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib12" 
                    id="prelib12"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib12'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>13</span> ¿Cómo se registran los aumentos y las disminuciones 
              de las cuentas de capital o resultados de naturaleza deudora? ¿Cuál es su saldo?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib13" 
                    id="prelib13"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib13'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>14</span> ¿Cómo se registran los aumentos y las disminuciones 
              de las cuentas de capital o resultados de naturaleza acreedora? ¿Cuál es su saldo?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib14" 
                    id="prelib14"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib14'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>15</span> ¿Cuáles son las reglas del cargo y del abono?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib15" 
                    id="prelib15"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib15'); ?></textarea>
      </div>
      
      <h5>II. COMPLETE LAS LÍNEAS EN BLANCO</h5>
      
      <div class="completar-lineas">
          <span class="num">1</span>
          Las cuentas de activo empiezan <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_1" 
                    name="comLin_1" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_1'); ?>" />, es decir, con una 
          anotación en el <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_2" 
                    name="comLin_2" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_2'); ?>" />.
      </div>
      
      <div class="completar-lineas">
          <span class="num">2</span>
          Las cuentas de pasivo empiezan <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_3" 
                    name="comLin_3" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_3'); ?>" />, es decir, con 
          una anotación en el <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_4" 
                    name="comLin_4" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_4'); ?>" />.
      </div>
      
      <div class="completar-lineas">
          <span class="num">3</span>
          Las cuentas de <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_5" 
                    name="comLin_5" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_5'); ?>" /> empiezan 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_6" 
                    name="comLin_6" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_6'); ?>" />, es decir, con una anotación en el haber.
      </div>
      
      <div class="completar-lineas">
          <span class="num">4</span>
          Los aumentos de activo se <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_7" 
                    name="comLin_7" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_7'); ?>" />.
      </div>
      
      <div class="completar-lineas">
          <span class="num">5</span>
          Las disminuciones de pasivo se <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_8" 
                    name="comLin_8" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_8'); ?>" />.
      </div>
      
      <div class="completar-lineas">
          <span class="num">6</span>
          Los aumentos de capital se <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_9" 
                    name="comLin_9" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_9'); ?>" />.
      </div>
      
      <div class="completar-lineas">
          <span class="num">7</span>
          Todas las cuentas de activo tienen <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_10" 
                    name="comLin_10" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_10'); ?>" /> deudor.
      </div>
      
      <div class="completar-lineas">
          <span class="num">8</span>
          Todas las cuentas de pasivo y <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_11" 
                    name="comLin_11" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_11'); ?>" /> tienen saldo 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_12" 
                    name="comLin_12" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_12'); ?>" />.
      </div>
      
      <div class="completar-lineas">
          <button onclick="calificarCompletar('comLin',12,rc12_cl);">CALIFICAR</button>
          <span id="comLin_cal"></span>
      </div>
      
      <h5>III. RELACIONE LAS COLUMNAS 1 Y 2</h5>
      
      <table class='correlaciones'>
          <tr>
              <th>Columna 1</th>
              <th>Columna 2</th>
          </tr>
          <tr>
              <td>
                  <span class="num">1</span>
                  <p>Todos los aumentos de pasivo y capital.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_1' id='corr1_1'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>10</option>
                  </select>
                  <span id='corr1_1-parcial'></span>
              </td>
              <td rowspan="10">
                  <ol>
                      <li>Cuentas de resultados deudoras</li>
                      <li>Se tiene que abonar cuando...</li>
                      <li>Cuentas de resultados acreedoras</li>
                      <li>Se abonan</li>
                      <li>Se cargan</li>
                      <li>Saldo deudor</li>
                      <li>Saldo acreedor</li>
                      <li>Cuentas reales</li>
                      <li>Cuentas nominales</li>
                      <li>Se tiene que cargar cuando...</li>
                  </ol>
              </td>
          </tr>
          <tr>
              <td>
                  <span class="num">2</span>
                  <p>Aumenta el activo, disminuye el pasivo y disminuye el capital.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_2' id='corr1_2'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>10</option>
                  </select>
                  <span id='corr1_2-parcial'></span>
              </td>
          </tr>
          
          <tr>
              <td>
                  <span class="num">3</span>
                  <p>A las cuentas de balance se les denomina.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_3' id='corr1_3'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>10</option>
                  </select>
                  <span id='corr1_3-parcial'></span>
              </td>
          </tr>
          
          <tr>
              <td>
                  <span class="num">4</span>
                  <p>Disminuye el activo, aumenta el pasivo y aumenta el capital.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_4' id='corr1_4'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>10</option>
                  </select>
                  <span id='corr1_4-parcial'></span>
              </td>
          </tr>
          
          <tr>
              <td>
                  <span class="num">5</span>
                  <p>Todas las cuentas de pasivo y capital tienen.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_5' id='corr1_5'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>10</option>
                  </select>
                  <span id='corr1_5-parcial'></span>
              </td>
          </tr>
          
          <tr>
              <td>
                  <span class="num">6</span>
                  <p>A las cuentas de resultados se les denomina.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_6' id='corr1_6'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>10</option>
                  </select>
                  <span id='corr1_6-parcial'></span>
              </td>
          </tr>
          
          <tr>
              <td>
                  <span class="num">7</span>
                  <p>Son las que registran ingresos, productos, ganancias, 
                      utilidades; por lo tanto, aumentan abonando, disminuyen 
                      cargando y tienen saldo acreedor.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_7' id='corr1_7'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>10</option>
                  </select>
                  <span id='corr1_7-parcial'></span>
              </td>
          </tr>
          
          <tr>
              <td>
                  <span class="num">8</span>
                  <p>Todos los aumentos de activo.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_8' id='corr1_8'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>10</option>
                  </select>
                  <span id='corr1_8-parcial'></span>
              </td>
          </tr>
          
          <tr>
              <td>
                  <span class="num">9</span>
                  <p>Todas las cuentas de activo tienen.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_9' id='corr1_9'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>10</option>
                  </select>
                  <span id='corr1_9-parcial'></span>
              </td>
          </tr>
          
          <tr>
              <td>
                  <span class="num">10</span>
                  <p>Son las que registran costos, gastos y pérdidas; por lo 
                      tanto, aumentan cargando, disminuyen abonando y tienen 
                      saldo deudor.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_10' id='corr1_10'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>10</option>
                  </select>
                  <span id='corr1_10-parcial'></span>
              </td>
          </tr>
                    
          <tr>
              <td id='corr1_cal'></td>
              <td><button onclick="calificarCorrelacion('corr1',10,corr12);" 
                          name='cal_corr1'>CALIFICAR</button></td>
          </tr>
      </table>
      
      <h5>IV. CONTESTE VERDADERO O FALSO</h5>
      
      <table class="verdadero-falso" id="vf_simple01">
          <tr>
              <th>Sentencia</th>
              <th>Verdadero</th>
              <th>Falso</th>
          </tr>
          
          <!-- tanda -->
          <tr>
              <td>1. Las cuentas de activo empiezan abonando.</td>
              <td id="vf_simple01_verfal_1v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_1" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_1')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_1f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_1" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_1')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>2. Las cuentas de pasivo empiezan abonando.</td>
              <td id="vf_simple01_verfal_2v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_2" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_2')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_2f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_2" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_2')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>3. Las cuentas de capital empiezan cargando.</td>
              <td id="vf_simple01_verfal_3v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_3" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_3')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_3f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_3" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_3')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>4. Las cuentas de activo aumentan abonando.</td>
              <td id="vf_simple01_verfal_4v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_4" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_4')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_4f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_4" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_4')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>5. Las cuentas de pasivo y de capital aumentan cargando.</td>
              <td id="vf_simple01_verfal_5v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_5" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_5')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_5f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_5" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_5')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>6. Las cuentas de activo disminuyen cargando.</td>
              <td id="vf_simple01_verfal_6v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_6" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_6')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_6f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_6" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_6')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>7. Las cuentas de pasivo y de capital disminuyen cargando.</td>
              <td id="vf_simple01_verfal_7v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_7" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_7')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_7f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_7" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_7')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>8. Las cuentas de activo tienen saldo deudor.</td>
              <td id="vf_simple01_verfal_8v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_8" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_8')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_8f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_8" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_8')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>9. Las cuentas de pasivo tiene saldo acreedor.</td>
              <td id="vf_simple01_verfal_9v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_9" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_9')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_9f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_9" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_9')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>10. Se tiene que cargar cuando disminuye el activo, aumenta 
                  el pasivo y aumenta el capital.</td>
              <td id="vf_simple01_verfal_10v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_10" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_10')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_10f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_10" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_10')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <tr>
              <td colspan="2" id="vf_simple01_verfal_cal"></td>
              <td>
                  <button onclick="calificarVerFalSimple('verfal',10,rc12_simpleA,'vf_simple01');">CALIFICAR</button>
              </td>
          </tr>
      </table>      
      
  </div>
  <!-- -->
  <div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
<script>
    var tc;
    var cCamCues = function(campo,valor,registro)
    {
      clearTimeout(tc);
      tc = window.setTimeout(function(){
          $.post('libreria/cCamCues.php',{
              modDatos: true,
              elcampo: campo,
              elvalor: valor,
              elregistro: registro
          },function(respuesta){
              if(respuesta === 'modificado')
              {
                  // Modificado
              } else {
                  // Error
                  console.log('-> '+respuesta);
              }
          });
      },1000);
    };
    var cResCues = function(cues,usu,cap,preg,resp)
    {
      clearTimeout(tc);
      tc = window.setTimeout(function(){
          $.post('libreria/cCamCues.php',{
              modRespuestas: true,
              cuestionario: cues,
              usuario: usu,
              capitulo: cap,
              pregunta: preg,
              respuesta:resp
          },function(respuesta){
              if(respuesta === 'modificado')
              {
                  // Modificado
              } else {
                  // Error
                  console.log('-> '+respuesta);
              }
          });
      },500);
    };
    
    window.onload = function(){
        protegerPLibres();
        protegerCompletar();
        protegerCorrelaciones();
        protegerVerFalSimple('vf_simple01');
    };
</script>
</body>
</html>