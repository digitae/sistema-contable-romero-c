<?php
include("libreria/principal.php");

if(isset($_SESSION['registrado']))
{
    header("Location: portada.php");
    exit;
}

$submit = filter_input(INPUT_POST, 'submit');
if(isset($submit)) {

	$usuario = utf8_decode(filter_input(INPUT_POST, 'usuario',
                FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW));
	$pwd = utf8_decode(filter_input(INPUT_POST, 'pwd',
                FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW));
	
	// buscar coincidencias
	$query = "SELECT IDusuario, nombre "
                . "FROM rom_usuarios "
                . "WHERE usuario = '$usuario' AND pwd = '$pwd'";
	$result = mysql_query($query) or die (mysql_error());
	$datos = mysql_fetch_array($result);
	$usuarios = mysql_num_rows($result);
	// si hay un registro...
	if($usuarios == 1){
		
		$userID = $datos['IDusuario'];
		$nombre = $datos['nombre'];
		$_SESSION['registrado'] = true;
		$_SESSION['userID'] = true;
		$_SESSION['userID'] = $userID;
		$_SESSION['nombre'] = true;
		$_SESSION['nombre'] = $nombre;
	
		header("Location: portada.php");
		exit;
		
	} else { // si no, entonces no está registrado.
		header("Location: sesion_registro.php?msg=error");
		exit;
	}
	
}

$mostrarError = FALSE;
$msg = filter_input(INPUT_GET, 'msg',
        FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
if(isset($msg) && $msg == 'error')
{
    $msg = TRUE;
}

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sistema Contable Romero</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script src="libreria/js_principal.js"></script>
<script>

function Registrado(theForm)

{
  if (theForm.usuario.value === "")

  {
    alert("El campo USUARIO está vacío.");
    theForm.usuario.focus();
    return (false);
  }
  if (theForm.pwd.value === "")

  {
    alert("El campo CONTRASEÑA está vacío.");
    theForm.pwd.focus();
    return (false);
  }

  return (true);

}

</script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
    <div id="cortinilla">
        <form class="form-2014 recPwd">
            <fieldset>
                <legend>Recuperación de contraseña</legend>
                <div>
                    <input type="text" id="recUsuario" 
                           placeholder="Ingresa tu nombre de usuario" />
                </div>
                <div id="msgUserRec">
                    El usuario no es válido. Intente de nuevo.
                </div>
                <div>
                    <input type="text" id="recCon" 
                           placeholder="¡La contraseña aparecerá aquí!" 
                           readonly="true" />
                </div>
                <div>
                    <button type="button" id="recuperar" 
                            onclick="recuperarPwd();">Recuperar contraseña</button>
                    <button type="button" 
                            onclick="cerrarCortinilla();"
                            id="cancelar">Cerrar</button>
                </div>
            </fieldset>
        </form>
    </div>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Registro de usuario</h1>
    </div>
    <div id="divTitularMenu">
        <a href="portada.php">Inicio</a> | 
        <a href="libreria/calculadora/calculadora.html" 
           target="_blank" 
           onclick="NewWindow(this.href,
                       'name',
                       '233',
                       '259',
                       'no');
               return false;">Calculadora</a> | 
        <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido">
  <div class="divContCuerpo">
      <?php
      if($msg){
          echo "<div id='msgM' class='msgError'><strong>Error al iniciar sesión.</strong>"
          . "<br />Vuelva a intentar.</div>";
          $script = <<<SCRIPT
                  <script>
                  var bloque = document.getElementById('msgM');
                  window.setTimeout(function(){
                        bloque.style.display = 'none';
                      },3000);
                  </script>
SCRIPT;
          echo $script;
      }
      ?>
      <div style="float: left; margin: 25px 0;">
          <img class="img-portada" src="imagenes/porPrincipiosConta.jpg" 
               alt="" height="375" />
      </div>
      <div style="float: left; margin-left: 100px; margin-top: 100px">
          <form class="form-2014" 
                action="sesion_registro.php" 
                method="POST" 
                autocomplete="off" 
                onsubmit="return Registrado(this);">
              <fieldset>
                  <legend>Iniciar sesión</legend>
                  <div>
                      <input type="text" 
                             name="usuario" 
                             placeholder="Nombre de usuario" 
                             required />
                  </div>
                  <div>
                      <input type="password" 
                             name="pwd" 
                             placeholder="Contraseña" 
                             required />
                  </div>
                  <div>
                      <a href="javascript:abrirRecPwd();" 
                         id="recPwd">Olvidé mi contraseña</a>
                  </div>
                  <div>
                      <button type="submit" 
                              name="submit">Iniciar sesión</button>
                  </div>
              </fieldset>
          </form>
          <div style="margin-top: 50px">
              <p>En caso de NO contar con un <br />
                  nombre de usuario y una <br />
              contraseña, entonces...</p>
              <a class="registrate" 
                 href="registro.php">Regístrate gratuitamente aquí.</a>
          </div>
      </div>
  </div>
  <!-- -->
  <div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
<script src="libreria/vendor/jquery/jquery-1.11.0.min.js"></script>
<script>
    var boton = document.getElementById('recPwd');
    var contenedor = document.getElementById('cortinilla');
    boton.addEventListener('click',abrirRecPwd);
    function abrirRecPwd() {
        
        // Abrir contenedor
        document.getElementById('recUsuario').value = '';
        document.getElementById('recCon').value = '';
        $('#cortinilla').fadeIn('fast');
    }
    function cerrarCortinilla() {
        document.getElementById('recUsuario').value = '';
        document.getElementById('recCon').value = '';
        $('#cortinilla').fadeOut('fast');
    }
    
    function recuperarPwd()
    {
        var usuario = document.getElementById('recUsuario').value;
        if(usuario === '')
        {
            alert('El campo usuario no puede estar vacío');
            exit;
        }
        document.getElementById('recCon').value = '';
        document.getElementById('msgUserRec').style.display = 'none';
        $.get('libreria/ajaxRecPwd.php?usuario='+usuario,
        function(response){
            if(response === 'desconocido')
            {
                $('#msgUserRec').fadeIn('fast');
                document.getElementById('recCon').value = '';
            } else {
                $('#msgUserRec').fadeOut('fast');
                document.getElementById('recCon').value = response;
            }
        });
    }
</script>
</body>
</html>