<?php
include 'libreria/principal.php';
include 'libreria/evaluaciones.php';

esUsuario(); // será usuario registrado?
$IDusuario = $_SESSION['userID'];
$capitulo = 16;
$registro = verificarCuestionario($capitulo, $IDusuario,  utf8_decode($Fecha));


?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> Principios de Contabilidad. Cuestionario de evaluación. Capítulo 16</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="libreria/js_principal.js"></script>
<script src="libreria/vendor/jquery/jquery-1.11.0.min.js"></script>
<script src="libreria/calCap.js"></script>
<script src="libreria/rc16.js"></script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Cuestionario de autoevaluación</h1>
    </div>
    <div id="divTitularMenu">
        <a href="portada.php">Inicio</a> | 
        <a href="libreria/calculadora/calculadora.html" 
           target="_blank" 
           onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | 
        <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido" style="width: 900px">
  <div class="divContCuerpo">
      
      <h3 class="titulo-principal">Capítulo 16</h3>
      <div class="migas">
          <ul>
              <li><a href="portada.php">Portada</a></li>
              <li><a href="javascript:history.back();">Página anterior</a></li>
              <li><img src="imagenes/print.png" 
                       alt="" 
                       height="25" 
                       style="float: left; margin-right: 5px;" />
                  <a href="javascript:print();">Imprimir página</a></li>
          </ul>
      </div>
      
      <table class="mis-datos">
          <tr>
              <td>Universidad o Institución:</td>
              <td>
                  <input oninput="cCamCues('unidad',this.value,<?php echo $registro ?>);" 
                         name="unidad" 
                         id='unidad' 
                         required 
                         value="<?php echo xCamCues('unidad', $registro) ?>" />
              </td>
              <td>Escuela o facultad:</td>
              <td>
                  <input oninput="cCamCues('facultad',this.value,<?php echo $registro ?>);" 
                         name="facultad" 
                         id='facultad' 
                         required 
                         value="<?php echo xCamCues('facultad', $registro) ?>" />
              </td>
          </tr>
          <tr>
              <td>Asignatura:</td>
              <td>
                  <input oninput="cCamCues('asignatura',this.value,<?php echo $registro ?>);" 
                         name="asignatura" 
                         id='asignatura' 
                         required 
                         value="<?php echo xCamCues('asignatura', $registro) ?>" />
              </td>
              <td>Alumno:</td>
              <td>
                  <?php echo nombreUsuario($IDusuario) ?>
              </td>
          </tr>
          <tr>
              <td>Maestro:</td>
              <td>
                  <input oninput="cCamCues('maestro',this.value,<?php echo $registro ?>);" 
                         name="maestro" 
                         id='maestro' 
                         required 
                         value="<?php echo xCamCues('maestro', $registro) ?>" />
              </td>
              <td>Fecha:</td>
              <td>
                  <input oninput="cCamCues('fecha',this.value,<?php echo $registro ?>);" 
                         name="fecha" 
                         id='fecha' 
                         required 
                         value="<?php echo xCamCues('fecha', $registro) ?>" />
              </td>
          </tr>
      </table>
      
      <h4 class="subtit-4">OBJETIVOS DE APRENDIZAJE</h4>
      
      <div class="objetivos">
          <p>Al finalizar este capítulo, el alumno será capaz de:</p>
          <ol>
              <li>Explicar y aplicar los distintos procedimientos para el 
                  registro de las operaciones de mercancías según las 
                  características particulares de cada empresa.</li>
              <li>Conocer y aplicar las particularidades del procedimiento 
                  global o de mercancías generales.</li>
              <li>Entender y aplicar el procedimiento analítico o pormenorizado 
                  y su aplicación.</li>
              <li>Entender y aplicar el procedimiento de inventarios perpetuos 
                  y su aplicación.</li>
          </ol>
      </div>
      
      <h4 class="subtit-4">EJERCICIOS</h4>
      
      <h5>I. RESUELVA LO SIGUIENTE</h5>
      
      <div class="pregunta-libre">
          <p><span>1</span> ¿Por qué es importante el registro y control de las 
              mercancías en las empresas?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib01" 
                    id="prelib01"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib01'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>2</span> Señale las principales operaciones que se realizan 
              con las mercancías.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib02" 
                    id="prelib02"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib02'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>3</span> Explique qué es el inventario inicial y qué enfoque 
              se emplea para su estudio.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib03" 
                    id="prelib03"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib03'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>4</span> ¿Qué son las compras y cómo se pueden estudiar?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib04" 
                    id="prelib04"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib04'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>5</span> ¿Qué tipo de asiento origina una compra al contado 
              y una a crédito, respectivamente?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib05" 
                    id="prelib05"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib05'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>6</span> ¿Qué son los gastos sobre compras?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib06" 
                    id="prelib06"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib06'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>7</span> ¿Qué asientos originan los gastos sobre compras al 
              contado y a crédito, respectivamente?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib07" 
                    id="prelib07"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib07'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>8</span> ¿Qué son las devoluciones sobre compras y bajo 
              qué enfoque se estudian?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib08" 
                    id="prelib08"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib08'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>9</span> ¿Qué tipo de asiento origina una devolución al 
              contado y una a crédito?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib09" 
                    id="prelib09"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib09'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>10</span> ¿Qué son las rebajas sobre compras y bajo qué 
              enfoque se estudian?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib10" 
                    id="prelib10"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib10'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>11</span> ¿Qué son los descuentos sobre compras?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib11" 
                    id="prelib11"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib11'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>12</span> Explique en qué consiste el método del precio neto 
              en el registro de los descuentos sobre compra, así como sus 
              ventajas y desventajas.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib12" 
                    id="prelib12"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib12'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>13</span> Explique en qué consiste el método del precio 
              bruto en el registro de los descuentos sobre compra, así como sus 
              ventajas y desventajas.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib13" 
                    id="prelib13"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib13'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>14</span> ¿Qué es el inventario final?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib14" 
                    id="prelib14"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib14'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>15</span> ¿Qué son las ventas y con qué enfoque se estudian?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib15" 
                    id="prelib15"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib15'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>16</span> ¿Qué tipo de asiento origina una venta al contado 
              y una a crédito?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib16" 
                    id="prelib16"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib16'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>17</span> ¿Qué son las devoluciones sobre ventas y con qué 
              enfoque se estudian?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib17" 
                    id="prelib17"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib17'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>18</span> ¿Qué tipo de asiento origina una devolución al 
              contado y una a crédito, respectivamente?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib18" 
                    id="prelib18"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib18'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>19</span> ¿Qué son las rebajas y bonificaciones sobre 
              venta y bajo qué enfoque se estudian?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib19" 
                    id="prelib19"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib19'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>20</span> ¿Qué tipo de asiento origina una rebaja al 
              contado y una a crédito?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib20" 
                    id="prelib20"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib20'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>21</span> ¿Qué son los descuentos sobre venta?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib21" 
                    id="prelib21"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib21'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>22</span> ¿Cuántos y cuáles procedimientos conoce para el 
              registro de las operaciones de mercancías?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib22" 
                    id="prelib22"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib22'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>23</span> En el procedimiento analítico o pormenorizado, 
              para determinar el costo de ventas es necesario conocer tres 
              elementos, señálelos.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib23" 
                    id="prelib23"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib23'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>24</span> ¿Qué cuentas emplea?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib24" 
                    id="prelib24"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib24'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>25</span> Señale sus ventajas y desventajas.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib25" 
                    id="prelib25"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib25'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>26</span> ¿En qué consiste el procedimiento de inventarios perpetuos?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib26" 
                    id="prelib26"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib26'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>27</span> ¿Qué cuentas emplea?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib27" 
                    id="prelib27"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib27'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>28</span> Señale sus ventajas y desventajas.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib28" 
                    id="prelib28"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib28'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>29</span> ¿Qué establece la NIF C-4 respecto de las fórmulas 
              de asignación del costo?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib29" 
                    id="prelib29"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib29'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>30</span> De conformidad con la NIF C-4, ¿en qué consiste 
              la fórmula del costo promedio?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib30" 
                    id="prelib30"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib30'); ?></textarea>
      </div>
      
  </div>
  <!-- -->
  <div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
<script>
    var tc;
    var cCamCues = function(campo,valor,registro)
    {
      clearTimeout(tc);
      tc = window.setTimeout(function(){
          $.post('libreria/cCamCues.php',{
              modDatos: true,
              elcampo: campo,
              elvalor: valor,
              elregistro: registro
          },function(respuesta){
              if(respuesta === 'modificado')
              {
                  // Modificado
              } else {
                  // Error
                  console.log('-> '+respuesta);
              }
          });
      },1000);
    };
    var cResCues = function(cues,usu,cap,preg,resp)
    {
      clearTimeout(tc);
      tc = window.setTimeout(function(){
          $.post('libreria/cCamCues.php',{
              modRespuestas: true,
              cuestionario: cues,
              usuario: usu,
              capitulo: cap,
              pregunta: preg,
              respuesta:resp
          },function(respuesta){
              if(respuesta === 'modificado')
              {
                  // Modificado
              } else {
                  // Error
                  console.log('-> '+respuesta);
              }
          });
      },500);
    };
    
    window.onload = function(){
        protegerPLibres();
    };
</script>
</body>
</html>