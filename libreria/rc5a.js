var rc5a_cl = ['obligación','ineludible','cuantificada','disminución','pasado',
    'recursos','aportados','externas','presente','ineludible','bienes','futuro','identificables',
'corto','vencimiento','un','ciclo','mayor',
'financieros','largo','trabajo','normal','no','doce',
'capital','lucrativas','contable','no',
'residual',
'financiero','capital','patrimonio','activo','propietarios','pasivos','externas','capital','interna',
'capital','recursos','aportados','internas','residual','netos','reembolso',
'contribuido',
'mantenido','equivalente',
'permanentemente','eliminadas',
'temporalmente','limitado','expiran',
'no','carece','tipo','patrocinadores','use'];

var rc5a_verfalR = ['falso','verdadero','verdadero','falso','falso','verdadero','falso'];

var rc5a_opcmulA = ['IVA pendiente de acreditar','Rentas pagadas por anticipado',
    'Utilidades acumuladas','Pérdida neta del ejercicio','Primas de seguros y fianzas',
    'Cuentas por cobrar a largo plazo'];

var rc5a_opcmulB = ['Patentes','Utilidad neta del ejercicio',
'Mobiliario y equipo de oficina','Proveedores','Reserva legal','Pérdidas acumuladas',
'Patrimonio restringido temporalmente'];

var rc5a_opcmulC = ['Capital social','Aportaciones para futuros aumentos de capital',
    'ISR por pagar','Prima en venta de acciones','Patrimonio no restringido'];

var rc5a_corr1 = [3,2,3,1,1];

var rc5a_simpleA = ['verdadero','falso','verdadero','verdadero','falso','verdadero',
    'falso','falso','falso','verdadero','verdadero','verdadero','falso','falso','verdadero'];


var rc5a_vi1 = [
    'caja',
    'bancos',
    'instrumentos financieros',
    'clientes',
    'iva acreditable',
    'mercancías',
    'papelería a consumirse en el presente ejercicio',
    'primas de seguros a devengarse en el ejercicio',
    'terrenos',
    'equipo de reparto',
    'documentos por cobrar a 24 meses',
    'primas de seguros a devengarse en 24 meses',
    'proveedores',
    'isr por pagar',
    'ptu por pagar',
    'rentas cobradas por anticipado a devengarse en el ejercicio',
    'acreedores a 18 meses',
    'utilidad neta del ejercicio',
    'utilidades acumuladas',
    'reserva legal'
];

var rc5a_vi2 = [
    'fondo fijo de caja chica',
    'deudores',
    'inventarios',
    'mobiliario y equipo',
    'derechos de autor',
    'patentes y marcas',
    'proveedores',
    'acreedores bancarios con vencimiento en el ejercicio',
    'impuestos y derechos por pagar',
    'impuestos y derechos retenidos por enterar',
    'iva causado',
    'acreedores hipotecarios con vencimiento a cinco años',
    'acreedores bancarios con vencimiento a tres años',
    'capital social',
    'aportaciones para futuros aumentos de capital',
    'pérdida neta del ejercicio'
];

var rc5a_vi3 = [
    'caja',
    'fondo fijo de caja chica',
    'bancos',
    'inversiones temporales a nueve meses',
    'clientes',
    'documentos por cobrar a seis meses',
    'deudores',
    'iva acreditable',
    'mercancías',
    'papelería a devengarse en once meses',
    'rentas pagadas por anticipado a devengarse en el ejercicio',
    'terrenos',
    'edificios',
    'mobiliario y equipo de oficina',
    'equipo de transporte',
    'derechos de autor',
    'crédito mercantil',
    'gastos de instalación',
    'gastos de constitución',
    'documentos por cobrar a 20 meses',
    'proveedores',
    'acreedores',
    'documentos por pagar',
    'acreedores bancarios a un año',
    'iva causado',
    'isr por pagar',
    'ptu por pagar',
    'acreedores hipotecarios a cinco años',
    'rentas cobradas por anticipado a devengarse en 16 meses',
    'acreedores bancarios a tres años',
    'capital social',
    'utilidad neta del ejercicio',
    'reserva legal'
];