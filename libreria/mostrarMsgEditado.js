    var padre = document.getElementById('form-razones');
    var msg = "¡Editado!";
    var hijo = document.createElement("span");
    hijo.setAttribute("id", "msg-editado");
    hijo.innerHTML = msg;
    padre.appendChild(hijo);
    window.setTimeout(function(){
        hijo.style.opacity = 1;
        window.setTimeout(function(){
            hijo.style.opacity = 0;
            window.setTimeout(function(){
                hijo.parentNode.removeChild(hijo);
            },500);
        },2000);
    },500);