<?php

## Cálculos para Razones Financieras

$l48 = extraRazones($tRazones, 'ventas', $IDpractica);
$l49 = extraRazones($tRazones, 'costo', $IDpractica);
$l50 = extraRazones($tRazones, 'compras', $IDpractica);

$d28 = extraerValorCampo('c7', $IDpractica, $tCantidades) + 
        extraerValorCampo('c8', $IDpractica, $tCantidades) + 
        extraerValorCampo('c9', $IDpractica, $tCantidades) + 
        extraerValorCampo('c10', $IDpractica, $tCantidades) + 
        extraerValorCampo('c11', $IDpractica, $tCantidades) + 
        extraerValorCampo('c12', $IDpractica, $tCantidades) + 
        extraerValorCampo('c13', $IDpractica, $tCantidades) + 
        extraerValorCampo('c14', $IDpractica, $tCantidades) + 
        extraerValorCampo('c15', $IDpractica, $tCantidades) + 
        extraerValorCampo('c16', $IDpractica, $tCantidades) + 
        extraerValorCampo('c17', $IDpractica, $tCantidades) + 
        extraerValorCampo('c18', $IDpractica, $tCantidades) + 
        extraerValorCampo('c19', $IDpractica, $tCantidades) + 
        extraerValorCampo('c20', $IDpractica, $tCantidades) + 
        extraerValorCampo('c21', $IDpractica, $tCantidades) + 
        extraerValorCampo('c22', $IDpractica, $tCantidades) + 
        extraerValorCampo('c23', $IDpractica, $tCantidades) + 
        extraerValorCampo('c24', $IDpractica, $tCantidades) + 
        extraerValorCampo('c25', $IDpractica, $tCantidades) + 
        extraerValorCampo('c26', $IDpractica, $tCantidades) + 
        extraerValorCampo('c27', $IDpractica, $tCantidades) + 
        extraerValorCampo('c28', $IDpractica, $tCantidades);
$d72 = extraerValorCampo('c57', $IDpractica, $tCantidades) + 
        extraerValorCampo('c58', $IDpractica, $tCantidades) + 
        extraerValorCampo('c59', $IDpractica, $tCantidades) + 
        extraerValorCampo('c60', $IDpractica, $tCantidades) + 
        extraerValorCampo('c61', $IDpractica, $tCantidades) + 
        extraerValorCampo('c62', $IDpractica, $tCantidades) + 
        extraerValorCampo('c63', $IDpractica, $tCantidades) + 
        extraerValorCampo('c64', $IDpractica, $tCantidades) + 
        extraerValorCampo('c65', $IDpractica, $tCantidades) + 
        extraerValorCampo('c66', $IDpractica, $tCantidades) + 
        extraerValorCampo('c67', $IDpractica, $tCantidades) + 
        extraerValorCampo('c68', $IDpractica, $tCantidades) + 
        extraerValorCampo('c69', $IDpractica, $tCantidades) + 
        extraerValorCampo('c70', $IDpractica, $tCantidades) + 
        extraerValorCampo('c71', $IDpractica, $tCantidades) + 
        extraerValorCampo('c72', $IDpractica, $tCantidades);
@$m4 = $d28 / $d72;

$o7 = extraerValorCampo('c19', $IDpractica, $tCantidades)+
        extraerValorCampo('c20', $IDpractica, $tCantidades)+
        extraerValorCampo('c21', $IDpractica, $tCantidades)+
        extraerValorCampo('c22', $IDpractica, $tCantidades)+
        extraerValorCampo('c23', $IDpractica, $tCantidades)+
        extraerValorCampo('c24', $IDpractica, $tCantidades)+
        extraerValorCampo('c25', $IDpractica, $tCantidades)+
        extraerValorCampo('c26', $IDpractica, $tCantidades)+
        extraerValorCampo('c27', $IDpractica, $tCantidades)+
        extraerValorCampo('c28', $IDpractica, $tCantidades);
$n7 = $d28;
@$p7 = $n7-$o7;
@$m7 = $p7/$d72;

$n10 = extraerValorCampo('c7', $IDpractica, $tCantidades)+
        extraerValorCampo('c8', $IDpractica, $tCantidades)+
        extraerValorCampo('c9', $IDpractica, $tCantidades)+
        extraerValorCampo('c10', $IDpractica, $tCantidades);
@$m10 = $n10/$d72;

$e54 = extraerValorCampo('c7', $IDpractica, $tCantidades)+
extraerValorCampo('c8', $IDpractica, $tCantidades)+
extraerValorCampo('c9', $IDpractica, $tCantidades)+
extraerValorCampo('c10', $IDpractica, $tCantidades)+
extraerValorCampo('c11', $IDpractica, $tCantidades)+
extraerValorCampo('c12', $IDpractica, $tCantidades)+
extraerValorCampo('c13', $IDpractica, $tCantidades)+
extraerValorCampo('c14', $IDpractica, $tCantidades)+
extraerValorCampo('c15', $IDpractica, $tCantidades)+
extraerValorCampo('c16', $IDpractica, $tCantidades)+
extraerValorCampo('c17', $IDpractica, $tCantidades)+
extraerValorCampo('c18', $IDpractica, $tCantidades)+
extraerValorCampo('c19', $IDpractica, $tCantidades)+
extraerValorCampo('c20', $IDpractica, $tCantidades)+
extraerValorCampo('c21', $IDpractica, $tCantidades)+
extraerValorCampo('c22', $IDpractica, $tCantidades)+
extraerValorCampo('c23', $IDpractica, $tCantidades)+
extraerValorCampo('c24', $IDpractica, $tCantidades)+
extraerValorCampo('c25', $IDpractica, $tCantidades)+
extraerValorCampo('c26', $IDpractica, $tCantidades)+
extraerValorCampo('c27', $IDpractica, $tCantidades)+
extraerValorCampo('c28', $IDpractica, $tCantidades)+
extraerValorCampo('c31', $IDpractica, $tCantidades)+
extraerValorCampo('c32', $IDpractica, $tCantidades)+
extraerValorCampo('c33', $IDpractica, $tCantidades)+
extraerValorCampo('c34', $IDpractica, $tCantidades)+
extraerValorCampo('c35', $IDpractica, $tCantidades)+
extraerValorCampo('c36', $IDpractica, $tCantidades)+
extraerValorCampo('c37', $IDpractica, $tCantidades)+
extraerValorCampo('c38', $IDpractica, $tCantidades)+
extraerValorCampo('c40', $IDpractica, $tCantidades)+
extraerValorCampo('c41', $IDpractica, $tCantidades)+
extraerValorCampo('c42', $IDpractica, $tCantidades)+
extraerValorCampo('c43', $IDpractica, $tCantidades)+
extraerValorCampo('c44', $IDpractica, $tCantidades)+
extraerValorCampo('c45', $IDpractica, $tCantidades)+
extraerValorCampo('c46', $IDpractica, $tCantidades)+
extraerValorCampo('c47', $IDpractica, $tCantidades)+
extraerValorCampo('c49', $IDpractica, $tCantidades)+
extraerValorCampo('c50', $IDpractica, $tCantidades)+
extraerValorCampo('c51', $IDpractica, $tCantidades)+
extraerValorCampo('c52', $IDpractica, $tCantidades)+
extraerValorCampo('c53', $IDpractica, $tCantidades)+
extraerValorCampo('c54', $IDpractica, $tCantidades);
$e79 = extraerValorCampo('c57', $IDpractica, $tCantidades)+
extraerValorCampo('c58', $IDpractica, $tCantidades)+
extraerValorCampo('c59', $IDpractica, $tCantidades)+
extraerValorCampo('c60', $IDpractica, $tCantidades)+
extraerValorCampo('c61', $IDpractica, $tCantidades)+
extraerValorCampo('c62', $IDpractica, $tCantidades)+
extraerValorCampo('c63', $IDpractica, $tCantidades)+
extraerValorCampo('c64', $IDpractica, $tCantidades)+
extraerValorCampo('c65', $IDpractica, $tCantidades)+
extraerValorCampo('c66', $IDpractica, $tCantidades)+
extraerValorCampo('c67', $IDpractica, $tCantidades)+
extraerValorCampo('c68', $IDpractica, $tCantidades)+
extraerValorCampo('c69', $IDpractica, $tCantidades)+
extraerValorCampo('c70', $IDpractica, $tCantidades)+
extraerValorCampo('c71', $IDpractica, $tCantidades)+
extraerValorCampo('c72', $IDpractica, $tCantidades)+
extraerValorCampo('c74', $IDpractica, $tCantidades)+
extraerValorCampo('c75', $IDpractica, $tCantidades)+
extraerValorCampo('c76', $IDpractica, $tCantidades)+
extraerValorCampo('c77', $IDpractica, $tCantidades)+
extraerValorCampo('c78', $IDpractica, $tCantidades)+
extraerValorCampo('c79', $IDpractica, $tCantidades);
$m13 = $e79 / $e54;

$e92 = extraerValorCampo('c82', $IDpractica, $tCantidades)+
extraerValorCampo('c83', $IDpractica, $tCantidades)+
extraerValorCampo('c84', $IDpractica, $tCantidades)+
extraerValorCampo('c86', $IDpractica, $tCantidades)+
extraerValorCampo('c87', $IDpractica, $tCantidades)+
extraerValorCampo('c88', $IDpractica, $tCantidades)+
extraerValorCampo('c89', $IDpractica, $tCantidades)+
extraerValorCampo('c90', $IDpractica, $tCantidades)+
extraerValorCampo('c91', $IDpractica, $tCantidades)+
extraerValorCampo('c92', $IDpractica, $tCantidades);
@$m16 = $e92/$e54;

@$m19 = $e92/$d72;

$o22 = extraerValorCampo('f11', $IDpractica, $tCantidades);
$n22 = extraerValorCampo('c11', $IDpractica, $tCantidades);
$p22 = $n22 + $o22;
$q22 = $p22 / 2;
@$m22 = $l48 / $q22;

@$m25 = 360 / $m22;

$o28 = extraerValorCampo('f19', $IDpractica, $tCantidades)+
        extraerValorCampo('f20', $IDpractica, $tCantidades)+
        extraerValorCampo('f21', $IDpractica, $tCantidades);
$n28 = extraerValorCampo('c19', $IDpractica, $tCantidades)+
        extraerValorCampo('c20', $IDpractica, $tCantidades)+
        extraerValorCampo('c21', $IDpractica, $tCantidades);
$p28 = $n28 + $o28;
$q28 = $p28 / 2;
@$m28 = $l49 / $q28;

@$m31 = 360 / $m28;

$o33 = extraerValorCampo('f57', $IDpractica, $tCantidades);
$n33 = extraerValorCampo('c57', $IDpractica, $tCantidades);
$p33 = $n33 + $o33;
$q33 = $p33 / 2;
@$m33 = $l50 / $q33;

@$m36 = 360 / $m33;

$m39 = extraerValorCampo('c86', $IDpractica, $tCantidades) / $l48;

@$m42 = extraerValorCampo('c86', $IDpractica, $tCantidades) / $e54;