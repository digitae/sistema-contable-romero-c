<?php

/**
 * MOSTRAR FORMULARIO DE DATOS EN RAZONES FINANCIERAS
 *
 * Clase para determinar si ya hay un registro 
 * de razones financieras y mostrar el respectivo
 * formulario para agregar o modificar datos.
 * @author Fernando Magrosoto V.
 * @category Sistema Romero C
 * @copyright (c) 2014, fernando Magrosoto
 */
class class_razones {
    
    ## PROPIEDADES
    private $existe = FALSE;
    private $tabla;
    private $practica;
    private $pagina;
    
    ## MÉTODOS
    
    /**
     * CONSTRUCTOR
     * 
     * @param int $practica El ID de la practica
     * @param string $tabla La tabla de razones
     * @param string $pagina La página de razones
     */
    function __construct($practica,$tabla,$pagina) {
        $this->practica = $practica;
        $this->tabla = $tabla;
        $this->pagina = $pagina;
        $this->saberExiste();
    }
    // fin función
    
    
    /**
     * DETERMINAR SI YA HAY UN REGISTRO PREVIO
     */
    private function saberExiste()
    {
        $query = "SELECT COUNT(*) "
                . "FROM $this->tabla "
                . "WHERE practica = $this->practica";
        $res = mysql_query($query) or die(mysql_error());
        $data = mysql_fetch_row($res);
        $num = $data[0];
        if($num != 0) {
            $this->existe = TRUE;
        }
    }
    // fin función
    
    
    /**
     * MOSTRAR FORMULARIO
     */
    public function pintarFormulario()
    {
        $txt_false = "Para poder generar el cuadro de 
            <strong>Razones Financieras</strong> es necesario llenar los 
            siguientes campos:";
        $txt_true = "Puede actualizar las <strong>Razones Financieras</strong> "
                . "modificando los valores siguientes:";
        $leyenda = ($this->existe)?$txt_true:$txt_false;
        $tAccion = ($this->existe)?'modificarRazones':'agregarRazones';
        $boton = ($this->existe)?'Actualizar valores':'Agregar valores';
        $ventas = "";
        $costo = "";
        $compras = "";
        $id = "";
        $practica = $this->practica;
        $cVentas = "";
        $cCosto = "";
        $cCompras = "";
        if($this->existe)
        {
            $ventas =  $this->extraerValores('ventas');
            $costo = $this->extraerValores('costo');
            $compras = $this->extraerValores('compras');
            $id = $this->extraerValores('id');
            $cVentas = " <span class='comentarios'>// Ventas netas</span>";
            $cCosto = " <span class='comentarios'>// Costo de ventas</span>";
            $cCompras = " <span class='comentarios'>// Compras netas</span>";
        }
        $tabla = $this->tabla;
        $pagina = $this->pagina;
        
        $formulario = <<<FORM
                <div class="celdaSubtit" style="margin-bottom: 10px;">{$leyenda}
          </div>
          <form class="form-2014" 
                action="libreria/acciones_razones.php" 
                method="POST" 
                autocomplete="off">
              <fieldset>
                  <div>$
                      <input id="ventas" 
                             oninput="validarFloat(this.value,this.id);" 
                             type="text" name="ventas" required 
                             placeholder="Ventas netas:"
                             value="{$ventas}" />{$cVentas}
                  </div>
                  <div>$
                      <input id="costo" 
                             oninput="validarFloat(this.value,this.id);" 
                             type="text" name="costo" required 
                             placeholder="Costo de ventas:" 
                             value="{$costo}" />{$cCosto}
                  </div>
                  <div>$
                      <input id="compras" 
                             oninput="validarFloat(this.value,this.id);" 
                             type="text" name="compras" required 
                             placeholder="Compras netas:" 
                             value="{$compras}" />{$cCompras}
                  </div>
                  <div>
                      <input type='hidden' value='{$id}' name='id' />
                      <input type='hidden' value='{$tabla}' name='tabla' />
                      <input type='hidden' value='{$practica}' name='practica' />
                      <input type='hidden' value='{$pagina}' name='pagina' />
                      <button type="submit" name="{$tAccion}">{$boton}</button>
                  </div>
              </fieldset>
          </form>
FORM;
        return $formulario;
    }
    // fin función
    
    
    
    /**
     * EXTRAER VALORES DE lA TABLA
     * 
     * @param string $campo El Campo del valor
     */
    private function extraerValores($campo)
    {
        $query = "SELECT $campo FROM $this->tabla "
                . "WHERE practica = $this->practica";
        $res = mysql_query($query) or die(mysql_error());
        $data = mysql_fetch_row($res);
        return $data[0];
    }
    // fin función



    /**
     * GETTER DE EXISTE
     */
    public function mostrarExiste()
    {
        return $this->existe;
    }
    
}
// EOF
