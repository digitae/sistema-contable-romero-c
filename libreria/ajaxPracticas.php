<?php
include 'principal.php';

#######################
## AJAX CON jQUERY
## 2014
## Fernando Magrosoto
#######################


## MODIFICAR FECHA DE PRÁCTICA
$modificarFechaPractica = filter_input(INPUT_POST, 'modificarFechaPractica',
        FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);

if($modificarFechaPractica)
{
    $fecha = utf8_decode(filter_input(INPUT_POST, 'fecha',
            FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_LOW));
    $practica = filter_input(INPUT_POST, 'practica',
            FILTER_VALIDATE_INT);
    $tabla = filter_input(INPUT_POST, 'tabla',
            FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_HIGH);
    $tipo = filter_input(INPUT_POST, 'tipo',
            FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_HIGH);
    $query = "UPDATE $tabla SET $tipo = '$fecha' "
            . "WHERE IDpractica = $practica";
    if(mysql_query($query) or die(mysql_error()))
    {
        echo "done";
    } else {
        echo "error";
    }
}
## fin


## MODIFICAR CAMPO DE CELDA
$modificarCelda = filter_input(INPUT_POST, 'modificarCelda',
        FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
if($modificarCelda)
{
    $celda = filter_input(INPUT_POST, 'celda',
            FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $valor = filter_input(INPUT_POST, 'valor',
            FILTER_VALIDATE_FLOAT);
    $practica = filter_input(INPUT_POST, 'practica',
            FILTER_VALIDATE_INT);
    $tabla = filter_input(INPUT_POST, 'tabla',
            FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    // Primero, verificar si hay un registro con esa celda
    $qvc = "SELECT COUNT(*) "
            . "FROM $tabla "
            . "WHERE celda = '$celda' AND practica = $practica";
    $rvc = mysql_query($qvc) or die('Verificar: '.mysql_error());
    $dvc = mysql_fetch_row($rvc);
    
    if($dvc[0] == 0)
    {
        // No hay registros, entonces hacer un INSERT
        $q = "INSERT INTO $tabla "
            . "(celda,valor,practica) "
            . "VALUES "
            . "('$celda','$valor',$practica)";
    } else {
        // Si hay registros, entonces hacer un UPDATE
        $q = "UPDATE $tabla SET valor = '$valor' "
                . "WHERE celda = '$celda' AND practica = $practica";
    }
    
    if(mysql_query($q) or die('error: '.mysql_error()))
    {
        echo "done";
    } else {
        echo "error";
    }
    
}
## fin

## SUMA D28
$sumad28 = filter_input(INPUT_POST, 'sumad28',
        FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
if($sumad28)
{
    $practica = filter_input(INPUT_POST, 'practica',
            FILTER_VALIDATE_INT);
    $tabla = filter_input(INPUT_POST, 'tabla',
            FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $suma = extraerValorCampo('c7', $practica, $tabla)+
            extraerValorCampo('c8', $practica, $tabla)+
            extraerValorCampo('c9', $practica, $tabla)+
            extraerValorCampo('c10', $practica, $tabla)+
            extraerValorCampo('c11', $practica, $tabla)+
            extraerValorCampo('c12', $practica, $tabla)+
            extraerValorCampo('c13', $practica, $tabla)+
            extraerValorCampo('c14', $practica, $tabla)+
            extraerValorCampo('c15', $practica, $tabla)+
            extraerValorCampo('c16', $practica, $tabla)+
            extraerValorCampo('c17', $practica, $tabla)+
            extraerValorCampo('c18', $practica, $tabla)+
            extraerValorCampo('c19', $practica, $tabla)+
            extraerValorCampo('c20', $practica, $tabla)+
            extraerValorCampo('c21', $practica, $tabla)+
            extraerValorCampo('c22', $practica, $tabla)+
            extraerValorCampo('c23', $practica, $tabla)+
            extraerValorCampo('c24', $practica, $tabla)+
            extraerValorCampo('c25', $practica, $tabla)+
            extraerValorCampo('c26', $practica, $tabla)+
            extraerValorCampo('c27', $practica, $tabla)+
            extraerValorCampo('c28', $practica, $tabla);
    $_SESSION['d28'] = number_format($suma,2,'.','');
    echo number_format($suma,2,'.','');
}
## fin

## SUMA D38
$sumad38 = filter_input(INPUT_POST, 'sumad38',
        FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
if($sumad38)
{
    $practica = filter_input(INPUT_POST, 'practica',
            FILTER_VALIDATE_INT);
    $tabla = filter_input(INPUT_POST, 'tabla',
            FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $suma = extraerValorCampo('c31', $practica, $tabla)+
            extraerValorCampo('c32', $practica, $tabla)+
            extraerValorCampo('c33', $practica, $tabla)+
            extraerValorCampo('c34', $practica, $tabla)+
            extraerValorCampo('c35', $practica, $tabla)+
            extraerValorCampo('c36', $practica, $tabla)+
            extraerValorCampo('c37', $practica, $tabla)+
            extraerValorCampo('c38', $practica, $tabla);
    $_SESSION['d38'] = number_format($suma,2,'.','');
    echo number_format($suma,2,'.','');
}
## fin

## SUMA D47
$sumad47 = filter_input(INPUT_POST, 'sumad47',
        FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
if($sumad47)
{
    $practica = filter_input(INPUT_POST, 'practica',
            FILTER_VALIDATE_INT);
    $tabla = filter_input(INPUT_POST, 'tabla',
            FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $suma = extraerValorCampo('c40', $practica, $tabla)+
            extraerValorCampo('c41', $practica, $tabla)+
            extraerValorCampo('c42', $practica, $tabla)+
            extraerValorCampo('c43', $practica, $tabla)+
            extraerValorCampo('c44', $practica, $tabla)+
            extraerValorCampo('c45', $practica, $tabla)+
            extraerValorCampo('c46', $practica, $tabla)+
            extraerValorCampo('c47', $practica, $tabla);
    $_SESSION['d47'] = number_format($suma,2,'.','');
    echo number_format($suma,2,'.','');
}
## fin

## SUMA D54
$sumad54 = filter_input(INPUT_POST, 'sumad54',
        FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
if($sumad54)
{
    $practica = filter_input(INPUT_POST, 'practica',
            FILTER_VALIDATE_INT);
    $tabla = filter_input(INPUT_POST, 'tabla',
            FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $suma = extraerValorCampo('c49', $practica, $tabla)+
            extraerValorCampo('c50', $practica, $tabla)+
            extraerValorCampo('c51', $practica, $tabla)+
            extraerValorCampo('c52', $practica, $tabla)+
            extraerValorCampo('c53', $practica, $tabla)+
            extraerValorCampo('c54', $practica, $tabla);
    $_SESSION['d54'] = number_format($suma,2,'.','');            
    echo number_format($suma,2,'.','');
}
## fin

## SUMA D72
$sumad72 = filter_input(INPUT_POST, 'sumad72',
        FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
if($sumad72)
{
    $practica = filter_input(INPUT_POST, 'practica',
            FILTER_VALIDATE_INT);
    $tabla = filter_input(INPUT_POST, 'tabla',
            FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $suma = extraerValorCampo('c57', $practica, $tabla)+
            extraerValorCampo('c58', $practica, $tabla)+
            extraerValorCampo('c59', $practica, $tabla)+
            extraerValorCampo('c60', $practica, $tabla)+
            extraerValorCampo('c61', $practica, $tabla)+
            extraerValorCampo('c62', $practica, $tabla)+
            extraerValorCampo('c63', $practica, $tabla)+
            extraerValorCampo('c64', $practica, $tabla)+
            extraerValorCampo('c65', $practica, $tabla)+
            extraerValorCampo('c66', $practica, $tabla)+
            extraerValorCampo('c67', $practica, $tabla)+
            extraerValorCampo('c68', $practica, $tabla)+
            extraerValorCampo('c69', $practica, $tabla)+
            extraerValorCampo('c70', $practica, $tabla)+
            extraerValorCampo('c71', $practica, $tabla)+
            extraerValorCampo('c72', $practica, $tabla);
    $_SESSION['d72'] = number_format($suma,2,'.','');
    echo number_format($suma,2,'.','');
}
## fin

## SUMA D79
$sumad79 = filter_input(INPUT_POST, 'sumad79',
        FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
if($sumad79)
{
    $practica = filter_input(INPUT_POST, 'practica',
            FILTER_VALIDATE_INT);
    $tabla = filter_input(INPUT_POST, 'tabla',
            FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $suma = extraerValorCampo('c74', $practica, $tabla)+
            extraerValorCampo('c75', $practica, $tabla)+
            extraerValorCampo('c76', $practica, $tabla)+
            extraerValorCampo('c77', $practica, $tabla)+
            extraerValorCampo('c78', $practica, $tabla)+
            extraerValorCampo('c79', $practica, $tabla);
    $_SESSION['d79'] = number_format($suma,2,'.','');
    echo number_format($suma,2,'.','');
}
## fin

## SUMA D84
$sumad84 = filter_input(INPUT_POST, 'sumad84',
        FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
if($sumad84)
{
    $practica = filter_input(INPUT_POST, 'practica',
            FILTER_VALIDATE_INT);
    $tabla = filter_input(INPUT_POST, 'tabla',
            FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $suma = extraerValorCampo('c82', $practica, $tabla)+
            extraerValorCampo('c83', $practica, $tabla)+
            extraerValorCampo('c84', $practica, $tabla);
    $_SESSION['d84'] = number_format($suma,2,'.','');
    echo number_format($suma,2,'.','');
}
## fin

## SUMA D92
$sumad92 = filter_input(INPUT_POST, 'sumad92',
        FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
if($sumad92)
{
    $practica = filter_input(INPUT_POST, 'practica',
            FILTER_VALIDATE_INT);
    $tabla = filter_input(INPUT_POST, 'tabla',
            FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $suma = extraerValorCampo('c86', $practica, $tabla)+
            extraerValorCampo('c87', $practica, $tabla)+
            extraerValorCampo('c88', $practica, $tabla)+
            extraerValorCampo('c89', $practica, $tabla)+
            extraerValorCampo('c90', $practica, $tabla)+
            extraerValorCampo('c91', $practica, $tabla)+
            extraerValorCampo('c92', $practica, $tabla);
    $_SESSION['d92'] = number_format($suma,2,'.','');
    echo number_format($suma,2,'.','');
}
## fin

## COLUMNA G

## SUMA G28
$sumag28 = filter_input(INPUT_POST, 'sumag28',
        FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
if($sumag28)
{
    $practica = filter_input(INPUT_POST, 'practica',
            FILTER_VALIDATE_INT);
    $tabla = filter_input(INPUT_POST, 'tabla',
            FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $suma = extraerValorCampo('f7', $practica, $tabla)+
            extraerValorCampo('f8', $practica, $tabla)+
            extraerValorCampo('f9', $practica, $tabla)+
            extraerValorCampo('f10', $practica, $tabla)+
            extraerValorCampo('f11', $practica, $tabla)+
            extraerValorCampo('f12', $practica, $tabla)+
            extraerValorCampo('f13', $practica, $tabla)+
            extraerValorCampo('f14', $practica, $tabla)+
            extraerValorCampo('f15', $practica, $tabla)+
            extraerValorCampo('f16', $practica, $tabla)+
            extraerValorCampo('f17', $practica, $tabla)+
            extraerValorCampo('f18', $practica, $tabla)+
            extraerValorCampo('f19', $practica, $tabla)+
            extraerValorCampo('f20', $practica, $tabla)+
            extraerValorCampo('f21', $practica, $tabla)+
            extraerValorCampo('f22', $practica, $tabla)+
            extraerValorCampo('f23', $practica, $tabla)+
            extraerValorCampo('f24', $practica, $tabla)+
            extraerValorCampo('f25', $practica, $tabla)+
            extraerValorCampo('f26', $practica, $tabla)+
            extraerValorCampo('f27', $practica, $tabla)+
            extraerValorCampo('f28', $practica, $tabla);
    $_SESSION['g28'] = number_format($suma,2,'.','');
    echo number_format($suma,2,'.','');
}
## fin

## SUMA D38
$sumag38 = filter_input(INPUT_POST, 'sumag38',
        FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
if($sumag38)
{
    $practica = filter_input(INPUT_POST, 'practica',
            FILTER_VALIDATE_INT);
    $tabla = filter_input(INPUT_POST, 'tabla',
            FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $suma = extraerValorCampo('f31', $practica, $tabla)+
            extraerValorCampo('f32', $practica, $tabla)+
            extraerValorCampo('f33', $practica, $tabla)+
            extraerValorCampo('f34', $practica, $tabla)+
            extraerValorCampo('f35', $practica, $tabla)+
            extraerValorCampo('f36', $practica, $tabla)+
            extraerValorCampo('f37', $practica, $tabla)+
            extraerValorCampo('f38', $practica, $tabla);
    $_SESSION['g38'] = number_format($suma,2,'.','');
    echo number_format($suma,2,'.','');
}
## fin

## SUMA G47
$sumag47 = filter_input(INPUT_POST, 'sumag47',
        FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
if($sumag47)
{
    $practica = filter_input(INPUT_POST, 'practica',
            FILTER_VALIDATE_INT);
    $tabla = filter_input(INPUT_POST, 'tabla',
            FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $suma = extraerValorCampo('f40', $practica, $tabla)+
            extraerValorCampo('f41', $practica, $tabla)+
            extraerValorCampo('f42', $practica, $tabla)+
            extraerValorCampo('f43', $practica, $tabla)+
            extraerValorCampo('f44', $practica, $tabla)+
            extraerValorCampo('f45', $practica, $tabla)+
            extraerValorCampo('f46', $practica, $tabla)+
            extraerValorCampo('f47', $practica, $tabla);
    $_SESSION['g47'] = number_format($suma,2,'.','');
    echo number_format($suma,2,'.','');
}
## fin

## SUMA G54
$sumag54 = filter_input(INPUT_POST, 'sumag54',
        FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
if($sumag54)
{
    $practica = filter_input(INPUT_POST, 'practica',
            FILTER_VALIDATE_INT);
    $tabla = filter_input(INPUT_POST, 'tabla',
            FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $suma = extraerValorCampo('f49', $practica, $tabla)+
            extraerValorCampo('f50', $practica, $tabla)+
            extraerValorCampo('f51', $practica, $tabla)+
            extraerValorCampo('f52', $practica, $tabla)+
            extraerValorCampo('f53', $practica, $tabla)+
            extraerValorCampo('f54', $practica, $tabla);
    $_SESSION['g54'] = number_format($suma,2,'.','');
    echo number_format($suma,2,'.','');
}
## fin

## SUMA D72
$sumag72 = filter_input(INPUT_POST, 'sumag72',
        FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
if($sumag72)
{
    $practica = filter_input(INPUT_POST, 'practica',
            FILTER_VALIDATE_INT);
    $tabla = filter_input(INPUT_POST, 'tabla',
            FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $suma = extraerValorCampo('f57', $practica, $tabla)+
            extraerValorCampo('f58', $practica, $tabla)+
            extraerValorCampo('f59', $practica, $tabla)+
            extraerValorCampo('f60', $practica, $tabla)+
            extraerValorCampo('f61', $practica, $tabla)+
            extraerValorCampo('f62', $practica, $tabla)+
            extraerValorCampo('f63', $practica, $tabla)+
            extraerValorCampo('f64', $practica, $tabla)+
            extraerValorCampo('f65', $practica, $tabla)+
            extraerValorCampo('f66', $practica, $tabla)+
            extraerValorCampo('f67', $practica, $tabla)+
            extraerValorCampo('f68', $practica, $tabla)+
            extraerValorCampo('f69', $practica, $tabla)+
            extraerValorCampo('f70', $practica, $tabla)+
            extraerValorCampo('f71', $practica, $tabla)+
            extraerValorCampo('f72', $practica, $tabla);
    $_SESSION['g72'] = number_format($suma,2,'.','');
    echo number_format($suma,2,'.','');
}
## fin

## SUMA D79
$sumag79 = filter_input(INPUT_POST, 'sumag79',
        FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
if($sumag79)
{
    $practica = filter_input(INPUT_POST, 'practica',
            FILTER_VALIDATE_INT);
    $tabla = filter_input(INPUT_POST, 'tabla',
            FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $suma = extraerValorCampo('f74', $practica, $tabla)+
            extraerValorCampo('f75', $practica, $tabla)+
            extraerValorCampo('f76', $practica, $tabla)+
            extraerValorCampo('f77', $practica, $tabla)+
            extraerValorCampo('f78', $practica, $tabla)+
            extraerValorCampo('f79', $practica, $tabla);
    $_SESSION['g79'] = number_format($suma,2,'.','');
    echo number_format($suma,2,'.','');
}
## fin

## SUMA D84
$sumag84 = filter_input(INPUT_POST, 'sumag84',
        FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
if($sumag84)
{
    $practica = filter_input(INPUT_POST, 'practica',
            FILTER_VALIDATE_INT);
    $tabla = filter_input(INPUT_POST, 'tabla',
            FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $suma = extraerValorCampo('f82', $practica, $tabla)+
            extraerValorCampo('f83', $practica, $tabla)+
            extraerValorCampo('f84', $practica, $tabla);
    $_SESSION['g84'] = number_format($suma,2,'.','');
    echo number_format($suma,2,'.','');
}
## fin

## SUMA D92
$sumag92 = filter_input(INPUT_POST, 'sumag92',
        FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
if($sumag92)
{
    $practica = filter_input(INPUT_POST, 'practica',
            FILTER_VALIDATE_INT);
    $tabla = filter_input(INPUT_POST, 'tabla',
            FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $suma = extraerValorCampo('f86', $practica, $tabla)+
            extraerValorCampo('f87', $practica, $tabla)+
            extraerValorCampo('f88', $practica, $tabla)+
            extraerValorCampo('f89', $practica, $tabla)+
            extraerValorCampo('f90', $practica, $tabla)+
            extraerValorCampo('f91', $practica, $tabla)+
            extraerValorCampo('f92', $practica, $tabla);
    $_SESSION['g92'] = number_format($suma,2,'.','');
    echo number_format($suma,2,'.','');
}
## fin


## Extraer cualqquier fecha
$extraerLaFecha = filter_input(INPUT_POST, 'extraerLaFecha',
        FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
if($extraerLaFecha)
{
 $fecha = filter_input(INPUT_POST, 'fecha',
         FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
 $practica = filter_input(INPUT_POST, 'practica',
         FILTER_VALIDATE_INT);
 $q = "SELECT $fecha FROM rom_cambios_practicas "
            . "WHERE IDpractica = $practica";
    $r = mysql_query($q) or die(mysql_error());
    $d = mysql_fetch_row($r);
    if($d[0] == "")
    {
        $s = "fecha no especificada";
    } else {
        $s = utf8_encode($d[0]);
    }
    echo $s;
}
## fin

## Extraer cualqquier fecha
$extraerLaFechaCuenta = filter_input(INPUT_POST, 'extraerLaFechaCuenta',
        FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
if($extraerLaFechaCuenta)
{
 $fecha = filter_input(INPUT_POST, 'fecha',
         FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
 $practica = filter_input(INPUT_POST, 'practica',
         FILTER_VALIDATE_INT);
 $q = "SELECT $fecha FROM rom_cuenta_practicas "
            . "WHERE IDpractica = $practica";
    $r = mysql_query($q) or die(mysql_error());
    $d = mysql_fetch_row($r);
    if($d[0] == "")
    {
        $s = "fecha no especificada";
    } else {
        $s = utf8_encode($d[0]);
    }
    echo $s;
}
## fin

