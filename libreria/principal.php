<?php
session_start();

##############################
## CONEXION A BASE DE DATOS ##
##############################
$dbhost = '68.178.143.49'; // Servidor de la BD
// $dbhost = 'localhost';
$dbuser = 'romeroC'; // Usuario
$dbpass = 'romC2014pwd!'; // Password
$dbname = 'romeroC'; // Base de datos

// Conexión
$conn = mysql_connect($dbhost, $dbuser, $dbpass) or die ('Error connecting to mysql');
mysql_select_db($dbname);
##############################
##############################

// función para saber si es un usuario registrado
function esUsuario() {
	if(!isset($_SESSION['registrado'])) {
		header("Location: sesion_registro.php");
		exit;
	}
}

// formatear la fecha
// Script hecho por Fernando Magrosoto
// Correo fmagrosoto@digitae.com.mx
// Gracias por descargar este fichero
$dia=date("D");
if ($dia == "Mon") {
$diaEsp="Lunes";
} elseif ($dia == "Tue") {
$diaEsp="Martes";
} elseif ($dia == "Wed") {
$diaEsp="Miércoles";
} elseif ($dia == "Thu") {
$diaEsp="Jueves";
} elseif ($dia == "Fri") {
$diaEsp="Viernes";
} elseif ($dia == "Sat") {
$diaEsp="Sábado";
} else {
$diaEsp="Domingo";
}

$mes=(date("F"));
if ($mes == "January") {
$mesEsp="enero";
} elseif ($mes == "February") {
$mesEsp="febrero";
} elseif ($mes == "March") {
$mesEsp="marzo";
} elseif ($mes == "April") {
$mesEsp="abril";
} elseif ($mes == "May") {
$mesEsp="mayo";
} elseif ($mes == "June") {
$mesEsp="junio";
} elseif ($mes == "July") {
$mesEsp="julio";
} elseif ($mes == "August") {
$mesEsp="agosto";
} elseif ($mes == "September") {
$mesEsp="septiembre";
} elseif ($mes == "October") {
$mesEsp="octubre";
} elseif ($mes == "November") {
$mesEsp="noviembre";
} else {
$mesEsp="diciembre";
}

$diaMes=(date("d"));
$agno=(date("Y"));

$Fecha= $diaEsp." ".$diaMes." de ".$mesEsp." de ".$agno;
$hora=(date("h:i:s: a"));

// arreglo de fecha
function arregloFecha($actual){
	$fecha1 = $actual;
	$fecha2 = explode("-", $fecha1);
	$fagno = $fecha2[0];
	$fmes = $fecha2[1];
	$fdia = $fecha2[2];
	$fechaConv = "$fdia-$fmes-$fagno";
	echo $fechaConv;
}
// arreglo de fecha
function arregloFechaAs($actual){
	$fecha1 = $actual;
	$fecha2 = explode("-", $fecha1);
	$fagno = $fecha2[0];
	$fmes = $fecha2[1];
	$fdia = $fecha2[2];
	
	$fdia2 = explode(" ", $fdia);
	$day = $fdia2[0];
	
	$fechaConv = "$day-$fmes-$fagno";
	echo $fechaConv;
}
// pintar Método
function pintarMetodo($IDmetodo){
	
	$queryMet = "SELECT Nmetodo FROM rom_metodos WHERE IDmetodo = '$IDmetodo'";
	$resultMet = mysql_query($queryMet);
	$rowMet = mysql_fetch_assoc($resultMet);
	$Nmetodo = $rowMet['Nmetodo'];
	echo utf8_encode($Nmetodo);
}

// diferencia entre HABER y DEBE (DIARIO)
function diferencia($a, $b) {
	
	if($a > $b){
		$diferencia = $a-$b;		
	} else {
		$diferencia = $b-$a;
	}
	return number_format($diferencia,2);
}

// Mostrar texto de ACCIONES
function mostrarAccion($status) {
	if($status==0){
		echo "Capturar";
	} else {
		echo "Revisar";	
	}
}

// Mostrar saldo
function mostrarSaldo($a, $b) {
	if($a > $b){
		$saldo = $a - $b;
		echo number_format($saldo,2);	
	} else {
		echo 0;	
	}
}
// Mostrar saldo final (RELACIONES)
function mostrarSaldoFinal($a, $b) {
	if($a > $b){
		$saldo = $a - $b;
		return $saldo;	
	} else {
		return 0;	
	}
}
// mostrar clave en MAYOR
function mostrarClave($a, $b){
	if($a == ""){
		$clave = $b;	
	} else {
		$clave = $a;
	}
	echo $clave;
}
// mostrar cantidad en MAYOR
function mostrarCantidad($a, $b){
	if($a == "0.00"){
		$cantidad = $b;	
	} else {
		$cantidad = $a;
	}
	echo $cantidad;
}
// mostrar NOMBRE de la cuenta (MAYOR)
function mostrarNombre($cuenta){
	$query = "SELECT Ncuenta from rom_cuentas WHERE clave = '$cuenta'";
	$result = mysql_query($query);
	$datos = mysql_fetch_assoc($result);
	$Ncuenta = $datos['Ncuenta'];
	echo utf8_encode($Ncuenta);
}
// mostrar NOMBRE dela subcuenta (AUXILIARES)
function mostrarNombreD($cuenta){
	$query = "SELECT Ndcuenta from rom_dcuenta WHERE claveD = '$cuenta'";
	$result = mysql_query($query);
	$datos = mysql_fetch_assoc($result);
	$Ndcuenta = $datos['Ndcuenta'];
	echo utf8_encode($Ndcuenta);
}

// mostrar saldo inicial (AUXILIAR)
function mostrarSaldoInicial($a,$b,$c) {
	$query =	"SELECT tipo, subcantidad FROM rom_cantidades
				LEFT OUTER JOIN rom_asiento ON rom_cantidades.asiento = rom_asiento.asientoR
				WHERE subcuenta = '$a' AND ejercicio = '$b' AND asiento = 1 AND status = 1";
	$result =	mysql_query($query) or die(mysql_error());
	$row =		mysql_fetch_assoc($result);
	$num =		mysql_num_rows($result);
	
	if($num == 0){
		$saldoInicial = 0;
	} else {
		if($row['tipo'] == $c){
			$saldoInicial = $row['subcantidad'];
		} else {
			$saldoInicial = 0;
		}
	}
	echo number_format($saldoInicial,2);	
}

// mostrar movimientos del periodo (AUXILIAR)
function mostrarMovPer($a,$b,$c) {
$queryTD =	"SELECT subcantidad
			FROM rom_cantidades
			INNER JOIN rom_asiento ON
			(rom_cantidades.ejercicio =  rom_asiento.Easiento)
			AND
			(rom_cantidades.asiento = rom_asiento.asientoR)
			WHERE (Easiento = '$c' AND status = 1)
			AND tipo = '$a'
			AND asiento != 1
			AND subcuenta = '$b'";
$resultTD = mysql_query($queryTD);
$rowTD = mysql_fetch_assoc($resultTD);
$totalDebe = 0;

do{
	$totalDebe = $totalDebe+$rowTD['subcantidad'];
} while ($rowTD = mysql_fetch_assoc($resultTD));

echo number_format($totalDebe,2);

}

// mostrar fecha del asiento (AUXILIARES)
function  mostrarFechaAsiento($a, $b){
	$query = "SELECT Fasiento FROM rom_asiento WHERE asientoR = '$a' AND Easiento = '$b'";
	$result = mysql_query($query);
	$dato = mysql_fetch_assoc($result);
	$fecha = $dato['Fasiento'];
	echo arregloFecha($fecha);
}
function  mostrarFechaAsiento_pruebas($a, $b){
	$query = "SELECT Fasiento FROM rom_asiento_pruebas WHERE asientoR = '$a' AND Easiento = '$b'";
	$result = mysql_query($query);
	$dato = mysql_fetch_assoc($result);
	$fecha = $dato['Fasiento'];
	echo arregloFecha($fecha);
}

// mostrar Redacción (AUXILIARES)
function mostrarRedaccion($asiento, $ejercicio){
	$query = "SELECT redaccion FROM rom_asiento
				WHERE asientoR = '$asiento' AND Easiento = '$ejercicio'";
	$result = mysql_query($query);
	$row = mysql_fetch_assoc($result);
	echo $row['redaccion'];
}

// mostrar el total de cada SUBCUENTA (RELACIONES)
function mostrarTotalRel($tipo,$subcuenta,$ejercicio){
	$query = "SELECT SUM(subcantidad) AS total
			FROM rom_cantidades
			INNER JOIN rom_asiento ON
			(rom_cantidades.ejercicio =  rom_asiento.Easiento)
			AND
			(rom_cantidades.asiento = rom_asiento.asientoR)
			WHERE (Easiento = '$ejercicio' AND status = 1)
			AND tipo = '$tipo'
			AND subcuenta = '$subcuenta'";
	$resul = mysql_query($query) or die(mysql_error());
	$row = mysql_fetch_assoc($resul);
	
	$total = $row['total'];
	echo number_format($total,2);
}
// mostrar el total de cada SUBCUENTA MOVIMIENTOS (RELACIONES)
function mostrarTotalRelMov($tipo,$subcuenta,$ejercicio){
	$query = "SELECT SUM(subcantidad) AS total
			FROM rom_cantidades
			INNER JOIN rom_asiento ON
			(rom_cantidades.ejercicio =  rom_asiento.Easiento)
			AND
			(rom_cantidades.asiento = rom_asiento.asientoR)
			WHERE (Easiento = '$ejercicio' AND status = 1)
			AND tipo = '$tipo'
			AND subcuenta = '$subcuenta'";
	$resul = mysql_query($query);
	$row = mysql_fetch_assoc($resul);
	
	$total = $row['total'];
	return $total;
}

// Mostrar el nombre del ejercicio
function pintarNejercicio($a){
	$query = "SELECT Nejercicio FROM rom_ejercicio WHERE IDejercicio = '$a'";
	$result = mysql_query($query);
	$dato = mysql_fetch_row($result);
	
	echo ucwords(utf8_encode($dato[0]));
}
// Mostrar el nombre del ejercicio de pruebas
function pintarNejercicioP($a){
	$query = "SELECT Nejercicio FROM rom_ejercicio_pruebas WHERE IDejercicio = '$a'";
	$result = mysql_query($query);
	$dato = mysql_fetch_row($result);
	
	echo ucwords(utf8_encode($dato[0]));
}

// Mostrar el total por cuenta (BALANZA aa)
function mostrarTotalBaa($cuenta, $ejercicio, $tipo){
	
	$query = "SELECT SUM(cantidad) AS Total
			FROM rom_cantidades
			INNER JOIN rom_asiento ON
			(rom_cantidades.ejercicio = rom_asiento.Easiento)
			AND
			(rom_cantidades.asiento = rom_asiento.asientoR)
			WHERE cuenta = '$cuenta' AND tipo = '$tipo' AND (Easiento = '$ejercicio' AND status = 1)";
	$result = mysql_query($query);
	$datos = mysql_fetch_assoc($result);	
	$cantidad = $datos['Total'];
	return $cantidad;
}
// fin
// Mostrar el total de las cuentas (BALANZA aa)
function mostrarTotalAllBaa($campo, $ejercicio){
	$query = "SELECT SUM($campo) AS Total
			FROM rom_balanza_aa
			WHERE ejercicio = '$ejercicio'";
	$result = mysql_query($query);
	$datos = mysql_fetch_assoc($result);	
	$cantidad = $datos['Total'];
	echo number_format($cantidad,2);
}

// Mostrar el total de las cuentas (BALANZA aa)
function mostrarTotalAllBda($campo, $ejercicio){
	$query = "SELECT SUM($campo) AS Total
			FROM rom_balanza_da
			WHERE ejercicio = '$ejercicio'";
	$result = mysql_query($query);
	$datos = mysql_fetch_assoc($result);	
	$cantidad = $datos['Total'];
	echo number_format($cantidad,2);
}

// Mostrar saldo (BALANZA)
function mostrarSaldoBalanza($a, $b) {
	if($a > $b){
		$saldo = $a - $b;
		return $saldo;	
	} else {
		return 0;
	}
}
//fin

// Poner valor del campo AJUSTES
function  ponerValor($campo, $ejercicio){
	$query = "SELECT cantidadAj FROM rom_ajustes WHERE campoAj = '$campo' AND ejercicioAj = '$ejercicio'";
	$result = mysql_query($query);
	$datos = mysql_fetch_array($result);
	$total = mysql_num_rows($result);
	
	if($total != 0){
		// $cantidad = number_format($datos['cantidadAj'],2);
            $cantidad = $datos['cantidadAj'];
	} else {
		$cantidad = 0;
	}
	return $cantidad;	
}
// fin

// Campo de BALANZA DESPUES DE AJUSTES que esté en AJUSTES
function ajusteBda($cuenta, $ejercicio, $tipo){
	// primero revisamos si existe una cuenta en AJUSTES
	$queryBuscar = "SELECT COUNT(IDdesajuste) AS registros from rom_desajustes
					WHERE cuentaDj = '$cuenta'
					AND tipoDj = '$tipo'
					AND ejercicioDj = '$ejercicio'";
	$resultBuscar = mysql_query($queryBuscar);
	$datoResult = mysql_fetch_array($resultBuscar);
	$registros = $datoResult["registros"];
	if($registros==0){ // No hay coincidencias
	
		// sacamos el saldo de cada registro
		$query = "SELECT SUM(cantidad) AS TotalD
			FROM rom_cantidades
			INNER JOIN rom_asiento ON
			(rom_cantidades.ejercicio = rom_asiento.Easiento)
			AND
			(rom_cantidades.asiento = rom_asiento.asientoR)
			WHERE cuenta = '$cuenta' AND tipo = 'd' AND (Easiento = '$ejercicio' AND status = 1)";
		$result = mysql_query($query);
		$datos = mysql_fetch_assoc($result);	
		$TotalD = $datos['TotalD'];
		
		$query = "SELECT SUM(cantidad) AS TotalH
			FROM rom_cantidades
			INNER JOIN rom_asiento ON
			(rom_cantidades.ejercicio = rom_asiento.Easiento)
			AND
			(rom_cantidades.asiento = rom_asiento.asientoR)
			WHERE cuenta = '$cuenta' AND tipo = 'h' AND (Easiento = '$ejercicio' AND status = 1)";
		$result = mysql_query($query);
		$datos = mysql_fetch_assoc($result);	
		$TotalH = $datos['TotalH'];
		
		if($TotalD > $TotalH){
			$saldo = $TotalD - $TotalH;
			$Ntipo = "d";
		} else {
			$saldo = $TotalH - $TotalD;
			$Ntipo = "h";
		}
		if($tipo == $Ntipo) {
			$cantidad = $saldo;
		} else {
			$cantidad = 0;
		}
		
		return $cantidad;
		
	} else { // si sí...
	
		$query = "SELECT SUM(cantidadDj) AS Total
			FROM rom_desajustes
			WHERE cuentaDj = '$cuenta' AND tipoDJ = '$tipo' AND ejercicioDj = '$ejercicio'";
		$result = mysql_query($query);
		$datos = mysql_fetch_assoc($result);	
		$cantidad = $datos['Total'];
		return $cantidad;
	}
} // fin

#######################
## últimas planillas ##
#######################

// función para poner las cantidads de BALANZA_AA
// en las balanzas finlas
function pintarCantidad($columna, $clave, $ejercicio){
	$query = "SELECT $columna FROM rom_balanza_aa WHERE clave = '$clave' AND ejercicio = '$ejercicio'";
	$result = mysql_query($query) or die(mysql_error());
	$dato = mysql_fetch_array($result);
	$cantidad = $dato[$columna];
	return $cantidad;
}

// función para poner las cantidads de BALANZA_DA
// en los balances finales
function pintarCantidadDa($columna, $clave, $ejercicio){
	$query = "SELECT $columna FROM rom_balanza_da WHERE clave = '$clave' AND ejercicio = '$ejercicio'";
	$result = mysql_query($query) or die(mysql_error());
	$dato = mysql_fetch_array($result);
	$cantidad = $dato[$columna];
	return $cantidad;
} // fin

// función para mostrar cantidad
// según regla de NEGATIVOS.
//
// La idea es... si solicitamos datos de PASIVO o ACTIVO
// de la columna x (debe o haber) y si comprobamos que el saldo es 0 .. entonces
// calculamos el saldo de la otra columna, si es > 0 entonces ese saldo
// lo pasamos como el saldo solicitado al principio pero con signo negativo.
//
function pintarCantReglaNeg($columna, $clave, $ejercicio){
	$colD = "Sdebe";
	$colH = "Shaber";
	// conocer que columna nos han solicitado
	if($columna == $colD){
		$col2 = $colH;
	} else {
		$col2 = $colD;
	}
	// hacer la búsqueda
	$query = "SELECT $columna FROM rom_balanza_da WHERE clave = '$clave' AND ejercicio = '$ejercicio'";
	$result = mysql_query($query) or die(mysql_error());
	$dato = mysql_fetch_array($result);
	$cantidad = $dato[$columna];
	// comprobar que sea = 0
	if($cantidad == 0){
		// buscamos en la otra columna un valor
		$query2 = "SELECT $col2 FROM rom_balanza_da WHERE clave = '$clave' AND ejercicio = '$ejercicio'";
		$result2 = mysql_query($query2) or die(mysql_error());
		$dato2 = mysql_fetch_array($result2);
		$cantidad2 = $dato2[$col2];
		// comprobamos que sea = 0
		if($cantidad2 == 0) {
			// si es igual a 0 entonces devolvemos el valor de cantidad, tal cual la pimer solicitud
			return $cantidad;
		} else {
			// si no es 0 entonces
			// AQUÍ VIENE LA REGLA
			// cantidad es igual a la cantidad de la segunda columna
			// pero pasada con signo negativo.
			$cantidad = $cantidad2*-1;
			return $cantidad;
		}
	} else { // si NO es igual a 0 entonces tenemos el valor de $columna y la devolvemos
		return $cantidad;
	}
} // fin

function pintarCantReglaNegReverse($columna, $clave, $ejercicio){
	$colD = "Sdebe";
	$colH = "Shaber";
	// conocer que columna nos han solicitado
	if($columna == $colD){
		$col2 = $colH;
	} else {
		$col2 = $colD;
	}
	// hacer la búsqueda
	$query = "SELECT $columna FROM rom_balanza_da WHERE clave = '$clave' AND ejercicio = '$ejercicio'";
	$result = mysql_query($query) or die(mysql_error());
	$dato = mysql_fetch_array($result);
	$cantidad = $dato[$columna];
	// comprobar que sea = 0
	if($cantidad == 0){
		// buscamos en la otra columna un valor
		$query2 = "SELECT $col2 FROM rom_balanza_da WHERE clave = '$clave' AND ejercicio = '$ejercicio'";
		$result2 = mysql_query($query2) or die(mysql_error());
		$dato2 = mysql_fetch_array($result2);
		$cantidad2 = $dato2[$col2];
		// comprobamos que sea = 0
		if($cantidad2 == 0) {
			// si es igual a 0 entonces devolvemos el valor de cantidad, tal cual la pimer solicitud
			return $cantidad;
		} else {
			// si no es 0 entonces
			// AQUÍ VIENE LA REGLA
			// cantidad es igual a la cantidad de la segunda columna
			// pero pasada con signo negativo.
			$cantidad = $cantidad2;
			return $cantidad;
		}
	} else { // si NO es igual a 0 entonces tenemos el valor de $columna y la devolvemos
		return $cantidad*-1;
	}
} // fin

// función para sacar saldos de auxiliar global
function calcularSaldosAG($tipoSaldo, $subcuenta, $ejercicio){
	$queryMd = "SELECT SUM(subcantidad) AS Md
			FROM rom_cantidades
			WHERE tipo = 'd'
			AND subcuenta = '$subcuenta'
			AND ejercicio = '$ejercicio'";
	$resultMd = mysql_query($queryMd) or die(mysql_error());
	$datoMd = mysql_fetch_array($resultMd);
	$Md = $datoMd['Md']; // Total de Movimientos D
	
	$queryMh = "SELECT SUM(subcantidad) AS Mh
			FROM rom_cantidades
			WHERE tipo = 'h'
			AND subcuenta = '$subcuenta'
			AND ejercicio = '$ejercicio'";
	$resultMh = mysql_query($queryMh) or die(mysql_error());
	$datoMh = mysql_fetch_array($resultMh);
	$Mh = $datoMh['Mh']; // Total de Movimientos H
	
	// extraer los saldos
	
	if($Md > $Mh){
		$saldoD = $Md - $Mh;
		$saldoH = 0;
	} else {
		$saldoD = 0;
		$saldoH = $Mh - $Md;
	}
	
	if($tipoSaldo == "saldoD"){
		$cantidad = $saldoD;
	} elseif ($tipoSaldo == "saldoH"){
		$cantidad = $saldoH;
	}
	
	return $cantidad;
	
} // fin

// función para extraer las fechas
// del primer y último asiento del ejercicio
function fechasPU($orden, $ejercicio){

	$query = "SELECT asientoR, Fasiento
			FROM `rom_asiento`
			WHERE Easiento = '$ejercicio'
			ORDER BY asientoR $orden
			LIMIT 0 , 1";
	$result = mysql_query($query);
	$datos = mysql_fetch_array($result);
	$fechaE = $datos['Fasiento'];
	return $fechaE;

} // fin
// función para extraer la fecha del asiento
function fechasAs($ejercicio){
	$query = "SELECT DISTINCT fechaAj
			FROM rom_ajustes
			WHERE ejercicioAj = '$ejercicio'";
	$result = mysql_query($query);
	$dato = mysql_fetch_assoc($result);
	$fechaAs = $dato['fechaAj'];
	return $fechaAs;
}

// sacar cantidades del MAYOR
function sacarCanMay($cuenta,$tipo,$asiento,$ejercicio){

	$query = "SELECT cantidad
		FROM rom_cantidades
		WHERE cuenta = '$cuenta'
		AND tipo = '$tipo'
		AND asiento = '$asiento'
		AND ejercicio = '$ejercicio'
		AND acumulable = 1";
	$result = mysql_query($query) or die(mysql_error());
	$dato = mysql_fetch_assoc($result);
	$cantidad = $dato['cantidad'];
	return $cantidad;

}
function sacarCanMayA($cuenta,$tipo,$asiento,$ejercicio){

	$query = "SELECT cantidad
		FROM rom_cantidades
		WHERE cuenta = '$cuenta'
		AND tipo = '$tipo'
		AND asiento = '$asiento'
		AND ejercicio = '$ejercicio'";
	$result = mysql_query($query) or die(mysql_error());
	$dato = mysql_fetch_assoc($result);
	$cantidad = $dato['cantidad'];
	return $cantidad;

}
function sacarCanMaySub($subcuenta,$tipo,$asiento,$ejercicio){

	$query = "SELECT subcantidad
		FROM `rom_cantidades`
		WHERE subcuenta = '$subcuenta'
		AND tipo = '$tipo'
		AND asiento = '$asiento'
		AND ejercicio = '$ejercicio'";
	$result = mysql_query($query) or die(mysql_error());
	$dato = mysql_fetch_assoc($result);
	$subcantidad = $dato['subcantidad'];
	return $subcantidad;

}
// función para prevenir divisiones sobre 0
function divCero($a,$b){
	if($b == 0){
		$cantidad = 0;
	} else {
		$cantidad = $a/$b;
	}
	return number_format($cantidad,2);
}


##########################
## MANEJAR FORMULARIOS  ##
##########################

// Para CERRAR ASIENTO
$SubmitCeAs = filter_input(INPUT_POST, 'SubmitCeAs');
if($SubmitCeAs){
	$redaccion = filter_input(INPUT_POST, 'redaccion', 
                FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
	$IDejercicio = filter_input(INPUT_POST, 'IDejercicio',FILTER_SANITIZE_NUMBER_INT);
	$status = filter_input(INPUT_POST, 'status',FILTER_SANITIZE_NUMBER_INT);
	$metodo = filter_input(INPUT_POST, 'metodo',FILTER_SANITIZE_NUMBER_INT);
        $asiento = filter_input(INPUT_POST, 'asiento', FILTER_SANITIZE_NUMBER_INT);
	
	$query = "UPDATE rom_asiento SET redaccion = '$redaccion', status = '$status'
	WHERE asientoR = '$asiento' AND Easiento = '$IDejercicio'";
	mysql_query($query) or die(mysql_error());
	
	header("Location: ../diarioFeAs.php?IDejercicio=$IDejercicio&metodo=$metodo&accion=actualizar&asiento=$asiento");
	exit;
}
// fin

$SubmitCeAs_pruebas = filter_input(INPUT_POST, 'SubmitCeAs_pruebas');
if($SubmitCeAs_pruebas){
	$redaccion = filter_input(INPUT_POST, 'redaccion', 
                FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
	$IDejercicio = filter_input(INPUT_POST, 'IDejercicio',FILTER_SANITIZE_NUMBER_INT);
	$status = filter_input(INPUT_POST, 'status',FILTER_SANITIZE_NUMBER_INT);
	$metodo = filter_input(INPUT_POST, 'metodo',FILTER_SANITIZE_NUMBER_INT);
        $asiento = filter_input(INPUT_POST, 'asiento', FILTER_SANITIZE_NUMBER_INT);
	
	$query = "UPDATE rom_asiento_pruebas SET redaccion = '$redaccion', status = '$status'
	WHERE asientoR = '$asiento' AND Easiento = '$IDejercicio'";
	mysql_query($query) or die(mysql_error());
	
	header("Location: ../diarioFeAs_pruebas.php?IDejercicio=$IDejercicio&metodo=$metodo&accion=actualizar&asiento=$asiento");
	exit;
}
// fin

$accion = filter_input(INPUT_GET, 'accion');

// Para eiminar una asiento (DIARIOSFEAS)
if($accion && $accion == "eliminar"){
	$asiento = filter_input(INPUT_GET, 'asiento',FILTER_SANITIZE_NUMBER_INT);
	$IDejercicio = filter_input(INPUT_GET, 'IDejercicio',FILTER_SANITIZE_NUMBER_INT);
	$metodo = filter_input(INPUT_GET, 'metodo',FILTER_SANITIZE_NUMBER_INT);
	// borrar asiento de rom_asiento
	$query = "DELETE FROM rom_asiento WHERE asientoR = '$asiento' AND Easiento = '$IDejercicio'";
	mysql_query($query) or die(mysql_error());
	// borrar entradas de rom_cantidades
	$query = "DELETE FROM rom_cantidades WHERE asiento = '$asiento' AND ejercicio = '$IDejercicio'";
	mysql_query($query) or die(mysql_error());
	
	header("Location: ../diarioFeAs.php?accion=eliminado&IDejercicio=$IDejercicio&metodo=$metodo&asiento=$asiento");
	exit;
}
if($accion && $accion == "eliminar_pruebas"){
	$asiento = filter_input(INPUT_GET, 'asiento',FILTER_SANITIZE_NUMBER_INT);
	$IDejercicio = filter_input(INPUT_GET, 'IDejercicio',FILTER_SANITIZE_NUMBER_INT);
	$metodo = filter_input(INPUT_GET, 'metodo',FILTER_SANITIZE_NUMBER_INT);
	// borrar asiento de rom_asiento
	$query = "DELETE FROM rom_asiento_pruebas WHERE asientoR = '$asiento' AND Easiento = '$IDejercicio'";
	mysql_query($query) or die(mysql_error());
	// borrar entradas de rom_cantidades
	$query = "DELETE FROM rom_cantidades_pruebas WHERE asiento = '$asiento' AND ejercicio = '$IDejercicio'";
	mysql_query($query) or die(mysql_error());
	
	header("Location: ../diarioFeAs_pruebas.php?accion=eliminado&IDejercicio=$IDejercicio&metodo=$metodo&asiento=$asiento");
	exit;
}
// fin

// Para eliminar una entrada (DIARIO)
if($accion && $accion == "eliminarEntrada") {
	$IDejercicio = filter_input(INPUT_GET, 'IDejercicio',FILTER_SANITIZE_NUMBER_INT);
	$asiento = filter_input(INPUT_GET, 'asiento',FILTER_SANITIZE_NUMBER_INT);
	$metodo = filter_input(INPUT_GET, 'metodo',FILTER_SANITIZE_NUMBER_INT);
	$IDcantidad = filter_input(INPUT_GET, 'IDcantidad', FILTER_SANITIZE_NUMBER_INT);
	
	// borrar registro
	// ** ATENCION **
	// la idea es borrar el registro de rom_cantidades.
	// Sólo que hay que corroborar si se trata de una cuenta ACUMULATIVA
	// o una cuenta DETALLE.
	// Si fuera DETALLE habría que eliminar el registro y actualizar la
	// ACUMULATIVA (restando la subcantidad).
	// Si se tratara de una ACUMULATIVA también habría que borrar
	// las cuentas DETALLE relacionadas a la cuenta ACUMULATIVA
	// pero .... si no es ninguna de las dos, entonces es una cuenta
	// normal y habría que eliminarla solamente.
	
	// --------------------------------------------
	// primero sacamos la información del registro
	
	$query = "SELECT cantidad, tipo, asiento, cuenta, subcuenta, subcantidad, ejercicio, acumulable
				FROM rom_cantidades
				WHERE IDcantidad = '$IDcantidad'";
	$result = mysql_query($query);
	$datos = mysql_fetch_assoc($result);
	
	// datos
	$cantidad = $datos['cantidad'];
	$tipo = $datos['tipo'];
	$asiento = $datos['asiento'];
	$cuenta = $datos['cuenta'];
	$subcuenta = $datos['subcuenta'];
	$subcantidad = $datos['subcantidad'];
	$ejercicio = $datos['ejercicio'];
	$acumulable = $datos['acumulable'];
	
	// EMPEZAMOS A DESCARTAR
	// primero, vemos si NO es una cuenta acumulable:
	if($subcuenta == "" && $acumulable == 0){
		// se trata de una cuenta normal
		// borramos sin problemas
		$query = "DELETE FROM rom_cantidades WHERE IDcantidad = '$IDcantidad'";
		mysql_query($query);
		// funciona bien!!
	}
	// segundo, vemos si es una cuenta ACUMULATIVA
	if($acumulable == 1){
		// se trata de una cuenta ACUMULTIVA
		// aquí tenemos que borrar esta cuenta y 
		// borra cuentas DETALLE que hubiera
		//
		// Borramos la cuenta ACUMULATIVA
		$query = "DELETE FROM rom_cantidades WHERE IDcantidad = '$IDcantidad'";
		mysql_query($query);
		// Borramos las cuentas detalla
		$query = "DELETE FROM rom_cantidades
		WHERE asiento = '$asiento' AND ejercicio = '$ejercicio'
		AND subcuenta LIKE '$cuenta%'";
		mysql_query($query) or die(mysql_error());
		// funciona bien!!
	}
	// tercero, vemos que es una cuenta DETALLE
	if($subcuenta && $acumulable == 0){
		// se trata de una cuenta DETALLE
		// aquí tenemos que borrar la cuenta
		// y actualizar la cantidad del ACUMULABLE
		//
		// Borramos la cuenta DETALLE
		$query = "DELETE FROM rom_cantidades WHERE IDcantidad = '$IDcantidad'";
		mysql_query($query);
		// Actualizamos la cuenta ACUMULATIVA
		// $subcuentaA = split("-",$subcuenta); FUNCION OBSOLETA PHP5.3
                $subcuentaA = explode("-",$subcuenta);
		$cuentaA = $subcuentaA[0];
		
		$query1 = "SELECT IDcantidad, cantidad, tipo FROM rom_cantidades
				WHERE asiento = '$asiento'
				AND cuenta = '$cuentaA'
				AND ejercicio = '$ejercicio'
				AND acumulable = 1";
		$result1 = mysql_query($query1);
		$dato1 = mysql_fetch_assoc($result1);
		
		$oldcantidad = $dato1['cantidad'];
		$IDcantidadA = $dato1['IDcantidad'];
		$oldtipo = $dato1['tipo'];
		
		// empiezan los cálculos para actualizar el registro ACUMULATIVO
		
		if($oldtipo != $tipo){
			$newcantidad = $oldcantidad + $subcantidad;
			$nuevoTipo = $oldtipo;			
		}
		if($oldtipo == $tipo){
			$newcantidad = $oldcantidad - $subcantidad;
			if($newcantidad > 0){
				$nuevoTipo = $oldtipo;
			} else {
				if($tipo == "d"){
					$nuevoTipo = "h";
				} else {
					$nuevoTipo = "d";
				}
				$newcantidad = $newcantidad * -1;
			}
		}
		
		// fin
		
		if($newcantidad != 0) {
			$query2 = "UPDATE rom_cantidades SET cantidad = '$newcantidad', tipo = '$nuevoTipo' WHERE IDcantidad = '$IDcantidadA'";
			mysql_query($query2) or die(mysql_error());
		} else {
			$query2 = "DELETE FROM rom_cantidades WHERE IDcantidad = '$IDcantidadA'";
			mysql_query($query2);
		}
	}
		
		
	// después de borrar, nos regresamos...
	header("Location: ../diario.php?IDejercicio=$IDejercicio&asiento=$asiento&metodo=$metodo&accion=eliminado");
	exit;
}

if($accion && $accion == 'eliminarEntrada_pruebas') {
	$IDejercicio = filter_input(INPUT_GET, 'IDejercicio',FILTER_SANITIZE_NUMBER_INT);
	$asiento = filter_input(INPUT_GET, 'asiento',FILTER_SANITIZE_NUMBER_INT);
	$metodo = filter_input(INPUT_GET, 'metodo',FILTER_SANITIZE_NUMBER_INT);
	$IDcantidad = filter_input(INPUT_GET, 'IDcantidad', FILTER_SANITIZE_NUMBER_INT);
	
	// borrar registro
	// ** ATENCION **
	// la idea es borrar el registro de rom_cantidades.
	// Sólo que hay que corroborar si se trata de una cuenta ACUMULATIVA
	// o una cuenta DETALLE.
	// Si fuera DETALLE habría que eliminar el registro y actualizar la
	// ACUMULATIVA (restando la subcantidad).
	// Si se tratara de una ACUMULATIVA también habría que borrar
	// las cuentas DETALLE relacionadas a la cuenta ACUMULATIVA
	// pero .... si no es ninguna de las dos, entonces es una cuenta
	// normal y habría que eliminarla solamente.
	
	// --------------------------------------------
	// primero sacamos la información del registro
	
	$query = "SELECT cantidad, tipo, asiento, cuenta, subcuenta, subcantidad, ejercicio, acumulable
				FROM rom_cantidades_pruebas
				WHERE IDcantidad = '$IDcantidad'";
	$result = mysql_query($query);
	$datos = mysql_fetch_assoc($result);
	
	// datos
	$cantidad = $datos['cantidad'];
	$tipo = $datos['tipo'];
	$asiento = $datos['asiento'];
	$cuenta = $datos['cuenta'];
	$subcuenta = $datos['subcuenta'];
	$subcantidad = $datos['subcantidad'];
	$ejercicio = $datos['ejercicio'];
	$acumulable = $datos['acumulable'];
	
	// EMPEZAMOS A DESCARTAR
	// primero, vemos si NO es una cuenta acumulable:
	if($subcuenta == "" && $acumulable == 0){
		// se trata de una cuenta normal
		// borramos sin problemas
		$query = "DELETE FROM rom_cantidades_pruebas WHERE IDcantidad = '$IDcantidad'";
		mysql_query($query);
		// funciona bien!!
	}
	// segundo, vemos si es una cuenta ACUMULATIVA
	if($acumulable == 1){
		// se trata de una cuenta ACUMULTIVA
		// aquí tenemos que borrar esta cuenta y 
		// borra cuentas DETALLE que hubiera
		//
		// Borramos la cuenta ACUMULATIVA
		$query = "DELETE FROM rom_cantidades_pruebas WHERE IDcantidad = '$IDcantidad'";
		mysql_query($query);
		// Borramos las cuentas detalla
		$query = "DELETE FROM rom_cantidades_pruebas
		WHERE asiento = '$asiento' AND ejercicio = '$ejercicio'
		AND subcuenta LIKE '$cuenta%'";
		mysql_query($query) or die(mysql_error());
		// funciona bien!!
	}
	// tercero, vemos que es una cuenta DETALLE
	if($subcuenta && $acumulable == 0){
		// se trata de una cuenta DETALLE
		// aquí tenemos que borrar la cuenta
		// y actualizar la cantidad del ACUMULABLE
		//
		// Borramos la cuenta DETALLE
		$query = "DELETE FROM rom_cantidades_pruebas WHERE IDcantidad = '$IDcantidad'";
		mysql_query($query);
		// Actualizamos la cuenta ACUMULATIVA
		// $subcuentaA = split("-",$subcuenta); FUNCION OBSOLETA PHP5.3
                $subcuentaA = explode("-",$subcuenta);
		$cuentaA = $subcuentaA[0];
		
		$query1 = "SELECT IDcantidad, cantidad, tipo FROM rom_cantidades_pruebas
				WHERE asiento = '$asiento'
				AND cuenta = '$cuentaA'
				AND ejercicio = '$ejercicio'
				AND acumulable = 1";
		$result1 = mysql_query($query1);
		$dato1 = mysql_fetch_assoc($result1);
		
		$oldcantidad = $dato1['cantidad'];
		$IDcantidadA = $dato1['IDcantidad'];
		$oldtipo = $dato1['tipo'];
		
		// empiezan los cálculos para actualizar el registro ACUMULATIVO
		
		if($oldtipo != $tipo){
			$newcantidad = $oldcantidad + $subcantidad;
			$nuevoTipo = $oldtipo;			
		}
		if($oldtipo == $tipo){
			$newcantidad = $oldcantidad - $subcantidad;
			if($newcantidad > 0){
				$nuevoTipo = $oldtipo;
			} else {
				if($tipo == "d"){
					$nuevoTipo = "h";
				} else {
					$nuevoTipo = "d";
				}
				$newcantidad = $newcantidad * -1;
			}
		}
		
		// fin
		
		if($newcantidad != 0) {
			$query2 = "UPDATE rom_cantidades_pruebas SET cantidad = '$newcantidad', tipo = '$nuevoTipo' WHERE IDcantidad = '$IDcantidadA'";
			mysql_query($query2) or die(mysql_error());
		} else {
			$query2 = "DELETE FROM rom_cantidades_pruebas WHERE IDcantidad = '$IDcantidadA'";
			mysql_query($query2);
		}
	}
		
		
	// después de borrar, nos regresamos...
	header("Location: ../diario_pruebas.php?IDejercicio=$IDejercicio&asiento=$asiento&metodo=$metodo&accion=eliminado");
	exit;
}
// fin

// Para borrar TODOS LOS DATOS de la base de datos
if(isset($_GET['delAllBD'])){
	$query = "TRUNCATE TABLE rom_ajustes";
	mysql_query($query) or die(mysql_error());
	// echo "AJUSTES borrado...<br />";
	$query = "TRUNCATE TABLE rom_asiento";
	mysql_query($query) or die(mysql_error());
	// echo "ASIENTOS borrado...<br />";
	$query = "TRUNCATE TABLE rom_balanza_aa";
	mysql_query($query) or die(mysql_error());
	// echo "BALANZAS borrado...<br />";
	$query = "TRUNCATE TABLE rom_balanza_da";
	mysql_query($query) or die(mysql_error());
	// echo "BALANZAS borrado...<br />";
	$query = "TRUNCATE TABLE rom_cantidades";
	mysql_query($query) or die(mysql_error());
	// echo "CANTIDADES borrado...<br />";
	$query = "TRUNCATE TABLE rom_ejercicio";
	mysql_query($query) or die(mysql_error());
	// echo "EJERCICIO borrado...<br />";
	
	header("Location: ../portada.php?accion=DBeliminada");
	exit;
}
// fin


## NUEVO 2014 ##

/**
 * VERSIÓN DEL SISTEMA
 * 
 * @author Fernando Magrosoto V.
 * @category Sistema Romero C
 * @copyright (c) 2014, Fernando Magrosoto
 */
define('VERSION', '6.0');


/**
 * EXTRAER EL NOMBRE DEL USUARIO
 * 
 * Función para extraer el nombre completo del usuario
 * en sesión, pasando como paŕametro el ID de usuario.
 * @author Fernando Magrosoto V
 * @category Sistema Romero C
 * @copyright (c) 2014, Fernando Magrosoto
 * @param int $IDusuario El ID de usuario
 */
function nombreUsuario($IDusuario)
{
    $query = "SELECT CONCAT(nombre, ' ', apellidos) "
            . "FROM rom_usuarios "
            . "WHERE IDusuario = $IDusuario";
    $result = mysql_query($query) or die(mysql_error());
    $data = mysql_fetch_row($result);
    $nombre = strtolower($data[0]);
    $nombre_ = ucwords($nombre);
    return utf8_encode($nombre_);
}
// fin función

/**
 * EXTRAER NOMBRE ENTIDAD
 * 
 * Función para extraer el nombre de la entidad
 * del ejericio de pŕactica.
 * Se pasa la tabla y el ID del ejericio como
 * paŕametro.
 * @author Fernando Magrosoto V
 * @category Sistema Romero C
 * @copyright (c) 2014, Fernando Magrosoto
 * @param int $IDpractica El ID de la práctica
 * @param string $tabla La tabla de la práctica
 */
function nombreEntidad($IDpractica, $tabla)
{
    $query = "SELECT Npractica "
            . "FROM $tabla "
            . "WHERE IDpractica = $IDpractica";
    $result = mysql_query($query) or die(mysql_error());
    $data = mysql_fetch_row($result);
    $entidad = strtolower($data[0]);
    $entidad_ = ucwords($entidad);
    return utf8_encode($entidad_);
}
// fin función

/**
 * SABER SI ES PŔACTICA NUEVA
 * 
 * Función para saber si se trata de una pŕactica
 * nueva (en cero) o ya tiene algún dato insertado.
 * @author Fernando Magrosoto V.
 * @category Sistema Romero C
 * @copyright (c) 2014, Fernando Magrosoto
 * @param int $IDpractica El ID de la práctica
 * @param string $tabla La tabla de las cifras
 */
function practicaStatus($IDpractica,$tabla)
{
    $query = "SELECT COUNT(*) "
            . "FROM $tabla "
            . "WHERE practica = $IDpractica";
    $result = mysql_query($query) or die(mysql_error());
    $data = mysql_fetch_row($result);
    if($data[0] == 0)
    {
        // No hay entradas, es nueva práctica
        return FALSE;
    } else {
        return TRUE;
    }
}
// fin función

/**
 * SABER SI HAY UNA FECHA DADA DE ALTA
 * 
 * Función para saber si hay una fecha dada de alta
 * en la tabla de prácticas.
 * @author Fernando Magrosoto V.
 * @category Sistema Romero C
 * @copyright (c) 2014, Fernando Magrosoto
 * @param int $IDpractica El ID de la práctica
 * @param string $tabla La tabla de la práctica
 * @param string $tipo EL tipo de fecha
 */
function fechaAlmacenada($IDpractica, $tabla, $tipo)
{
    $r = FALSE;
    $query = "SELECT $tipo FROM $tabla WHERE IDpractica = $IDpractica";
    $result = mysql_query($query) or die(mysql_error());
    $data = mysql_fetch_row($result);
    if($data != "")
    {
        $r = TRUE;
    }
    return $r;
}
// fin función


/**
 * VALIDAR PRACTICA
 * 
 * Función para validar si la práctica
 * es real.
 * @author Fernando Magrosoto V.
 * @category Sistema Romero C
 * @copyright (c) 2014, Fernando Magrosoto
 * @param int $IDpractica El ID de la práctica
 * @param string $tabla La tabla de la práctica
 * @return bool Falso si no hay
 */
function validarPractica($IDpractica, $tabla)
{
    $query = "SELECT COUNT(*) "
            . "FROM $tabla "
            . "WHERE IDpractica = $IDpractica";
    $result = mysql_query($query) or die(mysql_error());
    $data = mysql_fetch_row($result);
    if($data[0] == 0)
    {
        return FALSE;
    } else {
        return TRUE;
    }
}
// fin función


/**
 * EXTRAER FECHAS
 * 
 * Función para extraer fechas de las diferentes tablas
 * de prácticas.
 * @author Fernando Magrosoto V.
 * @category Sistema Romero C
 * @copyright (c) 2014, Fernando Magrosoto
 * @param int $IDpractica El ID de la práctica
 * @param string $tabla La tabla de la práctica
 * @param string $tipo EL tipo de fecha
 */
function extraerFechaPracticas($IDpractica,$tabla,$tipo)
{
    $query = "SELECT $tipo FROM $tabla WHERE IDpractica = $IDpractica";
    $result = mysql_query($query) or die(mysql_error());
    $data = mysql_fetch_row($result);
    $f = $data[0];
    return utf8_encode($f);
}
// fin función


/**
 * EXTRAER CANTIDADES DE CADA PRÁCTICA
 * 
 * Función para extraer el valor de cada campo.
 * @author Fernando Magrosoto V.
 * @category Sistema Romero C
 * @copyright (c) 2014, Fernando Magrosoto
 * @param string $celda El nombre de la celda
 * @param int $practica El ID de la practica
 * @param string $tabla La tabla del registro
 */
function extraerValorCampo($celda,$practica,$tabla)
{
    $q = "SELECT valor FROM $tabla "
            . "WHERE celda = '$celda' AND practica = $practica";
    $r = mysql_query($q) or die(mysql_error());
    $d = mysql_fetch_row($r);
    if($d[0] == "")
    {
        return "";
    } else {
        return $d[0];
    }
}
// fin función


/**
 * EXTRAER FECHA ACTUAL DE CADA PRÁCTICA
 * 
 * @param int $practica El ID de la practica
 * @param string $tabla La tabla del registro
 */
function extraerAactual($practica, $tabla)
{
    $query = "SELECT aactual "
            . "FROM $tabla "
            . "WHERE IDpractica = $practica";
    $res = mysql_query($query) or die(mysql_error());
    $data = mysql_fetch_row($res);
    if($data[0] == "")
    {
        $r = "No ha sido especificada";
    } else {
        $r = utf8_encode($data[0]);
    }
    return $r;
}
// fin función


/**
 * EXTRAER DATOS DE CUALQUIER TABLA
 * 
 * @param string $tabla La tabla en cuestión
 * @param string $campo El campo de búsqueda
 * @param int $ejercicio EL ID del ejercicio actual
 */
function extraInfo($tabla,$campo,$ejercicio)
{
    $query = "SELECT $campo FROM $tabla WHERE IDpractica = $ejercicio";
    $result = mysql_query($query) or die(mysql_error());
    $data = mysql_fetch_row($result);
    $respuesta = utf8_encode($data[0]);
    
    if($respuesta == "")
    {
        $respuesta = "sin asignar";
    }
    
    return $respuesta;
}
// fin función

/**
 * EXTRAER DATOS DE CUALQUIER TABLA
 * 
 * @param string $tabla La tabla en cuestión
 * @param string $campo El campo de búsqueda
 * @param int $ejercicio EL ID del ejercicio actual
 */
function extraRazones($tabla,$campo,$ejercicio)
{
    $query = "SELECT $campo FROM $tabla WHERE practica = $ejercicio";
    $result = mysql_query($query) or die(mysql_error());
    $data = mysql_fetch_row($result);
    $respuesta = $data[0];
    
    return $respuesta;
}