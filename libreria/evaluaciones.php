<?php

/**
 * VERIFICAR QUE HAYA UN REGISTRO DE CUESTIONARIO LLENO
 * 
 * Función para verificar que el usuario haya agregado un 
 * registro de cuestionario con el capítulo pasado como 
 * parámetro. @return int EL ID del registro
 * @author Fernando Magrosoto V.
 * @copyright (c) 2014, Fernando Magrosoto
 * @category Sistema Romero C
 * @param int $capitulo El número del capítulo
 * @param int $usuario EL ID del usuario
 * @param string $fecha La fecha de hoy
 */
function verificarCuestionario($capitulo,$usuario,$fecha)
{
    $q = "SELECT id FROM rom_cuestionario "
            . "WHERE usuario = $usuario "
            . "AND capitulo = $capitulo";
    $r = mysql_query($q) or die(mysql_error());
    $n = mysql_num_rows($r);
    $d = mysql_fetch_row($r);
    
    if($n == 0)
    {
        // No existe ningún registro, es decir, registro nuevo.
        // Ahora, agregar un nuevo registro.
        $qN = "INSERT INTO rom_cuestionario "
                . "(usuario,capitulo,fecha) "
                . "VALUES "
                . "($usuario,$capitulo,'$fecha')";
        if(mysql_query($qN) or die(mysql_error()))
        {
            $registro = mysql_insert_id();
        }
    } else 
    {
        $registro = $d[0];
    }
    
    return $registro;
}
// fin


/**
 * EXTRAER INFORMACIÓN DE LA BD DE CUESTIONARIOS
 * 
 * Extraer la información del campo pasado como parámetro 
 * de la BD de cuestionario.
 * @author Fernando Magrosoto V.
 * @copyright (c) 2014, Fernando Magrosoto V.
 * @category Sistema Romero C
 * @param string $campo El campo deseado
 * @param int $registro El ID del registro
 * @return string Valor del campo
 */
function xCamCues($campo,$registro)
{
    $q = "SELECT $campo FROM rom_cuestionario "
            . "WHERE id = $registro";
    $r = mysql_query($q) or die(mysql_error());
    $d = mysql_fetch_row($r);
    $valor = utf8_encode($d[0]);
    return $valor;
}
// fin


/**
 * EXTRAER INFORMACIÓN DE LA BD DE PREGUNTAS
 * 
 * Extraer la información de la pregunta pasada como
 * parámetro.
 * @author Fernando Magrosoto V.
 * @copyright (c) 2014, Fernando Magrosoto
 * @category Sistema Romero C
 * @param int $cuestiionario El ID del cuestionario
 * @param int $usuario El ID de usuario
 * @param int $capitulo El capítulo
 * @param string $pregunta El ID de la pregunta
 * @param return $string La respuesta
 */
function xPregCues($cuestionario,$usuario,$capitulo,$pregunta)
{
    $q = "SELECT respuesta FROM rom_cuestionario_respuestas "
            . "WHERE cuestionario = $cuestionario "
            . "AND usuario = $usuario "
            . "AND capitulo = $capitulo "
            . "AND pregunta = '$pregunta'";
    $r = mysql_query($q) or die(mysql_error());
    $n = mysql_num_rows($r);
    if($n == 0)
    {
        $respuesta = "";
    } else {
        $d = mysql_fetch_row($r);
        $respuesta = utf8_encode($d[0]);
    }
    return $respuesta;
}
// fin



/**
 * SELECCIONAR EL CAMPO DE CORRELACIONES
 * 
 * Script para hacer que el campo OPTION quede
 * seleccionado, comprobando la respuesta con el
 * valor de cada opción.
 * @author Fernando Magrosoto V.
 * @copyright (c) 2014, Fernando Magrosoto
 * @category Sistema Romero C
 * @param string $valor El valor del campo opción
 * @param string $respuesta La respuesta de la pregunta
 */
function selOpcion($valor,$respuesta)
{
    if($valor == $respuesta)
    {
        echo "selected";
    }
}
// fin