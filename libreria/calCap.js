var calificarCorrelacion = function(preg,num,res)
{
    if(confirm('¿Estás seguro de calificar? Revisa tus respuestas. Una vez calificado mo podrás corregirlas.'))
    {
        var calificacion = 0;
        var respuestas = [];
        for(var r=1; r<=num; r++)
        {
            n = document.getElementById(preg+'_'+r).value;
            respuestas.push(parseInt(n));
        }

        // Calificar
        for(var i=0; i<num; i++)
        {
            var parcial = preg+"_"+(i+1)+"-parcial";
            var msg = document.getElementById(parcial);
            if(res[i] === respuestas[i])
            {
                calificacion ++;
                msg.className = "correcto";
                msg.innerHTML = "¡¡ Correcto !!";
            } else 
            {
                msg.className = "incorrecto";
                msg.innerHTML = "¡¡ Incorrecto !!";
            }
        };

        ponerCalificacion(preg,calificacion,num);
        protegerCorrelaciones();
    }
};


var ponerCalificacion = function(preg,cal,num)
{
    var con = preg+"_cal";
    var res = document.getElementById(con);
    var calificacion = Math.round((cal/num) * 10);
    if(calificacion < 0){ calificacion = 0; }
    var msg = "";
    switch (calificacion)
    {
        case 10:
            msg = "Excelente. Buen trabajo.";
            break;
        case 9:
            msg = "Muy bien. Sigue así.";
            break;
        case 8:
            msg = "Bien. Esfuérzate más.";
            break;
        case 7:
            msg = "Debes de estudiar más. Échale muchas ganas.";
            break;
        case 6:
            msg = "Definitivamente debes de estudiar.";
            break;
        case 5:
            msg = "Muy mal. Tienes que pasar más tiempo en el libro.";
            break;
        default:
            msg = "Terrible calificación. ¿Dices que estudiaste?";
    }
    res.className = 'calificacion';
    res.innerHTML = "Tu calificación es <strong>: "+calificacion+"</strong>. | "+msg;
};


var calificarCompletar = function(preg,num,res)
{
    if(confirm('¿Estás seguro de calificar? Revisa tus respuestas. Una vez calificado no podrás corregirlas.'))
    {
        var calificacion = 0;
        var respuestas = [];
        for(var r=1; r<=num; r++)
        {
            n = document.getElementById(preg+'_'+r).value;
            respuestas.push(n);
        }
        // Calificar
        for(var i=0; i<num; i++)
        {
            var parcial = preg+"_"+(i+1);
            var msg = document.getElementById(parcial);
            if(res[i].toUpperCase() === respuestas[i].toUpperCase())
            {
                calificacion ++;
                msg.className = "corr";
            } else 
            {
                msg.className = "incorr";
            }
        };

        ponerCalificacion(preg,calificacion,num);
        protegerCompletar();
    }
};


var protegerPLibres = function()
{
    var preglibres = document.querySelectorAll('.pregunta-libre > textarea');
    var numPreglibres = preglibres.length;
    for(var i = 0; i<numPreglibres; i++)
    {
        if(preglibres[i].value !== "")
        {
            preglibres[i].readOnly = 'true';
        }
    }
};
    
var protegerCorrelaciones = function()
{
    var correlaciones = document.querySelectorAll('.correlaciones select');
    var numCorrelaciones = correlaciones.length;
    for(var i = 0; i<numCorrelaciones; i++)
    {
        if(correlaciones[i].value !== 'no')
        {
            correlaciones[i].disabled = 'true';
        }
    }
};

var protegerCompletar = function()
{
    var completar = document.querySelectorAll('.completar-lineas input');
    var numCompletar = completar.length;
    for(var i = 0; i<numCompletar; i++)
    {
        if(completar[i].value !== "")
        {
            completar[i].readOnly = 'true';
        }
    }
};

var protegerVerFal = function()
{
    // primero los textarea
    var textarea = document.querySelectorAll('.verdadero-falso textarea');
    var numtextarea = textarea.length;
    for(var i = 0; i<numtextarea; i++)
    {
        if(textarea[i].value !== "")
        {
            textarea[i].readOnly = 'true';
        }
    }
    
    // luego los radio
    var radios = document.querySelectorAll('.verdadero-falso input[type=\'radio\']');
    var numradios = radios.length;
    for(var i = 0; i<numradios; i++)
    {
        if(radios[i].checked)
        {
            var nombre = radios[i].name;
            var cada = document.querySelectorAll('.verdadero-falso input[name=\''+nombre+'\']');
            var numcada = cada.length;
            for(var a = 0; a < numcada; a++)
            {
                if(!cada[a].checked)
                {
                    cada[a].disabled = 'true';
                }
            }
        }
    }
};

var protegerVerFalSimple = function(id)
{
    // luego los radio
    var radios = document.querySelectorAll('#'+id+' input[type=\'radio\']');
    var numradios = radios.length;
    for(var i = 0; i<numradios; i++)
    {
        if(radios[i].checked)
        {
            var nombre = radios[i].name;
            var cada = document.querySelectorAll('#'+id+' input[name=\''+nombre+'\']');
            var numcada = cada.length;
            for(var a = 0; a < numcada; a++)
            {
                if(!cada[a].checked)
                {
                    cada[a].disabled = 'true';
                }
            }
        }
    }
};


var calificarVerFal = function(preg,num,resR,resI)
{
    if(confirm('¿Estás seguro de calificar? Revisa tus respuestas. Una vez calificado no podrás corregirlas.'))
    {
        calificacion = 0;
        // Primero los RADIOS
        var resRadio = [];
        var boton = document.querySelectorAll('.verdadero-falso input[type=\'radio\']');
        var botones = boton.length;
        for(var a = 0; a<botones; a++)
        {
            if(boton[a].checked)
            {
                resRadio.push(boton[a].value);
            }
        }
        // Calificar
        var s = '';
        for(var z=0; z<num; z++)
        {
            if(resR[z].toUpperCase() === resRadio[z].toUpperCase())
            {
                calificacion ++;
                s = (resRadio[z] === 'verdadero')?'v':'f';
                document.getElementById(preg+'_'+(z+1)+s+'_parcial').style.background = 'rgb(150,255,150)';
            } else 
            {
                s = (resRadio[z] === 'verdadero')?'v':'f';
                document.getElementById(preg+'_'+(z+1)+s+'_parcial').style.background = 'rgb(255,150,150)';
            }
        };

        // Después los TEXT
        var respuestas = [];
        for(var r=1; r<=num; r++)
        {
            n = document.getElementById(preg+'pa_'+r).value;
            respuestas.push(n);
        }
        // Calificar
        for(var i=0; i<num; i++)
        {
            var parcial = preg+"pa_"+(i+1)+'_parcial';
            var msg = document.getElementById(parcial);
            if(resI[i].toUpperCase() === respuestas[i].toUpperCase())
            {
                calificacion ++;
                msg.style.background = 'rgb(150,255,150)';
            } else 
            {
                msg.style.background = 'rgb(255,150,150)';
            }
        };

        ponerCalificacion(preg,calificacion,num*2);
        protegerVerFal();
    }
};


var calificarVerFalSimple = function(preg,num,resR,id)
{
    if(confirm('¿Estás seguro de calificar? Revisa tus respuestas. Una vez calificado no podrás corregirlas.'))
    {
        calificacion = 0;
        // Primero los RADIOS
        var resRadio = [];
        var boton = document.querySelectorAll('#'+id+' input[type=\'radio\']');
        var botones = boton.length;
        for(var a = 0; a<botones; a++)
        {
            if(boton[a].checked)
            {
                resRadio.push(boton[a].value);
            }
        }
        // Calificar
        var s = '';
        for(var z=0; z<num; z++)
        {
            if(resR[z].toUpperCase() === resRadio[z].toUpperCase())
            {
                calificacion ++;
                s = (resRadio[z] === 'verdadero')?'v':'f';
                document.getElementById(id+'_'+preg+'_'+(z+1)+s+'_parcial').style.background = 'rgb(150,255,150)';
            } else 
            {
                s = (resRadio[z] === 'verdadero')?'v':'f';
                document.getElementById(id+'_'+preg+'_'+(z+1)+s+'_parcial').style.background = 'rgb(255,150,150)';
            }
        };

        ponerCalificacion(id+'_'+preg,calificacion,num);
        protegerVerFalSimple(id);
    }
};


var calificarOpMul = function(preg,resp)
{
  if(confirm('¿Estás seguro de calificar? Revisa tus respuestas. Una vez calificado no podrás corregirlas.'))
  {
    var calificacion = 0;
    var lista = [];
    var cajas = document.querySelectorAll('.opcion-multiple input[name='+preg+']');
    var numCajas = cajas.length;
    
    // Primero popular el array con las respuestas
    for(var i=0; i<numCajas; i++)
    {
        if(cajas[i].checked)
        {
            lista.push(cajas[i].value);
        }
    }
    
    // Luego compararlas con el array de respuestas válidas
    var numLista = lista.length;
    for(var a = 0; a<numLista; a++)
    {
        var c = preg+'_'+lista[a]+'_parcial';
        var parcial = document.getElementById(c);
        // buscar lista[a] con alguna coincidencia del array de respuestas
        if(resp.indexOf(lista[a]) !== -1)
        {
            calificacion++;
            parcial.style.background = 'rgb(150,255,150)';
        } else {
            calificacion--;
            parcial.style.background = 'rgb(255,150,150)';
        }
    }
    ponerCalificacion(preg,calificacion,resp.length);
    protegerOpMul();
  }
};

var protegerOpMul = function()
{
  var cajas = document.querySelectorAll('.opcion-multiple input[type=\'checkbox\']');
  var numCajas = cajas.length;
  for(var i = 0; i < numCajas; i++)
  {
      if(cajas[i].checked)
      {
          cajas[i].disabled = 'true';
      }
  }
};


var calificarOpMulRadio = function(preg,num,resp,id)
{
    if(confirm('¿Estás seguro de calificar? Revisa tus respuestas. Una vez calificado no podrás corregirlas.'))
    {
        // Primero pasar a un array las respuestas
        var calificacion = 0;
        var respuestas = [];
        var campos = document.querySelectorAll('#'+id+' input[type=\'radio\']');
        var numCampos = campos.length;
        for(var a = 0;a < numCampos; a++)
        {
            if(campos[a].checked)
            {
                respuestas.push(campos[a].value);
            }
        }

        // Luego compararlos con las respuestas correctas
        for(var b = 0; b<num;b++)
        {
            if(resp[b].toUpperCase() === respuestas[b].toUpperCase())
            {
                calificacion ++;
                // poner verde la celda
                var celda = preg + '_' + (b+1) + respuestas[b] + '_parcial';
                var td = document.getElementById(celda);
                td.style.background = 'rgb(150,255,150)';
            } else {
                // poner en rojo la celda
                var celda = preg + '_' + (b+1) + respuestas[b] + '_parcial';
                var td = document.getElementById(celda);
                td.style.background = 'rgb(255,150,150)';
            }
        }
        ponerCalificacion(preg,calificacion,num);
        protegerOpMulRadio(id);
    }
};


var protegerOpMulRadio = function(id)
{
    var elementos = document.querySelectorAll('#'+id+' input[type=\'radio\']');
    var num = elementos.length;
    for(var i = 0;i<num;i++)
    {
        if(elementos[i].checked)
        {
            var cada = elementos[i].name;
            var radios = document.querySelectorAll('#'+id+' input[name=\''+cada+'\']');
            var numRadios = radios.length;
            for(var z = 0; z<numRadios; z++)
            {
                if(!radios[z].checked)
                {
                    radios[z].disabled = 'true';
                }
            }
        }
    }
};