// JavaScript Document

/* Inicialización de la instancia XMLHttpRequest */
/* ... es decir... AJAX */
function crearInstancia() {
	
	XMLHttp = false;
	
	if (window.XMLHttpRequest) {
		return new XMLHttpRequest();
	} else if (window.ActiveXObject) {
		var versiones = ["Msxml2.XMLHTTP.7.0", "Msxml2.XMLHTTP.6.0", "Msxml2.XMLHTTP.5.0", "Msxml2.XMLHTTP.4.0", "Msxml2.XMLHTTP.3.0", "Msxml2.XMLHTTP", "Microsoft.XMLHTTP"];
		for (var i=0;i<versiones.length;i++) {
			try {
				XMLHttp = new ActiveXObject(versiones[i]);
				if (XMLHttp) {
					return XMLHttp;
					break;
				}
			} catch (e) {};
		}
	}
}
/* AJAX ha quedado inicializado */ 



/* mandar al backend la información de la opción seleccionada */


function buscarTipo() {
	
	valor = document.Ncantidad.grupocuenta.value; // valor del SELECT	
	XMLHttp = crearInstancia(); // creamos la instancia de AJAX
	
	if(XMLHttp) {
		var url = "libreria/ajaxMetodos.php?Tcuenta="+valor // mandamos valor
		XMLHttp.open("GET", url, true)
		XMLHttp.onreadystatechange = pintarTipo // llamamos función
		XMLHttp.send(null)
	} else {
		alert('No se pudo crear la instancia');
	}
}
// fin

/* función para poner el resultado de responseText en la página */
function pintarTipo() {
	
	if (XMLHttp.readyState == 4) {
		
		var cadena = new String (XMLHttp.responseText)
		var segmentos = cadena.split(",")
		var numSegmentos = segmentos.length
		
		// poner un SELECCIONAR UNO... en el campo
		document.getElementById("tipocuenta").options[0] = new Option ("Seleccione uno...",0)
		// llenar el combo
		for (elemento=1; elemento<numSegmentos+1; elemento++)
		{
			var datos = segmentos[elemento-1].split("-");
			document.getElementById("tipocuenta").options[elemento] = new Option (datos[1],datos[0])
			// listo, funcional perfectamente
		}
		
		// borrar las opciones anteriores del combo
		document.getElementById("cuenta").length = 0
		document.getElementById("subcuenta").length = 0
		// opciones borradas
	}
}
// fin

function buscarCuenta() {
	
	valor = document.Ncantidad.tipocuenta.value; // valor del SELECT
	metodo = document.Ncantidad.metodo.value; // valor del SELECT
	
	XMLHttp = crearInstancia();
	
	if(XMLHttp) {
		var url = "libreria/ajaxMetodos.php?subcuenta="+valor+"&metodo="+metodo // mandamos valor
		XMLHttp.open("GET", url, true)
		XMLHttp.onreadystatechange = pintarCuenta // llamamos función
		XMLHttp.send(null)
	} else {
		alert('No se pudo crear la instancia');
	}
}
// fin

/* función para poner el resultado de responseText en la página */
function pintarCuenta() {
	
		// borrar las opciones anteriores del combo
		document.getElementById("cuenta").length = 0
		document.getElementById("subcuenta").length = 0
		// opciones borradas
	
	if (XMLHttp.readyState == 4) {
		
		var cadena = new String (XMLHttp.responseText)
		var segmentos = cadena.split(",")
		var numSegmentos = segmentos.length
		
		// poner un SELECCIONAR UNO en el campo
		document.getElementById("cuenta").options[0] = new Option ("Seleccione uno...",0)
		// llenamos combo
		for (elemento=1; elemento<numSegmentos+1; elemento++)
		{
			var datos = segmentos[elemento-1].split("-");
			document.getElementById("cuenta").options[elemento] = new Option ((datos[0]+" - "+datos[1]),datos[0])
			// listo, funcional perfectamente
		}		
	}
}
// fin

function buscarDetalle() {
	
	valor = document.Ncantidad.cuenta.value; // valor del SELECT
	
	XMLHttp = crearInstancia();
	
	if(XMLHttp) {
		var url = "libreria/ajaxMetodos.php?cuenta="+valor // mandamos valores
		XMLHttp.open("GET", url, true)
		XMLHttp.onreadystatechange = pintarDetalle // llamamos función
		XMLHttp.send(null)
	} else {
		alert('No se pudo crear la instancia');
	}
}
// fin

/* función para poner el resultado de responseText en la página */
function pintarDetalle() {
	
	// borrar las opciones anteriores del combo
	document.getElementById("subcuenta").length = 0
	// opciones borradas
	
	if (XMLHttp.readyState == 4) {
		
		var cadenaD = new String (XMLHttp.responseText)
		
		if(cadenaD == "no"){ // si no hay cuentas detalle (subcuentas)...
			document.getElementById("subcuenta").length = 0;
			document.getElementById("subcuenta").disabled=true;
			document.getElementById("cantidad").focus();
		} else { // si sí hay subcuentas...	
			document.getElementById("subcuenta").disabled=false;
			var segmentosD = cadenaD.split(",")
			var numSegmentosD = segmentosD.length
			
			// poner un SELECCIONAR UNO en el campo
			document.getElementById("subcuenta").options[0] = new Option ("Seleccione uno...","seleccionar");
			// llenamos combo
			for (elementoD=1; elementoD<numSegmentosD+1; elementoD++)
			{
			var datosD = segmentosD[elementoD-1].split("|");
			document.getElementById("subcuenta").options[elementoD] = new Option ((datosD[0]+" - "+datosD[1]),datosD[0])
			// listo, funcional perfectamente
			}
		}
	}
}
// fin

/* función para las páginas AUXILIARES */
function buscarCuentasDetalle()
{
	valor = document.listaCuentas.acumulativa.value; // valor del SELECT
	IDejercicio = document.listaCuentas.IDejercicio.value;
	metodo = document.listaCuentas.metodo.value;
	
	XMLHttp = crearInstancia();
	
	if(XMLHttp) {
		var url = "libreria/ajaxMetodos.php?cuentaAcumulativa="+valor+"&IDejercicio="+IDejercicio+"&metodo="+metodo // mandamos valores
		XMLHttp.open("GET", url, true)
		XMLHttp.onreadystatechange = pintarCuentasDetalle // llamamos función
		XMLHttp.send(null)
	} else {
		alert('No se pudo crear la instancia');
	}
}
// fin
function pintarCuentasDetalle() {
	
		// borrar las opciones anteriores del combo
		document.getElementById("detalle").length = 0
		// opciones borradas
	
	if (XMLHttp.readyState == 4) {
		
		var cadena = new String (XMLHttp.responseText)
		var segmentos = cadena.split(",")
		var numSegmentos = segmentos.length
		
		// poner un SELECCIONAR UNO en el campo
		document.getElementById("detalle").options[0] = new Option ("Seleccione uno...",0)
		// llenamos combo
		for (elemento=1; elemento<numSegmentos+1; elemento++)
		{
			var datos = segmentos[elemento-1].split("|");
			document.getElementById("detalle").options[elemento] = new Option ((datos[0]+" - "+datos[1]),datos[0])
			// listo, funcional perfectamente
		}
		document.getElementById("submit").disabled=false;
	}
}
// fin

// cmprobar nombre del ejercicio (PORTADA)
function comprobarNejercicio()
{
	valor = document.NuBD.Nejercicio.value; // valor del SELECT
	
	XMLHttp = crearInstancia();
	
	if(XMLHttp) {
		var url = "libreria/ajaxMetodos.php?Nejercicio="+valor // mandamos valores
		XMLHttp.open("GET", url, true)
		XMLHttp.onreadystatechange = pintarRespNejercicio // llamamos función
		XMLHttp.send(null)
	} else {
		alert('No se pudo crear la instancia');
	}
}
// fin

function pintarRespNejercicio()
{
	if (XMLHttp.readyState == 2) {
		document.getElementById("submitNuBD").value="Verificando nombre...";
	}
	if (XMLHttp.readyState == 4) {
		
		var cadena = new String (XMLHttp.responseText)
		if(cadena == "permitido"){
			document.getElementById("submitNuBD").disabled=false;
			document.getElementById("submitNuBD").value="Empezar";
		} else {
			document.getElementById("submitNuBD").value="¡Ya hay un ejercicio con el mismo nombre!";
			document.getElementById("Nejercicio").value="";
			document.getElementById("Nejercicio").focus();
		}
	}
}
// fin
