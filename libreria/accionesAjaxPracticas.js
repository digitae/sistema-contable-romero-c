var timer;
ajaxCambiarFecha = function(id,practica,tabla)
{
    pararTimer();
    timer = window.setTimeout(function(){
        var fecha = document.getElementById(id).value;

        $.post('libreria/ajaxPracticas.php',{
            modificarFechaPractica: true,
            tipo: id,
            fecha: fecha,
            tabla: tabla,
            practica: practica
            },function(respuesta){
                if(respuesta === 'done')
                {
                    // hacer algo para advertir OK
                } else {
                    // hacer algo paara advertir ERROR
                }
            });

    },1000);        

};
//

var pararTimer = function()
{
    clearTimeout(timer);
};
//

var ajaxCambiValor = function(cantidad,id,practica,tabla)
{
  if(isNaN(cantidad))
  {
      // Por el momento, vamos a dejar que introduzcan datos
      // negativos. Esto conlleva un riesgo...
      // alert('No es un número');
      // document.getElementById(id).value = "";
  } else {
      pararTimer();
      timer = window.setTimeout(function(){
          $.post('libreria/ajaxPracticas.php',{
          celda: id,
          valor: cantidad,
          tabla: tabla,
          modificarCelda: true,
          practica: practica
      },function(respuesta){
          if(respuesta === 'done')
          {
              // Hacer algo cuando OK
              sumarActivosR();
          } else {
              // Hacer algo cuando ERROR
          }
      });
      },500);
  }
};
//

///////////////////
// COLUMNA ACTIVO
///////////////////

/**
 * RESULTADO DE LA SUMA EN D28
 */
function sumaD28(practica,tabla)
{
  $.post('libreria/ajaxPracticas.php',{
      sumad28: true,
      practica: practica,
      tabla: tabla
  }).done(function(respuesta){
      var campo = document.getElementById('d28');
      campo.innerHTML = "$ "+respuesta;
      sumaD38(practica,tabla);
  });  
};
//

/**
 * RESULTADO DE LA SUMA EN D38
 */
function sumaD38(practica,tabla)
{
  $.post('libreria/ajaxPracticas.php',{
      sumad38: true,
      practica: practica,
      tabla: tabla
  }).done(function(respuesta){
      var campo = document.getElementById('d38');
      campo.innerHTML = "$ "+respuesta;
      sumaD47(practica,tabla);
  });  
};
//

/**
 * RESULTADO DE LA SUMA EN D47
 */
function sumaD47(practica,tabla)
{
  $.post('libreria/ajaxPracticas.php',{
      sumad47: true,
      practica: practica,
      tabla: tabla
  }).done(function(respuesta){
      var campo = document.getElementById('d47');
      campo.innerHTML = "$ "+respuesta;
      sumaD54(practica,tabla);
  });  
};
//

/**
 * RESULTADO DE LA SUMA EN D54
 */
function sumaD54(practica,tabla)
{
  $.post('libreria/ajaxPracticas.php',{
      sumad54: true,
      practica: practica,
      tabla: tabla
  }).done(function(respuesta){
      var campo = document.getElementById('d54');
      campo.innerHTML = "$ "+respuesta;
      sumaD72(practica,tabla);
  });  
};
//

/**
 * RESULTADO DE LA SUMA EN D72
 */
function sumaD72(practica,tabla)
{
  $.post('libreria/ajaxPracticas.php',{
      sumad72: true,
      practica: practica,
      tabla: tabla
  }).done(function(respuesta){
      var campo = document.getElementById('d72');
      campo.innerHTML = "$ "+respuesta;
      sumaD79(practica,tabla);
  });  
};
//

/**
 * RESULTADO DE LA SUMA EN D79
 */
function sumaD79(practica,tabla)
{
  $.post('libreria/ajaxPracticas.php',{
      sumad79: true,
      practica: practica,
      tabla: tabla
  }).done(function(respuesta){
      var campo = document.getElementById('d79');
      campo.innerHTML = "$ "+respuesta;
      sumaD84(practica,tabla);
  });  
};
//

/**
 * RESULTADO DE LA SUMA EN D84
 */
function sumaD84(practica,tabla)
{
  $.post('libreria/ajaxPracticas.php',{
      sumad84: true,
      practica: practica,
      tabla: tabla
  }).done(function(respuesta){
      var campo = document.getElementById('d84');
      campo.innerHTML = "$ "+respuesta;
      sumaD92(practica,tabla);
  });  
};
//

/**
 * RESULTADO DE LA SUMA EN D92
 */
function sumaD92(practica,tabla)
{
  $.post('libreria/ajaxPracticas.php',{
      sumad92: true,
      practica: practica,
      tabla: tabla
  }).done(function(respuesta){
      var campo = document.getElementById('d92');
      campo.innerHTML = "$ "+respuesta;
      totalE54();
  });  
};
//

/**
 * RESULTADO DE LA SUMA EN E54
 */
function totalE54()
{
  var d28 = document.getElementById('d28');
  var d38 = document.getElementById('d38');
  var d47 = document.getElementById('d47');
  var d54 = document.getElementById('d54');
  
  var suma = limpiarCantidad(d28.innerHTML)+
          limpiarCantidad(d38.innerHTML)+
          limpiarCantidad(d47.innerHTML)+
          limpiarCantidad(d54.innerHTML);
  var total = document.getElementById('e54');
  total.innerHTML = "$ "+suma;
  totalE79();
};
//

/**
 * RESULTADO DE LA SUMA EN E79
 */
function totalE79()
{
  var d72 = document.getElementById('d72');
  var d79 = document.getElementById('d79');
  
  
  var suma = limpiarCantidad(d72.innerHTML)+
          limpiarCantidad(d79.innerHTML);
  var total = document.getElementById('e79');
  total.innerHTML = "$ "+suma.toFixed(2);
  totalE92();
};
//

/**
 * RESULTADO DE LA SUMA EN E92
 */
function totalE92()
{
  var d84 = document.getElementById('d84');
  var d92 = document.getElementById('d92');
  
  
  var suma = limpiarCantidad(d84.innerHTML)+
          limpiarCantidad(d92.innerHTML);
  var total = document.getElementById('e92');
  total.innerHTML = "$ "+suma.toFixed(2);
};
//

/**
 * QUITAR EL SIGNO DE PESOS Y LOS ESPACIOS EN BLANCO
 * @param {string} basura La cifra
 */
function limpiarCantidad(basura)
{
    var a = basura.split('$');
    var b = a[1].trim();
    var patron = ',';
    var c = b.replace(patron,'');
    var limpiada = parseFloat(c);
    return limpiada;
}

///////////////////
// COLUMNA PASIVO
///////////////////

/**
 * RESULTATDO DE LA SUMA EN D28
 */
function sumaG28(practica,tabla)
{
  $.post('libreria/ajaxPracticas.php',{
      sumag28: true,
      practica: practica,
      tabla: tabla
  }).done(function(respuesta){
      var campo = document.getElementById('g28');
      campo.innerHTML = "$ "+respuesta;
      sumaG38(practica,tabla);
  });  
};
//

/**
 * RESULTATDO DE LA SUMA EN D38
 */
function sumaG38(practica,tabla)
{
  $.post('libreria/ajaxPracticas.php',{
      sumag38: true,
      practica: practica,
      tabla: tabla
  }).done(function(respuesta){
      var campo = document.getElementById('g38');
      campo.innerHTML = "$ "+respuesta;
      sumaG47(practica,tabla);
  });  
};
//

/**
 * RESULTATDO DE LA SUMA EN D47
 */
function sumaG47(practica,tabla)
{
  $.post('libreria/ajaxPracticas.php',{
      sumag47: true,
      practica: practica,
      tabla: tabla
  }).done(function(respuesta){
      var campo = document.getElementById('g47');
      campo.innerHTML = "$ "+respuesta;
      sumaG54(practica,tabla);
  });  
};
//

/**
 * RESULTATDO DE LA SUMA EN D54
 */
function sumaG54(practica,tabla)
{
  $.post('libreria/ajaxPracticas.php',{
      sumag54: true,
      practica: practica,
      tabla: tabla
  }).done(function(respuesta){
      var campo = document.getElementById('g54');
      campo.innerHTML = "$ "+respuesta;
      sumaG72(practica,tabla);
  });  
};
//

/**
 * RESULTATDO DE LA SUMA EN D72
 */
function sumaG72(practica,tabla)
{
  $.post('libreria/ajaxPracticas.php',{
      sumag72: true,
      practica: practica,
      tabla: tabla
  }).done(function(respuesta){
      var campo = document.getElementById('g72');
      campo.innerHTML = "$ "+respuesta;
      sumaG79(practica,tabla);
  });  
};
//

/**
 * RESULTATDO DE LA SUMA EN D79
 */
function sumaG79(practica,tabla)
{
  $.post('libreria/ajaxPracticas.php',{
      sumag79: true,
      practica: practica,
      tabla: tabla
  }).done(function(respuesta){
      var campo = document.getElementById('g79');
      campo.innerHTML = "$ "+respuesta;
      sumaG84(practica,tabla);
  });  
};
//

/**
 * RESULTATDO DE LA SUMA EN D84
 */
function sumaG84(practica,tabla)
{
  $.post('libreria/ajaxPracticas.php',{
      sumag84: true,
      practica: practica,
      tabla: tabla
  }).done(function(respuesta){
      var campo = document.getElementById('g84');
      campo.innerHTML = "$ "+respuesta;
      sumaG92(practica,tabla);
  });  
};
//

/**
 * RESULTATDO DE LA SUMA EN D92
 */
function sumaG92(practica,tabla)
{
  $.post('libreria/ajaxPracticas.php',{
      sumag92: true,
      practica: practica,
      tabla: tabla
  }).done(function(respuesta){
      var campo = document.getElementById('g92');
      campo.innerHTML = "$ "+respuesta;
      totalH54();
  });  
};
//

/**
 * RESULTADO DE LA SUMA EN E54
 */
function totalH54()
{
  var g28 = document.getElementById('g28');
  var g38 = document.getElementById('g38');
  var g47 = document.getElementById('g47');
  var g54 = document.getElementById('g54');
  
  var suma = limpiarCantidad(g28.innerHTML)+
          limpiarCantidad(g38.innerHTML)+
          limpiarCantidad(g47.innerHTML)+
          limpiarCantidad(g54.innerHTML);
  var total = document.getElementById('h54');
  total.innerHTML = "$ "+suma;
  totalH79();
};
//

/**
 * RESULTADO DE LA SUMA EN E79
 */
function totalH79()
{
  var g72 = document.getElementById('g72');
  var g79 = document.getElementById('g79');
  
  
  var suma = limpiarCantidad(g72.innerHTML)+
          limpiarCantidad(g79.innerHTML);
  var total = document.getElementById('h79');
  total.innerHTML = "$ "+suma.toFixed(2);
  totalH92();
};
//

/**
 * RESULTADO DE LA SUMA EN E92
 */
function totalH92()
{
  var g84 = document.getElementById('g84');
  var g92 = document.getElementById('g92');
  
  
  var suma = limpiarCantidad(g84.innerHTML)+
          limpiarCantidad(g92.innerHTML);
  var total = document.getElementById('h92');
  total.innerHTML = "$ "+suma.toFixed(2);
};
//


///////////////////////
// PRÁCTICA DE CAMBIOS
///////////////////////

var ajaxCambiaValorCambios = function(cantidad,id,practica,tabla)
{
  if(isNaN(cantidad))
  {
      // alert('No es un número');
      // document.getElementById(id).value = "";
  } else {
      pararTimer();
      timer = window.setTimeout(function(){
          $.post('libreria/ajaxPracticas.php',{
          celda: id,
          valor: cantidad,
          tabla: tabla,
          modificarCelda: true,
          practica: practica
      },function(respuesta){
          if(respuesta === 'done')
          {
              // Hacer algo cuando OK
              sumarActivos();
          } else {
              // Hacer algo cuando ERROR
          }
      });
      },500);
  }
};
//

var ajaxCambiaValorPR = function(cantidad,id,practica,tabla)
{
  if(isNaN(cantidad))
  {
      // alert('No es un número');
      // document.getElementById(id).value = "";
  } else {
      pararTimer();
      timer = window.setTimeout(function(){
          $.post('libreria/ajaxPracticas.php',{
          celda: id,
          valor: cantidad,
          tabla: tabla,
          modificarCelda: true,
          practica: practica
      },function(respuesta){
          if(respuesta === 'done')
          {
              // Hacer algo cuando OK
              calcularTotalesCambios();
          } else {
              // Hacer algo cuando ERROR
          }
      });
      },500);
  }
};
//

// CALCULAR TOTALES
var calcularTotalesCambios = function()
{
    // Totales verticales
    var c7 = extraerInput('c5') + extraerInput('c6');
    var c11 = c7 + extraerInput('c8') + extraerInput('c9') + extraerInput('c10');
    var c15 = c11 + extraerInput('c13') + extraerInput('c14');
    var d7 = extraerInput('d5') + extraerInput('d6');
    var d11 = d7 + extraerInput('d8') + extraerInput('d9') + extraerInput('d10');
    var d15 = d11 + extraerInput('d13') + extraerInput('d14');
    var e7 = extraerInput('e5') + extraerInput('e6');
    var e11 = e7 + extraerInput('e8') + extraerInput('e9') + extraerInput('e10');
    var e15 = e11 + extraerInput('e13') + extraerInput('e14');
    var f7 = extraerInput('f5') + extraerInput('f6');
    var f11 = f7 + extraerInput('f8') + extraerInput('f9') + extraerInput('f10');
    var f15 = f11 + extraerInput('f13') + extraerInput('f14');

    // Totales horizontales
    var g5 = extraerInput('c5') + extraerInput('d5') + extraerInput('e5') + extraerInput('f5');
    var g6 = extraerInput('c6') + extraerInput('d6') + extraerInput('e6') + extraerInput('f6');
    var g7 = g5 + g6;
    var g8 = extraerInput('c8') + extraerInput('d8') + extraerInput('e8') + extraerInput('f8');
    var g9 = extraerInput('c9') + extraerInput('d9') + extraerInput('e9') + extraerInput('f9');
    var g10 = extraerInput('c10') + extraerInput('d10') + extraerInput('e10') + extraerInput('f10');
    var g11 = g7 + g8 + g9 + g10;
    var g13 = extraerInput('c13') + extraerInput('d13') + extraerInput('e13') + extraerInput('f13');
    var g14 = extraerInput('c14') + extraerInput('d14') + extraerInput('e14') + extraerInput('f14');
    var g15 = g11 + g13 + g14;


    // Cambiar DOM
    document.getElementById('c7').innerHTML = '$ ' + c7.toFixed(2);
    document.getElementById('c11').innerHTML = '$ ' + c11.toFixed(2);
    document.getElementById('c15').innerHTML = '$ ' + c15.toFixed(2);
    document.getElementById('d7').innerHTML = '$ ' + d7.toFixed(2);
    document.getElementById('d11').innerHTML = '$ ' + d11.toFixed(2);
    document.getElementById('d15').innerHTML = '$ ' + d15.toFixed(2);
    document.getElementById('e7').innerHTML = '$ ' + e7.toFixed(2);
    document.getElementById('e11').innerHTML = '$ ' + e11.toFixed(2);
    document.getElementById('e15').innerHTML = '$ ' + e15.toFixed(2);
    document.getElementById('f7').innerHTML = '$ ' + f7.toFixed(2);
    document.getElementById('f11').innerHTML = '$ ' + f11.toFixed(2);
    document.getElementById('f15').innerHTML = '$ ' + f15.toFixed(2);

    document.getElementById('g5').innerHTML = '$ ' + g5.toFixed(2);
    document.getElementById('g6').innerHTML = '$ ' + g6.toFixed(2);
    document.getElementById('g7').innerHTML = '$ ' + g7.toFixed(2);
    document.getElementById('g8').innerHTML = '$ ' + g8.toFixed(2);
    document.getElementById('g9').innerHTML = '$ ' + g9.toFixed(2);
    document.getElementById('g10').innerHTML = '$ ' + g10.toFixed(2);
    document.getElementById('g11').innerHTML = '$ ' + g11.toFixed(2);
    document.getElementById('g13').innerHTML = '$ ' + g13.toFixed(2);
    document.getElementById('g14').innerHTML = '$ ' + g14.toFixed(2);
    document.getElementById('g15').innerHTML = '$ ' + g15.toFixed(2);
};


var extraerLaFecha = function(practica,fecha,id)
{
  $.post('libreria/ajaxPracticas.php',{
      extraerLaFecha: true,
      practica: practica,
      fecha: fecha
  }).done(function(respuesta){
      var elemento = document.getElementById(id);
      elemento.innerHTML = respuesta;
  });  
};

var extraerLaFechaCuenta = function(practica,fecha,id)
{
  $.post('libreria/ajaxPracticas.php',{
      extraerLaFechaCuenta: true,
      practica: practica,
      fecha: fecha
  }).done(function(respuesta){
      var elemento = document.getElementById(id);
      elemento.innerHTML = respuesta;
  });  
};



/////////////////////////
// PRÁCTICA DE BG CUENTA
/////////////////////////

var totalesCuenta = function()
{
    var tiempo;
    clearTimeout(tiempo);
    tiempo = window.setTimeout(function(){
        
        var totA1 = limpiarCantidad(document.getElementById('d28').innerHTML)+
            limpiarCantidad(document.getElementById('d38').innerHTML)+
            limpiarCantidad(document.getElementById('d47').innerHTML)+
            limpiarCantidad(document.getElementById('d54').innerHTML);
        document.getElementById('totA1').innerHTML = "$ "+totA1.toFixed(2);
        
        var totA2 = limpiarCantidad(document.getElementById('g28').innerHTML)+
            limpiarCantidad(document.getElementById('g38').innerHTML)+
            limpiarCantidad(document.getElementById('g47').innerHTML)+
            limpiarCantidad(document.getElementById('g54').innerHTML);
        document.getElementById('totA2').innerHTML = "$ "+totA2.toFixed(2);
        
        var totP1 = limpiarCantidad(document.getElementById('e79').innerHTML)+
                limpiarCantidad(document.getElementById('e92').innerHTML);
        document.getElementById('totP1').innerHTML = "$ "+totP1.toFixed(2);
        
        var totP2 = limpiarCantidad(document.getElementById('h79').innerHTML)+
                limpiarCantidad(document.getElementById('h92').innerHTML);
        document.getElementById('totP2').innerHTML = "$ "+totP2.toFixed(2);
        
    },1000);
};



///////////////////////
// PRÁCTICA DE ESTADO
///////////////////////

var ajaxCambiaValorEdo = function(cantidad,id,practica,tabla)
{
  if(isNaN(cantidad))
  {
      // alert('No es un número');
      // document.getElementById(id).value = "";
  } else {
      pararTimer();
      timer = window.setTimeout(function(){
          $.post('libreria/ajaxPracticas.php',{
          celda: id,
          valor: cantidad,
          tabla: tabla,
          modificarCelda: true,
          practica: practica
      },function(respuesta){
          if(respuesta === 'done')
          {
              // Hacer algo cuando OK
              calculosEdo();
          } else {
              // Hacer algo cuando ERROR
              console.log('Hay un error desde ajax');
          }
      });
      },500);
  }
};
//


/**
 * EXTRAER CANTIDAD DE UN CAMPO INPUT
 * 
 * Función para extraer la cantidad que haya un 
 * campo INPUT cuyo ID es pasado como parámetro.
 * @author Fernando Magrosoto
 * @param {id} id EL ID del campo de texto
 * @return {float} El valor del campo INPUT
 */
function extraerInput(id)
{
    var campo = document.getElementById(id);
    var cantidad = campo.value;
    if(cantidad === '')
    {
        cantidad = 0;
    }
    return parseFloat(cantidad);
}
// fin


/**
 * CÁLCULOS DE CADA CAMPO
 */
function calculosEdo()
{
    // Año actual
    var d13 = extraerInput('c11') + extraerInput('c12');
    var d16 = extraerInput('c14') + extraerInput('c15') + extraerInput('c16');
    var e8 = extraerInput('d6') + extraerInput('d7') + extraerInput('d8');
    var e17 = d13 - d16;
    var e18 = extraerInput('e10') + e17;
    var f9 = extraerInput('e5') - e8;
    var f20 = e18 - extraerInput('e19');
    var f21 = f9 - f20;
    var f22 = extraerInput('e23') + extraerInput('e24') + extraerInput('e25');
    var f26 = f21 - f22;
    var f27 = extraerInput('e28') - extraerInput('e29');
    var f30 = f26 + f27;
    var f31 = extraerInput('e32') - extraerInput('e33');
    var f34 = f30 + f31;
    var f35 = (f34 > 0)?f34*0.3:0;
    var f36 = f34 - f35;
    var g20 = parseFloat(f20 / f9);
    var g21 = f21 / f9;
    var g22 = f22 / f9;
    var g26 = f26 / f9;
    var g27 = f27 / f9;
    var g30 = f30 / f9;
    var g31 = f31 / f9;
    var g34 = f34 / f9;
    var g35 = f35 / f9;
    var g36 = f36 / f9;
    
    document.getElementById('d13').innerHTML = '$ ' + d13.toFixed(2);
    document.getElementById('d16').innerHTML = '$ ' + d16.toFixed(2);
    document.getElementById('e8').innerHTML = '$ ' + e8.toFixed(2);
    document.getElementById('e17').innerHTML = '$ ' + e17.toFixed(2);
    document.getElementById('e18').innerHTML = '$ ' + e18.toFixed(2);
    document.getElementById('f9').innerHTML = '$ ' + f9.toFixed(2);
    document.getElementById('f20').innerHTML = '$ ' + f20.toFixed(2);
    document.getElementById('f21').innerHTML = '$ ' + f21.toFixed(2);
    document.getElementById('f22').innerHTML = '$ ' + f22.toFixed(2);
    document.getElementById('f26').innerHTML = '$ ' + f26.toFixed(2);
    document.getElementById('f27').innerHTML = '$ ' + f27.toFixed(2);
    document.getElementById('f30').innerHTML = '$ ' + f30.toFixed(2);
    document.getElementById('f31').innerHTML = '$ ' + f31.toFixed(2);
    document.getElementById('f34').innerHTML = '$ ' + f34.toFixed(2);
    document.getElementById('f35').innerHTML = '$ ' + f35.toFixed(2);
    document.getElementById('f36').innerHTML = '$ ' + f36.toFixed(2);
    document.getElementById('g20').innerHTML = (!isFinite(g20))?0:(g20 * 100).toFixed(2)+'%';
    document.getElementById('g21').innerHTML = (!isFinite(g21))?0:(g21 * 100).toFixed(2)+'%';
    document.getElementById('g22').innerHTML = (!isFinite(g22))?0:(g22 * 100).toFixed(2)+'%';
    document.getElementById('g26').innerHTML = (!isFinite(g26))?0:(g26 * 100).toFixed(2)+'%';
    document.getElementById('g27').innerHTML = (!isFinite(g27))?0:(g27 * 100).toFixed(2)+'%';
    document.getElementById('g30').innerHTML = (!isFinite(g30))?0:(g30 * 100).toFixed(2)+'%';
    document.getElementById('g31').innerHTML = (!isFinite(g31))?0:(g31 * 100).toFixed(2)+'%';
    document.getElementById('g34').innerHTML = (!isFinite(g34))?0:(g34 * 100).toFixed(2)+'%';
    document.getElementById('g35').innerHTML = (!isFinite(g35))?0:(g35 * 100).toFixed(2)+'%';
    document.getElementById('g36').innerHTML = (!isFinite(g36))?0:(g36 * 100).toFixed(2)+'%';
    
    // Año anterior
    var j13 = extraerInput('i11') + extraerInput('i12');
    var j16 = extraerInput('i14') + extraerInput('i15') + extraerInput('i16');
    var k8 = extraerInput('j6') + extraerInput('j7') + extraerInput('j8');
    var k17 = j13 - j16;
    var k18 = extraerInput('k10') + k17;
    var l9 = extraerInput('k5') - k8;
    var l20 = k18 - extraerInput('k19');
    var l21 = l9 - l20;
    var l22 = extraerInput('k23') + extraerInput('k24') + extraerInput('k25');
    var l26 = l21 - l22;
    var l27 = extraerInput('k28') - extraerInput('k29');
    var l30 = l26 + l27;
    var l31 = extraerInput('k32') - extraerInput('k33');
    var l34 = l30 + l31;
    var l35 = (l34 > 0)?l34 * .3:0;
    var l36 = l34 - l35;
    var m20 = l20 / l9;
    var m21 = l21 / l9;
    var m22 = l22 / l9;
    var m26 = l26 / l9;
    var m27 = l27 / l9;
    var m30 = l30 / l9;
    var m31 = l31 / l9;
    var m34 = l34 / l9;
    var m35 = l35 / l9;
    var m36 = l36 / l9;
    
    document.getElementById('j13').innerHTML = '$ ' + j13.toFixed(2);
    document.getElementById('j16').innerHTML = '$ ' + j16.toFixed(2);
    document.getElementById('k8').innerHTML = '$ ' + k8.toFixed(2);
    document.getElementById('k17').innerHTML = '$ ' + k17.toFixed(2);
    document.getElementById('k18').innerHTML = '$ ' + k18.toFixed(2);
    document.getElementById('l9').innerHTML = '$ ' + l9.toFixed(2);
    document.getElementById('l20').innerHTML = '$ ' + l20.toFixed(2);
    document.getElementById('l21').innerHTML = '$ ' + l21.toFixed(2);
    document.getElementById('l22').innerHTML = '$ ' + l22.toFixed(2);
    document.getElementById('l26').innerHTML = '$ ' + l26.toFixed(2);
    document.getElementById('l27').innerHTML = '$ ' + l27.toFixed(2);
    document.getElementById('l30').innerHTML = '$ ' + l30.toFixed(2);
    document.getElementById('l31').innerHTML = '$ ' + l31.toFixed(2);
    document.getElementById('l34').innerHTML = '$ ' + l34.toFixed(2);
    document.getElementById('l35').innerHTML = '$ ' + l35.toFixed(2);
    document.getElementById('l36').innerHTML = '$ ' + l36.toFixed(2);
    document.getElementById('m20').innerHTML = (isNaN(m20))?0:(m20 * 100).toFixed(2)+'%';
    document.getElementById('m21').innerHTML = (isNaN(m21))?0:(m21 * 100).toFixed(2)+'%';
    document.getElementById('m22').innerHTML = (isNaN(m22))?0:(m22 * 100).toFixed(2)+'%';
    document.getElementById('m26').innerHTML = (isNaN(m26))?0:(m26 * 100).toFixed(2)+'%';
    document.getElementById('m27').innerHTML = (isNaN(m27))?0:(m27 * 100).toFixed(2)+'%';
    document.getElementById('m30').innerHTML = (isNaN(m30))?0:(m30 * 100).toFixed(2)+'%';
    document.getElementById('m31').innerHTML = (isNaN(m31))?0:(m31 * 100).toFixed(2)+'%';
    document.getElementById('m34').innerHTML = (isNaN(m34))?0:(m34 * 100).toFixed(2)+'%';
    document.getElementById('m35').innerHTML = (isNaN(m35))?0:(m35 * 100).toFixed(2)+'%';
    document.getElementById('m36').innerHTML = (isNaN(m36))?0:(m36 * 100).toFixed(2)+'%';
    
    // Almacenar variables
    sessionStorage.r5 = f9;
    sessionStorage.r6 = f20;
    sessionStorage.r7 = f9 - f20;
    sessionStorage.r8 = f22;
    sessionStorage.r9 = (f9 - f20) - f22;
    sessionStorage.r10 = f27;
    sessionStorage.r11 = ((f9 - f20) - f22) + f27;
    sessionStorage.r12 = f31;
    sessionStorage.r13 = (((f9 - f20) - f22) + f27) + f31;
    sessionStorage.r14 = f35;
    sessionStorage.r15 = ((((f9 - f20) - f22) + f27) + f31) - f35;
    sessionStorage.u5 = l9;
    sessionStorage.u6 = l20;
    sessionStorage.u7 = l9 - l20;
    sessionStorage.u8 = l22;
    sessionStorage.u9 = (l9 - l20) - l22;
    sessionStorage.u10 = l27;
    sessionStorage.u11 = ((l9 - l20) - l22) + l27;
    sessionStorage.u12 = l31;
    sessionStorage.u13 = (((l9 - l20) - l22) + l27) + l31;
    sessionStorage.u14 = l35;
    sessionStorage.u15 = ((((l9 - l20) - l22) + l27) + l31) - l35;
}
// fin