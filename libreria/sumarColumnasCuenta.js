/**
 * SUMAR LAS COLUMNAS DE ACTIVOS
 * 
 * Pasar los totales en celdad D y G
 */
var sumarActivos = function()
{
    var d28 = extraerInput('c7')+extraerInput('c8')+extraerInput('c9')+
            extraerInput('c10')+extraerInput('c11')+extraerInput('c12')+
            extraerInput('c13')+extraerInput('c14')+extraerInput('c15')+
            extraerInput('c16')+extraerInput('c17')+extraerInput('c18')+
            extraerInput('c19')+extraerInput('c20')+extraerInput('c21')+
            extraerInput('c22')+extraerInput('c23')+extraerInput('c24')+
            extraerInput('c25')+extraerInput('c26')+extraerInput('c27')+
            extraerInput('c28');
    
    var d38 = extraerInput('c31')+extraerInput('c32')+extraerInput('c33')+
            extraerInput('c34')+extraerInput('c35')+extraerInput('c36')+
            extraerInput('c37')+extraerInput('c38');
    
    var d47 = extraerInput('c40')+extraerInput('c41')+extraerInput('c42')+
            extraerInput('c43')+extraerInput('c44')+extraerInput('c45')+
            extraerInput('c46')+extraerInput('c47');
    
    var d54 = extraerInput('c49')+extraerInput('c50')+extraerInput('c51')+
            extraerInput('c52')+extraerInput('c53')+extraerInput('c54');
        
    var g28 = extraerInput('f7')+extraerInput('f8')+extraerInput('f9')+
            extraerInput('f10')+extraerInput('f11')+extraerInput('f12')+
            extraerInput('f13')+extraerInput('f14')+extraerInput('f15')+
            extraerInput('f16')+extraerInput('f17')+extraerInput('f18')+
            extraerInput('f19')+extraerInput('f20')+extraerInput('f21')+
            extraerInput('f22')+extraerInput('f23')+extraerInput('f24')+
            extraerInput('f25')+extraerInput('f26')+extraerInput('f27')+
            extraerInput('f28');
    
    var g38 = extraerInput('f31')+extraerInput('f32')+extraerInput('f33')+
            extraerInput('f34')+extraerInput('f35')+extraerInput('f36')+
            extraerInput('f37')+extraerInput('f38');
    
    var g47 = extraerInput('f40')+extraerInput('f41')+extraerInput('f42')+
            extraerInput('f43')+extraerInput('f44')+extraerInput('f45')+
            extraerInput('f46')+extraerInput('f47');
    
    var g54 = extraerInput('f49')+extraerInput('f50')+extraerInput('f51')+
            extraerInput('f52')+extraerInput('f53')+extraerInput('f54');
    
    var totA1 = d28+d38+d47+d54;
    var totA2 = g28+g38+g47+g54;
    
    document.getElementById('d28').innerHTML = "$ "+d28.toFixed(2);
    document.getElementById('d38').innerHTML = "$ "+d38.toFixed(2);
    document.getElementById('d47').innerHTML = "$ "+d47.toFixed(2);
    document.getElementById('d54').innerHTML = "$ "+d54.toFixed(2);
    document.getElementById('g28').innerHTML = "$ "+g28.toFixed(2);
    document.getElementById('g38').innerHTML = "$ "+g38.toFixed(2);
    document.getElementById('g47').innerHTML = "$ "+g47.toFixed(2);
    document.getElementById('g54').innerHTML = "$ "+g54.toFixed(2);
    document.getElementById('totA1').innerHTML = "$ "+totA1.toFixed(2);
    document.getElementById('totA2').innerHTML = "$ "+totA2.toFixed(2);
    
    sumarPasivos();
};
// fin

/**
 * SUMAR LAS COLUMNAS DE PASIVO
 * 
 * Pasar los totales en celdad D y G
 */
var sumarPasivos = function()
{
    var d72 = extraerInput('c57')+extraerInput('c58')+extraerInput('c59')+
            extraerInput('c60')+extraerInput('c61')+extraerInput('c62')+
            extraerInput('c63')+extraerInput('c64')+extraerInput('c65')+
            extraerInput('c66')+extraerInput('c67')+extraerInput('c68')+
            extraerInput('c69')+extraerInput('c70')+extraerInput('c71')+
            extraerInput('c72');
    var d79 = extraerInput('c74')+extraerInput('c75')+extraerInput('c76')+
            extraerInput('c77')+extraerInput('c78')+extraerInput('c79');
    var d84 = extraerInput('c82')+extraerInput('c83')+extraerInput('c84');
    var d92 = extraerInput('c86')+extraerInput('c87')+extraerInput('c88')+
            extraerInput('c89')+extraerInput('c90')+extraerInput('c91')+
            extraerInput('c92');
    var totP1 = d72+d79+d84+d92;
    
    var g72 = extraerInput('f57')+extraerInput('f58')+extraerInput('f59')+
            extraerInput('f60')+extraerInput('f61')+extraerInput('f62')+
            extraerInput('f63')+extraerInput('f64')+extraerInput('f65')+
            extraerInput('f66')+extraerInput('f67')+extraerInput('f68')+
            extraerInput('f69')+extraerInput('f70')+extraerInput('f71')+
            extraerInput('f72');
    var g79 = extraerInput('f74')+extraerInput('f75')+extraerInput('f76')+
            extraerInput('f77')+extraerInput('f78')+extraerInput('f79');
    var g84 = extraerInput('f82')+extraerInput('f83')+extraerInput('f84');
    var g92 = extraerInput('f86')+extraerInput('f87')+extraerInput('f88')+
            extraerInput('f89')+extraerInput('f90')+extraerInput('f91')+
            extraerInput('f92');
    var totP2 = g72+g79+g84+g92;
    
    var e79 = d72+d79;
    var e92 = d84+d92;
    var h79 = g72+g79;
    var h92 = g84+g92;
    
    document.getElementById('d72').innerHTML = "$ "+d72.toFixed(2);
    document.getElementById('d79').innerHTML = "$ "+d79.toFixed(2);
    document.getElementById('d84').innerHTML = "$ "+d84.toFixed(2);
    document.getElementById('d92').innerHTML = "$ "+d92.toFixed(2);
    document.getElementById('g72').innerHTML = "$ "+g72.toFixed(2);
    document.getElementById('g79').innerHTML = "$ "+g79.toFixed(2);
    document.getElementById('g84').innerHTML = "$ "+g84.toFixed(2);
    document.getElementById('g92').innerHTML = "$ "+g92.toFixed(2);
    
    document.getElementById('e79').innerHTML = "$ "+e79.toFixed(2);
    document.getElementById('e92').innerHTML = "$ "+e92.toFixed(2);
    document.getElementById('h79').innerHTML = "$ "+h79.toFixed(2);
    document.getElementById('h92').innerHTML = "$ "+h92.toFixed(2);
    
    document.getElementById('totP1').innerHTML = "$ "+totP1.toFixed(2);
    document.getElementById('totP2').innerHTML = "$ "+totP2.toFixed(2);
    
    ponerComparativo();
};
// fin