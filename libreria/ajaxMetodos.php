<?php
include 'principal.php';

##############################
## script para extraer      ##
## los datos de la tabla    ##
## rom_cuentas              ##
##############################


// query para sacar el tipo de
// cuenta de acuerdo al GRUPO del form de DIARIO
if(isset($_GET['Tcuenta'])){
	
	$gcuenta = $_GET['Tcuenta'];
	$query = "SELECT IDscuenta, Nscuenta FROM rom_scuenta
				WHERE gcuenta = '$gcuenta'
				ORDER BY IDscuenta ASC";
	$result = mysql_query($query);
	$datos = mysql_fetch_assoc($result);
	
	$enviar = "";
	
	do {
		$enviar .= $datos['IDscuenta']."-".$datos['Nscuenta'].",";
	} while($datos = mysql_fetch_assoc($result));
	
	echo substr($enviar, 0, -1);
}

if(isset($_GET['subcuenta'])){
	
	$subcuenta = $_GET['subcuenta'];
	$metodo = $_GET['metodo'];
	
	$query = "SELECT clave, Ncuenta FROM rom_cuentas
				WHERE (Mcuenta = 4 OR Mcuenta = '$metodo')
				AND subcuenta = '$subcuenta'";
	$result = mysql_query($query);
	$datos = mysql_fetch_assoc($result);
	
	$enviar = "";
	
	do {
		$enviar .= $datos['clave']."-".$datos['Ncuenta'].",";
	} while($datos = mysql_fetch_assoc($result));
	
	echo substr(utf8_encode($enviar), 0, -1);
}

if(isset($_GET['cuenta'])){
	
	$detalle = $_GET['cuenta'];
	
	$query = "SELECT DISTINCT Ndcuenta, claveD
				FROM rom_dcuenta
				WHERE claveA = '$detalle'";
	$result = mysql_query($query) or die(mysql_error());
	$datos = mysql_fetch_assoc($result);
	$numDatos = mysql_num_rows($result);
	
	if($numDatos == 0){
		$enviar = "no";
		echo $enviar;
	} else {
	
	$enviar = "";
	do {
		$enviar .= $datos['claveD']."|".$datos['Ndcuenta'].",";
	} while($datos = mysql_fetch_assoc($result));
	
	echo substr(utf8_encode($enviar), 0, -1);
	
	}	
}

if(isset($_GET['cuentaAcumulativa'])){
	$cuentaA = $_GET['cuentaAcumulativa'];
	$IDejercicio = $_GET['IDejercicio'];
	$metodo = $_GET['metodo'];
	
	$query =	"SELECT rom_cantidades.subcuenta, Ndcuenta FROM rom_cantidades
				INNER JOIN rom_dcuenta ON rom_cantidades.subcuenta = rom_dcuenta.claveD
				INNER JOIN rom_cuentas ON rom_cantidades.cuenta = rom_cuentas.clave
				INNER JOIN rom_asiento ON
				(rom_cantidades.ejercicio = rom_asiento.Easiento)
				AND
				(rom_cantidades.asiento = rom_asiento.asientoR)
				WHERE (Easiento = '$IDejercicio' AND status = 1)
				AND cuenta LIKE '$cuentaA%'
				AND (Mcuenta = 4 OR Mcuenta = '$metodo')
				AND acumulable != 1
				GROUP BY Ndcuenta
				ORDER BY rom_cantidades.subcuenta ASC";
	$result = mysql_query($query) or die(mysql_error());
	$row = mysql_fetch_assoc($result);
	
	$enviar = "";
	do {
		$enviar .= $row['subcuenta']."|".$row['Ndcuenta'].",";
	} while($row = mysql_fetch_assoc($result));
	
	echo substr(utf8_encode($enviar), 0, -1); // enviar resultados
}

// comprobar Nejercicio
if(isset($_GET['Nejercicio'])){
	$Nejercicio = $_GET['Nejercicio'];
	$query = "SELECT Nejercicio FROM rom_ejercicio
				WHERE Nejercicio = '$Nejercicio'";
	$resul = mysql_query($query);
	$numRows = mysql_num_rows($resul);
	if($numRows == 0){
		$enviar = "permitido";
	} else {
		$enviar = "utilizado";
	}
	echo $enviar;
}
