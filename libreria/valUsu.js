var boton = document.getElementById('submit');
    var msg = document.getElementById('msgUser');
    var tempo;
    function validarUsuario(usuario)
    {
        msg.innerHTML = "";
        boton.disabled = true;
        window.clearTimeout(tempo);
        tempo = window.setTimeout(function(){
            if(usuario.length < 5)
            {
                boton.disabled = true;
                msg.innerHTML = "Debe de contener más de 5 caracteres.";
            } else {
                $.get('libreria/ajaxVerUsu.php?usuario='+usuario,
                function(response){
                    boton.disabled = true;
                    if(response === 'repetido')
                    {
                        msg.innerHTML = 'Usuario repetido. Pruebe con algún otro.';
                    } else if(response === 'permitido')
                    {
                        boton.disabled = false;
                    }
                });
            }
        },500);
    }
    
function Validador(theForm)

{

  if (theForm.nombre.value === "")

  {
    alert("El campo NOMBRE está vacío.");
    theForm.nombre.focus();
    return (false);
  }
  if (theForm.apellidos.value === "")

  {
    alert("El campo APELLIDOS está vacío.");
    theForm.apellidos.focus();
    return (false);
  }
  if (theForm.correo.value === "")

  {
    alert("El campo CORREO está vacío.");
    theForm.correo.focus();
    return (false);
  } else {
      // Ahora, comprobar el correo
        expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(!expr.test(theForm.correo.value)){
            alert('El campo CORREO no es válido.');
            theForm.correo.focus();
            return false;
        }
  }
  if (theForm.institucion.value === "")

  {
    alert("El campo INSTITUCION está vacío.");
    theForm.institucion.focus();
    return (false);
  }
  if (theForm.usuario.value.length < 5)

  {
    alert("El campo USUARIO está vacío o tiene menos de 5 caracteres.");
    theForm.usuario.focus();
    boton.disabled = true;
    return (false);
  }
  if (theForm.pwd.value === "")

  {
    alert("El campo CONTRASEÑA está vacío.");
    theForm.pwd.focus();
    return (false);
  }

  return (true);

}