<div id="divHeader">
  <div id="divHeaderCont">
    <div id="divHeaderContA">
        <div class="h_sistema">SISTEMA CONTABLE</div>
        <div class="h_romero">ROMERO</div>
        <div class="h_version">Versión <?php echo VERSION ?></div>
    </div>
    <div id="divHeaderContB">
        <div class="h_fecha"><?php echo $Fecha; ?></div>
        <div class="h_libro">PRINCIPIOS DE CONTABILIDAD <em>Quinta edición</em></div>
        <div class="h_autor">Álvaro Javier Romero López</div>
    </div>
  </div>
</div>