<?php
include 'principal.php';

// Modificar datos de la BD
$modDatos = filter_input(INPUT_POST, 'modDatos',
        FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
if($modDatos)
{
    $campo = filter_input(INPUT_POST, 'elcampo',
            FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $valor = utf8_decode(filter_input(INPUT_POST, 'elvalor',
            FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW));
    $registro = filter_input(INPUT_POST, 'elregistro',
            FILTER_VALIDATE_INT);
    $q = "UPDATE rom_cuestionario SET $campo = '$valor' "
            . "WHERE id = $registro";
    if(mysql_query($q) or die(mysql_error()))
    {
        echo "modificado";
    } else {
        echo "error";
    }
}
// fin



// Modificar respuestas de la BD
$modRespuestas = filter_input(INPUT_POST, 'modRespuestas',
        FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
if($modRespuestas)
{
    $cuestionario = filter_input(INPUT_POST, 'cuestionario',
            FILTER_VALIDATE_INT);
    $usuario = filter_input(INPUT_POST, 'usuario',
            FILTER_VALIDATE_INT);
    $capitulo = filter_input(INPUT_POST, 'capitulo',
            FILTER_VALIDATE_INT);
    $p = filter_input(INPUT_POST, 'pregunta',
            FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $pregunta = utf8_decode($p);
    $r = filter_input(INPUT_POST, 'respuesta',
            FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $respuesta = utf8_decode($r);
    
    // Primero, buscar si ya hay un registro agregado
    $qB = "SELECT COUNT(id) FROM rom_cuestionario_respuestas "
            . "WHERE cuestionario = $cuestionario "
            . "AND usuario = $usuario "
            . "AND pregunta = '$pregunta'";
    $rB = mysql_query($qB) or die(mysql_error());
    $daB = mysql_fetch_row($rB);
    if($daB[0] == 0)
    {
        // No hay registro, así que insertamos uno...
        $qN = "INSERT INTO rom_cuestionario_respuestas "
            . "(cuestionario,usuario,capitulo,pregunta,respuesta,fecha) "
            . "VALUES "
            . "($cuestionario,$usuario,$capitulo,'$pregunta','$respuesta',NOW())";
    } else {
        // Ya hay uno, entonces modificamos...
        $qN = "UPDATE rom_cuestionario_respuestas SET respuesta = '$respuesta' "
            . "WHERE cuestionario = $cuestionario "
            . "AND usuario = $usuario "
            . "AND pregunta = '$pregunta'";
    }
    
    if(mysql_query($qN) or die(mysql_error()))
    {
        echo "modificado";
    } else {
        echo "error";
    }
}
// fin


$delRespuesta = filter_input(INPUT_POST, 'delRespuesta',
        FILTER_VALIDATE_BOOLEAN,FILTER_NULL_ON_FAILURE);
if($delRespuesta)
{
    $cuestionario = filter_input(INPUT_POST, 'cuestionario',
            FILTER_VALIDATE_INT);
    $usuario = filter_input(INPUT_POST, 'usuario',
            FILTER_VALIDATE_INT);
    $capitulo = filter_input(INPUT_POST, 'capitulo',
            FILTER_VALIDATE_INT);
    $p = filter_input(INPUT_POST, 'pregunta',
            FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $pregunta = utf8_decode($p);
    $r = filter_input(INPUT_POST, 'respuesta',
            FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $respuesta = utf8_decode($r);
    
    $query = "DELETE FROM rom_cuestionario_respuestas "
            . "WHERE cuestionario=$cuestionario "
            . "AND usuario = $usuario "
            . "AND capitulo = $capitulo "
            . "AND pregunta = '$pregunta'";
    if(mysql_query($query))
    {
        echo "eliminado";
    } else {
        echo "error";
    }
}
// fin