var rc5_cl = ['recurso','identificado','beneficios',
    'recursos','entidad','beneficios','futuros','controlados','identificables','unidades',
'corto','separadas','excepto','liquidez','todos','plazo',
'normal','periodo','adquisición','realización','equivalentes',
'circulante','equivalentes','beneficio','venta','consumo','un','corto','mayor',
'largo','tangibles','financieros','no','recuperables','normal',
'intangibles','sustancia','producción','prestación','beneficios',
'recursos','características','beneficios','futuros','mayor'];

var rc5_verfalR = ['falso','verdadero','verdadero','verdadero','falso',
    'verdadero','verdadero','falso','falso','verdadero'];

var rc5_opcmulA = ['Fondos a largo plazo','Gastos de constitución',
    'Equipo de entrega y reparto','Cuentas por cobrar a largo plazo',
'Equipo de transporte','Mobiliario y equipo de oficina'];

var rc5_opcmulB = ['Patentes','Fondo fijo de caja chica','Derechos de autor',
    'Documentos por cobrar','Inventarios o almacén'];

var rc5_opcmulC = ['Fondos a largo plazo','Cuentas por cobrar a largo plazo',
    'Pagos anticipados a largo plazo','Inmuebles no utilizados','IVA acreditable'];

var rc5_corr1 = [5,4,2,7,8,3,1,6];

var rc5_simpleB = ['falso','falso','verdadero','verdadero',
    'falso','verdadero','falso','verdadero','verdadero',
'falso','verdadero','falso','falso','falso','verdadero'];

var rc5_simpleC = ['verdadero','falso','verdadero','verdadero','falso',
    'verdadero','falso','verdadero','verdadero','falso',
'verdadero','falso','falso','falso','verdadero'];