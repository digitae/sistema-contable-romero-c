<?php
include 'principal.php';

$agregarRazones = filter_input(INPUT_POST, 'agregarRazones');
$modificarRazones = filter_input(INPUT_POST, 'modificarRazones');

$ventas = filter_input(INPUT_POST, 'ventas',
            FILTER_VALIDATE_FLOAT);
$costo = filter_input(INPUT_POST, 'costo',
        FILTER_VALIDATE_FLOAT);
$compras = filter_input(INPUT_POST, 'compras',
        FILTER_VALIDATE_FLOAT);
$tabla = filter_input(INPUT_POST, 'tabla',
        FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
$practica = filter_input(INPUT_POST, 'practica',
        FILTER_VALIDATE_INT);
$pagina = filter_input(INPUT_POST, 'pagina',
        FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);



## Agregar razones
if(isset($agregarRazones))
{
    $query = "INSERT INTO $tabla (practica,ventas,costo,compras) "
            . "VALUES ($practica,$ventas,$costo,$compras)";
    if(mysql_query($query) or die(mysql_error()))
    {
        header("Location: ../".$pagina."?IDpractica=".$practica);
        exit;
    }
}
// fin



## Modificar razones
if(isset($modificarRazones))
{
    $id = filter_input(INPUT_POST, 'id',
            FILTER_VALIDATE_INT);
    $query = "UPDATE $tabla SET "
            . "ventas = $ventas,"
            . "costo = $costo,"
            . "compras = $compras "
            . "WHERE id = $id";
    if(mysql_query($query) or die(mysql_error()))
    {
        header("Location: ../".$pagina."?IDpractica=".$practica."&editado=true");
        exit;
    }
}
// fin