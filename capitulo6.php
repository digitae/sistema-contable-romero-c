<?php
include 'libreria/principal.php';
include 'libreria/evaluaciones.php';

esUsuario(); // será usuario registrado?
$IDusuario = $_SESSION['userID'];
$capitulo = 6;
$registro = verificarCuestionario($capitulo, $IDusuario,  utf8_decode($Fecha));


?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> Principios de Contabilidad. Cuestionario de evaluación. Capítulo 6</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="libreria/js_principal.js"></script>
<script src="libreria/vendor/jquery/jquery-1.11.0.min.js"></script>
<script src="libreria/calCap.js"></script>
<script src="libreria/rc6.js"></script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Cuestionario de autoevaluación</h1>
    </div>
    <div id="divTitularMenu">
        <a href="portada.php">Inicio</a> | 
        <a href="libreria/calculadora/calculadora.html" 
           target="_blank" 
           onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | 
        <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido" style="width: 900px">
  <div class="divContCuerpo">
      
      <h3 class="titulo-principal">Capítulo 6</h3>
      <div class="migas">
          <ul>
              <li><a href="portada.php">Portada</a></li>
              <li><a href="javascript:history.back();">Página anterior</a></li>
              <li><img src="imagenes/print.png" 
                       alt="" 
                       height="25" 
                       style="float: left; margin-right: 5px;" />
                  <a href="javascript:print();">Imprimir página</a></li>
          </ul>
      </div>
      
      <table class="mis-datos">
          <tr>
              <td>Universidad o Institución:</td>
              <td>
                  <input oninput="cCamCues('unidad',this.value,<?php echo $registro ?>);" 
                         name="unidad" 
                         id='unidad' 
                         required 
                         value="<?php echo xCamCues('unidad', $registro) ?>" />
              </td>
              <td>Escuela o facultad:</td>
              <td>
                  <input oninput="cCamCues('facultad',this.value,<?php echo $registro ?>);" 
                         name="facultad" 
                         id='facultad' 
                         required 
                         value="<?php echo xCamCues('facultad', $registro) ?>" />
              </td>
          </tr>
          <tr>
              <td>Asignatura:</td>
              <td>
                  <input oninput="cCamCues('asignatura',this.value,<?php echo $registro ?>);" 
                         name="asignatura" 
                         id='asignatura' 
                         required 
                         value="<?php echo xCamCues('asignatura', $registro) ?>" />
              </td>
              <td>Alumno:</td>
              <td>
                  <?php echo nombreUsuario($IDusuario) ?>
              </td>
          </tr>
          <tr>
              <td>Maestro:</td>
              <td>
                  <input oninput="cCamCues('maestro',this.value,<?php echo $registro ?>);" 
                         name="maestro" 
                         id='maestro' 
                         required 
                         value="<?php echo xCamCues('maestro', $registro) ?>" />
              </td>
              <td>Fecha:</td>
              <td>
                  <input oninput="cCamCues('fecha',this.value,<?php echo $registro ?>);" 
                         name="fecha" 
                         id='fecha' 
                         required 
                         value="<?php echo xCamCues('fecha', $registro) ?>" />
              </td>
          </tr>
      </table>
      
      <h4 class="subtit-4">OBJETIVOS DE APRENDIZAJE</h4>
      
      <div class="objetivos">
          <p>Al finalizar este capítulo, el alumno será capaz de:</p>
          <ol>
              <li>Identificar los estados financieros como medio por el cual la 
                contabilidad comunica información financiera útil para la toma de 
                decisiones</li>
              <li>Identificar y aplicar en los estados financieros las 
                características cualitativas primarias y secundarias de la 
                información financiera.</li>
          </ol>
      </div>
      
      <h4 class="subtit-4">EJERCICIOS</h4>
      
      <h5>I. RESUELVA LO SIGUIENTE</h5>
      <div class="pregunta-libre">
          <p><span>1</span> Mencione qué es la información financiera en los términos de la NIF A-3.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib01" 
                    id="prelib01"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib01'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>2</span> De acuerdo con las necesidades del usuario, ¿qué 
              tipo de decisiones le permite tomar la información que la 
              contabilidad le comunica?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib02" 
                    id="prelib02"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib02'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>3</span> La información financiera satisface al usuario 
              general si le comunica elementos de juicio sobre ciertos factores. 
              Señale cuáles son.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib03" 
                    id="prelib03"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib03'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>4</span> ¿Qué son los estados financieros?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib04" 
                    id="prelib04"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib04'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>5</span> Diga qué estados financieros integran una 
              presentación razonablemente adecuada de las entidades lucrativas.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib05" 
                    id="prelib05"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib05'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>6</span> Diga qué estados financieros integran una 
              presentación razonablemente adecuada de las entidades no lucrativas.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib06" 
                    id="prelib06"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib06'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>7</span> Señale cómo se dividen las características cualitativas.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib07" 
                    id="prelib07"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib07'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>8</span> Mencione cuáles son las características cualitativas 
              secundarias de la confiabilidad.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib08" 
                    id="prelib08"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib08'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>9</span> Mencione cuáles son las características cualitativas 
              secundarias de la información financiera que la hacen relevante.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib09" 
                    id="prelib09"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib09'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>10</span> Señale a qué se refiere la característica 
              cualitativa secundaria de posibilidad de predicción y confirmación.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib10" 
                    id="prelib10"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib10'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>11</span> Señale a qué se refiere la característica 
              cualitativa secundaria de importancia relativa.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib11" 
                    id="prelib11"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib11'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>12</span> Explique a qué se refiere la oportunidad como 
              restricción de los niveles máximos de las características cualitativas.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib12" 
                    id="prelib12"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib12'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>13</span> Explique a qué se refiere el equilibrio entre 
              las características cualitativas.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib13" 
                    id="prelib13"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib13'); ?></textarea>
      </div>
      
      <!-- fin preguntas libres -->
      
      <h5>II. COMPLETE LAS LÍNEAS EN BLANCO</h5>
      
      <div class="completar-lineas">
          <span class="num">1</span>
          La <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_1" 
                    name="comLin_1" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_1'); ?>" /> como característica 
          fundamental de los estados financieros es la cualidad de 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_2" 
                    name="comLin_2" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_2'); ?>" /> a las 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_3" 
                    name="comLin_3" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_3'); ?>" /> comunes del usuario 
          general y constituye el punto de partida para derivar las 
          características cualitativas.
      </div>
      
      <div class="completar-lineas">
          <span class="num">2</span>
          NIF A–1: Las características cualitativas <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_4" 
                    name="comLin_4" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_4'); ?>" /> 
          establecen que la información financiera deber ser 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_5" 
                    name="comLin_5" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_5'); ?>" />, relevante, 
          comprensible y <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_6" 
                    name="comLin_6" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_6'); ?>" />.
      </div>
      
      <div class="completar-lineas">
          <span class="num">3</span>
          La <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_7" 
                    name="comLin_7" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_7'); ?>" /> como característica 
          cualitativa primaria de la información financiera es aquella que se 
          manifiesta cuando su <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_8" 
                    name="comLin_8" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_8'); ?>" /> es <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_9" 
                    name="comLin_9" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_9'); ?>" /> 
          con las transacciones, <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_10" 
                    name="comLin_10" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_10'); ?>" /> internas y 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_11" 
                    name="comLin_11" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_11'); ?>" /> sucedidos, por lo 
          que el usuario la <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_12" 
                    name="comLin_12" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_12'); ?>" /> y utiliza para 
          tomar <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_13" 
                    name="comLin_13" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_13'); ?>" /> basándose en ella.
      </div>
      
      <div class="completar-lineas">
          <span class="num">4</span>
          La <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_14" 
                    name="comLin_14" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_14'); ?>" /> como característica 
          cualitativa primaria de la información financiera se manifiesta cuando 
          su efecto es capaz de <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_15" 
                    name="comLin_15" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_15'); ?>" /> en las <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_16" 
                    name="comLin_16" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_16'); ?>" /> económicas del usuario.
      </div>
      
      <div class="completar-lineas">
          <span class="num">5</span>
          La <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_17" 
                    name="comLin_17" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_17'); ?>" /> es una cualidad 
          esencial de la información financiera, la cual requiere que el mensaje 
          sea correctamente <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_18" 
                    name="comLin_18" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_18'); ?>" /> para facilitar su 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_19" 
                    name="comLin_19" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_19'); ?>" /> 
          por parte de  los usuarios, 
          por ello, se debe buscar la manera más adecuada para hacerlos 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_20" 
                    name="comLin_20" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_20'); ?>" /> al usuario general.
      </div>
      
      <div class="completar-lineas">
          <span class="num">6</span>
          La <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_21" 
                    name="comLin_21" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_21'); ?>" /> precisa que la 
          información sea elaborada de modo tal que le permita al usuario 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_22" 
                    name="comLin_22" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_22'); ?>" /> a lo largo del 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_23" 
                    name="comLin_23" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_23'); ?>" />, es decir, 
          identificar y analizar <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_24" 
                    name="comLin_24" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_24'); ?>" /> y similitudes con 
          la información de la <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_25" 
                    name="comLin_25" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_25'); ?>" /> entidad y la de otras entidades, para 
          apreciar la <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_26" 
                    name="comLin_26" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_26'); ?>" />,  mantenimiento o 
          retroceso de la entidad.
      </div>
      
      <div class="completar-lineas">
          <span class="num">7</span>
          Para que la información sea <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_27" 
                    name="comLin_27" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_27'); ?>" />, debe reflejar 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_28" 
                    name="comLin_28" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_28'); ?>" />, transformaciones 
          internas y <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_29" 
                    name="comLin_29" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_29'); ?>" /> <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_30" 
                    name="comLin_30" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_30'); ?>" /> sucedidos. La 
          verdad acredita la <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_31" 
                    name="comLin_31" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_31'); ?>" /> y 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_32" 
                    name="comLin_32" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_32'); ?>" /> del usuario en la 
          información financiera.
      </div>
      
      <div class="completar-lineas">
          <span class="num">8</span>
          Para que la información financiera sea <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_33" 
                    name="comLin_33" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_33'); ?>" />, su contenido debe 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_34" 
                    name="comLin_34" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_34'); ?>" /> con las 
          operaciones y eventos económicos que afectaron a la entidad; por tanto, 
          para que los estados financieros transmitan una imagen 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_35" 
                    name="comLin_35" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_35'); ?>" /> o una presentación 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_36" 
                    name="comLin_36" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_36'); ?>" /> de la situación 
          financiera de la entidad deben elaborarse con base en las normas.
      </div>
      
      <div class="completar-lineas">
          <span class="num">9</span>
          La información financiera debe presentarse de manera 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_37" 
                    name="comLin_37" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_37'); ?>" />, es decir, que no 
          se encuentre equivocada, <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_38" 
                    name="comLin_38" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_38'); ?>" /> o distorsionada 
          para <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_39" 
                    name="comLin_39" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_39'); ?>" /> de grupos o 
          sectores que puedan perseguir intereses particulares diferentes a los 
          del usuario general de la información financiera. Los estados 
          financieros deben estar libres de <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_40" 
                    name="comLin_40" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_40'); ?>" />, es decir, no deben 
          estar <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_41" 
                    name="comLin_41" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_41'); ?>" /> por juicios que 
          orillen a un resultado <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_42" 
                    name="comLin_42" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_42'); ?>" />, ya que de no ser 
          así, la información perdería la <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_43" 
                    name="comLin_43" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_43'); ?>" />.
      </div>
      
      <div class="completar-lineas">
          <span class="num">10</span>
          Para ser <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_44" 
                    name="comLin_44" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_44'); ?>" /> la información 
          financiera debe poder <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_45" 
                    name="comLin_45" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_45'); ?>" /> y validarse. El 
          sistema de control <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_46" 
                    name="comLin_46" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_46'); ?>" /> ayuda a que la 
          información financiera pueda ser sometida a <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_47" 
                    name="comLin_47" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_47'); ?>" /> por cualquier 
          interesado, utilizando para este fin información provista por la 
          entidad o a través de fuentes de información <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_48" 
                    name="comLin_48" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_48'); ?>" />.
      </div>
      
      <div class="completar-lineas">
          <span class="num">11</span>
          La información financiera es útil en el proceso de toma de decisiones, 
          pero, al mismo tiempo, su obtención origina <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_49" 
                    name="comLin_49" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_49'); ?>" />, los 
          <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_50" 
                    name="comLin_50" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_50'); ?>" /> derivados de la 
          información financiera deben <input oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    type="text" 
                    class="clinea" 
                    id="comLin_51" 
                    name="comLin_51" 
                    value="<?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'comLin_51'); ?>" /> el costo de obtenerla.
      </div>
      
      <div class="completar-lineas">
          <button onclick="calificarCompletar('comLin',51,rc6_cl);">CALIFICAR</button>
          <span id="comLin_cal"></span>
      </div>
      <!-- fin líneas en blanco -->
      
      <h5>III. PREGUNTAS DE OPCIÓN MÚLTIPLE</h5>
      
      <table class="opcion-multiple-radio" id="opradio">
          <tr>
              <th colspan="2">1. Las características cualitativas primarias establecen que la 
                  información financiera debe ser:</th>
          </tr>
          <tr>
              <td id="opmulR_1a_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_1" value="a" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_1')==='a')?' checked':'' ?> />
              </td>
              <td>
                  a) Útil, confiable, relevante y comparable.
              </td>
          </tr>
          <tr>
              <td id="opmulR_1b_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_1" value="b" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_1')==='b')?' checked':'' ?> />
              </td>
              <td>
                  b) Útil, relevante, confiable y comprensible
              </td>
          </tr>
          <tr>
              <td id="opmulR_1c_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_1" value="c" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_1')==='c')?' checked':'' ?> />
              </td>
              <td>
                  c) Confiable, relevante, comprensible y comparable.
              </td>
          </tr>
          <tr>
              <td id="opmulR_1d_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_1" value="d" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_1')==='d')?' checked':'' ?> />
              </td>
              <td>
                  d) Relevante, oportuna, útil y comprensible
              </td>
          </tr>
          <tr>
              <td id="opmulR_1e_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_1" value="e" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_1')==='e')?' checked':'' ?> />
              </td>
              <td>
                  e) Confiable, oportuna, útil y relevante.
              </td>
          </tr>
          <tr>
              <th colspan="2">2. Las características cualitativas secundarias de la 
                  confiabilidad son:</th>
          </tr>
          <tr>
              <td id="opmulR_2a_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_2" value="a" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_2')==='a')?' checked':'' ?> />
              </td>
              <td>
                  a) Veracidad, representatividad, objetividad y verificabilidad 
                  e información suficiente.
              </td>
          </tr>
          <tr>
              <td id="opmulR_2b_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_2" value="b" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_2')==='b')?' checked':'' ?> />
              </td>
              <td>
                  b) Utilidad, relevancia, veracidad, representatividad e información suficiente.
              </td>
          </tr>
          <tr>
              <td id="opmulR_2c_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_2" value="c" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_2')==='c')?' checked':'' ?> />
              </td>
              <td>
                  c) Comprensibilidad, comparabilidad, información suficiente, importancia relativa y relevancia.
              </td>
          </tr>
          <tr>
              <td id="opmulR_2d_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_2" value="d" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_2')==='d')?' checked':'' ?> />
              </td>
              <td>
                  d) Objetividad, verificabilidad, comparabilidad, importancia relativa y utilidad.
              </td>
          </tr>
          <tr>
              <td id="opmulR_2e_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_2" value="e" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_2')==='e')?' checked':'' ?> />
              </td>
              <td>
                  e) Veracidad, representatividad, objetividad, comparabilidad e importancia relativa
              </td>
          </tr>
          <tr>
              <th colspan="2">3. Las características cualitativas secundarias de la relevancia son:</th>
          </tr>
          <tr>
              <td  id="opmulR_3a_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_3" value="a" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_3')==='a')?' checked':'' ?> />
              </td>
              <td>
                  a) Posibilidad de predicción y confirmación e información suficiente.
              </td>
          </tr>
          <tr>
              <td id="opmulR_3b_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_3" value="b" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_3')==='b')?' checked':'' ?> />
              </td>
              <td>
                  b) Posibilidad de predicción y confirmación e importancia relativa.
              </td>
          </tr>
          <tr>
              <td id="opmulR_3c_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_3" value="c" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_3')==='c')?' checked':'' ?> />
              </td>
              <td>
                  c) Comprensibilidad e importancia relativa.
              </td>
          </tr>
          <tr>
              <td id="opmulR_3d_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_3" value="d" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_3')==='d')?' checked':'' ?> />
              </td>
              <td>
                  d) Información suficiente e importancia relativa.
              </td>
          </tr>
          <tr>
              <td id="opmulR_3e_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_3" value="e" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_3')==='e')?' checked':'' ?> />
              </td>
              <td>
                  e) Veracidad e importancia relativa.
              </td>
          </tr>
          <tr>
              <th colspan="2">4. Las restricciones a las características 
                  cualitativas se componen de los conceptos siguientes: </th>
          </tr>
          <tr>
              <td id="opmulR_4a_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_4" value="a" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_4')==='a')?' checked':'' ?> />
              </td>
              <td>
                  a) Oportunidad, confiabilidad y utilidad.
              </td>
          </tr>
          <tr>
              <td id="opmulR_4b_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_4" value="b" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_4')==='b')?' checked':'' ?> />
              </td>
              <td>
                  b) Oportunidad, relación entre costo y beneficio.
              </td>
          </tr>
          <tr>
              <td id="opmulR_4c_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_4" value="c" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_4')==='c')?' checked':'' ?> />
              </td>
              <td>
                  c) Relación entre costo y beneficio y utilidad.
              </td>
          </tr>
          <tr>
              <td id="opmulR_4d_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_4" value="d" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_4')==='d')?' checked':'' ?> />
              </td>
              <td>
                  d) Utilidad y confiabilidad.
              </td>
          </tr>
          <tr>
              <td id="opmulR_4e_parcial">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" name="opmulR_4" value="e" 
                    <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'opmulR_4')==='e')?' checked':'' ?> />
              </td>
              <td>
                  e) La oportunidad, la relación costo-beneficio y el equilibrio 
                  entre las características cualitativas.
              </td>
          </tr>
          <tr>
              <th colspan="2">&nbsp;</th>
          </tr>
          <tr>
              <td>
                  <button onclick="calificarOpMulRadio('opmulR',4,rc6_omr,'opradio');">CALIFICAR</button>
              </td>
              <td id="opmulR_cal"></td>
          </tr>
      </table>
      
      <!-- fin oopción múltiple -->
      
  </div>
  <!-- -->
  <div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
<script>
    var tc;
    var cCamCues = function(campo,valor,registro)
    {
      clearTimeout(tc);
      tc = window.setTimeout(function(){
          $.post('libreria/cCamCues.php',{
              modDatos: true,
              elcampo: campo,
              elvalor: valor,
              elregistro: registro
          },function(respuesta){
              if(respuesta === 'modificado')
              {
                  // Modificado
              } else {
                  // Error
                  console.log('-> '+respuesta);
              }
          });
      },1000);
    };
    var cResCues = function(cues,usu,cap,preg,resp)
    {
      clearTimeout(tc);
      tc = window.setTimeout(function(){
          $.post('libreria/cCamCues.php',{
              modRespuestas: true,
              cuestionario: cues,
              usuario: usu,
              capitulo: cap,
              pregunta: preg,
              respuesta:resp
          },function(respuesta){
              if(respuesta === 'modificado')
              {
                  // Modificado
              } else {
                  // Error
                  console.log('-> '+respuesta);
              }
          });
      },500);
    };
    
    window.onload = function(){
        protegerPLibres();
        protegerCompletar();
        protegerOpMulRadio('opradio');
    };
</script>
</body>
</html>