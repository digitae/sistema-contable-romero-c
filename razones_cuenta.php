<?php
include("libreria/principal.php");
include 'libreria/class_razones.php';

esUsuario(); // será usuario registrado?
$IDusuario = $_SESSION['userID'];
$IDpractica = filter_input(INPUT_GET, 'IDpractica',
        FILTER_VALIDATE_INT);
if(!isset($IDpractica) || empty($IDpractica) 
        || !validarPractica($IDpractica, 'rom_cuenta_practicas'))
{
    header("Location: portada.php");
    exit;
}
$editado = filter_input(INPUT_GET, 'editado',
        FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
if(!isset($editado))
{
    $editado = FALSE;
}
$form = new class_razones($IDpractica, 'rom_cuenta_razones','razones_cuenta.php');
$generado = $form->mostrarExiste();

$tabla = 'rom_cuenta_practicas';
$tCantidades = 'rom_cuenta_cantidades';
$tRazones = 'rom_cuenta_razones';
if($generado){
    include 'libreria/razones_totales.php';
}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sistema Contable Romero</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="libreria/js_principal.js"></script>
<script src="libreria/vendor/jquery/jquery-1.11.0.min.js"></script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>7.2 Estado de situación financiera, NIF B-6, cuenta</h1>
    </div>
    <div id="divTitularMenu">
        <a href="portada.php">Inicio</a> | 
        <a href="libreria/calculadora/calculadora.html" 
           target="_blank" 
           onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | 
        <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido" style="width: 1000px;">
  <div class="divContCuerpo">
      <h3 class="titulo-principal">Razones Financieras</h3>
      <div class="migas">
          <ul>
              <li><a href="portada.php">Portada</a></li>
              <li><a href="portada_cuenta.php">Prácticas</a></li>
              <li><img src="imagenes/print.png" 
                       alt="" 
                       height="25" 
                       style="float: left; margin-right: 5px;" />
                  <a href="javascript:print();">Imprimir página</a></li>
              <li><a href="practicas_cuenta.php?IDpractica=<?php echo $IDpractica ?>">Estado de situación financiera</a></li>
          </ul>
      </div>
      <div style="margin-bottom: 25px;"></div>
      
      <div id="form-razones" style="margin: 0 auto 25px auto; width: 450px; position: relative;">
          <?php echo $form->pintarFormulario(); ?>
      </div>
      
      <?php if($generado) { ?>
      <table class="razones">
          <tr>
              <td class="resaltado" colspan="3">
                  Universidad: <span class="datos"><?php echo extraInfo($tabla,'unidad',$IDpractica) ?></span>
              </td>
          </tr>
          <tr>
              <td class="resaltado" colspan="3">
                  Escuela o Facultad: <span class="datos"><?php echo extraInfo($tabla,'facultad',$IDpractica) ?></span>
              </td>
          </tr>
          <tr>
              <td class="resaltado" colspan="3">
                  Asignatura: <span class="datos"><?php echo extraInfo($tabla,'asignatura',$IDpractica) ?></span>
              </td>
          </tr>
          <tr>
              <td class="resaltado" colspan="3">
                  Alumno: <span class="datos"><?php echo nombreUsuario($IDusuario) ?></span></td>
          </tr>
          <tr>
              <td class="resaltado" colspan="3">
                  Maestro: <span class="datos"><?php echo extraInfo($tabla,'maestro',$IDpractica) ?></span>
              </td>
          </tr>
          <tr>
              <td class="resaltado" colspan="3">
                  Fecha de realización: <span class="datos"><?php echo extraInfo($tabla,'fechaTarea',$IDpractica) ?></span>
              </td>
          </tr>
          <tr>
              <td class="resaltado" colspan="3">Nombre de la entidad: <span class="datos"><?php echo nombreEntidad($IDpractica, 'rom_cuenta_practicas') ?></span></td>
          </tr>
          <tr>
              <td class="resaltado" colspan="3">Razones Financieras al <span class="datos"><?php echo extraerAactual($IDpractica, 'rom_cuenta_practicas') ?></span></td>
          </tr>
          <tr>
              <td class="area" rowspan="2">CAPITAL DE TRABAJO =</td>
              <td class="desc">AC</td>
              <td id="m4" class="resaltado" rowspan="2"><?php echo number_format($m4,2) ?></td>
          </tr>
          <tr>
              <td class="desc">PC</td>
          </tr>
          <tr>
              <td class="area" rowspan="2">SEVERA O PRUEBA DE ÁCIDO =</td>
              <td class="desc">AC - (Inv. + PA)</td>
              <td id="m7" class="resaltado" rowspan="2"><?php echo number_format($m7,2) ?></td>
          </tr>
          <tr>
              <td class="desc">PC</td>
          </tr>
          <tr>
              <td class="area" rowspan="2">LIQUIDEZ =</td>
              <td class="desc">E</td>
              <td id="m10" class="resaltado" rowspan="2"><?php echo number_format($m10,2) ?></td>
          </tr>
          <tr>
              <td class="desc">PC</td>
          </tr>
          <tr>
              <td class="area" rowspan="2">ENDEUDAMIENTO =</td>
              <td class="desc">PT</td>
              <td id="m13" class="resaltado" rowspan="2"><?php echo number_format($m13,2) ?></td>
          </tr>
          <tr>
              <td class="desc">AT</td>
          </tr>
          <tr>
              <td class="area" rowspan="2">INVERSIÓN Y CAPITAL CONTABLE =</td>
              <td class="desc">CC</td>
              <td id="m16" class="resaltado" rowspan="2"><?php echo number_format($m16,2) ?></td>
          </tr>
          <tr>
              <td class="desc">AT</td>
          </tr>
          <tr>
              <td class="area" rowspan="2">PROTECCIÓN AL PASIVO CIRCULANTE =</td>
              <td class="desc">CC</td>
              <td id="m19" class="resaltado" rowspan="2"><?php echo number_format($m19,2) ?></td>
          </tr>
          <tr>
              <td class="desc">PC</td>
          </tr>
          <tr>
              <td class="area" rowspan="2">ROTACIÓN DE CUENTAS POR COBRAR =</td>
              <td class="desc">VN</td>
              <td id="m22" class="resaltado" rowspan="2"><?php echo number_format($m22,2) ?></td>
          </tr>
          <tr>
              <td class="desc">PCC</td>
          </tr>
          <tr>
              <td class="area" rowspan="2">PLAZO MEDIO DE COBRO =</td>
              <td class="desc">360</td>
              <td id="m25" class="resaltado" rowspan="2"><?php echo number_format($m25,2) ?></td>
          </tr>
          <tr>
              <td class="desc">RCC</td>
          </tr>
          <tr>
              <td class="area" rowspan="2">ROTACIÓN DE INVENTARIOS =</td>
              <td class="desc">CV</td>
              <td id="m28" class="resaltado" rowspan="2"><?php echo number_format($m28,2) ?></td>
          </tr>
          <tr>
              <td class="desc">PI</td>
          </tr>
          <tr>
              <td class="area" rowspan="2">PLAZO MEDIO DE VENTAS =</td>
              <td class="desc">360</td>
              <td id="m31" class="resaltado" rowspan="2"><?php echo number_format($m31,2) ?></td>
          </tr>
          <tr>
              <td class="desc">RI</td>
          </tr>
          <tr>
              <td class="area" rowspan="2">ROTACIÓN DE CUENTAS POR PAGAR =</td>
              <td class="desc">CN</td>
              <td id="m33" class="resaltado" rowspan="2"><?php echo number_format($m33,2) ?></td>
          </tr>
          <tr>
              <td class="desc">PCP</td>
          </tr>
          <tr>
              <td class="area" rowspan="2">PLAZO MEDIO DE PAGO =</td>
              <td class="desc">360</td>
              <td id="m36" class="resaltado" rowspan="2"><?php echo number_format($m36,2) ?></td>
          </tr>
          <tr>
              <td class="desc">RCP</td>
          </tr>
          <tr>
              <td class="area" rowspan="2">MARGEN DE UTILIDAD =</td>
              <td class="desc">UN</td>
              <td id="m39" class="resaltado" rowspan="2"><?php echo number_format($m39,2) ?></td>
          </tr>
          <tr>
              <td class="desc">VN</td>
          </tr>
          <tr>
              <td class="area" rowspan="2">RENTABILIDAD INVERSIÓN =</td>
              <td class="desc">UN</td>
              <td id="m42" class="resaltado" rowspan="2"><?php echo number_format($m42,2) ?></td>
          </tr>
          <tr>
              <td class="desc">AT</td>
          </tr>
      </table>
      <?php } ?>
      
  </div>
  <!-- -->
  <div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
<script>
    var validarFloat = function(valor,id)
    {
        if(isNaN(valor))
        {
            // alert('Esto no es un número');
            // var campo = document.getElementById(id);
            // campo.value = "";
        }
    };
</script>
<?php if($editado) { ?>
<script src="libreria/mostrarMsgEditado.js"></script>
<?php } ?>
</body>
</html>