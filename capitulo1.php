<?php
include 'libreria/principal.php';
include 'libreria/evaluaciones.php';

esUsuario(); // será usuario registrado?
$IDusuario = $_SESSION['userID'];
$capitulo = 1;
$registro = verificarCuestionario($capitulo, $IDusuario,$Fecha);


?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> Principios de Contabilidad. Cuestionario de evaluación. Capítulo 1</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="libreria/js_principal.js"></script>
<script src="libreria/vendor/jquery/jquery-1.11.0.min.js"></script>
<script src="libreria/calCap.js"></script>
<script>
    var corr1 = [4,1,2,3];
    var corr2 = [3,5,7,6,8,2,4,1];
    var corr3 = [3,2,1];
</script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Cuestionario de autoevaluación</h1>
    </div>
    <div id="divTitularMenu">
        <a href="portada.php">Inicio</a> | 
        <a href="libreria/calculadora/calculadora.html" 
           target="_blank" 
           onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | 
        <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido" style="width: 900px">
  <div class="divContCuerpo">
      
      <h3 class="titulo-principal">Capítulo 1</h3>
      <div class="migas">
          <ul>
              <li><a href="portada.php">Portada</a></li>
              <li><a href="javascript:history.back();">Página anterior</a></li>
              <li><img src="imagenes/print.png" 
                       alt="" 
                       height="25" 
                       style="float: left; margin-right: 5px;" />
                  <a href="javascript:print();">Imprimir página</a></li>
          </ul>
      </div>
      
      <table class="mis-datos">
          <tr>
              <td>Universidad o Institución:</td>
              <td>
                  <input oninput="cCamCues('unidad',this.value,<?php echo $registro ?>);" 
                         name="unidad" 
                         id='unidad' 
                         required 
                         value="<?php echo xCamCues('unidad', $registro) ?>" />
              </td>
              <td>Escuela o facultad:</td>
              <td>
                  <input oninput="cCamCues('facultad',this.value,<?php echo $registro ?>);" 
                         name="facultad" 
                         id='facultad' 
                         required 
                         value="<?php echo xCamCues('facultad', $registro) ?>" />
              </td>
          </tr>
          <tr>
              <td>Asignatura:</td>
              <td>
                  <input oninput="cCamCues('asignatura',this.value,<?php echo $registro ?>);" 
                         name="asignatura" 
                         id='asignatura' 
                         required 
                         value="<?php echo xCamCues('asignatura', $registro) ?>" />
              </td>
              <td>Alumno:</td>
              <td>
                  <?php echo nombreUsuario($IDusuario) ?>
              </td>
          </tr>
          <tr>
              <td>Maestro:</td>
              <td>
                  <input oninput="cCamCues('maestro',this.value,<?php echo $registro ?>);" 
                         name="maestro" 
                         id='maestro' 
                         required 
                         value="<?php echo xCamCues('maestro', $registro) ?>" />
              </td>
              <td>Fecha:</td>
              <td>
                  <input oninput="cCamCues('fecha',this.value,<?php echo $registro ?>);" 
                         name="fecha" 
                         id='fecha' 
                         required 
                         value="<?php echo xCamCues('fecha', $registro) ?>" />
              </td>
          </tr>
      </table>
      
      <h4 class="subtit-4">OBJETIVOS DE APRENDIZAJE</h4>
      
      <div class="objetivos">
          <p>Al finalizar este capítulo, el alumno será capaz de:</p>
          <ol>
              <li>Describir el concepto de profesión y comprender que la 
                  contaduría pública es una profesión que satisface la necesidad 
                  de información financiera, por medio de estados financieros 
                  que se utilizan para tomar decisiones.</li>
              <li>Comprender que la contabilidad financiera es la rama de la 
                  contaduría pública mediante la cual se puede elaborar la 
                  información financiera.</li>
              <li>Entender el papel que desempeña el profesional de la contaduría 
                  pública, para lo cual deberá identificar los requisitos que 
                  integran el perfil de un contador público, así como visualizar 
                  su campo de acción profesional.</li>
              <li>Identificar la importancia que tiene la contaduría pública con 
                  el fin de visualizar y pronosticar su futuro.</li>
          </ol>
      </div>
      
      <h4 class="subtit-4">EJERCICIOS</h4>
      
      <h5>I. RESUELVA LO SIGUIENTE</h5>
      
      <div class="pregunta-libre">
          <p><span>1</span> Diga qué es la contaduría pública como profesión.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib01" 
                    id="prelib01"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib01'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>2</span> Señale los requisitos que debe reunir un profesional 
              de la contabilidad.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib02" id="prelib02"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib02'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>3</span> Para calificar una actividad con el término de 
              profesional y que a sus ejecutantes se les pueda llamar 
              profesionales, deben cumplir determinados requisitos. 
              Señale los más importantes.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib03" id="prelib03"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib03'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>4</span> Explique la importancia de la adecuada 
              administración de la información de los recursos financieros.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib04" id="prelib04"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib04'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>5</span> En relación con los recursos, ¿cuál es la opinión 
              de Ballesteros Inda y Martín Granados?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib05" id="prelib05"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib05'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>6</span> ¿Qué se entiende por finanzas?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib06" id="prelib06"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib06'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>7</span> ¿Por qué la contabilidad financiera es la materia 
              más importante en la licenciatura de contaduría pública?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib07" id="prelib07"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib07'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>8</span> Mencione cuál es la función del contador 
          público en las empresas.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib08" id="prelib08"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib08'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>9</span> ¿En qué fecha se fundó el Instituto Mexicano 
              de Contadores Públicos?</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib09" id="prelib09"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib09'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>10</span> Señale en qué fecha se constituyó el Consejo Mexicano 
          de Normas de Información Financiera (CINIF).</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib10" id="prelib10"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib10'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>11</span> Señale a partir de qué fecha la emisión de los 
              principios de contabilidad dejó de ser responsabilidad del 
              Instituto Mexicano de Contadores Públicos, A.C., a través de la 
              Comisión de Principios de Contabilidad (CPC), función que se 
              transfirió al Consejo Mexicano de Normas de Información 
              Financiera, A.C. (CINIF).</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib11" id="prelib11"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib11'); ?></textarea>
      </div>
      
      <div class="pregunta-libre">
          <p><span>12</span> Mencione cuáles son los objetivos del CINIF.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" 
                    name="prelib12" id="prelib12"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib12'); ?></textarea>
      </div>
      
      <h5>II. CORRELACIONE LAS COLUMNAS 1 Y 2</h5>
      
      <table class='correlaciones'>
          <caption>1. Requisitos de una profesión</caption>
          <tr>
              <th>Columna 1</th>
              <th>Columna 2</th>
          </tr>
          <tr>
              <td>
                  <ol type='a'>
                      <li>Bachillerato previo o estudios equivalentes</li>
                      <li>Conjunto de conocimientos especializados adquiridos 
                      en una universidad</li>
                      <li>Título profesional expedido por una institución 
                      docente superior autorizada</li>
                  </ol>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_1' id='corr1_1'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>1 Intelectuales</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>2 Legales</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>3 Sociales</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>4 Académicos</option>
                  </select>
                  <span id='corr1_1-parcial'></span>
              </td>
              <td rowspan="4">
                  <ol>
                      <li>Intelectuales</li>
                      <li>Legales</li>
                      <li>Sociales</li>
                      <li>Académicos</li>
                  </ol>
              </td>
          </tr>
          <tr>
              <td>
                  <ol type='a'>
                      <li>Capacidad de observación</li>
                      <li>Capacidad de juicio</li>
                      <li>Capacidad de comunicación</li>
                      <li>Capacidad para tomar decisiones</li>
                  </ol>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_2' id="corr1_2">
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>1 Intelectuales</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>2 Legales</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>3 Sociales</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>4 Académicos</option>
                  </select>
                  <span id='corr1_2-parcial'></span>
              </td>
          </tr>
          <tr>
              <td>
                  <ol type='a'>
                      <li>El reconocimiento de la ley reglamentaria de los 
                      artículos 4º y 5º de la Constitución Política de los 
                      Estados Unidos Mexicanos, referentes al ejercicio de 
                      las profesiones</li>
                      <li>La existencia de un cuerpo colegiado que vele por el 
                      bienestar y progreso de la profesión</li>
                  </ol>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_3' id='corr1_3'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>1 Intelectuales</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>2 Legales</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>3 Sociales</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>4 Académicos</option>
                  </select>
                  <span id='corr1_3-parcial'></span>
              </td>
          </tr>
          <tr>
              <td>
                  <ol type='a'>
                      <li>Ser una actividad dotada de interés público</li>
                      <li>Estar regida por un conjunto de normas que estipulen 
                      una conducta a seguir</li>
                      <li>Cumplir por lo menos con un estándar de calidad en 
                      sus servicios</li>
                  </ol>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_4' id='corr1_4'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>1 Intelectuales</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>2 Legales</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>3 Sociales</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>4 Académicos</option>
                  </select>
                  <span id='corr1_4-parcial'></span>
              </td>
          </tr>
          <tr>
              <td id='corr1_cal'></td>
              <td><button onclick="calificarCorrelacion('corr1',4,corr1);" 
                          name='cal_corr1'>CALIFICAR</button></td>
          </tr>
      </table>

      <table class="correlaciones">
          <caption>2. Áreas de conocimiento en apoyo a la contaduría</caption>
          <tr>
              <th>Columna 1</th>
              <th>Columna 2</th>
          </tr>
          <tr>
              <td>
                  <p>Provee las herramientas necesarias para ubicar a las entidades 
                  dentro de un marco de legalidad.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr2_1' id="corr2_1">
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_1')) ?>>1 Administración</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_1')) ?>>2 Ciencias Sociales</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_1')) ?>>3 Derecho</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_1')) ?>>4 Economía</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_1')) ?>>5 Finanzas</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_1')) ?>>6 Legislación Fiscal</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_1')) ?>>7 Informática</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_1')) ?>>8 Matemáticas</option>
                  </select>
                  <span id='corr2_1-parcial'></span>
              </td>
              <td rowspan="8">
                  <ol>
                      <li>Administración</li>
                      <li>Ciencias Sociales</li>
                      <li>Derecho</li>
                      <li>Economía</li>
                      <li>Finanzas</li>
                      <li>Legislación Fiscal</li>
                      <li>Informática</li>
                      <li>Matemáticas</li>
                  </ol>
              </td>
          </tr>
          <tr>
              <td>
                  <p>Analizan la obtención y aplicación óptimas de recursos 
                  financieros de las entidades para el logro de su misión.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr2_2' id="corr2_2">
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_2')) ?>>1 Administración</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_2')) ?>>2 Ciencias Sociales</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_2')) ?>>3 Derecho</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_2')) ?>>4 Economía</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_2')) ?>>5 Finanzas</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_2')) ?>>6 Legislación Fiscal</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_2')) ?>>7 Informática</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_2')) ?>>8 Matemáticas</option>
                  </select>
                  <span id='corr2_2-parcial'></span>
              </td>
          </tr>
          <tr>
              <td>
                  <p>Se dedica al estudio, diseño y desarrollo de sistemas de 
                  información acordes con las necesidades de las entidades.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr2_3' id="corr2_3">
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_3')) ?>>1 Administración</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_3')) ?>>2 Ciencias Sociales</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_3')) ?>>3 Derecho</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_3')) ?>>4 Economía</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_3')) ?>>5 Finanzas</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_3')) ?>>6 Legislación Fiscal</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_3')) ?>>7 Informática</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_3')) ?>>8 Matemáticas</option>
                  </select>
                  <span id='corr2_3-parcial'></span>
              </td>
          </tr>
          <tr>
              <td>
                  <p>Estudia y reglamenta las normas de carácter impositivo para 
                  la obtención de recursos por parte del Estado con el objeto 
                  de cumplir la función de dar servicios públicos.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr2_4' id='corr2_4'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_4')) ?>>1 Administración</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_4')) ?>>2 Ciencias Sociales</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_4')) ?>>3 Derecho</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_4')) ?>>4 Economía</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_4')) ?>>5 Finanzas</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_4')) ?>>6 Legislación Fiscal</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_4')) ?>>7 Informática</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_4')) ?>>8 Matemáticas</option>
                  </select>
                  <span id='corr2_4-parcial'></span>
              </td>
          </tr>
          <tr>
              <td>
                  <p>Permite analizar y resolver problemas con un procedimiento 
                  lógico.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr2_5' id="corr2_5">
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_5')) ?>>1 Administración</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_5')) ?>>2 Ciencias Sociales</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_5')) ?>>3 Derecho</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_5')) ?>>4 Economía</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_5')) ?>>5 Finanzas</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_5')) ?>>6 Legislación Fiscal</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_5')) ?>>7 Informática</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_5')) ?>>8 Matemáticas</option>
                  </select>
                  <span id='corr2_5-parcial'></span>
              </td>
          </tr>
          <tr>
              <td>
                  <p>Permiten conocer la realidad humana tanto en lo individual 
                  como en lo social.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr2_6' id="corr2_6">
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_6')) ?>>1 Administración</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_6')) ?>>2 Ciencias Sociales</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_6')) ?>>3 Derecho</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_6')) ?>>4 Economía</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_6')) ?>>5 Finanzas</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_6')) ?>>6 Legislación Fiscal</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_6')) ?>>7 Informática</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_6')) ?>>8 Matemáticas</option>
                  </select>
                  <span id='corr2_6-parcial'></span>
              </td>
          </tr>
          <tr>
              <td>
                  <p>Se dedica al estudio de las necesidades y satisfactores que 
                  determinan la riqueza de los países.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr2_7' id="corr2_7">
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_7')) ?>>1 Administración</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_7')) ?>>2 Ciencias Sociales</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_7')) ?>>3 Derecho</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_7')) ?>>4 Economía</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_7')) ?>>5 Finanzas</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_7')) ?>>6 Legislación Fiscal</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_7')) ?>>7 Informática</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_7')) ?>>8 Matemáticas</option>
                  </select>
                  <span id='corr2_7-parcial'></span>
              </td>
          </tr>
          <tr>
              <td>
                  <p>Entendida como la conducción de grupos humanos hacia el logro 
                  de objetivos (misión) de la organización, mediante la 
                  optimización de sus recursos.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr2_8' id="corr2_8">
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_8')) ?>>1 Administración</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_8')) ?>>2 Ciencias Sociales</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_8')) ?>>3 Derecho</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_8')) ?>>4 Economía</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_8')) ?>>5 Finanzas</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_8')) ?>>6 Legislación Fiscal</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_8')) ?>>7 Informática</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr2_8')) ?>>8 Matemáticas</option>>
                  </select>
                  <span id='corr2_8-parcial'></span>
              </td>
          </tr>
          <tr>
              <td id='corr2_cal'></td>
              <td><button onclick="calificarCorrelacion('corr2',8,corr2)" 
                          name='cal_corr2'>CALIFICAR</button></td>
          </tr>
      </table>
      
      <table class="correlaciones">
          <caption>3. Actitudes, conocimientos y habilidades del contador 
              público</caption>
          <tr>
              <th>Columna 1</th>
              <th>Columna 2</th>
          </tr>
          <tr>
              <td>
                  <p>Analizar las diferentes tendencias socioeconómicas, 
                  financieras, tributarias y contables que se generan en el 
                  contexto nacional y global.</p>
                  <p>Con base en una visión integral y los conocimientos de 
                      las diversas disciplinas, poder hacer análisis que permitan 
                      tomar decisiones.</p>
                  <p>Conocimientos de las áreas fundamentales de la profesión.</p>
                  <ol type='a'>
                      <li>Contabilidad</li>
                      <li>Finanzas</li>
                      <li>Auditoría</li>
                      <li>Costos</li>
                      <li>Fiscal</li>
                  </ol>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr3_1' id="corr3_1">
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr3_1')) ?>>1 Habilidades</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr3_1')) ?>>2 Actitudes</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr3_1')) ?>>3 Conocimientos</option>
                  </select>
                  <span id='corr3_1-parcial'></span>
              </td>
              <td rowspan="3">
                  <ol>
                      <li>Habilidades</li>
                      <li>Actitudes</li>
                      <li>Conocimientos</li>
                  </ol>
              </td>
          </tr>
          <tr>
              <td>
                  <p>Ejercer su profesión con Ética y espíritu de servicio.</p>
                  <p>Participar con responsabilidad social y compromiso con el 
                  desarrollo sustentable del país.</p>
                  <p>Asumir la obligación de apertura al cambio y respeto hacia 
                      la diversidad cultural.</p>
                  <p>De estudio permanente.</p>
                  <p>De apertura al cambio.</p>
                  <p>De apertura y respeto hacia otras culturas.</p>
                  <p>Valorar de manera Reflexiva y crítica ante las nuevas 
                      propuestas teóricas y técnicas que surjan en su 
                      profesión.</p>
                  <p>Desarrollar una práctica profesional proactiva, innovadora y 
                  emprendedora con el compromiso de superación y estudio 
                  permanente.</p>
                  <p>Negociadora y conciliadora.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr3_2' id="corr3_2">
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr3_2')) ?>>1 Habilidades</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr3_2')) ?>>2 Actitudes</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr3_2')) ?>>3 Conocimientos</option>
                  </select>
                  <span id='corr3_2-parcial'></span>
              </td>
          </tr>
          <tr>
              <td>
                  <p>Desarrollar una visión integral sobre los objetivos de las 
                  organizaciones y sus estrategias financieras.</p>
                  <p>Aplicar los conocimientos adquiridos en forma crítica, en su 
                  ejercicio profesional.</p>
                  <p>Conducir grupos y participar en equipos multidisciplinarios 
                  para fundamentar la toma de decisiones financieras.</p>
                  <p>Asesorar en materia financiera, contable y tributaria. </p>
                  <p>Diseñar procedimientos contables acordes con la normatividad 
                  y disposiciones legales, tributarias y de normatividad de 
                  la profesión.</p>
                  <p>Operar sistemas de cómputo y comunicación para la generación 
                  de la información financiera y administrativa.</p>
                  <p>Desarrollar una actitud crítica que le permita aplicar los 
                  conocimientos adquiridos para llevar a cabo auditorías de 
                  estados financieros.</p>
                  <p>Diseñar los métodos y procedimientos para el control interno 
                  de las organizaciones. Mantenerse actualizado 
                  profesionalmente.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr3_3' id="corr3_3">
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr3_3')) ?>>1 Habilidades</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr3_3')) ?>>2 Actitudes</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr3_3')) ?>>3 Conocimientos</option>
                  </select>
                  <span id='corr3_3-parcial'></span>
              </td>
          </tr>
          <tr>
              <td id='corr3_cal'></td>
              <td><button onclick="calificarCorrelacion('corr3',3,corr3)" 
                          name='cal_corr3'>CALIFICAR</button></td>
          </tr>
      </table>
      
  </div>
  <!-- -->
  <div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
<script>
    var tc;
    var cCamCues = function(campo,valor,registro)
    {
      clearTimeout(tc);
      tc = window.setTimeout(function(){
          $.post('libreria/cCamCues.php',{
              modDatos: true,
              elcampo: campo,
              elvalor: valor,
              elregistro: registro
          },function(respuesta){
              if(respuesta === 'modificado')
              {
                  // Modificado
              } else {
                  // Error
                  console.log('-> '+respuesta);
              }
          });
      },1000);
    };
    var cResCues = function(cues,usu,cap,preg,resp)
    {
      clearTimeout(tc);
      tc = window.setTimeout(function(){
          $.post('libreria/cCamCues.php',{
              modRespuestas: true,
              cuestionario: cues,
              usuario: usu,
              capitulo: cap,
              pregunta: preg,
              respuesta:resp
          },function(respuesta){
              if(respuesta === 'modificado')
              {
                  // Modificado
              } else {
                  // Error
                  console.log('-> '+respuesta);
              }
          });
      },500);
    };
    
    window.onload = function(){
        protegerPLibres();
        protegerCorrelaciones();
    };
    
</script>
</body>
</html>