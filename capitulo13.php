<?php
include 'libreria/principal.php';
include 'libreria/evaluaciones.php';

esUsuario(); // será usuario registrado?
$IDusuario = $_SESSION['userID'];
$capitulo = 13;
$registro = verificarCuestionario($capitulo, $IDusuario,  utf8_decode($Fecha));


?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> Principios de Contabilidad. Cuestionario de evaluación. Capítulo 13</title>
<link href="css/principal.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="libreria/js_principal.js"></script>
<script src="libreria/vendor/jquery/jquery-1.11.0.min.js"></script>
<script src="libreria/calCap.js"></script>
<script src="libreria/rc13.js"></script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<!-- div Header -->
<?php include("libreria/header.php"); ?>
<!-- fin Header -->
<!-- div Titular -->
<div id="divTitular">
  <div id="divTitularM">
    <div id="divTitularTitulo">
      <h1>Cuestionario de autoevaluación</h1>
    </div>
    <div id="divTitularMenu">
        <a href="portada.php">Inicio</a> | 
        <a href="libreria/calculadora/calculadora.html" 
           target="_blank" 
           onclick="NewWindow(this.href,'name','233','259','no');return false;">Calculadora</a> | 
        <a href="ayuda.htm">Ayuda</a></div>
  </div>
</div>
<!-- fin Titular -->
<!-- div Contenido -->
<div id="divContenido" style="width: 900px">
  <div class="divContCuerpo">
      
      <h3 class="titulo-principal">Capítulo 13</h3>
      <div class="migas">
          <ul>
              <li><a href="portada.php">Portada</a></li>
              <li><a href="javascript:history.back();">Página anterior</a></li>
              <li><img src="imagenes/print.png" 
                       alt="" 
                       height="25" 
                       style="float: left; margin-right: 5px;" />
                  <a href="javascript:print();">Imprimir página</a></li>
          </ul>
      </div>
      
      <table class="mis-datos">
          <tr>
              <td>Universidad o Institución:</td>
              <td>
                  <input oninput="cCamCues('unidad',this.value,<?php echo $registro ?>);" 
                         name="unidad" 
                         id='unidad' 
                         required 
                         value="<?php echo xCamCues('unidad', $registro) ?>" />
              </td>
              <td>Escuela o facultad:</td>
              <td>
                  <input oninput="cCamCues('facultad',this.value,<?php echo $registro ?>);" 
                         name="facultad" 
                         id='facultad' 
                         required 
                         value="<?php echo xCamCues('facultad', $registro) ?>" />
              </td>
          </tr>
          <tr>
              <td>Asignatura:</td>
              <td>
                  <input oninput="cCamCues('asignatura',this.value,<?php echo $registro ?>);" 
                         name="asignatura" 
                         id='asignatura' 
                         required 
                         value="<?php echo xCamCues('asignatura', $registro) ?>" />
              </td>
              <td>Alumno:</td>
              <td>
                  <?php echo nombreUsuario($IDusuario) ?>
              </td>
          </tr>
          <tr>
              <td>Maestro:</td>
              <td>
                  <input oninput="cCamCues('maestro',this.value,<?php echo $registro ?>);" 
                         name="maestro" 
                         id='maestro' 
                         required 
                         value="<?php echo xCamCues('maestro', $registro) ?>" />
              </td>
              <td>Fecha:</td>
              <td>
                  <input oninput="cCamCues('fecha',this.value,<?php echo $registro ?>);" 
                         name="fecha" 
                         id='fecha' 
                         required 
                         value="<?php echo xCamCues('fecha', $registro) ?>" />
              </td>
          </tr>
      </table>
      
      <h4 class="subtit-4">OBJETIVOS DE APRENDIZAJE</h4>
      
      <div class="objetivos">
          <p>Al finalizar este capítulo, el alumno será capaz de:</p>
          <ol>
              <li>Comprender y expresar el significado de las diferentes cuentas.</li>
              <li>Comprender y expresar mediante cargos y abonos los aumentos y 
                  disminuciones de las diferentes cuentas al inicio, durante y 
                  al final del ejercicio.</li>
              <li>Comprender y expresar la naturaleza y presentación del saldo 
                  de las diferentes cuentas en los estados financieros.</li>
              <li>Diseñar catálogos de cuentas.</li>
          </ol>
      </div>
      
      <h4 class="subtit-4">EJERCICIOS</h4>
      
      <h5>I. RESUELVA LO SIGUIENTE</h5>
      
      <div class="pregunta-libre">
          <p><span>1</span> De todas las cuentas estudiadas, comente su 
              concepto, por qué cargan y abonan al inicio, durante y al 
              finalizar el ejercicio, la naturaleza de su saldo y su 
              presentación en el balance general o en el estado de resultados.</p>
          <textarea oninput="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name="prelib01" 
                    id="prelib01"><?php echo xPregCues($registro, 
                  $IDusuario, $capitulo, 'prelib01'); ?></textarea>
      </div>
      
      <h5>II. RELACIONE LAS COLUMNAS 1 Y 2</h5>
      
      <table class='correlaciones'>
          <tr>
              <th>Columna 1</th>
              <th>Columna 2</th>
          </tr>
          <tr>
              <td>
                  <span class="num">1</span>
                  <p>Registra operaciones derivadas de la venta de mercancías o 
                      la prestación de servicios, única y exclusivamente a 
                      crédito, ya sean documentadas o no.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_1' id='corr1_1'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>10</option>
                      <option value="11" <?php selOpcion(11, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>11</option>
                      <option value="12" <?php selOpcion(12, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_1')) ?>>12</option>
                  </select>
                  <span id='corr1_1-parcial'></span>
              </td>
              <td rowspan="12">
                  <ol>
                      <li>Proveedores</li>
                      <li>Gastos de administración</li>
                      <li>Documentos por pagar</li>
                      <li>Activo circulante</li>
                      <li>Deudores</li>
                      <li>Acreedores</li>
                      <li>Gastos de venta</li>
                      <li>Clientes</li>
                      <li>Gastos de constitución</li>
                      <li>Activo intangible</li>
                      <li>Capital</li>
                      <li>Activo no circulante</li>
                  </ol>
              </td>
          </tr>
          <tr>
              <td>
                  <span class="num">2</span>
                  <p>La papelería, propaganda, primas de seguros y fianzas, 
                      rentas y los intereses pagados por anticipado cuya 
                      utilización se lleva a cabo dentro de un año o en el 
                      ciclo financiero a corto plazo se presentan dentro del...</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_2' id='corr1_2'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>10</option>
                      <option value="11" <?php selOpcion(11, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>11</option>
                      <option value="12" <?php selOpcion(12, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_2')) ?>>12</option>
                  </select>
                  <span id='corr1_2-parcial'></span>
              </td>
          </tr>
          
          <tr>
              <td>
                  <span class="num">3</span>
                  <p>Registra las erogaciones pagadas cuando se lleva a cabo el 
                      proceso de constitución como sociedad mercantil.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_3' id='corr1_3'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>10</option>
                      <option value="11" <?php selOpcion(11, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>11</option>
                      <option value="12" <?php selOpcion(12, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_3')) ?>>12</option>
                  </select>
                  <span id='corr1_3-parcial'></span>
              </td>
          </tr>
          
          <tr>
              <td>
                  <span class="num">4</span>
                  <p>Registra operaciones con empresas o personas a las que se 
                      compraron bienes o servicios distintos de mercancías a 
                      crédito.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_4' id='corr1_4'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>10</option>
                      <option value="11" <?php selOpcion(11, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>11</option>
                      <option value="12" <?php selOpcion(12, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_4')) ?>>12</option>
                  </select>
                  <span id='corr1_4-parcial'></span>
              </td>
          </tr>
          
          <tr>
              <td>
                  <span class="num">5</span>
                  <p>Registra las erogaciones que la empresa realiza con la 
                      finalidad de lograr los objetivos para los que fue creada, 
                      es decir, relacionadas con las personas que toman decisiones.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_5' id='corr1_5'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>10</option>
                      <option value="11" <?php selOpcion(11, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>11</option>
                      <option value="12" <?php selOpcion(12, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_5')) ?>>12</option>
                  </select>
                  <span id='corr1_5-parcial'></span>
              </td>
          </tr>
          
          <tr>
              <td>
                  <span class="num">6</span>
                  <p>Registra las compras de conceptos distintos de mercancías o 
                      prestación de servicios, única y exclusivamente a crédito 
                      documentado, letras de cambio o pagarés.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_6' id='corr1_6'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>10</option>
                      <option value="11" <?php selOpcion(11, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>11</option>
                      <option value="12" <?php selOpcion(12, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_6')) ?>>12</option>
                  </select>
                  <span id='corr1_6-parcial'></span>
              </td>
          </tr>
          
          <tr>
              <td>
                  <span class="num">7</span>
                  <p>Registra operaciones derivadas de la compra de mercancías, 
                      única y exclusivamente a crédito, ya sean documentadas o no.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_7' id='corr1_7'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>10</option>
                      <option value="11" <?php selOpcion(11, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>11</option>
                      <option value="12" <?php selOpcion(12, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_7')) ?>>12</option>
                  </select>
                  <span id='corr1_7-parcial'></span>
              </td>
          </tr>
          
          <tr>
              <td>
                  <span class="num">8</span>
                  <p>Registra operaciones de venta de conceptos distintos de 
                      mercancías o prestación de servicios, los préstamos 
                      concedidos y otros análogos, única y exclusivamente a 
                      crédito, sin garantía documental.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_8' id='corr1_8'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>10</option>
                      <option value="11" <?php selOpcion(11, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>11</option>
                      <option value="12" <?php selOpcion(12, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_8')) ?>>12</option>
                  </select>
                  <span id='corr1_8-parcial'></span>
              </td>
          </tr>
          
          <tr>
              <td>
                  <span class="num">9</span>
                  <p>Registra las erogaciones realizadas con la finalidad de 
                      incrementar el volumen de ventas.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_9' id='corr1_9'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>10</option>
                      <option value="11" <?php selOpcion(11, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>11</option>
                      <option value="12" <?php selOpcion(12, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_9')) ?>>12</option>
                  </select>
                  <span id='corr1_9-parcial'></span>
              </td>
          </tr>
          
          <tr>
              <td>
                  <span class="num">10</span>
                  <p>Los terrenos, edificios, equipo de reparto, transporte, 
                      etc., se presentan en el balance general en el…</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_10' id='corr1_10'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>10</option>
                      <option value="11" <?php selOpcion(11, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>11</option>
                      <option value="12" <?php selOpcion(12, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_10')) ?>>12</option>
                  </select>
                  <span id='corr1_10-parcial'></span>
              </td>
          </tr>
          
          <tr>
              <td>
                  <span class="num">11</span>
                  <p>Derechos de autor, patentes, marcas, crédito mercantil, 
                      gastos preoperativos, son cuentas del…</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_11' id='corr1_11'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_11')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_11')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_11')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_11')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_11')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_11')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_11')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_11')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_11')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_11')) ?>>10</option>
                      <option value="11" <?php selOpcion(11, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_11')) ?>>11</option>
                      <option value="12" <?php selOpcion(12, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_11')) ?>>12</option>
                  </select>
                  <span id='corr1_11-parcial'></span>
              </td>
          </tr>
          
          <tr>
              <td>
                  <span class="num">12</span>
                  <p>Registra las aportaciones y retiros del propietario, 
                      en empresas de propiedad individual.</p>
                  <select onchange="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.id,this.value);" name='corr1_12' id='corr1_12'>
                      <option value='no'>Respuesta</option>
                      <option value="1" <?php selOpcion(1, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_12')) ?>>1</option>
                      <option value="2" <?php selOpcion(2, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_12')) ?>>2</option>
                      <option value="3" <?php selOpcion(3, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_12')) ?>>3</option>
                      <option value="4" <?php selOpcion(4, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_12')) ?>>4</option>
                      <option value="5" <?php selOpcion(5, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_12')) ?>>5</option>
                      <option value="6" <?php selOpcion(6, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_12')) ?>>6</option>
                      <option value="7" <?php selOpcion(7, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_12')) ?>>7</option>
                      <option value="8" <?php selOpcion(8, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_12')) ?>>8</option>
                      <option value="9" <?php selOpcion(9, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_12')) ?>>9</option>
                      <option value="10" <?php selOpcion(10, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_12')) ?>>10</option>
                      <option value="11" <?php selOpcion(11, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_12')) ?>>11</option>
                      <option value="12" <?php selOpcion(12, xPregCues($registro, 
                  $IDusuario, $capitulo, 'corr1_12')) ?>>12</option>
                  </select>
                  <span id='corr1_12-parcial'></span>
              </td>
          </tr>
                    
          <tr>
              <td id='corr1_cal'></td>
              <td><button onclick="calificarCorrelacion('corr1',12,corr13);" 
                          name='cal_corr1'>CALIFICAR</button></td>
          </tr>
      </table>
      
      <h5>III. SEÑALE SI EL ENUNCIADO ES VERDADERO O FALSO</h5>
      
      <table class="verdadero-falso" id="vf_simple01">
          <tr>
              <th>Enunciado</th>
              <th>Verdadero</th>
              <th>Falso</th>
          </tr>
          
          <!-- tanda -->
          <tr>
              <td>1. Cuando la cuenta de bancos tiene saldo acreedor, se 
                  muestra como un pasivo a corto plazo o circulante.</td>
              <td id="vf_simple01_verfal_1v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_1" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_1')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_1f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_1" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_1')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>2. Las cuentas que representan pagos anticipados, como 
                  papelería y útiles, propaganda, primas de seguros y fianzas, 
                  rentas e intereses pagados por anticipado siempre se presentan 
                  como un activo diferido.</td>
              <td id="vf_simple01_verfal_2v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_2" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_2')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_2f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_2" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_2')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>3. La cuenta rentas cobradas por anticipado, al devengarse, 
                  se convierte en utilidad.</td>
              <td id="vf_simple01_verfal_3v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_3" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_3')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_3f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_3" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_3')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>4. La cuenta aportaciones para futuros aumentos de capital 
                  aumenta cargando y disminuye abonando, y su saldo es deudor.</td>
              <td id="vf_simple01_verfal_4v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_4" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_4')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_4f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_4" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_4')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>5. Las cuentas de resultados, tales como gastos de venta, 
                  administración financieros, etc., tienen saldo inicial, 
                  es decir, se cargan al iniciar el ejercicio.</td>
              <td id="vf_simple01_verfal_5v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_5" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_5')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_5f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_5" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_5')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>6. La cuenta pérdidas y ganancias sólo tiene movimientos al 
                  finalizar el ejercicio y se conoce como cuenta liquidadora.</td>
              <td id="vf_simple01_verfal_6v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_6" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_6')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_6f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_6" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_6')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <!-- tanda -->
          <tr>
              <td>7. El saldo deudor de la cuenta gastos de constitución se 
                  presenta dentro del estado de resultados como parte de los 
                  gastos de operación.</td>
              <td id="vf_simple01_verfal_7v_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_7" 
                         value="verdadero" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_7')==='verdadero')?' checked':'' ?> /></td>
              
              <td id="vf_simple01_verfal_7f_parcial" style="text-align: center">
                  <input onclick="cResCues(<?php echo $registro ?>,
                    <?php echo $IDusuario ?>,<?php echo $capitulo ?>,
                    this.name,this.value);" type="radio" 
                         name="simple01_verfal_7" 
                         value="falso" <?php echo (xPregCues($registro, 
                  $IDusuario, $capitulo, 'simple01_verfal_7')==='falso')?' checked':'' ?> /></td>
          </tr>
          <!-- fin -->
          
          <tr>
              <td colspan="2" id="vf_simple01_verfal_cal"></td>
              <td>
                  <button onclick="calificarVerFalSimple('verfal',7,rc13_simpleA,'vf_simple01');">CALIFICAR</button>
              </td>
          </tr>
      </table>
      
  </div>
  <!-- -->
  <div id="divContRemate">&nbsp;</div>
</div>
<!-- fin Contenido -->
<!-- div Footer -->
<?php include("libreria/footer.php"); ?>
<!-- fin Footer -->
<script>
    var tc;
    var cCamCues = function(campo,valor,registro)
    {
      clearTimeout(tc);
      tc = window.setTimeout(function(){
          $.post('libreria/cCamCues.php',{
              modDatos: true,
              elcampo: campo,
              elvalor: valor,
              elregistro: registro
          },function(respuesta){
              if(respuesta === 'modificado')
              {
                  // Modificado
              } else {
                  // Error
                  console.log('-> '+respuesta);
              }
          });
      },1000);
    };
    var cResCues = function(cues,usu,cap,preg,resp)
    {
      clearTimeout(tc);
      tc = window.setTimeout(function(){
          $.post('libreria/cCamCues.php',{
              modRespuestas: true,
              cuestionario: cues,
              usuario: usu,
              capitulo: cap,
              pregunta: preg,
              respuesta:resp
          },function(respuesta){
              if(respuesta === 'modificado')
              {
                  // Modificado
              } else {
                  // Error
                  console.log('-> '+respuesta);
              }
          });
      },500);
    };
    
    window.onload = function(){
        protegerCorrelaciones();
        protegerVerFalSimple('vf_simple01');
    };
</script>
</body>
</html>