<?php
include("libreria/principal.php");

// comprobar que han iniciado un ejercicio
if(isset($_GET['IDejercicio']) && isset($_GET['metodo']) && isset($_GET['asiento'])){
	$IDejercicio = $_GET['IDejercicio'];
	$metodo = $_GET['metodo'];
	$asiento = $_GET['asiento'];
} else {
	header("Location: portada.php?ejercicio=false");
	exit;	
}
//fin

// extraer datos para llenar la balanza
// NOTA: asumimos que todos los asientos están cerrados
$queryCuentas = "SELECT DISTINCT cuenta FROM rom_cantidades
				WHERE subcuenta = '' AND ejercicio = '$IDejercicio'
				ORDER BY cuenta ASC";
$resulCuentas = mysql_query($queryCuentas);
$datosCuentas = mysql_fetch_assoc($resulCuentas);

// extraer total
$totalMd = 0;
$totalMh = 0;
$totalSd = 0;
$totalSh = 0;

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Balanza antes de ajustes</title>
<link href="css/imprimible.css" rel="stylesheet" type="text/css" />

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6288383-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- fin GA -->

</head>

<body>
<div id="header">
  <h1>Sistema Contable Romero</h1>
</div>
<h2>Balanza antes de ajustes</h2>
<div id="info">
Ejercicio: <span class="divContCuerpo">
<?php pintarNejercicio($IDejercicio); ?>
</span> - Método: <span class="divContCuerpo">
<?php pintarMetodo($metodo); ?>
</span></div>
<div>
<fieldset>
  <legend>Registros del ejercicio</legend>
  <div class="datos">
    <table cellspacing="5">
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td colspan="2" align="center" valign="middle" class="datos_celdaTit">Movimientos</td>
        <td colspan="2" align="center" valign="middle" class="datos_celdaTit">Saldos</td>
      </tr>
      <tr>
        <td align="center" valign="middle" class="datos_celdaSubTit">Clave</td>
        <td align="center" valign="middle" class="datos_celdaSubTit">Cuenta</td>
        <td align="center" valign="middle" class="datos_celdaSubTitA">Debe</td>
        <td align="center" valign="middle" class="datos_celdaSubTitB">Haber</td>
        <td align="center" valign="middle" class="datos_celdaSubTitA">Debe</td>
        <td align="center" valign="middle" class="datos_celdaSubTitB">Haber</td>
      </tr><?php do{
				$cuenta = $datosCuentas['cuenta']; // cuenta de la iteración
				// DEBE
				$queryMd = "SELECT SUM(cantidad) AS Md FROM rom_cantidades
							WHERE tipo = 'd' AND cuenta = '$cuenta' AND ejercicio = '$IDejercicio'";
				$resulMd = mysql_query($queryMd);
				$datosMd = mysql_fetch_array($resulMd);
				$Md = $datosMd['Md']; // movimiento DEBE
				// HABER
				$queryMh = "SELECT SUM(cantidad) AS Mh FROM rom_cantidades
							WHERE tipo = 'h' AND cuenta = '$cuenta' AND ejercicio = '$IDejercicio'";
				$resulMh = mysql_query($queryMh);
				$datosMh = mysql_fetch_array($resulMh);
				$Mh = $datosMh['Mh']; // movimiento HABER
				// SALDOS
				if($Md > $Mh){
					$Sd = $Md - $Mh;
					$Sh = 0;
				} else {
					$Sh = $Mh - $Md;
					$Sd = 0;
				}
				// totales
				$totalMd += $Md;
				$totalMh += $Mh;
				$totalSd += $Sd;
				$totalSh += $Sh;
				?>
      <tr>
        <td class="datos_celdaDato"><span class="celdaListaDat"><?php echo $cuenta; ?></span></td>
        <td class="datos_celdaDato"><span class="celdaListaDat">
          <?php mostrarNombre($cuenta); ?>
        </span></td>
        <td align="left" valign="middle" class="datos_celdaNumeroA">$ <span class="celdaListaDatA"><?php echo number_format($Md,2) ?></span></td>
        <td align="left" valign="middle" class="datos_celdaNumeroV">$ <span class="celdaListaDatV"><?php echo number_format($Mh,2) ?></span></td>
        <td align="left" valign="middle" class="datos_celdaNumeroA">$ <span class="celdaListaDatA"><?php echo number_format($Sd,2) ?></span></td>
        <td align="left" valign="middle" class="datos_celdaNumeroV">$ <span class="celdaListaDatV"><?php echo number_format($Sh,2) ?></span></td>
      </tr><?php } while($datosCuentas = mysql_fetch_assoc($resulCuentas)); ?>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" align="right" valign="middle" class="datos_celdaTit">Sumas iguales:</td>
        <td align="left" valign="middle" class="datos_celdaNumeroA"><strong>$ 
          
        <?php echo number_format($totalMd,2); ?></strong></td>
        <td align="left" valign="middle" class="datos_celdaNumeroV"><strong>$ 
          
        <?php echo number_format($totalMh,2); ?></strong></td>
        <td align="left" valign="middle" class="datos_celdaNumeroA"><strong>$ 
          
        <?php echo number_format($totalSd,2); ?></strong></td>
        <td align="left" valign="middle" class="datos_celdaNumeroV"><strong>$ 
          
        <?php echo number_format($totalSh,2); ?></strong></td>
      </tr>
    </table>
  </div>
</fieldset>
</div>
<div id="footer">&copy;2011 - Sistema Contable Romero | Todos los Derechos Reservados.</div>
</body>
</html>
